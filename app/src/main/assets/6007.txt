{"head": {
    "function_id": "1004",
    "return_flag": "0",
    "return_message": "success"
  },"body":{
    "prod_base_info": {
        "prod_id": "G15072200000005",
        "prod_name": "面包新语面包现金卡",
        "prod_resume": "仅售85元，市场价值100元面包新语面包现金卡（免费送货上门），上海多店通用",
        "prod_property": "2",
        "prod_pic_url_list": [
            {
                "pic_url": "http: ///172.18.0.78/tongle/p1.jpg"
            },
            {
                "pic_url": "http: ///172.18.0.78/tongle/p1.jpg"
            }
        ],
        "prod_status": "2",
        "mstore_id": "1507000030",
        "mstore_name": "面包新语",
        "price": 100,
        "show_price": 85,
        "stock_qty": 100000,
        "spec_detail_list": [
            {
                "prod_spec_id": "S000000000001",
                "prod_spec_name": "团购券规格",
                "spec_detail_list": [
                    {
                        "spec_detail_id": "A000000001",
                        "spec_detail_name": "张"
                    }
                ]
            }
        ],
        "good_rating_percent": 95,
        "good_rating_qty": 25,
        "prod_favorited_by_me": "1",
        "trade_prod_favorited_by_me": "0",
        "post_id": "15072300000001",
        "relation_id": "15072300000005",
        "sender": "本商品是面包新语代为发货",
        "point_rule_url": "http://172.18.0.78/tongle/point_rule.html",
        "point_rule_title": "商品积分详情",
        "sold_qty": 33000,
        "buy_limit_qty": 1
    },
    "coupon_flag": {
        "ready_to_retire_flg": "0",
        "expired_refund_flg": "0",
        "non_refundable_flg": "1"
    },
    "coupon_store_info": {
        "store_total_count": "25",
        "store_list": [
            {
                "code": "SP20160304001",
                "name": "面包新语百联世茂店",
                "address": "南京东路819号百联世茂国际广场106店铺",
                "phone": "021-63608807"
            }
        ]
    },
    "coupon_purchase_info": {
        "head": "西点券",
        "content_list": [
            {
                "item": "面包新语面包现金卡",
                "qty": "1张",
                "price": 100
            }
        ],
        "content_total_price": 100,
        "content_actually_price": 85,
        "footer": "使用年限：开卡后3年以内使用"
    },
    "coupon_purchase_notice_list": [
        {
            "title": "配送范围",
            "desc": "上海可配送"
        },
        {
            "title": "发货时间",
            "desc": "完成支付后2-3个工作日内发货"
        }
    ],
    "ui_btn_control_list": [{
        "btn_func_key": "sale",
        "btn_status": "0",
        "btn_name": "立即购买"
    }]
}}
