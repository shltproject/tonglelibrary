package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by fengguowei on 2017/1/12.
 * 魔店热门帖子详情 10004
 */
public class PostDetail implements Serializable {
    private String post_id;
    private String magazine_pic;
    private String is_hot_cake;
    private List<MagazineDetail> magazine_goods_list;
    private int show_order;

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getMagazine_pic() {
        return magazine_pic;
    }

    public void setMagazine_pic(String magazine_pic) {
        this.magazine_pic = magazine_pic;
    }

    public String getIs_hot_cake() {
        return is_hot_cake;
    }

    public void setIs_hot_cake(String is_hot_cake) {
        this.is_hot_cake = is_hot_cake;
    }

    public List<MagazineDetail> getMagazine_goods_list() {
        return magazine_goods_list;
    }

    public void setMagazine_goods_list(List<MagazineDetail> magazine_goods_list) {
        this.magazine_goods_list = magazine_goods_list;
    }

    public int getShow_order() {
        return show_order;
    }

    public void setShow_order(int show_order) {
        this.show_order = show_order;
    }
}
