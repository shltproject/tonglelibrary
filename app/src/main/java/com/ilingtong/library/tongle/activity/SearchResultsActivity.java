package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.SearchResultAdapter;
import com.ilingtong.library.tongle.protocol.SortProdResult;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SearchEditText;

public class SearchResultsActivity extends BaseActivity implements SearchEditText.ISearchListener, View.OnClickListener, AbsListView.OnScrollListener {
    private LinearLayout backLayout;
    private SearchEditText edtSearch;
    private ImageView prodClassImg;
    private TextView txtMultipleSort;
    private TextView txtSalesSort;
    private TextView txtNewProdSort;
    private LinearLayout llPriceSort;
    private TextView txtPriceSort;
    private ImageView priceSortImg;
    private SearchResultAdapter searchResultAdapter;//商品列表Adapter
    private GridView gvList;//商品网格列表
    private RelativeLayout rlyReplace;//无数据时提示
    private String searchType;//搜索类型 关键字/分类检索
    private String mStoreId;//店铺Id
    private String searchKey;//关键字
    private String searchFieldId;//分类检索id
    private boolean priceSortFlag = false;//标识价格的排序方式
    private boolean isLoadMoreFlag;//是否可加载更多
    private boolean isBottomFlag;//是否滑动到底部
    private String mKbn;//分类区分
    private String mOrder;//排序方式
    private String mSortKey;//翻页用Key

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results_layout);
        searchType = getIntent().getStringExtra("searchType");
        mStoreId = getIntent().getStringExtra("storeId");
        searchKey = getIntent().getStringExtra("searchKey");
        searchFieldId = getIntent().getStringExtra("searchFieldId");
        initView();
        txtMultipleSort.performClick();
    }

    private void initView() {
        backLayout = (LinearLayout) findViewById(R.id.back_layout);
        edtSearch = (SearchEditText) findViewById(R.id.search_edt);
        prodClassImg = (ImageView) findViewById(R.id.prod_class_img);
        if (searchType.equals(TongleAppConst.SEARCH_KEY)) {
            edtSearch.setText(searchKey);
        }
        edtSearch.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //禁止长按出现粘贴情形
                return true;
            }
        });
        edtSearch.setSearchListener(this);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StoreSearchActivity.launcher(SearchResultsActivity.this, mStoreId, edtSearch.getText().toString().trim());
            }
        });
        prodClassImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StoreFieldActivity.launcher(SearchResultsActivity.this, mStoreId);
                finish();
            }
        });

        txtMultipleSort = (TextView) findViewById(R.id.multiple_sort);
        txtSalesSort = (TextView) findViewById(R.id.sales_sort);
        txtNewProdSort = (TextView) findViewById(R.id.new_prod_sort);
        llPriceSort = (LinearLayout) findViewById(R.id.price_sort);
        txtPriceSort = (TextView) findViewById(R.id.price_sort_txt);
        priceSortImg = (ImageView) findViewById(R.id.price_sort_img);
        rlyReplace = (RelativeLayout) findViewById(R.id.rl_replace);
        txtMultipleSort.setOnClickListener(this);
        txtSalesSort.setOnClickListener(this);
        txtNewProdSort.setOnClickListener(this);
        llPriceSort.setOnClickListener(this);

        searchResultAdapter = new SearchResultAdapter(this, new SearchResultAdapter.IOnItemClickListener() {
            @Override
            public void onItemClick(View view, String goods_id) {
                //跳转到商品详情页
                CollectProductDetailActivity.launch(SearchResultsActivity.this, goods_id, TongleAppConst.ACTIONID_MSTORE, "", mStoreId, "");
            }
        });
        gvList = (GridView) findViewById(R.id.store_goods_gv_list);
        gvList.setAdapter(searchResultAdapter);
        gvList.setOnScrollListener(this);
    }

    @Override
    public void search() {
    }

    /**
     * @param activity ；storeId 店铺Id;searchType 搜索类型;searchFieldId 分类搜索Id
     * @author fengguowei
     * @date 2017/2/16 14:12
     * @Title: ${enclosing_method}
     * @Description: 分类搜索跳转到结果页面
     */

    public static void launchActivityByField(Activity activity, String storeId, String searchType, String searchFieldId) {
        Intent intent = new Intent(activity, SearchResultsActivity.class);
        intent.putExtra("storeId", storeId);
        intent.putExtra("searchType", searchType);
        intent.putExtra("searchFieldId", searchFieldId);
        activity.startActivity(intent);
    }

    /**
     * @param activity ；storeId 店铺Id;searchType 搜索类型;searchKey 搜索的关键字
     * @author fengguowei
     * @date 2017/2/16 14:12
     * @Title: ${enclosing_method}
     * @Description: 关键字搜索跳转到结果页面
     */

    public static void launchActivityByKey(Activity activity, String storeId, String searchType, String searchKey) {
        Intent intent = new Intent(activity, SearchResultsActivity.class);
        intent.putExtra("storeId", storeId);
        intent.putExtra("searchType", searchType);
        intent.putExtra("searchKey", searchKey);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    public void onClick(View v) {
        isBottomFlag = false;
        isLoadMoreFlag = false;
        String index = TongleAppConst.MULTIPLE_SORT;
        mOrder = TongleAppConst.SORT_UP;
        mSortKey = null;
        if (v.getId() == R.id.multiple_sort) {
            index = TongleAppConst.MULTIPLE_SORT;
            mKbn = TongleAppConst.MULTIPLE_SORT;
            priceSortFlag = false;
            if (searchType.equals(TongleAppConst.SEARCH_KEY)) {
                doKeyRequest(mKbn, mOrder);
            } else if (searchType.equals(TongleAppConst.SEARCH_CLASS)) {
                doFieldRequest(mKbn, mOrder);
            }
        } else if (v.getId() == R.id.sales_sort) {
            index = TongleAppConst.SALES_SORT;
            mKbn = TongleAppConst.SALES_SORT;
            priceSortFlag = false;
            if (searchType.equals(TongleAppConst.SEARCH_KEY)) {
                doKeyRequest(mKbn, mOrder);
            } else if (searchType.equals(TongleAppConst.SEARCH_CLASS)) {
                doFieldRequest(mKbn, mOrder);
            }
        } else if (v.getId() == R.id.new_prod_sort) {
            index = TongleAppConst.NEW_PROD_SORT;
            mKbn = TongleAppConst.NEW_PROD_SORT;
            priceSortFlag = false;
            if (searchType.equals(TongleAppConst.SEARCH_KEY)) {
                doKeyRequest(mKbn, mOrder);
            } else if (searchType.equals(TongleAppConst.SEARCH_CLASS)) {
                doFieldRequest(mKbn, mOrder);
            }
        } else if (v.getId() == R.id.price_sort) {//价格
            index = TongleAppConst.PRICE_SORT;
            mKbn = TongleAppConst.PRICE_SORT;
            if (searchType.equals(TongleAppConst.SEARCH_KEY)) {
                if (priceSortFlag) {
                    mOrder = TongleAppConst.SORT_DOWN;
                    doKeyRequest(TongleAppConst.PRICE_SORT, mOrder);
                } else {
                    mOrder = TongleAppConst.SORT_UP;
                    doKeyRequest(TongleAppConst.PRICE_SORT, mOrder);
                }
            } else if (searchType.equals(TongleAppConst.SEARCH_CLASS)) {
                if (priceSortFlag) {
                    mOrder = TongleAppConst.SORT_DOWN;
                    doFieldRequest(TongleAppConst.PRICE_SORT, mOrder);
                } else {
                    mOrder = TongleAppConst.SORT_UP;
                    doFieldRequest(TongleAppConst.PRICE_SORT, mOrder);
                }
            }
            if (priceSortFlag) {
                priceSortImg.setImageResource(R.drawable.sort_down_icon);
            } else {
                priceSortImg.setImageResource(R.drawable.sort_up_icon);
            }
            priceSortFlag = !priceSortFlag;
        }
        setIndex(index);
    }

    private void setIndex(String index) {
        switch (index) {
            case TongleAppConst.MULTIPLE_SORT:
                txtMultipleSort.setTextColor(getResources().getColor(R.color.search_result_sort_selected_color));
                txtSalesSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                txtNewProdSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                txtPriceSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                break;
            case TongleAppConst.SALES_SORT:
                txtSalesSort.setTextColor(getResources().getColor(R.color.search_result_sort_selected_color));
                txtMultipleSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                txtNewProdSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                txtPriceSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                break;
            case TongleAppConst.NEW_PROD_SORT:
                txtNewProdSort.setTextColor(getResources().getColor(R.color.search_result_sort_selected_color));
                txtMultipleSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                txtSalesSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                txtPriceSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                break;
            case TongleAppConst.PRICE_SORT:
                txtPriceSort.setTextColor(getResources().getColor(R.color.search_result_sort_selected_color));
                txtMultipleSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                txtSalesSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                txtNewProdSort.setTextColor(getResources().getColor(R.color.search_result_sort_normal_color));
                break;
            default:
                break;
        }
    }

    /**
     * @param kbn 排序区分 order-升/降顺序
     * @author fengguowei
     * @date 2017/2/15 15:34
     * @Title: ${enclosing_method}
     * @Description: 分类检索
     */
    private void doFieldRequest(String kbn, String order) {
        ServiceManager.SearchStoreProdByField(mSortKey, order, kbn, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, searchFieldId, mStoreId, new Response.Listener<SortProdResult>() {
            @Override
            public void onResponse(SortProdResult sortProdResult) {
                if (TongleAppConst.SUCCESS.equals(sortProdResult.getHead().getReturn_flag())) {
                    gvList.setVisibility(View.VISIBLE);
                    rlyReplace.setVisibility(View.GONE);
                    if (mSortKey == null) {//第一次加载
                        if (sortProdResult.getBody().getProd_info().getProd_list().size() <= 0) {
                            gvList.setVisibility(View.GONE);
                            rlyReplace.setVisibility(View.VISIBLE);
                            return;
                        } else {
                            mSortKey = sortProdResult.getBody().getProd_info().getProd_list().get(sortProdResult.getBody().getProd_info().getProd_list().size() - 1).getSort_key();
                        }
                        searchResultAdapter.setItems(sortProdResult.getBody().getProd_info().getProd_list());
                    } else {//加载更多
                        if (sortProdResult.getBody().getProd_info().getProd_list().size() > 0) {
                            mSortKey = sortProdResult.getBody().getProd_info().getProd_list().get(sortProdResult.getBody().getProd_info().getProd_list().size() - 1).getSort_key();
                        }
                        searchResultAdapter.addItems(sortProdResult.getBody().getProd_info().getProd_list());
                    }
                    if (sortProdResult.getBody().getProd_info().getProd_list().size() <Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                        isLoadMoreFlag = false;
                    } else {
                        isLoadMoreFlag = true;
                    }
                } else {
                    if (mSortKey == null) {
                        gvList.setVisibility(View.GONE);
                        rlyReplace.setVisibility(View.VISIBLE);
                        return;
                    }
                    ToastUtils.toastLong(sortProdResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (mSortKey == null) {
                    gvList.setVisibility(View.GONE);
                    rlyReplace.setVisibility(View.VISIBLE);
                } else {
                    ToastUtils.toastLong(volleyError.getMessage());
                }
            }
        });
    }

    /**
     * @param kbn 排序区分
     * @author fengguowei
     * @date 2017/2/15 15:35
     * @Title: ${enclosing_method}
     * @Description: 关键字检索
     */

    private void doKeyRequest(String kbn, String order) {
        ServiceManager.SearchStoreProdByKey(mSortKey, order, kbn, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, searchKey, mStoreId, new Response.Listener<SortProdResult>() {
            @Override
            public void onResponse(SortProdResult sortProdResult) {
                if (TongleAppConst.SUCCESS.equals(sortProdResult.getHead().getReturn_flag())) {
                    if (TongleAppConst.SUCCESS.equals(sortProdResult.getHead().getReturn_flag())) {
                        gvList.setVisibility(View.VISIBLE);
                        rlyReplace.setVisibility(View.GONE);
                        if (mSortKey == null) {//第一次加载
                            if (sortProdResult.getBody().getProd_info().getProd_list().size() <= 0) {
                                gvList.setVisibility(View.GONE);
                                rlyReplace.setVisibility(View.VISIBLE);
                                return;
                            }else {
                                mSortKey = sortProdResult.getBody().getProd_info().getProd_list().get(sortProdResult.getBody().getProd_info().getProd_list().size() - 1).getSort_key();
                            }
                            searchResultAdapter.setItems(sortProdResult.getBody().getProd_info().getProd_list());
                        } else {//加载更多
                            if (sortProdResult.getBody().getProd_info().getProd_list().size() > 0) {
                                mSortKey = sortProdResult.getBody().getProd_info().getProd_list().get(sortProdResult.getBody().getProd_info().getProd_list().size() - 1).getSort_key();
                            }
                            searchResultAdapter.addItems(sortProdResult.getBody().getProd_info().getProd_list());
                        }
                        if (sortProdResult.getBody().getProd_info().getProd_list().size() < Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                            isLoadMoreFlag = false;
                        } else {
                            isLoadMoreFlag = true;
                        }
                    } else {
                        if (mSortKey == null) {
                            gvList.setVisibility(View.GONE);
                            rlyReplace.setVisibility(View.VISIBLE);
                            return;
                        }
                        ToastUtils.toastLong(sortProdResult.getHead().getReturn_message());
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (mSortKey == null) {
                    gvList.setVisibility(View.GONE);
                    rlyReplace.setVisibility(View.VISIBLE);
                } else {
                    ToastUtils.toastLong(volleyError.getMessage());
                }
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == SCROLL_STATE_IDLE && isBottomFlag) {
            if (isLoadMoreFlag) {//未加载完所有数据，加载数据，并且还原isLoadEnd值为false，重新定位列表底部
                if (searchType.equals(TongleAppConst.SEARCH_CLASS)) {
                    doFieldRequest(mKbn, mOrder);
                } else if (searchType.equals(TongleAppConst.SEARCH_KEY)) {
                    doKeyRequest(mKbn, mOrder);
                }
                isBottomFlag = false;
            } else {//加载完了所有的数据
                ToastUtils.toastShort(getString(R.string.common_list_end));
            }
        }

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem + visibleItemCount == totalItemCount) {
            View lastVisibleItemView = gvList.getChildAt(gvList.getChildCount() - 1);
            if (lastVisibleItemView != null && lastVisibleItemView.getBottom() == gvList.getHeight()) {
                isBottomFlag = true;
            } else {
                isBottomFlag = false;
            }
        }
    }
}
