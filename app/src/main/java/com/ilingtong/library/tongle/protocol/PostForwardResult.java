package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: cuishuang
 * Date: 2015/7/13
 * Time: 15:22
 * Email: shuangcui@isoftstone.com
 * Desc: 帖子转发类
 */
public class PostForwardResult implements Serializable {
    private BaseInfo head;
    private PostForwordDataInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public PostForwordDataInfo getBody() {
        return body;
    }

    public void setBody(PostForwordDataInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
