package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/2/14
 * Desc:6023接口 body 店铺分类列表信息类
 */

public class StoreFieldListInfo implements Serializable{
    private List<StoreFieldItemInfo> field_list;//店铺分类信息列表

    public List<StoreFieldItemInfo> getField_list() {
        return field_list;
    }

    public void setField_list(List<StoreFieldItemInfo> field_list) {
        this.field_list = field_list;
    }
}
