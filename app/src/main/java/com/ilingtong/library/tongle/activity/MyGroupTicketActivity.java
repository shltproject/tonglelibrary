package com.ilingtong.library.tongle.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.MyGroupTicketListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.CouponListResult;
import com.ilingtong.library.tongle.protocol.CouponOrderInfo;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/7
 * Time: 16:27
 * Email: liuting@ilingtong.com
 * Desc:我的团购券
 */
public class MyGroupTicketActivity extends BaseActivity implements View.OnClickListener, XListView.IXListViewListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标
    private TextView mTxtGroup;//团购订单
    private XListView mLvTicket;//我的团购券列表
    private RelativeLayout mRlyReplace;//没有团购券时

    private CouponListResult mCouponListResult;//团购券实体类
    private MyGroupTicketListAdapter mMyGroupTicketListAdapter;//我的团购券列表Adapter
    private List<CouponOrderInfo> mTicketList = new ArrayList<>();//团购券列表
    private boolean loadAllFlag = false;  //ture时表示已经加载全部数据

    private Dialog mDialog;//加载对话框
    public static final int REQUESTCODE_TO_GROUPTICKETDETAIL = 10001;

    public static final int REFRESH_LIST = 0;  //刷新列表
    public static final int NO_DATA_REFRESH = 1;  //没有数据时刷新
    private boolean clearFlag=true;//清空标志,true为清空
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_LIST:  //刷新列表
                    mRlyReplace.setVisibility(View.GONE);
                    mLvTicket.setRefreshTime();
                    mMyGroupTicketListAdapter.notifyDataSetChanged();
                    break;
                case NO_DATA_REFRESH:   //没有团购券，隐藏listview显示无数据时替换画面。
                    mLvTicket.setVisibility(View.GONE);
                    mRlyReplace.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xlistview_common_layout);
        initView();
        getData("", "");
    }

    public void initView() {
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtGroup = (TextView) findViewById(R.id.top_btn_text);
        mTxtTitle.setText(getResources().getString(R.string.my_group_ticket_top_name));
        mTxtGroup.setText(getResources().getString(R.string.my_group_order_top_name));
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mTxtGroup.setVisibility(View.VISIBLE);
        mTxtGroup.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14); //14SP
        mImgBack.setOnClickListener(this);
        mTxtGroup.setOnClickListener(this);
        mTxtGroup.setBackgroundResource(R.color.topview_bgcolor);

        mLvTicket = (XListView) findViewById(R.id.xlistview_common_lv_list);
        mRlyReplace = (RelativeLayout) findViewById(R.id.xlistview_common_rly_replace);
        mLvTicket.setPullLoadEnable(false);
        mLvTicket.setPullRefreshEnable(true);
        mLvTicket.setXListViewListener(this, 0);
        mTicketList = new ArrayList<>();
        mTicketList.clear();

        mDialog = DialogUtils.createLoadingDialog(MyGroupTicketActivity.this);
        mDialog.setCancelable(false);

        mMyGroupTicketListAdapter = new MyGroupTicketListAdapter(this, mTicketList);
        mLvTicket.setAdapter(mMyGroupTicketListAdapter);
    }

    /**
     * 取得数据
     */
    public void getData(String orderNo, String outOfDate) {
        mDialog.show();
//        mCouponListResult=new CouponListResult();
//        mCouponListResult= (CouponListResult)TestInterface.parseJson(CouponListResult.class, "6010.txt");
        ServiceManager.doMyGroupTicketListRequest(TongleAppInstance.getInstance().getUserID(), orderNo, outOfDate, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, getSuccessListener(), errorListener());
        //initData();
    }

    /**
     * 接口响应成功回调
     *
     * @return
     */
    private Response.Listener<CouponListResult> getSuccessListener() {
        return new Response.Listener<CouponListResult>() {
            @Override
            public void onResponse(CouponListResult couponListResult) {
                mDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(couponListResult.getHead().getReturn_flag())) {
                    //刷新获取数据后清空数据
                    if(mTicketList!=null&&clearFlag){
                        mTicketList.clear();
                    }
                    if (couponListResult.getBody().getData_total_count() < 1) {
                        handler.sendEmptyMessage(NO_DATA_REFRESH);
                    } else {
                        mTicketList.addAll(couponListResult.getBody().getCoupon_list());
                        loadAllFlag = (mTicketList.size() < couponListResult.getBody().getData_total_count()) ? false : true;
                        mLvTicket.setPullLoadEnable(!loadAllFlag);
                        handler.sendEmptyMessage(REFRESH_LIST);
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + couponListResult.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 接口响应失败回调
     *
     * @return
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mDialog.dismiss();
                ToastUtils.toastLong(R.string.sys_exception);
            }
        };
    }
//    /**
//     * 初始化数据
//     */
//    public void initData() {
//        if (mCouponListResult.getBody().getCoupon_list().size() > 0) {//有数据时显示数据列表
//            mRlyReplace.setVisibility(View.GONE);
//            mLvTicket.setVisibility(View.VISIBLE);
//            mTicketList.addAll(mCouponListResult.getBody().getCoupon_list());
//            mMyGroupTicketListAdapter = new MyGroupTicketListAdapter(MyGroupTicketActivity.this, mTicketList);
//            mLvTicket.setAdapter(mMyGroupTicketListAdapter);
//        } else {//无数据是提示没有相关数据
//            mRlyReplace.setVisibility(View.VISIBLE);
//            mLvTicket.setVisibility(View.GONE);
//        }
//    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.left_arrow_btn:
//                finish();
//                break;
//            case R.id.top_btn_text://团购订单
//                startActivity(new Intent(this, MyGroupOrderActivity.class));
//                break;
//            default:
//                break;
//        }
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.top_btn_text) {
            //团购订单
            startActivity(new Intent(this, MyGroupOrderActivity.class));
        }
    }

    @Override
    public void onRefresh(int id) {
        clearFlag=true;
        mLvTicket.setRefreshTime();
        getData("", "");
    }

    @Override
    public void onLoadMore(int id) {
        if (loadAllFlag) {
            ToastUtils.toastShort(R.string.common_list_end);
        } else {
            clearFlag=false;
            getData(mTicketList.get(mTicketList.size() - 1).getOrder_no(), mTicketList.get(mTicketList.size() - 1).getOut_of_date());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUESTCODE_TO_GROUPTICKETDETAIL && resultCode == RESULT_OK) {
            onRefresh(0);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
