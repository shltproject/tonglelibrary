package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.TLPagerAdapter;
import com.ilingtong.library.tongle.protocol.LoginResult;
import com.ilingtong.library.tongle.protocol.VersionResult;
import com.ilingtong.library.tongle.service.VersionServiceLogin;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;
import com.umeng.analytics.MobclickAgent;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/5/9
 * Time: 10:37
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class LoginWelcomActivity extends BaseActivity implements ViewPager.OnPageChangeListener {
    private ViewPager viewPager;
    private ImageView[] tips;
    private List<View> listViews;
    private View view1, view2, view3;
    private SharedPreferences preferences, sp, userSp;
    private TLPagerAdapter pageAdapter;
    View inflate;
    AlertDialog alert;
    Button version_cancle, version_sure;
    private ProgressBar pb;
    private TextView tv;
    public static int loading_process;
    String url;
    AlertDialog.Builder builder;
    private String[] needPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};//存储权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类
    private SelectDialog dialog;
    //创建handler
    private Handler BroadcastHandler = new Handler() {
        public void handleMessage(Message msg) {
            Beginning();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobclickAgent.setDebugMode(true);
        MobclickAgent.openActivityDurationTrack(false);//禁止默认的页面统计
        //读取SharedPreferences中需要的数据
        preferences = getSharedPreferences("count", MODE_PRIVATE);
        sp = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE); //用来存放用户名密码
        userSp = getApplicationContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);  //用来缓存用户个人数据信息

        int count = preferences.getInt("count", 0);
        loading_process = 0;
        //判断程序与第几次运行，如果是第一次运行则跳转到引导页面。（默认自动登录）第一次登录时将记住密码和自动登录设置为true。
        if (count == 0) {
            sp.edit().putBoolean(TongleAppInstance.getInstance().getIfAutoLoginKeyName(), true).commit();
            sp.edit().putBoolean(TongleAppInstance.getInstance().getIfRemberKeyName(), true).commit();
            openGuidepage();
            Handler hand = new Handler();
            hand.postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    String mVersionCode = getVersionCode(LoginWelcomActivity.this);
                    //内部编号，01代表通乐
                    ServiceManager.doVersionRequest(TongleAppConst.ANDROID_PHONE, mVersionCode, TongleAppInstance.getInstance().getApp_inner_no(), successListener(), errorListener());
                }
            }, 2000);
            count = 1;
        } else {
            //不是第一次，检测版本号

            openPager();
        }
        SharedPreferences.Editor editor = preferences.edit();
        //存入数据
        editor.putInt("count", ++count);
        //提交修改
        editor.commit();
    }

    /**
     * 功能：请求网络响应成功，返回数据
     */
    private Response.Listener<VersionResult> successListener() {
        return new Response.Listener<VersionResult>() {
            @Override
            public void onResponse(final VersionResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                        url = response.getBody().getRecently_version_link();
                        if (getVersionName(LoginWelcomActivity.this).equals(response.getBody().getRecently_version_no())) {
                            //  1,没有更新
                            //如果自动登录则直接登录，否则跳转到登录页面
                            if (sp.getBoolean(TongleAppInstance.getInstance().getIfAutoLoginKeyName(), false)) {
                                login(sp.getString(TongleAppInstance.getInstance().getLoginUseridKeyName(), ""), sp.getString(TongleAppInstance.getInstance().getLoginPwdKeyName(), ""));
                            } else {
                                Intent intent = new Intent(LoginWelcomActivity.this, LoginActivity.class);
                                startActivity(intent);
                                LoginWelcomActivity.this.finish();
                            }
                        } else {
                            //有更新，弹出提示框
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginWelcomActivity.this);
                            inflate = LayoutInflater.from(LoginWelcomActivity.this).inflate(R.layout.login_version_layout, null);
                            builder.setView(inflate);
                            alert = builder.create();
                            alert.setCanceledOnTouchOutside(false);
                            alert.show();

                            version_cancle = (Button) inflate.findViewById(R.id.version_cancle);
                            //不选择版本更新
                            version_cancle.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View convertView) {
                                    //判断是否强制更新，0为是，则退出程序
                                    if (enforceUpdate(response.getBody().getRecently_version_no())) {
                                        finish();
                                    } else {
                                        //否则不更新版本，继续使用旧版本
                                        //如果自动登录则直接登录，否则跳转到登录页面
                                        if (sp.getBoolean(TongleAppInstance.getInstance().getIfAutoLoginKeyName(), false)) {
                                            login(sp.getString(TongleAppInstance.getInstance().getLoginUseridKeyName(), ""), sp.getString(TongleAppInstance.getInstance().getLoginPwdKeyName(), ""));
                                        } else {
                                            Intent intent = new Intent(LoginWelcomActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            LoginWelcomActivity.this.finish();
                                        }
                                        alert.dismiss();
                                    }
                                }
                            });
                            version_sure = (Button) inflate.findViewById(R.id.version_sure);
                            //选择版本更新
                            version_sure.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View convertView) {
                                    alert.dismiss();
                                    requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(LoginWelcomActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                                        @Override
                                        public void requestSuccess() {
                                            Message msg = BroadcastHandler.obtainMessage();
                                            BroadcastHandler.sendMessage(msg);
                                        }

                                        @Override
                                        public void requestFail() {
                                            dialog = new SelectDialog(LoginWelcomActivity.this, getString(R.string.please_open_the_storage_permission), new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    //判断是否强制更新，0为是，则退出程序
                                                    if (enforceUpdate(response.getBody().getRecently_version_no())) {
                                                        LoginWelcomActivity.this.finish();
                                                    } else {
                                                        //否则不更新版本，继续使用旧版本
                                                        //如果自动登录则直接登录，否则跳转到登录页面
                                                        if (sp.getBoolean(TongleAppInstance.getInstance().getIfAutoLoginKeyName(), false)) {
                                                            login(sp.getString(TongleAppInstance.getInstance().getLoginUseridKeyName(), ""), sp.getString(TongleAppInstance.getInstance().getLoginPwdKeyName(), ""));
                                                        } else {
                                                            Intent intent = new Intent(LoginWelcomActivity.this, LoginActivity.class);
                                                            startActivity(intent);
                                                            LoginWelcomActivity.this.finish();
                                                        }
                                                    }
                                                }
                                            });
                                            dialog.show();
                                        }
                                    });
                                    requestPermissionUtils.checkPermissions(LoginWelcomActivity.this);
                                }
                            });
                        }
                    }
                } else {
                    ToastUtils.toastLong(response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 登录
     *
     * @param strNumber   手机号
     * @param strPassword 密码
     */
    private void login(String strNumber, String strPassword) {
        if (!"".equals(strNumber) && !"".equals(strPassword)) {
            ServiceManager.doLogin(strNumber, strPassword, new Response.Listener<LoginResult>() {
                @Override
                public void onResponse(LoginResult response) {
                    {
                        if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                            TongleAppInstance.getInstance().setUserID(response.getBody().getUserID());
                            TongleAppInstance.getInstance().setToken(response.getBody().getToken());
                            TongleAppInstance.getInstance().setUser_photo_url(response.getBody().getUser_head_photo_url());

                            //缓存重要数据
                            userSp.edit().putString("token", response.getBody().getToken()).commit();
                            userSp.edit().putString("user_id", response.getBody().getUserID()).commit();// 用户ID
                            userSp.edit().putString("user_photo_url", response.getBody().getUser_head_photo_url()).commit();// 头像URL

                            Intent mainIntent = new Intent(LoginWelcomActivity.this, MainActivity.class);
                            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(mainIntent);
                        } else {
                            //登录失败，跳转到登录界面
                            Intent intent = new Intent(LoginWelcomActivity.this, LoginActivity.class);
                            startActivity(intent);
                            ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                        }
                        LoginWelcomActivity.this.finish();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    ToastUtils.toastLong(getString(R.string.sys_exception));
                    Intent intent = new Intent(LoginWelcomActivity.this, LoginActivity.class);
                    startActivity(intent);
                    LoginWelcomActivity.this.finish();
                }
            });
        } else {
            Intent intent = new Intent(LoginWelcomActivity.this, LoginActivity.class);
            startActivity(intent);
            ToastUtils.toastShort(getString(R.string.common_userid_pwd_null));
            LoginWelcomActivity.this.finish();
        }
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(volleyError.toString());
                //请求版本失败，跳转登陆界面
                Intent intent = new Intent(LoginWelcomActivity.this, LoginActivity.class);
                startActivity(intent);
                LoginWelcomActivity.this.finish();
            }
        };
    }

    /**
     * 获取本地应用版本号 versionCode
     */
    public String getVersionCode(Context context) {
        String versionCode = "0.0";
        int iVer = 0;
        try {
            // 获取软件版本号，对应AndroidManifest.xml下android:versionCode
            iVer = context.getPackageManager().getPackageInfo("com.ilingtong.app.tongle", 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionCode = String.valueOf(iVer);
        return versionCode;
    }

    /**
     * 获取本地应用版本 versionName
     */
    public String getVersionName(Context context) {
        String versionName = "0.0.0";
        try {
            // 获取软件版本号，对应AndroidManifest.xml下android:versionName
            versionName = context.getPackageManager().getPackageInfo(TongleAppInstance.getInstance().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    //demo开机展示页
    private void openPager() {
        setContentView(R.layout.login_welcom_layout);
        TextView open_bg = (TextView) findViewById(R.id.open_bg);
        Bitmap bit = readBitMap(this, R.drawable.open_bg);
        Drawable drawable = new BitmapDrawable(bit);
        open_bg.setBackgroundDrawable(drawable);
        Handler hand = new Handler();
        hand.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                String mVersionCode = getVersionCode(LoginWelcomActivity.this);
                //内部编号，01代表通乐
                ServiceManager.doVersionRequest(TongleAppConst.ANDROID_PHONE, mVersionCode, TongleAppInstance.getInstance().getApp_inner_no(), successListener(), errorListener());
            }
        }, 2000);

    }

    //demo引导页
    private void openGuidepage() {

        setContentView(R.layout.pager_main);
        listViews = new ArrayList<View>();
        view1 = LayoutInflater.from(LoginWelcomActivity.this).inflate(R.layout.openpager_first, null);
        TextView img1 = (TextView) view1.findViewById(R.id.openpager_first);
        Bitmap pho1 = readBitMap(this, R.drawable.guidepage1);
        Drawable drawable = new BitmapDrawable(pho1);
        img1.setBackgroundDrawable(drawable);
        view2 = LayoutInflater.from(LoginWelcomActivity.this).inflate(R.layout.openpager_second, null);
        TextView img2 = (TextView) view2.findViewById(R.id.openpager_second);
        Bitmap pho2 = readBitMap(this, R.drawable.guidepage2);
        Drawable drawable2 = new BitmapDrawable(pho2);
        img2.setBackgroundDrawable(drawable2);
        view3 = LayoutInflater.from(LoginWelcomActivity.this).inflate(R.layout.openpager_third, null);
        TextView img3 = (TextView) view3.findViewById(R.id.openpager_third);
        Bitmap pho3 = readBitMap(this, R.drawable.guidepage3);
        Drawable drawable3 = new BitmapDrawable(pho3);
        img3.setBackgroundDrawable(drawable3);
        Button btn = (Button) view3.findViewById(R.id.open_third_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginWelcomActivity.this, LoginActivity.class);
                startActivity(intent);
                LoginWelcomActivity.this.finish();
            }
        });
        listViews.add(view1);
        listViews.add(view2);
        listViews.add(view3);

        pageAdapter = new TLPagerAdapter(listViews);
        ViewGroup group = (ViewGroup) findViewById(R.id.viewGroup);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(pageAdapter);

        //将点点加入到ViewGroup中
        tips = new ImageView[listViews.size()];
        for (int i = 0; i < tips.length; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(10, 10));
            tips[i] = imageView;
            if (i == 0) {
                tips[i].setBackgroundResource(R.drawable.page_indicator_focused);
            } else {
                tips[i].setBackgroundResource(R.drawable.page_indicator_unfocused);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            layoutParams.leftMargin = 5;
            layoutParams.rightMargin = 5;
            group.addView(imageView, layoutParams);
        }
        //设置监听，主要是设置点点的背景
        viewPager.setOnPageChangeListener(this);
        //设置ViewPager的默认项, 设置为长度的100倍，这样子开始就能往左滑动
        viewPager.setCurrentItem(0);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageSelected(int arg0) {
        setImageBackground(arg0 % listViews.size());
    }

    /**
     * 设置选中的tip的背景
     *
     * @param selectItems
     */
    private void setImageBackground(int selectItems) {
        for (int i = 0; i < tips.length; i++) {
            if (i == selectItems) {
                tips[i].setBackgroundResource(R.drawable.page_indicator_focused);
            } else {
                tips[i].setBackgroundResource(R.drawable.page_indicator_unfocused);
            }
        }
    }

    public static Bitmap readBitMap(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        //获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    //开始加载文件
    public void Beginning() {

        LinearLayout ll = (LinearLayout) LayoutInflater.from(LoginWelcomActivity.this).inflate(
                R.layout.layout_loadapk, null);
        pb = (ProgressBar) ll.findViewById(R.id.down_pb);
        tv = (TextView) ll.findViewById(R.id.tv);
        builder = new AlertDialog.Builder(LoginWelcomActivity.this);
        builder.setView(ll);
        builder.setTitle(getString(R.string.common_update_alert_title));
        builder.setCancelable(true);
        builder.setNegativeButton(getString(R.string.common_update_alert_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //后台启动下载服务
                        Intent intent1 = new Intent(LoginWelcomActivity.this, VersionServiceLogin.class);
                        startService(intent1);
                        dialog.dismiss();
//
//                        //跳到登录界面
//                        Intent intent=new Intent(LoginWelcomActivity.this,LoginActivity.class);
//                        startActivity(intent);
                        LoginWelcomActivity.this.finish();
                    }
                });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

                //后台启动下载服务
                Intent intent1 = new Intent(LoginWelcomActivity.this, VersionServiceLogin.class);
                startService(intent1);
//
////                //隐藏更新进度，跳到登录界面
//                Intent intent=new Intent(LoginWelcomActivity.this,LoginActivity.class);
//                startActivity(intent);
                LoginWelcomActivity.this.finish();

            }
        });

        builder.show();
        //开启线程根据url请求apk
        new Thread() {
            public void run() {
                loadFile(url);
            }
        }.start();
    }

    //下载apk
    public void loadFile(String url) {
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);
        HttpResponse response;
        try {
            response = client.execute(get);

            HttpEntity entity = response.getEntity();
            float length = entity.getContentLength();

            InputStream is = entity.getContent();
            FileOutputStream fileOutputStream = null;
            if (is != null) {
                File file = new File(Environment.getExternalStorageDirectory(),
                        "Tongle.apk");
                fileOutputStream = new FileOutputStream(file);
                byte[] buf = new byte[1024];
                int ch = -1;
                float count = 0;
                while ((ch = is.read(buf)) != -1) {
                    fileOutputStream.write(buf, 0, ch);
                    count += ch;
                    sendMsg(1, (int) (count * 100 / length));
                }
            }
            sendMsg(2, 0);
            fileOutputStream.flush();
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        } catch (Exception e) {
            sendMsg(-1, 0);
        }
    }

    private void sendMsg(int flag, int c) {
        Message msg = new Message();
        msg.what = flag;
        msg.arg1 = c;
        handler.sendMessage(msg);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {// 定义一个Handler，用于处理下载线程与UI间通讯
            if (!Thread.currentThread().isInterrupted()) {
                if (msg.what == 1) {
                    //进度条
                    pb.setProgress(msg.arg1);
                    loading_process = msg.arg1;
                    tv.setText(String.format(getString(R.string.common_update_loading_txt), loading_process));
                } else if (msg.what == 2) {
                    //安装APK

                    LoginWelcomActivity.this.finish();
                    File file = new File(Environment.getExternalStorageDirectory(), "Tongle.apk");
                    installApk(file);
                } else if (msg.what == -1) {
                    //报错
                    ToastUtils.toastShort(msg.getData().getString("error"));
                }
            }
            super.handleMessage(msg);
        }
    };

    //安装APK
    private void installApk(File file) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        startActivity(intent);
    }

    /**
     * 是否强制更新
     *
     * @param newVersionName 服务器端的版本
     * @return true 强制更新；false 不强制更新
     */
    private boolean enforceUpdate(String newVersionName) {
        if (!formartVersion(getVersionName(this)).getMajor().equals(formartVersion(newVersionName).getMajor())) {
            return true;
        } else if (!formartVersion(getVersionName(this)).getMinor().equals(formartVersion(newVersionName).getMinor())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取version的 major，minor，point
     *
     * @param versionName 版本名称
     * @return
     */
    private versionEntity formartVersion(String versionName) {
        versionEntity versionEntity = new versionEntity();
        versionEntity.setMajor(versionName.substring(0, versionName.indexOf(".")));
        versionEntity.setMinor(versionName.substring(versionName.indexOf(".") + 1, versionName.lastIndexOf(".")));
        versionEntity.setPoint(versionName.substring(versionName.lastIndexOf(".") + 1));

        return versionEntity;
    }

    /**
     * 内部类  版本对应的三个值
     */
    class versionEntity {
        private String major;  //不一致，必须强制更新
        private String minor;  //不一致，必须强制更新
        private String point;   //不一致，不要求强制更新

        public String getMajor() {
            return major;
        }

        public void setMajor(String major) {
            this.major = major;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public String getMinor() {
            return minor;
        }

        public void setMinor(String minor) {
            this.minor = minor;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
