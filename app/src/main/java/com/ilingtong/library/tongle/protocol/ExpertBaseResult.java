package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/17
 * Time: 14:41
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ExpertBaseResult implements Serializable {
    private BaseInfo head;
    private ExpertBaseInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ExpertBaseInfo getBody() {
        return body;
    }

    public void setBody(ExpertBaseInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
