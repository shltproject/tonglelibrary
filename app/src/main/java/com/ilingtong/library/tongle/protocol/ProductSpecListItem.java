package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/21
 * Time: 17:10
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ProductSpecListItem implements Serializable {
    public String prod_spec_id;
    public String prod_spec_name;
    public String can_be_modify_flag;
    public ArrayList<SpecDetailListItem> spec_detail_list;
}
