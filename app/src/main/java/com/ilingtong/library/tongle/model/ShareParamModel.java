package com.ilingtong.library.tongle.model;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/8/2.
 * mail: wuqian@ilingtong.com
 * Description:分享所需参数
 */

public class ShareParamModel implements Serializable {
    public String title;    //标题
    public String desc;        //微信文字备注
    public String link;        // 帖子链接
    public String imgUrl;       //图片地址

    public ShareParamModel(String title, String desc, String link, String imgUrl) {
        this.title = title;
        this.desc = desc;
        this.link = link;
        this.imgUrl = imgUrl;
    }
}
