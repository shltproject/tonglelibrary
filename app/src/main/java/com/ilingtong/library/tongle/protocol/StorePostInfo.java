package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/1/11
 * Desc:店铺动态帖子信息类
 * used by 10015
 */

public class StorePostInfo implements Serializable {
    private int row_num;//行号
    private String user_id;//用户ID
    private String user_nick_name;//用户昵称
    private String user_signature;//用户签名
    private String user_head_photo_url;//用户头像URL
    private String post_id;//帖子ID
    private String post_title;//帖子标题
    private String post_comment;//帖子内容
    private String post_time;//帖子发布时间
    private String post_thumbnail_pic_url;//帖子缩略图
    private String top_flg;//帖子是否置顶Flag

    public int getRow_num() {
        return row_num;
    }

    public void setRow_num(int row_num) {
        this.row_num = row_num;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_nick_name() {
        return user_nick_name;
    }

    public void setUser_nick_name(String user_nick_name) {
        this.user_nick_name = user_nick_name;
    }

    public String getUser_signature() {
        return user_signature;
    }

    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }

    public String getUser_head_photo_url() {
        return user_head_photo_url;
    }

    public void setUser_head_photo_url(String user_head_photo_url) {
        this.user_head_photo_url = user_head_photo_url;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_comment() {
        return post_comment;
    }

    public void setPost_comment(String post_comment) {
        this.post_comment = post_comment;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public String getPost_thumbnail_pic_url() {
        return post_thumbnail_pic_url;
    }

    public void setPost_thumbnail_pic_url(String post_thumbnail_pic_url) {
        this.post_thumbnail_pic_url = post_thumbnail_pic_url;
    }

    public String getTop_flg() {
        return top_flg;
    }

    public void setTop_flg(String top_flg) {
        this.top_flg = top_flg;
    }
}
