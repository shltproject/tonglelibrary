package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by fengguowei on 2017/1/12.
 * 店铺基本信息 10004
 */
public class MStoreBaseInfo implements Serializable {
    private String store_user_id;
    private String store_logo;
    private String store_hp_pic;
    private String store_name;
    private int fans_count;
    private int goods_count;
    private String favorite_flg;
    public String overseas_flg;    //海淘店铺标志

    public String getStore_user_id() {
        return store_user_id;
    }

    public void setStore_user_id(String store_user_id) {
        this.store_user_id = store_user_id;
    }

    public String getStore_logo() {
        return store_logo;
    }

    public void setStore_logo(String store_logo) {
        this.store_logo = store_logo;
    }

    public String getStore_hp_pic() {
        return store_hp_pic;
    }

    public void setStore_hp_pic(String store_hp_pic) {
        this.store_hp_pic = store_hp_pic;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public int getFans_count() {
        return fans_count;
    }

    public void setFans_count(int fans_count) {
        this.fans_count = fans_count;
    }

    public int getGoods_count() {
        return goods_count;
    }

    public void setGoods_count(int goods_count) {
        this.goods_count = goods_count;
    }

    public String getFavorite_flg() {
        return favorite_flg;
    }

    public void setFavorite_flg(String favorite_flg) {
        this.favorite_flg = favorite_flg;
    }
}
