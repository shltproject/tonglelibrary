package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/13
 * Time: 15:14
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class FirstPostThumbnailPicUrl implements Serializable {
    public String pic_url;
}
