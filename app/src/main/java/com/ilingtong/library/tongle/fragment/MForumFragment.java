package com.ilingtong.library.tongle.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.SearchActivity;
import com.ilingtong.library.tongle.adapter.ForumListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.CollectForumResult;
import com.ilingtong.library.tongle.protocol.SearchResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.mob.tools.gui.PullToRequestView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2015/10/29.
 * mail: wuqian@ilingtong.com
 * Description: 收藏 -- M贴
 */
public class MForumFragment extends LazyFragment implements XListView.IXListViewListener, SearchActivity.setKeywordsListener {
    private XListView forumListView;
    private ForumListAdapter forumListAdaper;
    private List<com.ilingtong.library.tongle.protocol.UserFollowPostList> UserFollowPostList = new ArrayList<>();

    public static boolean UPDATE_LIST_FLAG = false; //是否刷新列表的标准
    public static boolean MFORUMFRAGMENT_UPDATE_FLAG = false; //进入页面刷新标志。为ture时表示刷新，反之不刷新
    private int listIndex = -1; //表示是从 position位置跳转到M客详情的
    private int intoType = 0;
    // 标志位，标志已经初始化完成。
    private boolean isPrepared;
    private boolean isLoadData;  //是否加载过数据
    private String key;   //搜索关键字
    private Dialog progressDialog;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    forumListView.setRefreshTime();
                    forumListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    /**
     * 静态工厂方法需要一个int型的值来初始化fragment的参数，然后返回新的fragment到调用者
     *
     * @param intoType 在哪个页面创建
     * @return
     */
    public static MForumFragment newInstance(int intoType) {
        MForumFragment fragment = new MForumFragment();
        Bundle args = new Bundle();
        args.putInt(TongleAppConst.INTO_TYPE, intoType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.collect_forum_layout, null);
        initView(view);
        isPrepared = true;
        isLoadData = false;
        lazyLoad();
        return view;
    }

    /**
     * 初始化view
     *
     * @param view
     */
    private void initView(View view) {
        progressDialog = DialogUtils.createLoadingDialog(getActivity());
        intoType = getArguments().getInt(TongleAppConst.INTO_TYPE);
        UserFollowPostList.clear();

        forumListView = (XListView) view.findViewById(R.id.product_listview);
        forumListAdaper = new ForumListAdapter(getActivity(), UserFollowPostList);
        forumListView.setXListViewListener(this, 0);
        forumListView.setPullLoadEnable(false);
        forumListView.setAdapter(forumListAdaper);

        //点击列表Item，进入帖子详情
        forumListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(getActivity(), CollectForumDetailActivity.class);
                detailIntent.putExtra("post_id", UserFollowPostList.get(position - 1).post_id);
                detailIntent.putExtra(TongleAppConst.INTO_TYPE, intoType);
                listIndex = position;
                startActivityForResult(detailIntent, 10001);
            }
        });
    }

    /**
     * 点击M帖列表进入详情后，返回的回调。如果在详情页面有取消关注，回到该页面时刷新列表。没有则不刷新
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO && requestCode == 10001 && UPDATE_LIST_FLAG) {
            if (listIndex > 0) {
                UserFollowPostList.remove(listIndex - 1);
                UPDATE_LIST_FLAG = false;
                mHandler.sendEmptyMessage(0);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 调用接口请求数据
     */
    private void doRequest() {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) { //收藏
            ServiceManager.doCollectForumRequest(TongleAppInstance.getInstance().getUserID(), "", "1", TongleAppConst.FETCH_COUNT, successListener(true), errorListener());
        } else if (intoType == TongleAppConst.FINDFRAGMENT_INTO) { //发现热门
            //发现进入的M帖
            ServiceManager.hotSearchByTypeRequest(TongleAppConst.FIND_TYPE_POST, "", TongleAppConst.FETCH_COUNT, hotSearchSuccessListener(true), errorListener());
        } else if (intoType == TongleAppConst.SEARCH_INTO) { //关键字搜索进入
            if (!TextUtils.isEmpty(key)) {
                progressDialog.show();
                ServiceManager.searchRequest(TongleAppConst.FIND_TYPE_POST, key, "", TongleAppConst.FETCH_COUNT, hotSearchSuccessListener(true), errorListener());
            }
        }
        isLoadData = true;
    }

    /**
     * 刷新
     *
     * @param id
     */
    @Override
    public void onRefresh(int id) {
        doRequest();
    }

    /**
     * 加载更多
     *
     * @param id
     */
    @Override
    public void onLoadMore(int id) {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) { //收藏
            ServiceManager.doCollectForumRequest(TongleAppInstance.getInstance().getUserID(), UserFollowPostList.get(UserFollowPostList.size() - 1).post_id, "1", TongleAppConst.FETCH_COUNT, successListener(false), errorListener());
        } else if (intoType == TongleAppConst.FINDFRAGMENT_INTO) { //发现热门
            //发现进入的M帖
            ServiceManager.hotSearchByTypeRequest(TongleAppConst.FIND_TYPE_POST, UserFollowPostList.get(UserFollowPostList.size() - 1).rownum, TongleAppConst.FETCH_COUNT, hotSearchSuccessListener(false), errorListener());
        } else if (intoType == TongleAppConst.SEARCH_INTO) { //关键字搜索进入
            ServiceManager.searchRequest(TongleAppConst.FIND_TYPE_POST, key, UserFollowPostList.get(UserFollowPostList.size() - 1).post_id, TongleAppConst.FETCH_COUNT, hotSearchSuccessListener(false), errorListener());
        }
    }

    /**
     * 获取热门搜索的帖子 网络响应成功
     *
     * @return
     */
    private Response.Listener<SearchResult> hotSearchSuccessListener(final boolean clearListFlag) {
        return new Response.Listener<SearchResult>() {
            @Override
            public void onResponse(SearchResult searchResult) {
                progressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(searchResult.getHead().getReturn_flag())) {
                    if (UserFollowPostList != null && clearListFlag) {
                        UserFollowPostList.clear();
                    }
                    UserFollowPostList.addAll(searchResult.getBody().getPost_list());
                    forumListView.setPullLoadEnable(searchResult.getBody().getData_total_count() > UserFollowPostList.size() ? true : false);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + searchResult.getHead().getReturn_message());
                }
                //调用handler，发送消息
                mHandler.sendEmptyMessage(0);
            }
        };
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener(final boolean clearFlag) {
        return new Response.Listener<CollectForumResult>() {
            @Override
            public void onResponse(CollectForumResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    //成功获取数据后，清空list
                    if (UserFollowPostList != null && clearFlag) {
                        UserFollowPostList.clear();
                    }
                    UserFollowPostList.addAll(response.getBody().getPost_list());
                    //调用handler，发送消息
                    mHandler.sendEmptyMessage(0);
                    if (Integer.parseInt(response.getBody().getData_total_count()) > UserFollowPostList.size()) {
                        //flag = true;
                        forumListView.setPullLoadEnable(true);
                    } else {
                        //flag = false;
                        forumListView.setPullLoadEnable(false);
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        }
        //防止每次显示fragment都去加载数据. intoType为收藏时例外，是否刷新由MEXPERTFRAGMENT_UPDATE_FLAG 和 UPDATE_LIST_FLAG决定
        if (!isLoadData) {
            doRequest();
        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
            if (MFORUMFRAGMENT_UPDATE_FLAG || UPDATE_LIST_FLAG) {
                onRefresh(0);
            }
        }
    }

    /**
     * intotype为 关键字搜索时调用。
     *
     * @param keywords
     */
    @Override
    public void setKeywords(String keywords) {
        if (!TextUtils.isEmpty(keywords) && !keywords.equals(key)) {
            isLoadData = false;
        }
        key = keywords;
        lazyLoad();
    }
}
