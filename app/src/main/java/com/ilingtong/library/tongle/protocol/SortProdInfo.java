package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/2/14
 * Desc:6021、6022接口 body 店铺商品信息类
 */

public class SortProdInfo implements Serializable {
    private SortProdListInfo prod_info;//店铺商品信息

    public SortProdListInfo getProd_info() {
        return prod_info;
    }

    public void setProd_info(SortProdListInfo prod_info) {
        this.prod_info = prod_info;
    }
}
