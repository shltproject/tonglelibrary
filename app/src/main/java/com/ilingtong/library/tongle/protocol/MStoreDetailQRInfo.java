package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 16:59
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class MStoreDetailQRInfo implements Serializable {
    private String mstore_qr_code_url;

    public String getMstore_qr_code_url() {
        return mstore_qr_code_url;
    }

    @Override
    public String toString() {
        return "mstore_qr_code_url:" + mstore_qr_code_url;
    }
}