package com.ilingtong.library.tongle.protocol;

import java.util.ArrayList;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/1/11
 * Desc:10015接口 body 店铺帖子列表信息
 */

public class StorePostListInfo {
    private int data_total_count;//数据总条数
    private ArrayList<StorePostInfo> user_post_list;//指定会员帖子条数

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public ArrayList<StorePostInfo> getUser_post_list() {
        return user_post_list;
    }

    public void setUser_post_list(ArrayList<StorePostInfo> user_post_list) {
        this.user_post_list = user_post_list;
    }
}
