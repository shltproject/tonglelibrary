package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.IntroResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * User: lengjiqiang
 * Date: 2015/6/24
 * Time: 10:56
 * Email: jqleng@isoftstone.com
 * Desc: 功能介绍 页
 */
public class SettingIntroActivity extends BaseActivity implements View.OnClickListener{
    private TextView topNameTxt;
    private ImageView returnBtn;
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_comm);
        initView();
    }
    public void initView(){
        topNameTxt = (TextView) findViewById(R.id.top_name);
        webView = (WebView) findViewById(R.id.webview);
        returnBtn = (ImageView) findViewById(R.id.left_arrow_btn);
        returnBtn.setOnClickListener(this);
        returnBtn.setVisibility(View.VISIBLE);
        topNameTxt.setVisibility(View.VISIBLE);
        topNameTxt.setText(getString(R.string.setting_intro_top_name));
        ServiceManager.doIntroRequest(successListener(), errorListener());
    }
    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<IntroResult> successListener() {
        return new Response.Listener<IntroResult>() {
            @Override
            public void onResponse(IntroResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.loadUrl(response.getBody().getFunction_introduction_url());
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception)+response.getHead().getFunction_id() + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn ){
            finish();
        }
    }
}
