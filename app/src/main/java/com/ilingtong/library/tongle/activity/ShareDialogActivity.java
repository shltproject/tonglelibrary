package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.PostForwardResult;
import com.ilingtong.library.tongle.protocol.ProdForwardResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * Created by wuqian on 2016/5/10.
 * mail: wuqian@ilingtong.com
 * Description:分享dialog。分享到微信，朋友圈，微博，通乐
 */
public class ShareDialogActivity extends BaseActivity implements View.OnClickListener {
    private EditText commentsEdit;
    private LinearLayout weixinShare;
    private LinearLayout weixinfriendShare;
    private LinearLayout sinaShare;
    private LinearLayout tongleShare;
    private ImageButton cancelBtn;
    private boolean clickable;  //为true时表示可点击。避免多次点击事件
    private Dialog progressDialog;
    private String message;   //转发时备注内容
    private int type;  //分享商品或分享帖子类别

    private String relationId;  //二维码关联id
    private String opreationId;  //转发的商品id或者帖子id。商品转发时为商品id，帖子转发时为帖子id

    public static final int TYPE_SHARE_PRODUCT = 100;  //为该值时表示操作为商品转发
    public static final int TYPE_SHARE_POST = 101;   //为该值时表示操作为帖子转发
    private final int shareToWeixin = 1;  //分享给微信好友
    private final int shareToMoments = 2;  //分享到微信朋友圈
    private final int shareToSina = 3;  //分享到微博
    private final int shareToTongLe = 4;  //分享到通乐

    private final int shareSuccess = 0;
    private final int shareFailure = 1;
    private final int shareCancel = 2;
    private String[] needPermissions={Manifest.permission.WRITE_EXTERNAL_STORAGE};//存储权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case shareSuccess:
                    ToastUtils.toastShort(getString(R.string.share_success));
                    ShareDialogActivity.this.finish();
                    break;
                case shareFailure:
                    ToastUtils.toastShort(getString(R.string.share_failure) + msg.obj);
                    break;
                case shareCancel:
                    ToastUtils.toastShort(getString(R.string.share_cancel));
                    break;
            }
        }
    };

    /**
     * @param activity
     * @param repostType  分享类别。当前只能为
     *                    public final int TYPE_SHARE_PRODUCT = 100;  //为该值时表示操作为商品转发
     *                    public final int TYPE_SHARE_POST = 101;   //为该值时表示操作为帖子转发
     * @param opreationId 帖子id或商品id
     */
    public static void launch(Activity activity, int repostType, String opreationId, String relationId) {
        launcher(activity,repostType,opreationId,relationId);
    }
    public static void launcher(Context context, int repostType, String opreationId, String relationId) {
        Intent intent = new Intent(context, ShareDialogActivity.class);
        intent.putExtra("repostType", repostType);
        intent.putExtra("opreationId", opreationId);
        intent.putExtra("relationId", relationId);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.share_dialog_layout);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
        type = getIntent().getIntExtra("repostType", 0);
        opreationId = getIntent().getStringExtra("opreationId");
        relationId = getIntent().getStringExtra("relationId");
        initView();
    }

    private void initView() {
        commentsEdit = (EditText) findViewById(R.id.comments_edit);
        weixinShare = (LinearLayout) findViewById(R.id.weixin_share);
        weixinfriendShare = (LinearLayout) findViewById(R.id.weixinfriend_share);
        sinaShare = (LinearLayout) findViewById(R.id.sina_share);
        tongleShare = (LinearLayout) findViewById(R.id.tongle_share);
        cancelBtn = (ImageButton) findViewById(R.id.cancel_btn);
        progressDialog = DialogUtils.createLoadingDialog(this);

        weixinShare.setOnClickListener(this);
        weixinfriendShare.setOnClickListener(this);
        sinaShare.setOnClickListener(this);
        tongleShare.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.weixin_share) {   //分享给微信好友
//            share(TongleAppConst.FORWARDTYPE_WEIXIN, shareToWeixin);
            checkPermissions(TongleAppConst.FORWARDTYPE_WEIXIN, shareToWeixin,needPermissions);
        } else if (i == R.id.weixinfriend_share) {   //分享到朋友圈
//            share(TongleAppConst.FORWARDTYPE_WEIXIN, shareToMoments);
            checkPermissions(TongleAppConst.FORWARDTYPE_WEIXIN, shareToMoments,needPermissions);
        } else if (i == R.id.sina_share) {  //分享到微博
//            share(TongleAppConst.FORWARDTYPE_SINA, shareToSina);
            checkPermissions(TongleAppConst.FORWARDTYPE_SINA, shareToSina,needPermissions);
        } else if (i == R.id.tongle_share) {  //分享到通乐
            share(TongleAppConst.FORWARDTYPE_INNER, shareToTongLe);
        } else if (i == R.id.cancel_btn) {  //取消分享
            ShareDialogActivity.this.finish();
        }
    }

    /**
     * 检查权限是否取得
     *
     * @param repost_type  转发类别（接口）
     * @param platform     分享到指定平台
     * @param permissions  所需权限
     */
    private void checkPermissions(final String repost_type, final int platform, String... permissions) {
        requestPermissionUtils=RequestPermissionUtils.getRequestPermissionUtils(ShareDialogActivity.this, permissions, new RequestPermissionUtils.IRequestPermissionsListener() {
            @Override
            public void requestSuccess() {
                share(repost_type,platform);
            }

            @Override
            public void requestFail() {
                SelectDialog dialog=new SelectDialog(ShareDialogActivity.this,getString(R.string.please_open_the_storage_permission));
                dialog.show();
            }
        });
        requestPermissionUtils.checkPermissions(ShareDialogActivity.this);
    }

    /**
     * 分享
     *
     * @param repostType   转发类别（接口）
     *                     public static final String FORWARDTYPE_WEIXIN = "3";//微信转发类别
     *                     public static final String FORWARDTYPE_INNER = "1"; //站内转发类别
     *                     public static final String FORWARDTYPE_SINA = "2"; //新浪微博转发类别
     * @param platformType 分享到指定平台（app）
     *                     private final int shareToWeixin = 1;  //分享给微信好友
     *                     private final int shareToMoments = 2;  //分享到微信朋友圈
     *                     private final int shareToSina = 3;  //分享到微博
     *                     private final int shareToTongLe = 4;  //分享到通乐
     */
    private void share(String repostType, int platformType) {
        progressDialog.show();
        message = commentsEdit.getText().toString();
        if (type == TYPE_SHARE_PRODUCT) {
            ServiceManager.doProdForwardRequest(TongleAppInstance.getInstance().getUserID(), opreationId, message, repostType, relationId, productSuccessListener(platformType), errorListener);
        } else if (type == TYPE_SHARE_POST) {
            ServiceManager.doPostForwardRequest(TongleAppInstance.getInstance().getUserID(), opreationId, message, repostType, postSuccessListener(platformType), errorListener);
        }
    }

    /**
     * 商品转发接口成功回调
     *
     * @param platformType 分享到指定平台类别
     *                     private final int shareToWeixin = 1;  //分享给微信好友
     *                     private final int shareToMoments = 2;  //分享到微信朋友圈
     *                     private final int shareToSina = 3;  //分享到微博
     *                     private final int shareToTongLe = 4;  //分享到通乐
     * @return
     */
    private Response.Listener productSuccessListener(final int platformType) {
        return new Response.Listener<ProdForwardResult>() {
            @Override
            public void onResponse(ProdForwardResult response) {

                progressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {

                    switch (platformType) {
                        case shareToWeixin:
                            ShareSDK.initSDK(ShareDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                            HashMap<String, Object> hasMap1 = new HashMap<String, Object>();
                            hasMap1.put("Id", "4");
                            hasMap1.put("SortId", "4");
                            hasMap1.put("AppId", TongleAppInstance.getInstance().getWECHAT_APPID());
                            hasMap1.put("AppSecret", TongleAppInstance.getInstance().getWECHAT_SECRET());
                            hasMap1.put("BypassApproval", "false");
                            hasMap1.put("Enable", "true");
                            ShareSDK.setPlatformDevInfo(Wechat.NAME, hasMap1);

                            Platform platform_weinxin = ShareSDK.getPlatform(Wechat.NAME);

                            //设置分享的参数：
                            Wechat.ShareParams sp = new Wechat.ShareParams();
                            sp.setShareType(Platform.SHARE_WEBPAGE);
                            if (commentsEdit.getText().toString().equals("")) {
                                commentsEdit.setText(response.getBody().getMagazine_title() + "");
                            }
                            sp.setTitle(response.getBody().getMagazine_title());
                            sp.setText(response.getBody().getWx_profile());
                            sp.setUrl(response.getBody().getProd_link_url());
                            sp.setImageUrl(response.getBody().getProd_image_url());
                            setPlatformActionListener(platform_weinxin);
                            // 分享
                            platform_weinxin.share(sp);
                            break;
                        case shareToMoments:
                            ShareSDK.initSDK(ShareDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                            HashMap<String, Object> hasMap2 = new HashMap<String, Object>();
                            hasMap2.put("Id", "5");
                            hasMap2.put("SortId", "5");
                            hasMap2.put("AppId", TongleAppInstance.getInstance().getWECHAT_APPID());
                            hasMap2.put("AppSecret", TongleAppInstance.getInstance().getWECHAT_SECRET());
                            hasMap2.put("BypassApproval", "false");
                            hasMap2.put("Enable", "true");
                            ShareSDK.setPlatformDevInfo(WechatMoments.NAME, hasMap2);
                            Platform platformMoments = ShareSDK.getPlatform(WechatMoments.NAME);
                            //设置分享的参数：

                            if (commentsEdit.getText().toString().equals("")) {
                                commentsEdit.setText(response.getBody().getMagazine_title() + "");
                            }
                            Wechat.ShareParams shareParams = new Wechat.ShareParams();
                            shareParams.setTitle(response.getBody().getMagazine_title());
                            shareParams.setText(response.getBody().getWx_profile());
                            shareParams.setUrl(response.getBody().getProd_link_url());
                            shareParams.setImageUrl(response.getBody().getProd_image_url());
                            shareParams.setShareType(Platform.SHARE_WEBPAGE);
                            setPlatformActionListener(platformMoments);
                            // 分享
                            platformMoments.share(shareParams);
                            break;
                        case shareToSina:
                            ShareSDK.initSDK(ShareDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                            HashMap<String, Object> hasMap3 = new HashMap<String, Object>();
                            hasMap3.put("Id", "1");
                            hasMap3.put("SortId", "1");
                            hasMap3.put("AppKey", TongleAppInstance.getInstance().getSINA_APPKEY());
                            hasMap3.put("AppSecret", TongleAppInstance.getInstance().getSINA_APPSECRET());
                            hasMap3.put("RedirectUrl", TongleAppInstance.getInstance().getSINA_REDIRECT());
                            hasMap3.put("ShareByAppClient", "true");
                            hasMap3.put("Enable", "true");
                            hasMap3.put("isNewApi","true");
                            ShareSDK.setPlatformDevInfo(SinaWeibo.NAME, hasMap3);
                            Platform platformSina = ShareSDK.getPlatform(SinaWeibo.NAME);
                            //设置分享的参数：

                            if (commentsEdit.getText().toString().equals("")) {
                                commentsEdit.setText(getString(R.string.setting_about));
                            }
                            SinaWeibo.ShareParams shareParamsSina = new SinaWeibo.ShareParams();
                            shareParamsSina.setText(commentsEdit.getText().toString() + " " + response.getBody().getProd_link_url());
                            shareParamsSina.setImageUrl(response.getBody().getProd_image_url());
                            setPlatformActionListener(platformSina);
                            // 分享
                            platformSina.share(shareParamsSina);
                            break;
                        case shareToTongLe:
                            ToastUtils.toastShort(getString(R.string.forword_success));
                            ShareDialogActivity.this.finish();
                            break;
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 分享回调监听
     *
     * @param platform
     */
    private void setPlatformActionListener(Platform platform) {
        platform.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                handler.sendEmptyMessage(shareSuccess);
                Log.e("tag", "complete");
            }
            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                Message msg = new Message();
                msg.what = shareFailure;
                msg.obj = throwable.getLocalizedMessage();
                handler.sendMessage(msg);
                Log.e("tag", "error"+throwable.getLocalizedMessage());
            }
            @Override
            public void onCancel(Platform platform, int i) {
                handler.sendEmptyMessage(shareCancel);
                Log.e("tag", "cancel");
            }
        });
    }

    /**
     * 帖子转发接口成功回调
     *
     * @param platformType 分享到指定平台类别
     *                     private final int shareToWeixin = 1;  //分享给微信好友
     *                     private final int shareToMoments = 2;  //分享到微信朋友圈
     *                     private final int shareToSina = 3;  //分享到微博
     *                     private final int shareToTongLe = 4;  //分享到通乐
     * @return
     */
    private Response.Listener postSuccessListener(final int platformType) {
        return new Response.Listener<PostForwardResult>() {
            @Override
            public void onResponse(PostForwardResult response) {

                progressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {

                    switch (platformType) {
                        case shareToWeixin:
                            ShareSDK.initSDK(ShareDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                            HashMap<String, Object> hasMap1 = new HashMap<String, Object>();
                            hasMap1.put("Id", "4");
                            hasMap1.put("SortId", "4");
                            hasMap1.put("AppId", TongleAppInstance.getInstance().getWECHAT_APPID());
                            hasMap1.put("AppSecret", TongleAppInstance.getInstance().getWECHAT_SECRET());
                            hasMap1.put("BypassApproval", "false");
                            hasMap1.put("Enable", "true");
                            ShareSDK.setPlatformDevInfo(Wechat.NAME, hasMap1);

                            Platform platform_weinxin = ShareSDK.getPlatform(Wechat.NAME);

                            //设置分享的参数：
                            Wechat.ShareParams sp = new Wechat.ShareParams();
                            sp.setShareType(Platform.SHARE_WEBPAGE);
                            if (commentsEdit.getText().toString().equals("")) {
                                commentsEdit.setText(response.getBody().getMagazine_title() + "");
                            }
                            sp.setTitle(response.getBody().getMagazine_title());
                            sp.setText(response.getBody().getWx_profile());
                            sp.setUrl(response.getBody().getPost_link_url());
                            sp.setImageUrl(response.getBody().getPost_image_url());
                            setPlatformActionListener(platform_weinxin);
                            // 分享
                            platform_weinxin.share(sp);
                            break;
                        case shareToMoments:
                            ShareSDK.initSDK(ShareDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                            HashMap<String, Object> hasMap2 = new HashMap<String, Object>();
                            hasMap2.put("Id", "5");
                            hasMap2.put("SortId", "5");
                            hasMap2.put("AppId", TongleAppInstance.getInstance().getWECHAT_APPID());
                            hasMap2.put("AppSecret", TongleAppInstance.getInstance().getWECHAT_SECRET());
                            hasMap2.put("BypassApproval", "false");
                            hasMap2.put("Enable", "true");
                            ShareSDK.setPlatformDevInfo(WechatMoments.NAME, hasMap2);
                            Platform platformMoments = ShareSDK.getPlatform(WechatMoments.NAME);
                            //设置分享的参数：

                            if (commentsEdit.getText().toString().equals("")) {
                                commentsEdit.setText(response.getBody().getMagazine_title() + "");
                            }
                            Wechat.ShareParams shareParams = new Wechat.ShareParams();
                            shareParams.setTitle(response.getBody().getMagazine_title());
                            shareParams.setText(response.getBody().getWx_profile());
                            shareParams.setUrl(response.getBody().getPost_link_url());
                            shareParams.setImageUrl(response.getBody().getPost_image_url());
                            shareParams.setShareType(Platform.SHARE_WEBPAGE);
                            setPlatformActionListener(platformMoments);
                            // 分享
                            platformMoments.share(shareParams);
                            break;
                        case shareToSina:
                            ShareSDK.initSDK(ShareDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                            HashMap<String, Object> hasMap3 = new HashMap<String, Object>();
                            hasMap3.put("Id", "1");
                            hasMap3.put("SortId", "1");
                            hasMap3.put("AppKey", TongleAppInstance.getInstance().getSINA_APPKEY());
                            hasMap3.put("AppSecret", TongleAppInstance.getInstance().getSINA_APPSECRET());
                            hasMap3.put("RedirectUrl", TongleAppInstance.getInstance().getSINA_REDIRECT());
                            hasMap3.put("ShareByAppClient", "true");
                            hasMap3.put("Enable", "true");
                            hasMap3.put("isNewApi","true");
                            ShareSDK.setPlatformDevInfo(SinaWeibo.NAME, hasMap3);
                            Platform platformSina = ShareSDK.getPlatform(SinaWeibo.NAME);
                            //设置分享的参数：

                            if (commentsEdit.getText().toString().equals("")) {
                                commentsEdit.setText(getString(R.string.setting_about));
                            }
                            SinaWeibo.ShareParams shareParamsSina = new SinaWeibo.ShareParams();
                            shareParamsSina.setText(commentsEdit.getText().toString() + " " + response.getBody().getPost_link_url());
                            shareParamsSina.setImageUrl(response.getBody().getPost_image_url());
                            setPlatformActionListener(platformSina);
                            // 分享
                            platformSina.share(shareParamsSina);
                            break;
                        case shareToTongLe:
                            ToastUtils.toastShort(getString(R.string.forword_success));
                            ShareDialogActivity.this.finish();
                            break;
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            progressDialog.dismiss();
            ToastUtils.toastLong(getString(R.string.sys_exception));
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode,permissions,paramArrayOfInt);
    }
}
