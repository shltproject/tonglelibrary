package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:6015接口的body
 */
public class MyCouponAvailableInfo implements Serializable {
    private List<MyCouponAvailableBaseInfo> voucher;  //我的可用优惠券列表

    public List<MyCouponAvailableBaseInfo> getVoucher() {
        return voucher;
    }

    public void setVoucher(List<MyCouponAvailableBaseInfo> voucher) {
        this.voucher = voucher;
    }
}
