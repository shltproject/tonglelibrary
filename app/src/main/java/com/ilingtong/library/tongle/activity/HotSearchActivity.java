package com.ilingtong.library.tongle.activity;

import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.CollectFragmentPagerAdapter;
import com.ilingtong.library.tongle.fragment.MExpertFragment;
import com.ilingtong.library.tongle.fragment.MForumFragment;
import com.ilingtong.library.tongle.fragment.MProductFragment;
import com.ilingtong.library.tongle.fragment.MStoreFragment;

/**
 * User: shiyuchong
 * Date: 2015/6/26
 * Time: 9:26
 * Email: ycshi@isoftstone.com
 * Desc: 热门搜索页面
 */
public class HotSearchActivity extends BaseFragmentActivity {
    private View topview;
    private ImageView leftArrowBtn;
    private TextView topname;
    private LinearLayout searchlayout;
    private SearchView search;
    //Tab控件
    private TextView expertText;
    private TextView forumText;
    private TextView storeText;
    private TextView productText;
    private ViewPager viewPager;
    //image
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;

    private ProgressDialog mProgressDialog;
    private Fragment[] fragments = new Fragment[4];
    private int currIndex = 0;
    //目标对应页
    private int target = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_collect_layout);
        initView();
        initFragments();
    }

    public void initView() {
        target = Integer.parseInt(String.valueOf(getIntent().getExtras().get("target")));

        topview = findViewById(R.id.proceeds_top_view);
        topname = (TextView) findViewById(R.id.top_name);
        leftArrowBtn = (ImageView) findViewById(R.id.left_arrow_btn);
        searchlayout = (LinearLayout) findViewById(R.id.ll_search);
        search = (SearchView) findViewById(R.id.search_view);
        expertText = (TextView) findViewById(R.id.expert_text);
        forumText = (TextView) findViewById(R.id.forum_text);
        storeText = (TextView) findViewById(R.id.store_text);
        productText = (TextView) findViewById(R.id.product_text);
        viewPager = (ViewPager) findViewById(R.id.collect_pager);
        imageView1 = (ImageView) findViewById(R.id.collect_cursor1);
        imageView2 = (ImageView) findViewById(R.id.collect_cursor2);
        imageView3 = (ImageView) findViewById(R.id.collect_cursor3);
        imageView4 = (ImageView) findViewById(R.id.collect_cursor4);

        leftArrowBtn.setVisibility(View.VISIBLE);
        leftArrowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //设置标题
        topname.setText(getString(R.string.hot_search_top_name));
        topname.setGravity(View.TEXT_ALIGNMENT_CENTER);
        topname.setVisibility(View.VISIBLE);

        expertText.setOnClickListener(new TextOnClickListener(0));
        forumText.setOnClickListener(new TextOnClickListener(1));
        storeText.setOnClickListener(new TextOnClickListener(2));
        productText.setOnClickListener(new TextOnClickListener(3));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(getString(R.string.hot_search_dialog_title));
        mProgressDialog.setMessage(getString(R.string.hot_search_dialog_message));

    }

    private void initFragments() {
        fragments[0] = MExpertFragment.newInstance(TongleAppConst.FINDFRAGMENT_INTO);
        fragments[1] = MForumFragment.newInstance(TongleAppConst.FINDFRAGMENT_INTO);
        fragments[2] = MStoreFragment.newInstance(TongleAppConst.FINDFRAGMENT_INTO);
        fragments[3] = MProductFragment.newInstance(TongleAppConst.FINDFRAGMENT_INTO);
        CollectFragmentPagerAdapter adapter = new CollectFragmentPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPageOnPageChangeListener());
        viewPager.setCurrentItem(target);
    }

    private class TextOnClickListener implements View.OnClickListener {
        private int index = 0;

        public TextOnClickListener(int i) {
            index = i;
        }

        public void onClick(View v) {
            viewPager.setCurrentItem(index);
        }
    }

    public class ViewPageOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int arg0) {
            currIndex = arg0;
            int i = currIndex + 1;
            setTabTitle(i);
        }
    }
    /**
     * 设置选项卡title的字体颜色
     *
     * @param tabId
     */
    public void setTabTitle(int tabId) {
        //viewpager顶部导航游标，并设置游标显示位置
        if (tabId == 1) {
            imageView1.setVisibility(View.VISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 2) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.VISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 3) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.VISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 4) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.VISIBLE);
        }
        //viewpager顶部文字导航，并设置文字颜色
        Resources resource = (Resources) getResources();
        ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_selecttext_color);
        ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_text_color);
        if (tabId == 1) {
            expertText.setTextColor(selectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 2) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(selectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 3) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(selectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 4) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(selectedTextColor);
        }
    }
}
