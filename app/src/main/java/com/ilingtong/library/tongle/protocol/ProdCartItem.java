package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: syc
 * Date: 2015/6/89
 * Time: 21:44
 * Email: ycshi@isoftstone.com
 * Desc: 功能[1025]
 */
public class ProdCartItem implements Serializable {
    public String product_id;
    public String order_qty;
    public ArrayList<ProdSpecListItem> prod_spec_list;

    @Override
    public String toString() {
        return "product_id:" + product_id + "\r\n" +
                "order_qty:" + order_qty + "\r\n" +
                "prod_spec_list:" + prod_spec_list;
    }
}
