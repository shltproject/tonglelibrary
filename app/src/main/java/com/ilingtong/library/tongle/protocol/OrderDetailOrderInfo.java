package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class OrderDetailOrderInfo implements Serializable {
    public OrderDetailHeadInfo head_info;
    public ArrayList<OrderDetailOrderDetailInfo> order_detail;
    public OrderDetailAddInfo add_info;
    public String pay_type_id;
    public String pay_type_name;
    public OrderDetailShippingInfo shipping_info;
    public OrderDetailInvoiceInfo invoice_info;
    public OrderDetailLogisticsInfo logistics_info;
    public String goods_return_url;
    public String goods_return_title;
    public String vouchers_name;      // add at 2016/04/07  抵用券名
    public String money;      // add at 2016/04/07   抵用券金额
    public String use_conditions;      // add at 2016/04/07  抵用券使用条件

    @Override
      public String toString() {
        return "pay_type_id:" + pay_type_id + "\r\n" +
                "pay_type_name:" + pay_type_name + "\r\n" +
                "head_info:" + head_info + "\r\n" +
                "order_detail:" + order_detail + "\r\n" +
                "add_info:" + add_info + "\r\n" +
                "shipping_info:" + shipping_info + "\r\n" +
                "invoice_info:" + invoice_info + "\r\n" +
                "goods_return_title:" + goods_return_title + "\r\n" +
                "goods_return_url:" + goods_return_url + "\r\n" +
                "logistics_info" + logistics_info;
    }
}
