package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 22:03
 * Email: jqleng@isoftstone.com
 * Desc: 1029接口返回json
 */
public class OrderInfoResult implements Serializable {
    private BaseInfo head;
    private OrderInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public OrderInfo getBody() {
        return body;
    }

    public void setBody(OrderInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
