package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.StoreGoodsInfo;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Package:com.ilingtong.library.tongle.adapter
 * author:liuting
 * Date:2017/1/11
 * Desc:店铺主题商品列表Adapter
 */

public class StoreGoodsGridAdapter extends BaseAdapter {
    private ArrayList<StoreGoodsInfo> listGoods = new ArrayList<>();//商品列表
    private Context context;//上下文
    private LayoutInflater inflater;
    private IOnItemClickListener listener;//事件监听

    public interface IOnItemClickListener{
        void onItemClick(View view,String goods_id);
    }

    /**
     * @param context 上下文
     */
    public StoreGoodsGridAdapter(Context context,IOnItemClickListener listener) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.listener=listener;
    }

    @Override
    public int getCount() {
        return listGoods.size();
    }

    @Override
    public Object getItem(int position) {
        return listGoods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 在原有的数据上添加新数据
     *
     * @param itemList
     */
    public void addItems(ArrayList<StoreGoodsInfo> itemList) {
        this.listGoods.addAll(itemList);
        notifyDataSetChanged();
    }

    /**
     * 设置为新的数据，旧数据会被清空
     *
     * @param itemList
     */
    public void setItems(ArrayList<StoreGoodsInfo> itemList) {
        this.listGoods.clear();
        this.listGoods = itemList;
        notifyDataSetChanged();
    }

    /**
     * 清空数据
     */
    public void clearItems() {
        listGoods.clear();
        notifyDataSetChanged();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            convertView = inflater.inflate(R.layout.grid_item_store_goods_layout,null);
            holder = new ViewHolder();
            holder.imgPic = (ImageView) convertView.findViewById(R.id.store_goods_img_pic);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.store_goods_tv_title);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.store_goods_tv_price);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        ImageLoader.getInstance().displayImage(listGoods.get(position).getSpe_mobile_pic_addr(), holder.imgPic, ImageOptionsUtils.getOptions());
        holder.tvTitle.setText(listGoods.get(position).getGoods_name());
        holder.tvPrice.setText(FontUtils.priceFormat(listGoods.get(position).getGoods_price()));
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(listener!=null){
                   listener.onItemClick(v,listGoods.get(position).getGoods_id());
               }
            }
        });
        return convertView;
    }

    static class ViewHolder{
        private ImageView imgPic;//商品图片
        private TextView tvTitle;//商品标题
        private TextView tvPrice;//商品价格
    }
}
