package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.ProductRatingListItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/5/26
 * Time: 10:19
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ProdCommentListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<ProductRatingListItem> list;
    ProductRatingListItem dataItem = new ProductRatingListItem();
    DisplayImageOptions options_round = new DisplayImageOptions.Builder()
            .showStubImage(R.drawable.head_icon_defualt)            // 设置图片下载期间显示的图片
    .showImageForEmptyUri(R.drawable.head_icon_defualt)    // 设置图片Uri为空或是错误的时候显示的图片
    .showImageOnFail(R.drawable.head_icon_defualt)        // 设置图片加载或解码过程中发生错误显示的图片
    .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
    .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
    .displayer(new RoundedBitmapDisplayer(180))    // 设置成圆角图片
            .bitmapConfig(Bitmap.Config.RGB_565)
    .build();

    public ProdCommentListAdapter(Context context, ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_prod_comment, null);
            holder = new ViewHolder();
            holder.headicon = (ImageView) view.findViewById(R.id.comm_head_icon);
            holder.comm_name = (TextView) view.findViewById(R.id.comm_name);
            holder.comm_time = (TextView) view.findViewById(R.id.comm_time);
            holder.comm_memo = (TextView) view.findViewById(R.id.comm_memo);
            holder.ratingBarLevel = (RatingBar) view.findViewById(R.id.comm_ratingbar_level);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        dataItem = list.get(position);
        ImageLoader.getInstance().displayImage(dataItem.head_photo_url, holder.headicon,options_round);
        holder.comm_name.setText((CharSequence) dataItem.user_nick_name);
        holder.comm_time.setText((CharSequence) dataItem.rating_date);
        holder.comm_memo.setText((CharSequence) dataItem.memo);
        holder.ratingBarLevel.setRating(Float.parseFloat(dataItem.level));
        return view;
    }

    static class ViewHolder {
        ImageView headicon;
        TextView comm_name;
        TextView comm_time;
        TextView comm_memo;
        RatingBar ratingBarLevel;
    }
}

