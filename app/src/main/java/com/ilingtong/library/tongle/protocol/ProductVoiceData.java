package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/6/28.
 * mail: wuqian@ilingtong.com
 * Description:音频文件 use by 2045
 */
public class ProductVoiceData implements Serializable {
    public String product_voice_file;
}
