package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/11
 * Time: 15:23
 * Email: jqleng@isoftstone.com
 * Desc: 删除购物车中的商品类
 */
public class DeleteInCartResult implements Serializable {
    private BaseInfo head;
    private DeleteInCartInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public DeleteInCartInfo getBody() {
        return body;
    }

    public void setBody(DeleteInCartInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
