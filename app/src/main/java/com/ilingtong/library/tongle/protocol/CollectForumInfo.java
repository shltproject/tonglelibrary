package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 11:51
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CollectForumInfo implements Serializable {

    private String data_total_count;
    private ArrayList<UserFollowPostList> post_list;

    public String getData_total_count() {
        return data_total_count;
    }

    public ArrayList<UserFollowPostList> getPost_list() {
        return post_list;
    }

    @Override
    public String toString() {
        return "data_total_count:" + data_total_count + "\r\n" +
                "post_list:" + post_list;
    }
}
