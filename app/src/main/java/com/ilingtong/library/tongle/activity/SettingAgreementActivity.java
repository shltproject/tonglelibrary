package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.AgreementResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * User: lengjiqiang
 * Date: 2015/6/24
 * Time: 11:32
 * Email: jqleng@isoftstone.com
 * Desc: 我的--》我的设置--》版权申报--》用户协议
 */
public class SettingAgreementActivity extends BaseActivity implements View.OnClickListener{
    private TextView top_name;
    private ImageView left_arrow_btn;
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_comm);
        initView();
        doRequest();
    }
    public void initView(){
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        webView = (WebView) findViewById(R.id.webview);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setText(getString(R.string.setting_agreement_top_name));
        top_name.setVisibility(View.VISIBLE);
    }
    public void doRequest(){
        ServiceManager.doAgreementRequest(successListener(), errorListener());
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn){
            finish();
        }else if(v.getId() == R.id.top_btn_text){
            setResult(TongleAppConst.AGREEMENT_RESULT);
            finish();
        }
    }
    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<AgreementResult> successListener() {
        return new Response.Listener<AgreementResult>() {
            @Override
            public void onResponse(AgreementResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    String url = response.getBody().getUser_agreement_url();
                    if (!TextUtils.isEmpty(url)){
                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.loadUrl(url);
                    }
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception)+ response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        };
    }
}