package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/2/14
 * Desc:6021、6022接口返回json 对应entity
 */

public class SortProdResult extends BaseResult implements Serializable {
    private SortProdInfo body;//店铺商品信息类body

    public SortProdInfo getBody() {
        return body;
    }

    public void setBody(SortProdInfo body) {
        this.body = body;
    }
}
