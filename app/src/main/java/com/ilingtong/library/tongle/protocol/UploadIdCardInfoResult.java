package com.ilingtong.library.tongle.protocol;

/**
 * @ClassName: UploadIdCardInfoResult
 * @Package: com.ilingtong.library.tongle.protocol
 * @Description: 6028 上传身份证信息接口返回实体类
 * @author: liuting
 * @Date: 2017/6/12 15:09
 */

public class UploadIdCardInfoResult extends BaseResult{
    private UploadIdCardInfo body;//审核信息类

    public UploadIdCardInfo getBody() {
        return body;
    }

    public void setBody(UploadIdCardInfo body) {
        this.body = body;
    }
}
