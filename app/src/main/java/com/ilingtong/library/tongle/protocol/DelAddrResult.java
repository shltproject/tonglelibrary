package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/14
 * Time: 0:05
 * Email: jqleng@isoftstone.com
 * Desc: 删除收货地址类，该类作为序列化参数通过网络传输
 */
public class DelAddrResult implements Serializable {
    private BaseInfo head;
    private DelAddrInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public DelAddrInfo getBody() {
        return body;
    }

    public void setBody(DelAddrInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
