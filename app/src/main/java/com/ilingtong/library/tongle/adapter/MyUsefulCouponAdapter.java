package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.MyCouponAvailableBaseInfo;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/18
 * Time: 10:09
 * Email: liuting@ilingtong.com
 * Desc:我的可用优惠券Adapter
 */
public class MyUsefulCouponAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<MyCouponAvailableBaseInfo> mLvCoupon;
//    private MyCouponAvailableBaseInfo mCouponItem = new MyCouponAvailableBaseInfo();
    private Context mContext;
    private String mCouponId;

    public MyUsefulCouponAdapter(Context context, List<MyCouponAvailableBaseInfo> couponList,String couponId) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvCoupon = couponList;
        this.mCouponId=couponId;
    }

    @Override
    public int getCount() {
        return mLvCoupon.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvCoupon.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.select_coupon_list_item, null);
            holder = new ViewHolder();
            holder.couponName = (TextView) view.findViewById(R.id.select_coupon_list_item_txt_name);
            holder.couponCondition = (TextView) view.findViewById(R.id.select_coupon_list_item_txt_conditions);
            holder.imgSelect=(ImageView) view.findViewById(R.id.select_coupon_list_item_img_select);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        MyCouponAvailableBaseInfo mCouponItem = (MyCouponAvailableBaseInfo) getItem(position);
        holder.couponName.setText(mCouponItem.getVoucher_base().getVouchers_name());
        holder.couponCondition.setText(mCouponItem.getVoucher_base().getUse_conditions_memo());
        if(mCouponItem.getVoucher_base().getVouchers_number_id().equals(mCouponId)){//如果之前选择过，则显示已选择
            holder.imgSelect.setImageDrawable(mContext.getResources().getDrawable(R.drawable.check_box_press));
        }
        else{
            holder.imgSelect.setImageDrawable(mContext.getResources().getDrawable(R.drawable.check_box_normal));
        }

        return view;
    }

    static class ViewHolder {
        TextView couponName;//优惠券券名
        TextView couponCondition;//优惠券限制条件
        ImageView imgSelect;//选择优惠券
    }
}

