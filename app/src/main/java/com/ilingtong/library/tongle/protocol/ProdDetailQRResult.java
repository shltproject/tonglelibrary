package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/5
 * Time: 14:30
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ProdDetailQRResult implements Serializable {
    private BaseInfo head;
    private ProdDetailQRInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ProdDetailQRInfo getBody() {
        return body;
    }

    public void setBody(ProdDetailQRInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
