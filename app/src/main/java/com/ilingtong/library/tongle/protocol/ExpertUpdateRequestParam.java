package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/19
 * Time: 11:13
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ExpertUpdateRequestParam implements Serializable {
    public String user_id;
    public String expert_user_id;
    public String post_id;
    public String forward;
    public String fetch_count;
    public String action;
}
