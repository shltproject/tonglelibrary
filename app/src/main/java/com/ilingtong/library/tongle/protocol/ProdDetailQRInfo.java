package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/5
 * Time: 14:32
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ProdDetailQRInfo implements Serializable {
    private String prod_qr_code_url;
    private String latest_relation_id;

    public String getProd_qr_code_url() {
        return prod_qr_code_url;
    }
    public String getLatest_relation_id() {
        return latest_relation_id;
    }

    @Override
    public String toString() {
        return "prod_qr_code_url:" + prod_qr_code_url+ "\r\n" +
                "latest_relation_id:" + latest_relation_id;
    }
}
