package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 10:58
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class BaseDataResult implements Serializable {
    private BaseInfo head;
    private BaseDataInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public BaseDataInfo getBody() {
        return body;
    }

    public void setBody(BaseDataInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
