package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.StoreSearchKeywordGridAdapter;
import com.ilingtong.library.tongle.protocol.SearchKeywordInfo;
import com.ilingtong.library.tongle.protocol.SearchKeywordResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SearchEditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.activity
 * author:liuting
 * Date:2017/2/14
 * Desc:店铺搜索页
 */

public class StoreSearchActivity extends Activity implements View.OnClickListener, SearchEditText.ISearchListener {
    private SearchEditText svProd;//搜索框
    private TextView tvCancel;//取消
    private GridView gvKeyword;//关键词布局
    private Dialog dialog;//提示dialog
    private StoreSearchKeywordGridAdapter storeSearchKeywordGridAdapter;//关键词Adapter
    private List<SearchKeywordInfo> listKeyword;//关键词集合
    private ArrayList<String> listHint;//关键字提示
    private ArrayAdapter<String> adapterHint;//关键字提示Adapter
    private String mStoreId;//魔店ID
    private ListView lvHint;//提示
    private LinearLayout layoutKeyword;//关键词布局
    private String mFindKey;//关键词

    /**
     * 页面跳转
     *
     * @param activity
     * @param store_id 魔店ID
     */
    public static void launcher(Activity activity, String store_id) {
        Intent intent = new Intent(activity, StoreSearchActivity.class);
        intent.putExtra("store_id", store_id);
        activity.startActivity(intent);
    }

    /**
     * 页面跳转
     *
     * @param activity
     * @param store_id   魔店ID
     * @param find_key   搜索关键词
     */
    public static void launcher(Activity activity, String store_id,String find_key) {
        Intent intent = new Intent(activity, StoreSearchActivity.class);
        intent.putExtra("store_id", store_id);
        intent.putExtra("find_key", find_key);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_search);
        initView();
        doRequest();
    }

    /**
     * 初始化
     */
    private void initView() {
        listKeyword = new ArrayList<>();
        listHint = new ArrayList<>();
        mStoreId = getIntent().getExtras().getString("store_id");
        mFindKey =getIntent().getExtras().getString("find_key","");
        svProd = (SearchEditText) findViewById(R.id.store_search_sv_prod);
        tvCancel = (TextView) findViewById(R.id.store_search_tv_cancel);
        gvKeyword = (GridView) findViewById(R.id.store_search_gv_keyword);
        lvHint = (ListView) findViewById(R.id.store_search_lv_hint);
        layoutKeyword = (LinearLayout) findViewById(R.id.store_search_layout_keyword);

        dialog = DialogUtils.createLoadingDialog(StoreSearchActivity.this);
        dialog.setCancelable(true);

        tvCancel.setOnClickListener(this);
        storeSearchKeywordGridAdapter = new StoreSearchKeywordGridAdapter(StoreSearchActivity.this, listKeyword, new StoreSearchKeywordGridAdapter.IOnItemClickListener() {
            @Override
            public void onItemClick(View v, String find_key) {
                SearchResultsActivity.launchActivityByKey(StoreSearchActivity.this, mStoreId, TongleAppConst.SEARCH_KEY, find_key);
            }
        });
        gvKeyword.setAdapter(storeSearchKeywordGridAdapter);

        adapterHint = new ArrayAdapter(StoreSearchActivity.this,
                android.R.layout.simple_dropdown_item_1line, listHint);
        lvHint.setAdapter(adapterHint);
        lvHint.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                svProd.setText(listHint.get(position).toString());
                SearchResultsActivity.launchActivityByKey(StoreSearchActivity.this, mStoreId, TongleAppConst.SEARCH_KEY, listHint.get(position).toString());
            }
        });
        svProd.setSearchListener(this);
        svProd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //输入框有内容，显示提示列表，清空内容后，显示搜索发现
                if (!TextUtils.isEmpty(svProd.getText())) {
                    layoutKeyword.setVisibility(View.GONE);
                    lvHint.setVisibility(View.VISIBLE);
                    showSearchHint(svProd.getText().toString().trim());
                } else {
                    layoutKeyword.setVisibility(View.VISIBLE);
                    lvHint.setVisibility(View.GONE);
                }
            }
        });
        //获取到关键词不空，则显示在搜索框中
        if(!TextUtils.isEmpty(mFindKey)){
           svProd.setText(mFindKey);
            svProd.setSelection(mFindKey.length());
        }
    }

    /**
     * 显示提示的内容
     *
     * @param content 输入的内容
     */
    public void showSearchHint(String content) {
        //接口还未返回结果时，输入框内容已变更，原来的结果不显示在列表中
        if (listHint != null) {
            listHint.clear();
            adapterHint.notifyDataSetChanged();
        }

        ServiceManager.getSearchHint(content, TongleAppConst.FETCH_COUNT, new Response.Listener<SearchKeywordResult>() {
            @Override
            public void onResponse(SearchKeywordResult searchKeywordResult) {
                if (TongleAppConst.SUCCESS.equals(searchKeywordResult.getHead().getReturn_flag())) {
                    for (int i = 0; i < searchKeywordResult.getBody().getKey_list().size(); i++) {
                        listHint.add(searchKeywordResult.getBody().getKey_list().get(i).getContent());
                    }
                    adapterHint.notifyDataSetChanged();
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + searchKeywordResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        });
    }

    /**
     * 请求数据
     */
    public void doRequest() {
        showDialog();
        ServiceManager.getSearchKeyword(new Response.Listener<SearchKeywordResult>() {
            @Override
            public void onResponse(SearchKeywordResult searchKeywordResult) {
                dismissDialog();
                if (TongleAppConst.SUCCESS.equals(searchKeywordResult.getHead().getReturn_flag())) {
                    listKeyword.addAll(searchKeywordResult.getBody().getKey_list());
                    storeSearchKeywordGridAdapter.notifyDataSetChanged();
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + searchKeywordResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dismissDialog();
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.store_search_tv_cancel) {//取消，finish当前页
            finish();
        }
    }

    /**
     * dismiss dialog
     */
    public void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    /**
     * 显示Dialog
     */
    public void showDialog() {
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissDialog();
    }

    @Override
    public void search() {
        if (!TextUtils.isEmpty(svProd.getText().toString().trim())) {
            SearchResultsActivity.launchActivityByKey(StoreSearchActivity.this, mStoreId, TongleAppConst.SEARCH_KEY, svProd.getText().toString().trim());
        }
    }
}
