package com.ilingtong.library.tongle.activity;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.CollectFragmentPagerAdapter;
import com.ilingtong.library.tongle.fragment.MExpertFragment;
import com.ilingtong.library.tongle.fragment.MForumFragment;
import com.ilingtong.library.tongle.fragment.MProductFragment;
import com.ilingtong.library.tongle.fragment.MStoreFragment;
import com.ilingtong.library.tongle.widget.SearchEditText;

import java.util.ArrayList;

/**
 * User: shiyuchong
 * Date: 2015/6/23
 * Time: 9:26
 * Email: ycshi@isoftstone.com
 * Desc:  搜索页面
 */
public class SearchActivity extends BaseFragmentActivity implements SearchEditText.ISearchListener{
    private LinearLayout back_layout;
    //Tab控件
    private TextView expertText;
    private TextView forumText;
    private TextView storeText;
    private TextView productText;
    private ViewPager viewPager;
    //image
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    //pager 与 适配器
    private int currIndex = 0;
    private Fragment[] fragments = new Fragment[4];
    private ArrayList<setKeywordsListener> mFragmentSet = new ArrayList<setKeywordsListener>();
    private SearchEditText search_edt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_collect_layout);
        initView();
    }

    private void initView() {
        //隐藏普通标题栏，显示带搜索框的标题栏
        findViewById(R.id.search_topview).setVisibility(View.VISIBLE);
        findViewById(R.id.proceeds_top_view).setVisibility(View.GONE);
        back_layout = (LinearLayout)findViewById(R.id.back_layout);
        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchActivity.this.finish();
            }
        });

        search_edt = (SearchEditText) findViewById(R.id.search_edt);
        search_edt.setSearchListener(this);
        expertText = (TextView) findViewById(R.id.expert_text);
        forumText = (TextView) findViewById(R.id.forum_text);
        storeText = (TextView) findViewById(R.id.store_text);
        productText = (TextView) findViewById(R.id.product_text);
        viewPager = (ViewPager) findViewById(R.id.collect_pager);
        imageView1 = (ImageView) findViewById(R.id.collect_cursor1);
        imageView2 = (ImageView) findViewById(R.id.collect_cursor2);
        imageView3 = (ImageView) findViewById(R.id.collect_cursor3);
        imageView4 = (ImageView) findViewById(R.id.collect_cursor4);

        expertText.setOnClickListener(new TextOnClickListener(0));
        forumText.setOnClickListener(new TextOnClickListener(1));
        storeText.setOnClickListener(new TextOnClickListener(2));
        productText.setOnClickListener(new TextOnClickListener(3));

        viewPager.setOnPageChangeListener(new ViewPageOnPageChangeListener());
        setMyAdapter();
    }


    /**
     * 显示内容
     */
    private void setMyAdapter() {
        fragments[0] = MExpertFragment.newInstance(TongleAppConst.SEARCH_INTO);
        fragments[1] = MForumFragment.newInstance(TongleAppConst.SEARCH_INTO);
        fragments[2] = MStoreFragment.newInstance(TongleAppConst.SEARCH_INTO);
        fragments[3] = MProductFragment.newInstance(TongleAppConst.SEARCH_INTO);
        mFragmentSet.add((MExpertFragment)fragments[0]);
        mFragmentSet.add((MForumFragment) fragments[1]);
        mFragmentSet.add((MStoreFragment) fragments[2]);
        mFragmentSet.add((MProductFragment) fragments[3]);
        CollectFragmentPagerAdapter adapter = new CollectFragmentPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }



    @Override
    public void search() {
        if (!TextUtils.isEmpty(search_edt.getText().toString().trim())){
            for (int i = 0; i < mFragmentSet.size(); i++) {
                mFragmentSet.get(i).setKeywords(search_edt.getText().toString().trim());
            }
        }
    }
    /**
     * TextView监听
     */
    private class TextOnClickListener implements View.OnClickListener {
        private int index = 0;

        public TextOnClickListener(int i) {
            index = i;
        }

        public void onClick(View v) {
            viewPager.setCurrentItem(index);
        }
    }

    /**
     * viewpager监听
     */

    public class ViewPageOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {

            Toast.makeText(SearchActivity.this, getString(R.string.common_search_now), Toast.LENGTH_LONG);

        }

        public void onPageSelected(int arg0) {
            currIndex = arg0;
            int i = currIndex + 1;
            setTabTitle(i);
        }
    }

    /**
     * 设置选项卡title的字体颜色
     *
     * @param tabId
     */
    public void setTabTitle(int tabId) {
        //viewpager顶部导航游标，并设置游标显示位置
        if (tabId == 1) {
            imageView1.setVisibility(View.VISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 2) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.VISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 3) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.VISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 4) {

            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.VISIBLE);
        }
        //viewpager顶部文字导航，并设置文字颜色
        Resources resource = (Resources) getResources();
        ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_selecttext_color);
        ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_text_color);
        if (tabId == 1) {
            expertText.setTextColor(selectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 2) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(selectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 3) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(selectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 4) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(selectedTextColor);
        }
    }

    public interface setKeywordsListener{
        public void setKeywords(String keywords);
    }
}
