package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/23
 * Time: 10:37
 * Email: jqleng@isoftstone.com
 * Desc: 意见反馈信息类,该包含通过网络返回的数据
 */
public class FeedbackInfo implements Serializable{
    private String title_text;
    private String contact_url;

    public String getTitle_text() { return title_text; }
    public String getContact_url() {
        return contact_url;
    }

    @Override
    public String toString() {
        return "title_text:" + title_text + "\r\n" +
                "contact_url:" + contact_url;
    }
}
