package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.demo.Base64;
import com.ilingtong.library.tongle.protocol.ProductVoiceResult;
import com.ilingtong.library.tongle.utils.AudioTrackPlayThread;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Package:com.ilingtong.library.tongle.activity
 * author:liuting
 * Date:2016/12/6
 * Desc:显示二维码和播放音频
 */

public class ShowQRCodeActivity extends BaseActivity implements View.OnClickListener {
    private String url;    //图片地址
    private String code_type;   //二维码类别
    private String object_id;   //魔店id，会员id，商品id，组织id
    private TextView txt_play_pcm;//播放音频提示文本
    private CheckBox checkBox_paly_pcm;//播放音频开关
    byte[] data = null;
    Thread mThread = null;
    final int EVENT_PLAY_OVER = 0x100;
    private String[] needPermissions={Manifest.permission.RECORD_AUDIO};//麦克风权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            if (message.what == EVENT_PLAY_OVER) {
                checkBox_paly_pcm.setChecked(false);
            }
        }
    };

    /**
     * 页面跳转
     *
     * @param activity
     * @param url         图片地址
     * @param object_id   二维码类别
     * @param code_type   魔店id，会员id，商品id，组织id
     */
    public static void launcher(Activity activity, String url, String object_id, String code_type) {
        Intent intent =new Intent(activity,ShowQRCodeActivity.class);
        intent.putExtra("url",url);
        intent.putExtra("object_id",object_id);
        intent.putExtra("code_type",code_type);
        activity.startActivity(intent);
    }

    /**
     * 页面跳转
     *
     * @param fragment
     * @param url         图片地址
     * @param object_id   二维码类别
     * @param code_type   魔店id，会员id，商品id，组织id
     */
    public static void launcher(Fragment fragment, String url, String object_id, String code_type) {
        Intent intent =new Intent(fragment.getActivity(),ShowQRCodeActivity.class);
        intent.putExtra("url",url);
        intent.putExtra("object_id",object_id);
        intent.putExtra("code_type",code_type);
        fragment.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.expert_popwindow_layout);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
        ImageView coverImage = (ImageView) findViewById(R.id.qr_user);
        url=getIntent().getStringExtra("url");
        code_type=getIntent().getStringExtra("code_type");
        object_id=getIntent().getStringExtra("object_id");

        if (TextUtils.isEmpty(object_id)) {
            findViewById(R.id.relativelayout_play_pcm).setVisibility(View.GONE);
        }

        txt_play_pcm = (TextView) findViewById(R.id.txt_play_pcm);
        checkBox_paly_pcm = (CheckBox) findViewById(R.id.checkbox_play_pcm);
        checkBox_paly_pcm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    requestPermissionUtils= RequestPermissionUtils.getRequestPermissionUtils(ShowQRCodeActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                        @Override
                        public void requestSuccess() {
                            //播放
                            play();
                        }
                        @Override
                        public void requestFail() {
                            SelectDialog dialog=new SelectDialog(ShowQRCodeActivity.this,getString(R.string.please_open_audio_permission));
                            dialog.show();
                            checkBox_paly_pcm.setChecked(false);
                        }
                    });
                    requestPermissionUtils.checkPermissions(ShowQRCodeActivity.this);

                } else {
                    txt_play_pcm.setText(getString(R.string.play_prod_pcm));
                    stop();
                }
            }
        });
        ImageLoader.getInstance().displayImage(url, coverImage, ImageOptionsUtils.getOptions());
    }

    /**
     * 从接口获取音频文件流
     */
    private void getPcmData() {
        ServiceManager.getProductVoice(code_type, object_id, new Response.Listener<ProductVoiceResult>() {
            @Override
            public void onResponse(ProductVoiceResult productVoiceResult) {
                if (TongleAppConst.SUCCESS.equals(productVoiceResult.getHead().getReturn_flag())) {
                    data = Base64.decode(productVoiceResult.body.product_voice_file);
                    play();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + productVoiceResult.getHead().getReturn_message());
                    checkBox_paly_pcm.setChecked(false);
                }
                checkBox_paly_pcm.setEnabled(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                checkBox_paly_pcm.setEnabled(true);
                checkBox_paly_pcm.setChecked(false);
            }
        });
    }

    /**
     * 开始播放
     */
    private void play() {
        if (data == null) {
            ToastUtils.toastShort(getString(R.string.please_wait_for_loading));
            txt_play_pcm.setText(R.string.loading_pcm);
            checkBox_paly_pcm.setEnabled(false);
            getPcmData();
            return;
        }

        if (mThread == null) {
            mThread = new Thread(new AudioTrackPlayThread(data, mHandler, 100));
            mThread.start();
            txt_play_pcm.setText(getString(R.string.stop_prod_pcm));
        }

    }

    /**
     * 停止播放
     */
    private void stop() {
        if (mThread != null) {
            AudioTrackPlayThread.flag = false;
            mThread = null;
        }
    }

    @Override
    protected void onStop() {
        stop();
        super.onStop();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode,permissions,paramArrayOfInt);
    }
}
