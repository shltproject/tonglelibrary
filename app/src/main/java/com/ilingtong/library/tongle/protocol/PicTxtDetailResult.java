package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/26
 * Time: 10:54
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class PicTxtDetailResult implements Serializable {
    private BaseInfo head;
    private PicTxtDetailListInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public PicTxtDetailListInfo getBody() {
        return body;
    }

    public void setBody(PicTxtDetailListInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }

}
