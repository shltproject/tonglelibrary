package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/2/14
 * Desc:店铺分类商品/关键字检索商品信息列表类
 * used by 6021、6022
 */

public class SortProdListInfo implements Serializable {
    private int data_total_count;//商品总数
    private List<SortProdItemInfo> prod_list;//商品列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<SortProdItemInfo> getProd_list() {
        return prod_list;
    }

    public void setProd_list(List<SortProdItemInfo> prod_list) {
        this.prod_list = prod_list;
    }
}
