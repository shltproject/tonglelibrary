package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/2
 * Time: 23:30
 * Email: jqleng@isoftstone.com
 * Desc: 二维码扫描数据类，通过该对象，被用作网络请求参数
 */
public class ScanDataResult implements Serializable {
    private BaseInfo head;
    private ScanDataInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ScanDataInfo getBody() {
        return body;
    }

    public void setBody(ScanDataInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
