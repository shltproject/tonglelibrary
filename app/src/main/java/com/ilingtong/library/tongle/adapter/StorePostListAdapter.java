package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.StorePostInfo;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

/**
 * Package:com.ilingtong.app.cedrus.adapter
 * author:liuting
 * Date:2017/1/11
 * Desc:店铺动态帖子列表Adapter
 */

public class StorePostListAdapter extends BaseAdapter {
    private ArrayList<StorePostInfo> listPost = new ArrayList<>();//帖子列表
    private Context context;//上下文
    private LayoutInflater inflater;
    private IOnItemClickListener listener;//事件监听

    public interface IOnItemClickListener{
        void onItemClick(View view,String post_id);
    }

    /**
     * @param context 上下文
     */
    public StorePostListAdapter(Context context,IOnItemClickListener listener) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return listPost.size();
    }

    @Override
    public Object getItem(int position) {
        return listPost.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 在原有的数据上添加新数据
     *
     * @param itemList
     */
    public void addItems(ArrayList<StorePostInfo> itemList) {
        this.listPost.addAll(itemList);
        notifyDataSetChanged();
    }

    /**
     * 设置为新的数据，旧数据会被清空
     *
     * @param itemList
     */
    public void setItems(ArrayList<StorePostInfo> itemList) {
        this.listPost.clear();
        this.listPost = itemList;
        notifyDataSetChanged();
    }

    /**
     * 清空数据
     */
    public void clearItems() {
        listPost.clear();
        notifyDataSetChanged();
    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null){
            view = inflater.inflate(R.layout.list_item_store_post_layout,null);
            holder = new ViewHolder();
            holder.imgPic = (ImageView) view.findViewById(R.id.store_post_img_pic);
            holder.tvTime = (TextView) view.findViewById(R.id.store_post_tv_time);
            view.setTag(holder);
        }else{
            holder = (ViewHolder)view.getTag();
        }
        ImageLoader.getInstance().displayImage(listPost.get(position).getPost_thumbnail_pic_url(), holder.imgPic, ImageOptionsUtils.getOptions(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (loadedImage == null) {
                    super.onLoadingComplete(imageUri, view, loadedImage);
                } else {
                    float es = (float) (DipUtils.getScreenWidth(context) - context.getResources().getDimensionPixelSize(R.dimen.mstore_home_head_icon_left) * 2) / (float) loadedImage.getWidth();
                    int height = (int) (loadedImage.getHeight() * es);
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
                    params.height = height;
                    view.setLayoutParams(params);
                }
            }
        });
        holder.tvTime.setText(listPost.get(position).getPost_time());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onItemClick(v,listPost.get(position).getPost_id());
                }
            }
        });
        return view;
    }
    static class ViewHolder{
        ImageView imgPic;//帖子封面图
        TextView tvTime;//时间
    }
}
