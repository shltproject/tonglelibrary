package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/21
 * Time: 17:39
 * Email: jqleng@isoftstone.com
 * Dest:use by 6007 6011
 *
 */
public class ProdPicUrlListItem implements Serializable {
    public String pic_url;
}
