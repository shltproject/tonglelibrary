package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:我的优惠券列表的item
 * use by 6014
 */
public class MyCouponInfo implements Serializable {
    private MyCouponBaseInfo voucher_base;    //优惠券基本信息
    private MyCouponLinkInfo voucher_link_info;    //优惠券链接信息

    public MyCouponBaseInfo getVoucher_base() {
        return voucher_base;
    }

    public void setVoucher_base(MyCouponBaseInfo voucher_base) {
        this.voucher_base = voucher_base;
    }

    public MyCouponLinkInfo getVoucher_link_info() {
        return voucher_link_info;
    }

    public void setVoucher_link_info(MyCouponLinkInfo voucher_link_info) {
        this.voucher_link_info = voucher_link_info;
    }
}
