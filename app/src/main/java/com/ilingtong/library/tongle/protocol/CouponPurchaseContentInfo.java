package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/10.
 * mail: wuqian@ilingtong.com
 * Description:团购详情的团购内容对象
 * use by 6007 6011
 */
public class CouponPurchaseContentInfo implements Serializable {
   private String item;  //项目名称
   private String qty;  //数量
   private double price;  //金额

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
