package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/26
 * Time: 14:18
 * Email: leishuai@isoftstone.com
 * Desc: 产品规格信息类，该类包含网络返回的数据
 */
public class ProdParameterListInfo implements Serializable {

    private ArrayList<ProdParameterListItem> prod_parameter_list;

    public ArrayList<ProdParameterListItem> getProd_parameter_list() {
        return prod_parameter_list;
    }

    @Override
    public String toString() {
        return  "prod_parameter_list:" + prod_parameter_list;
    }
}
