package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * @ClassName: AvailableMoneyResult
 * @Package: com.ilingtong.library.tongle.protocol
 * @Description: 6036接口(我的可用余额取得) 返回json结构
 * @author: liuting
 * @Date: 2017/9/7 8:58
 */
public class AvailableMoneyResult extends BaseResult implements Serializable {
    public AvailableMoneyInfo body; //我的可用余额body类
}
