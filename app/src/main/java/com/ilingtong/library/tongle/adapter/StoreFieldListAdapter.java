package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.external.ScrollGridView;
import com.ilingtong.library.tongle.protocol.StoreFieldItemInfo;

import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.adapter
 * author:liuting
 * Date:2017/2/14
 * Desc:店铺分类列表Adapter
 */

public class StoreFieldListAdapter extends BaseAdapter {
    private List<StoreFieldItemInfo> listField;//店铺分类列表
    private Context context;//上下文
    private LayoutInflater inflater;
    private IOnClickListener listener;//事件监听

    public StoreFieldListAdapter(Context context, List<StoreFieldItemInfo> listField,IOnClickListener listener) {
        this.listField = listField;
        this.context = context;
        this.listener=listener;
        inflater = LayoutInflater.from(context);
    }

    public interface IOnClickListener {
        void onFieldClickListener(View view,String field_id);//分类事件监听，传入分类id
        void onSubFieldClickListener(View view,String field_id);//子分类事件监听，传入分类id
    }

    @Override
    public int getCount() {
        return listField.size();
    }

    @Override
    public Object getItem(int position) {
        return listField.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        StoreSubFieldGridAdapter storeSubFieldGridAdapter;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_store_field_layout, null);
            holder = new ViewHolder();
            holder.llyName = (LinearLayout) convertView.findViewById(R.id.store_field_lly_name);
            holder.tvName = (TextView) convertView.findViewById(R.id.store_field_tv_name);
            holder.gvSubField = (ScrollGridView) convertView.findViewById(R.id.store_field_gv_sub_field);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvName.setText(listField.get(position).getField_name());
        holder.llyName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onFieldClickListener(v,listField.get(position).getField_id());
                }
            }
        });

        //存在子分类时设置子分类列表，否则不显示
        if(listField.get(position).getSub_field_list().size()>0){
            holder.gvSubField.setVisibility(View.VISIBLE);
            storeSubFieldGridAdapter = new StoreSubFieldGridAdapter(context, listField.get(position).getSub_field_list(), new StoreSubFieldGridAdapter.IOnItemClickListener() {
                @Override
                public void onItemClick(View v, String field_id) {
                    if(listener!=null){
                        listener.onSubFieldClickListener(v,field_id);
                    }
                }
            });
            holder.gvSubField.setAdapter(storeSubFieldGridAdapter);
        }else{
            holder.gvSubField.setVisibility(View.GONE);
        }
        return convertView;
    }

    static class ViewHolder {
        LinearLayout llyName;//分类Layout
        TextView tvName;//分类名称
        ScrollGridView gvSubField;//子分类列表
    }
}
