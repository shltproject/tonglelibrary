package com.ilingtong.library.tongle.listener;

import android.graphics.Bitmap;
import android.view.View;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by fengguowei on 2017/1/13.
 */
public class ImageLoadListener implements ImageLoadingListener {
    private String themeName;

    public ImageLoadListener(String themeName) {
        this.themeName = themeName;
    }

    @Override
    public void onLoadingStarted(String s, View view) {

    }

    @Override
    public void onLoadingFailed(String s, View view, FailReason failReason) {

    }

    @Override
    public void onLoadingComplete(String s, View view, Bitmap bitmap) {

    }

    @Override
    public void onLoadingCancelled(String s, View view) {

    }

    public String getThemeName()
    {
        return themeName;
    }
}
