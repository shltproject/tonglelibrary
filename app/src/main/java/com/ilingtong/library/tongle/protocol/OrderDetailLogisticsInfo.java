package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class OrderDetailLogisticsInfo implements Serializable {
    public String shipping_company;
    public String shipping_no;
    public String shipping_time;

    @Override
    public String toString() {
        return "shipping_company:" + shipping_company + "\r\n" +
                "shipping_no:" + shipping_no + "\r\n" +
                "shipping_time:" + shipping_time;
    }
}
