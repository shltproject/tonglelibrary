package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by fengguowei on 2017/1/12.
 * 魔店热门帖子 10004
 */
public class MStorePost implements Serializable {
    private int data_total_count;
    private List<PostDetail> store_post_list;

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<PostDetail> getStore_post_list() {
        return store_post_list;
    }

    public void setStore_post_list(List<PostDetail> store_post_list) {
        this.store_post_list = store_post_list;
    }
}
