package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.external.CircleImageView;
import com.ilingtong.library.tongle.fragment.MineFragment;
import com.ilingtong.library.tongle.protocol.MineResult;
import com.ilingtong.library.tongle.protocol.ModifyMessageResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.ImageUtil;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

/**
 * @author cuishuang E-mail:shuangcui@isoftstone.com
 * @Date 2015年8月25日
 * @Time 下午3:58
 * 我的信息
 */
public class SettingMyInfoActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;
    private TextView top_btn_text;
    private ImageView left_arrow_btn;

    private CircleImageView iv_head;//頭像
    private LinearLayout layout_head;//頭像
    private RelativeLayout layout_nicename;//昵称
    private RelativeLayout layout_sex;//性別
    private RelativeLayout layout_realname;//真实姓名
    private RelativeLayout layout_carId;//身份证号码
    private RelativeLayout layout_point_off;//积分开关

    private TextView my_sex;//性别
    public static TextView nick_username;//昵称
    public static TextView txt_realname;//真实姓名
    public static TextView txt_my_carId;//身份证号码
    public static TextView txt_my_point_off;//积分开关

    private static final int PHOTO_REQUEST_CAMERA = 1;// 拍照
    private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
    private static final int PHOTO_REQUEST_CUT = 3;// 结果

    public static final int INFO_NAME = 4;
    public static final int INFO_NICE_NAME = 5;
    public static final int INFO_CARID = 6;
    public static final int ID_CARD_INFO_REQUEST_CODE = 7;//获取身份证信息请求码

    private Bitmap bitmap;
    private AlertDialog alert;

    /* 头像名称 */
    private static final String PHOTO_FILE_NAME = "temp_photo.jpg";
    private File tempFile;
    private String[] needPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};//存储和相机权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类
    private MineResult mineResult;//我的基本信息类
    private Dialog dialogLoading;//加载对话框
    private RelativeLayout layoutCardPic;//身份证图片
    private View viewCardPic;//间隔线

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_myinfo_layout);
        initView();
    }

    private void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        top_btn_text = (TextView) findViewById(R.id.top_btn_text);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        layout_nicename = (RelativeLayout) findViewById(R.id.layout_nicename);
        layout_sex = (RelativeLayout) findViewById(R.id.layout_sex);
        layout_realname = (RelativeLayout) findViewById(R.id.layout_realname);
        layout_carId = (RelativeLayout) findViewById(R.id.layout_carId);
        layout_point_off = (RelativeLayout) findViewById(R.id.layout_point_off);
        my_sex = (TextView) findViewById(R.id.my_sex);
        nick_username = (TextView) findViewById(R.id.nick_username);
        txt_realname = (TextView) findViewById(R.id.txt_realname);
        txt_my_carId = (TextView) findViewById(R.id.my_carId);
        txt_my_point_off = (TextView) findViewById(R.id.txt_my_point_off);
        layout_head = (LinearLayout) findViewById(R.id.layout_head);
        iv_head = (CircleImageView) findViewById(R.id.iv_head);
        layoutCardPic = (RelativeLayout) findViewById(R.id.my_info_layout_card_pic);
        viewCardPic =(View)findViewById(R.id.my_info_view_card_pic);

        top_name.setVisibility(View.VISIBLE);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_btn_text.setVisibility(View.VISIBLE);
        left_arrow_btn.setOnClickListener(this);
        layout_nicename.setOnClickListener(this);
        layout_sex.setOnClickListener(this);
        layout_realname.setOnClickListener(this);
        layout_carId.setOnClickListener(this);
        layout_point_off.setOnClickListener(this);
        layout_head.setOnClickListener(this);
        iv_head.setOnClickListener(this);
        top_btn_text.setOnClickListener(this);
        layoutCardPic.setOnClickListener(this);

        top_name.setText(getString(R.string.setting_my_info_top_name));
        top_btn_text.setText(getString(R.string.setting_my_info_top_txt));

        nick_username.setText(TongleAppInstance.getInstance().getUser_nick_name() + "");//昵称
        txt_realname.setText(TongleAppInstance.getInstance().getUser_name() + "");//姓名
        txt_my_carId.setText(TongleAppInstance.getInstance().getUser_id_no() + "");
        if (TongleAppInstance.getInstance().getUser_sex().equals("1")) {//性别
            my_sex.setText(getString(R.string.setting_my_info_sex_man));
        } else {
            my_sex.setText(getString(R.string.setting_my_info_sex_woman));
        }
        if (TongleAppInstance.getInstance().getUser_customs_flag().equals("1")) {//积分开关
            txt_my_point_off.setText(getString(R.string.setting_my_info_point_on));
        } else {
            txt_my_point_off.setText(getString(R.string.setting_my_info_point_off));
        }
        mineResult = new MineResult();

        ImageLoader.getInstance().displayImage(TongleAppInstance.getInstance().getUser_photo_url(), iv_head, ImageOptionsUtils.getRoundOptions());//头像

        //目前酬客中因为要刷新身份证信息才需要再次请求2012接口取得用户信息，通乐中暂时不需要
        if (TongleAppInstance.getInstance().getPackageName().equals("com.ilingtong.app.chouke")) {
            dialogLoading = DialogUtils.createLoadingDialog(SettingMyInfoActivity.this);
            doRequest();
        }

        if (TongleAppInstance.getInstance().getPackageName().equals("com.ilingtong.app.tongle")) {//通乐中不显示
            layoutCardPic.setVisibility(View.GONE);
            viewCardPic.setVisibility(View.GONE);
        }else if (TongleAppInstance.getInstance().getPackageName().equals("com.ilingtong.app.chouke")) {//酬客中显示
            layoutCardPic.setVisibility(View.VISIBLE);
            viewCardPic.setVisibility(View.VISIBLE);
        }
    }

    /**
     * @throws
     * @author: liuting
     * @date: 2017/6/12 16:20
     * @MethodName: doRequest
     * @Description: 获取我的基本信息
     * @param:
     * @return: void
     */
    public void doRequest() {
        dialogLoading.show();
        ServiceManager.doMineRequest(TongleAppInstance.getInstance().getUserID(), new Response.Listener<MineResult>() {

            @Override
            public void onResponse(MineResult result) {
                dialogLoading.dismiss();
                if (TongleAppConst.SUCCESS.equals(result.getHead().getReturn_flag())) {
                    mineResult = result;
                    if (!TextUtils.isEmpty(result.getBody().getUser_id_no())) {
                        txt_my_carId.setText(result.getBody().getUser_id_no());
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + result.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialogLoading.dismiss();
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (view.getId() == R.id.top_btn_text) {//点击确认按钮
            //头像二进制
            String uphoto;
            if (bitmap == null || bitmap.equals("")) {
                uphoto = "";
            } else {
                uphoto = ImageUtil.bitmaptoString(bitmap);
            }
            TongleAppInstance.getInstance().setUser_nick_name(nick_username.getText().toString());//昵称
            TongleAppInstance.getInstance().setUser_name(txt_realname.getText().toString());//姓名
            TongleAppInstance.getInstance().setUser_id_no(txt_my_carId.getText().toString());//身份证号码

            ServiceManager.ModifyMessageRequest(TongleAppInstance.getInstance().getUserID(), uphoto, TongleAppInstance.getInstance().getUser_nick_name(), TongleAppInstance.getInstance().getUser_sex(), TongleAppInstance.getInstance().getUser_id_no(), TongleAppInstance.getInstance().getUser_customs_flag(), TongleAppInstance.getInstance().getUser_name(), successListener(), errorListener());
        } else if (view.getId() == R.id.layout_head) {//头像
            View head_view = LayoutInflater.from(this).inflate(R.layout.headview_dialog_layout, null);
            TextView takephoto = (TextView) head_view.findViewById(R.id.takePhoto);
            TextView myPicture = (TextView) head_view.findViewById(R.id.myPicture);
            TextView dialog_cancle = (TextView) head_view.findViewById(R.id.dialog_cancle);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(head_view);
            alert = builder.create();
            alert.setCanceledOnTouchOutside(true);
            alert.show();
            //拍照
            takephoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(SettingMyInfoActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                        @Override
                        public void requestSuccess() {
                            //拍照
                            camera();
                        }

                        @Override
                        public void requestFail() {
                            SelectDialog dialog = new SelectDialog(SettingMyInfoActivity.this, getString(R.string.please_open_camera_storage_permission));
                            dialog.show();
                        }
                    });
                    requestPermissionUtils.checkPermissions(SettingMyInfoActivity.this);
                }
            });
            //我的相册
            myPicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gallery();
                }
            });
            //取消
            dialog_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });
        } else if (view.getId() == R.id.layout_nicename) {//昵称
            String ss = nick_username.getText().toString();
            Intent intent = new Intent(this, SettingMyinfoNameActivity.class);
            intent.putExtra("name", ss);
            startActivityForResult(intent, INFO_NICE_NAME);
        } else if (view.getId() == R.id.layout_sex) {//性别
            View sex_view = LayoutInflater.from(this).inflate(R.layout.sex_dialog_layout, null);
            TextView boy = (TextView) sex_view.findViewById(R.id.boy);
            TextView girl = (TextView) sex_view.findViewById(R.id.girl);

            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setView(sex_view);
            final AlertDialog alert1 = builder1.create();
            alert1.setCanceledOnTouchOutside(true);
            alert1.show();
            boy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    my_sex.setText(getString(R.string.setting_my_info_sex_man));
                    TongleAppInstance.getInstance().setUser_sex("1");
                    alert1.dismiss();
                }
            });
            girl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    my_sex.setText(getString(R.string.setting_my_info_sex_woman));
                    TongleAppInstance.getInstance().setUser_sex("2");
                    alert1.dismiss();
                }
            });
        } else if (view.getId() == R.id.layout_point_off) {//开关
            View sex_view = LayoutInflater.from(this).inflate(R.layout.integral_dialog_layout, null);
            TextView integral_open = (TextView) sex_view.findViewById(R.id.integral_open);
            TextView integral_close = (TextView) sex_view.findViewById(R.id.integral_close);

            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setView(sex_view);
            final AlertDialog alert2 = builder1.create();
            alert2.setCanceledOnTouchOutside(true);
            alert2.show();
            integral_open.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txt_my_point_off.setText(getString(R.string.setting_my_info_point_on));
                    TongleAppInstance.getInstance().setUser_customs_flag("1");
                    alert2.dismiss();
                }
            });
            integral_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txt_my_point_off.setText(getString(R.string.setting_my_info_point_off));
                    TongleAppInstance.getInstance().setUser_customs_flag("2");
                    alert2.dismiss();
                }
            });

        } else if (view.getId() == R.id.layout_realname) {//姓名
            String ss = txt_realname.getText().toString();
            Intent intent = new Intent(this, SettingMyRealNameActivity.class);
            intent.putExtra("name", ss);
            startActivityForResult(intent, INFO_NAME);
        } else if (view.getId() == R.id.layout_carId) {//身份证号
                //通乐中跳转到身份证号信息输入页面
                String ss = txt_my_carId.getText().toString();
                Intent intent = new Intent(this, SettingMyIdCardActivity.class);
                intent.putExtra("name", ss);
                startActivityForResult(intent, INFO_CARID);
        } else if (view.getId() == R.id.my_info_layout_card_pic) {//身份信息
            if (TongleAppInstance.getInstance().getPackageName().equals("com.ilingtong.app.chouke")) {
                //酬客中进入到身份证照片展示页面
                if (mineResult.getBody() != null) {
                    SettingIdCardInfoActivity.launcher(SettingMyInfoActivity.this, mineResult.getBody().getId_info(), ID_CARD_INFO_REQUEST_CODE);
                } else {
                    ToastUtils.toastShort(getString(R.string.get_id_card_info_errror_please_try_again));
                }
            }
        }
    }

    /*
     * 从相册获取
     */

    public void gallery() {
        // 激活系统图库，选择一张图片
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PHOTO_REQUEST_GALLERY) {
                if (data != null) {
                    // 得到图片的全路径
                    Uri uri = data.getData();
                    crop(uri);
                }

            } else if (requestCode == PHOTO_REQUEST_CAMERA) {
                if (hasSdcard()) {
                    tempFile = new File(Environment.getExternalStorageDirectory(),
                            PHOTO_FILE_NAME);
                    crop(Uri.fromFile(tempFile));
                } else {
                    ToastUtils.toastShort(getString(R.string.common_sd_null));
                }

            } else if (requestCode == PHOTO_REQUEST_CUT) {
                try {
                    bitmap = data.getParcelableExtra("data");
                    iv_head.setImageBitmap(bitmap);
                    alert.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == INFO_NAME) {//姓名
                txt_realname.setText(data.getStringExtra("text") + "");
            } else if (requestCode == INFO_NICE_NAME) {//昵称
                nick_username.setText(data.getStringExtra("text") + "");
            } else if (requestCode == INFO_CARID) {//身份证号
                txt_my_carId.setText(data.getStringExtra("text") + "");
            } else if (requestCode == ID_CARD_INFO_REQUEST_CODE) {//身份证照片更新成功后，身份证号码也要更新
                doRequest();
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /*
     * 从相机获取
	 */
    public void camera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        // 判断存储卡是否可以用，可用进行存储
        if (hasSdcard()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), PHOTO_FILE_NAME)));
        }
        startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
    }


    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 剪切头像图片
     *
     * @param uri
     * @function:
     * @author:Jerry
     * @date:2013-12-30
     */
    private void crop(Uri uri) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例，1：1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 280);
        intent.putExtra("outputY", 280);
        // 图片格式
        intent.putExtra("outputFormat", "JPEG");
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", true);// true:不返回uri，false：返回uri
        startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }


    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<ModifyMessageResult>() {
            @Override
            public void onResponse(ModifyMessageResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    TongleAppInstance.getInstance().setUser_photo_url(response.getBody().getUser_photo_url());
                    if (!TextUtils.isEmpty(TongleAppInstance.getInstance().getUser_phone())) {
                        ToastUtils.toastShort(getString(R.string.common_update_success));
                        if (MineFragment.instance != null) {
                            MineFragment.instance.doRequest();
                        }
                    }
                    finish();
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
