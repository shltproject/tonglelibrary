package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description: 团购订单基本信息
 * use by 6013 6012
 */
public class CouponOrderBaseInfo implements Serializable {
    private String order_no;   //		订单号
    private String order_create_time;    //		订单生成日期
    private String prod_id;     //商品ID
    private String prod_name;      //商品名称
    private int order_qty;   //商品订购数量
    private double order_amount;  // 商品订购总价
    private int unused_qty;   //可使用团购券数量
    private int refund_qty;    //k可退团购券数量
    private int used_qty;    //已消费团购券数量
    private int refunding_qty;   //退款中团购券数量   add at 2016/04/14
    private int expire_qty;    //已过期团购券数量  add at 2016/04/14

    private OrderPayInfo order_pay_info;   //订单支付情报  6013接口中不包含该字段
    private ArrayList<ProdSpecListInfo> prod_spec_list;   //规格列表
    private List<ProdPicUrlListItem> prod_pic_url_list;   //商品图片URL列表
    private MyCouponBaseInfo voucher_base;    //优惠券基本信息
    private String relation_id;//二维码关联ID
    private double order_price;   //购买单价
    private String evaluate_flag;   //是否可评价标志  0：可评价  1：不可评价   add at 2016/04/13

    public String getEvaluate_flag() {
        return evaluate_flag;
    }

    public int getRefunding_qty() {
        return refunding_qty;
    }

    public void setRefunding_qty(int refunding_qty) {
        this.refunding_qty = refunding_qty;
    }

    public int getExpire_qty() {
        return expire_qty;
    }

    public void setExpire_qty(int expire_qty) {
        this.expire_qty = expire_qty;
    }

    public void setEvaluate_flag(String evaluate_flag) {
        this.evaluate_flag = evaluate_flag;
    }
    public MyCouponBaseInfo getVoucher_base() {
        return voucher_base;
    }

    public void setVoucher_base(MyCouponBaseInfo voucher_base) {
        this.voucher_base = voucher_base;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getOrder_create_time() {
        return order_create_time;
    }

    public void setOrder_create_time(String order_create_time) {
        this.order_create_time = order_create_time;
    }

    public String getProd_id() {
        return prod_id;
    }

    public void setProd_id(String prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public int getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(int order_qty) {
        this.order_qty = order_qty;
    }

    public double getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(double order_amount) {
        this.order_amount = order_amount;
    }

    public int getUnused_qty() {
        return unused_qty;
    }

    public void setUnused_qty(int unused_qty) {
        this.unused_qty = unused_qty;
    }

    public int getRefund_qty() {
        return refund_qty;
    }

    public void setRefund_qty(int refund_qty) {
        this.refund_qty = refund_qty;
    }

    public int getUsed_qty() {
        return used_qty;
    }
    public void setUsed_qty(int used_qty) {
        this.used_qty = used_qty;
    }

    public OrderPayInfo getOrder_pay_info() {
        return order_pay_info;
    }

    public void setOrder_pay_info(OrderPayInfo order_pay_info) {
        this.order_pay_info = order_pay_info;
    }

    public ArrayList<ProdSpecListInfo> getProd_spec_list() {
        return prod_spec_list;
    }

    public void setProd_spec_list(ArrayList<ProdSpecListInfo> prod_spec_list) {
        this.prod_spec_list = prod_spec_list;
    }

    public List<ProdPicUrlListItem> getProd_pic_url_list() {
        return prod_pic_url_list;
    }

    public void setProd_pic_url_list(List<ProdPicUrlListItem> prod_pic_url_list) {
        this.prod_pic_url_list = prod_pic_url_list;
    }

    public String getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(String relation_id) {
        this.relation_id = relation_id;
    }

    public double getOrder_price() {
        return order_price;
    }

    public void setOrder_price(double order_price) {
        this.order_price = order_price;
    }
}
