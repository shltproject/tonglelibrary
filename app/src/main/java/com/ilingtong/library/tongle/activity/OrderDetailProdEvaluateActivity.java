package com.ilingtong.library.tongle.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.OrderDetailEvaluateListAdaper;
import com.ilingtong.library.tongle.external.NewActivity;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.model.OrderDetailModel;
import com.ilingtong.library.tongle.protocol.OrderDetailOrderDetailInfo;
import com.ilingtong.library.tongle.protocol.OrderDetailResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.List;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc: 产品评价
 */
public class OrderDetailProdEvaluateActivity extends NewActivity implements View.OnClickListener {
    private ImageView left_arrow_btn;
    private TextView top_name;
    private XListView listview;
    private OrderDetailModel OrderDetailModel;
    private String order;

    private List<OrderDetailOrderDetailInfo> list;
    public static final int EVALUATE_REQUESTCODE = 10001;
    private boolean isEvaluated;  //是否评价完成 为true时表示评价完成

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xlistview_comm_layout);
        getData();
        initView();
        if (list == null || list.size() < 1) {
            doRequest();
        } else {
            updateListView();
        }

    }

    public void getData() {
        order = getIntent().getExtras().getString("order_no");
        list = (List<OrderDetailOrderDetailInfo>) getIntent().getExtras().getSerializable("list");

    }

    //初始化控件
    void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        listview = (XListView) findViewById(R.id.xlistview);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(getString(R.string.order_detail_prod_evaluate_top_name));

        //订单基本信息
        if (OrderDetailModel == null)
            OrderDetailModel = new OrderDetailModel();
    }

    public void doRequest() {
        ServiceManager.OrderDetailRequest(TongleAppInstance.getInstance().getUserID(), order, successListener(), errorListener());
    }


    @Override
    public void onRefresh(int id) {
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        }
    }

    //给listview填充数据
    public void updateListView() {
        OrderDetailEvaluateListAdaper adapter = new OrderDetailEvaluateListAdaper(this, list, order);
        listview.setPullLoadEnable(false);
        listview.setPullRefreshEnable(true);
        listview.setXListViewListener(this, 0);
        listview.setRefreshTime();
        listview.setAdapter(adapter);
    }

    /**
     * 功能：订单详情网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<OrderDetailResult>() {
            @Override
            public void onResponse(OrderDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    list = response.getBody().order_info.order_detail;
                    updateListView();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                isEvaluated = !TongleAppConst.YES.equals(response.getBody().order_info.head_info.evaluate_flag);
            }
        };
    }

    /**
     * 功能：订单详情请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EVALUATE_REQUESTCODE && resultCode == RESULT_OK) {
            onRefresh(0);
        }
    }

    @Override
    public void finish() {
        if (isEvaluated){
            setResult(RESULT_OK);   //表示回到订单列表页面是应该隐藏改订单的“立即评价”按钮
        }
        super.finish();
    }
}