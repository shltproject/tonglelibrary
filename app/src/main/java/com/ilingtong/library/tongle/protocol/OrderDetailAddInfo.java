package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class OrderDetailAddInfo implements Serializable {
    public String address_no;
    public String consignee;
    public String tel;
    public String province;
    public String area;
    public String address;

    @Override
    public String toString() {
        return "address_no:" + address_no + "\r\n" +
                "consignee:" + consignee + "\r\n" +
                "tel:" + tel + "\r\n" +
                "province:" + province + "\r\n" +
                "area:" + area + "\r\n" +
                "address:" + address;
    }
}
