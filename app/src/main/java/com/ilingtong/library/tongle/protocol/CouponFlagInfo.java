package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/10.
 * mail: wuqian@ilingtong.com
 * Description:团购商品退货标志信息 是否随时退，是否过期退，不可退标志 0:是；1:否
 * use by 6007
 */
public class CouponFlagInfo implements Serializable{
    private String ready_to_retire_flg;    //随时退标志
    private String expired_refund_flg;    //过期退标志
    private String non_refundable_flg;    //不可退标志

    public String getReady_to_retire_flg() {
        return ready_to_retire_flg;
    }

    public void setReady_to_retire_flg(String ready_to_retire_flg) {
        this.ready_to_retire_flg = ready_to_retire_flg;
    }

    public String getExpired_refund_flg() {
        return expired_refund_flg;
    }

    public void setExpired_refund_flg(String expired_refund_flg) {
        this.expired_refund_flg = expired_refund_flg;
    }

    public String getNon_refundable_flg() {
        return non_refundable_flg;
    }

    public void setNon_refundable_flg(String non_refundable_flg) {
        this.non_refundable_flg = non_refundable_flg;
    }
}
