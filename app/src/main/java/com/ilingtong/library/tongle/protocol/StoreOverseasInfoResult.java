package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/7/14.
 * mail: wuqian@ilingtong.com
 * Description: 店铺当期海淘信息
 * 6033接口返回json body
 */

public class StoreOverseasInfoResult extends BaseResult implements Serializable{
    public StoreOverseasGroup body;
}
