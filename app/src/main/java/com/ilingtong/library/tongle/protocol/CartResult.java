package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/3
 * Time: 17:51
 * Email: jqleng@isoftstone.com
 * Dest: 购物车
 */
public class CartResult implements Serializable {
    private BaseInfo head;
    private CartInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public CartInfo getBody() {
        return body;
    }

    public void setBody(CartInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
