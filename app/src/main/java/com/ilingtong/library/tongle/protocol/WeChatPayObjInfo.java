package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/8/7
 * Time: 9:30
 * Email: leishuai@isoftstone.com
 * Desc: 微信信息类
 */
public class WeChatPayObjInfo implements Serializable {
    private WeChatPayInfo wexin_pay_info;

    public WeChatPayInfo getWexin_pay_info() {
        return wexin_pay_info;
    }

    @Override
    public String toString() {
                return "wexin_pay_info:" + wexin_pay_info;
    }
}
