package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:6014接口（我的优惠券信息）返回json 对应的entity
 */
public class MyCouponInfoResult extends BaseResult implements Serializable{
    private MyCouponInfoList body;

    public MyCouponInfoList getBody() {
        return body;
    }

    public void setBody(MyCouponInfoList body) {
        this.body = body;
    }
}
