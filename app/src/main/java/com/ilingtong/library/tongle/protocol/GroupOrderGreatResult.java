package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:6009（团购券订单提交）接口返回json对应entity
 */
public class GroupOrderGreatResult extends BaseResult implements Serializable{
    private GroupOrderGreatBodyInfo body;

    public GroupOrderGreatBodyInfo getBody() {
        return body;
    }

    public void setBody(GroupOrderGreatBodyInfo body) {
        this.body = body;
    }
}
