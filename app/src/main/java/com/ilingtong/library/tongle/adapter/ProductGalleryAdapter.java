package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.MagazineDetail;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.adapter
 * author:liuting
 * Date:2017/1/13
 * Desc:滚动商品Adapter
 */

public class ProductGalleryAdapter extends
        RecyclerView.Adapter<ProductGalleryAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onProductItemClick(View view, String goods_id, String relation_id);//商品点击事件

        void onMoreItemClick(View view);//更多商品点击事件
    }

    private OnItemClickListener mOnItemClickListener;//事件监听

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    private LayoutInflater mInflater;
    private List<MagazineDetail> listProduct;//商品列表

    public ProductGalleryAdapter(Context context, List<MagazineDetail> list_product) {
        mInflater = LayoutInflater.from(context);
        listProduct = list_product;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View arg0) {
            super(arg0);
        }

        ImageView imgPic;//商品图片
        TextView tvTitle;//商品标题
        TextView tvPrice;//商品价格
        TextView tvMore;//查看更多
        LinearLayout layoutInfo;//商品信息布局
    }

    @Override
    public int getItemCount() {
        return listProduct.size() > 5 ? 6 : listProduct.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recycler_item_product_layout,
                viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.imgPic = (ImageView) view.findViewById(R.id.product_img_pic);
        viewHolder.tvTitle = (TextView) view.findViewById(R.id.product_tv_title);
        viewHolder.tvPrice = (TextView) view.findViewById(R.id.product_tv_price);
        viewHolder.tvMore = (TextView) view.findViewById(R.id.product_tv_more);
        viewHolder.layoutInfo = (LinearLayout) view.findViewById(R.id.product_layout_info);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        //商品栏最多显示5个商品，超过后显示查看更多，跳转到帖子详情
        if (listProduct.size() > 5 && position == 5) {
            viewHolder.tvMore.setVisibility(View.VISIBLE);
            viewHolder.layoutInfo.setVisibility(View.INVISIBLE);
            viewHolder.imgPic.setVisibility(View.GONE);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onMoreItemClick(v);
                    }
                }
            });
            return;
        }
        viewHolder.tvMore.setVisibility(View.GONE);
        viewHolder.layoutInfo.setVisibility(View.VISIBLE);
        viewHolder.imgPic.setVisibility(View.VISIBLE);
        ImageLoader.getInstance().displayImage(listProduct.get(position).getSpe_mobile_pic_addr(), viewHolder.imgPic, ImageOptionsUtils.getOptions());
        viewHolder.tvTitle.setText(listProduct.get(position).getGoods_name());
        viewHolder.tvPrice.setText(FontUtils.priceFormat(listProduct.get(position).getGoods_price()));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onProductItemClick(v, listProduct.get(position).getGoods_id(), listProduct.get(position).getRelation_id());
                }
            }
        });
    }

}

