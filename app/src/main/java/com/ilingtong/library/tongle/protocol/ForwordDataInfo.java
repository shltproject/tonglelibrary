package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: cuishuang
 * Date: 2015/7/11
 * Time: 14:30
 * Email: shuangcui@isoftstone.com
 * Desc: 商品转发类，该类包含通过网络返回的数据
 */
public class ForwordDataInfo implements Serializable {
    private String prod_link_url;
    private String prod_image_url;
    private String magazine_title;   //转发后杂志标题 add at 2016/3/21
    private String wx_profile;//转发后微信说明 add at 2016/3/21

    public String getProd_link_url() {
        return prod_link_url;
    }

    public void setProd_link_url(String prod_link_url) {
        this.prod_link_url = prod_link_url;
    }

    public String getProd_image_url() {
        return prod_image_url;
    }

    public void setProd_image_url(String prod_image_url) {
        this.prod_image_url = prod_image_url;
    }

    public String getMagazine_title() {
        return magazine_title;
    }

    public void setMagazine_title(String magazine_title) {
        this.magazine_title = magazine_title;
    }

    public String getWx_profile() {
        return wx_profile;
    }

    public void setWx_profile(String wx_profile) {
        this.wx_profile = wx_profile;
    }
}
