package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.protocol.PostDetail;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.widget.LinearLayoutManagerPlus;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.adapter
 * author:liuting
 * Date:2017/1/13
 * Desc:店铺首页帖子Adapter
 */

public class StoreHomePostAdapter extends BaseAdapter {
    private Context context;//上下文
    private LayoutInflater inflater;
    private IOnItemClickListener listener;//事件监听
    private ProductGalleryAdapter productGalleryAdapter;//滚动商品Adapter
    private List<PostDetail> listPost = new ArrayList<>();//帖子列表

    public interface IOnItemClickListener {
        void onPostItemClick(View view, String post_id);//帖子封面图点击事件

        void onProductItemClick(View view, String goods_id, String relation_id, String postId);//商品点击事件
    }

    /**
     * @param context 上下文
     */
    public StoreHomePostAdapter(Context context, IOnItemClickListener listener) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return listPost.size();
    }

    @Override
    public Object getItem(int position) {
        return listPost.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 在原有的数据上添加新数据
     *
     * @param itemList
     */
    public void addItems(List<PostDetail> itemList) {
        this.listPost.addAll(itemList);
        notifyDataSetChanged();
    }

    /**
     * 设置为新的数据，旧数据会被清空
     *
     * @param itemList
     */
    public void setItems(List<PostDetail> itemList) {
        this.listPost.clear();
        this.listPost = itemList;
        notifyDataSetChanged();
    }

    /**
     * 清空数据
     */
    public void clearItems() {
        listPost.clear();
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_store_home_layout, null);
            holder = new ViewHolder();
            holder.imgMagazinePic = (ImageView) view.findViewById(R.id.store_home_img_magazine_pic);
            holder.recyclerProduct = (RecyclerView) view.findViewById(R.id.store_home_recycler_product);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        //帖子中包如只包含一个商品或者没有商品时，则商品栏不显示
        if (listPost.get(position).getMagazine_goods_list().size() <= 1) {
            holder.recyclerProduct.setVisibility(View.GONE);
        } else {
            holder.recyclerProduct.setVisibility(View.VISIBLE);
            LinearLayoutManagerPlus linearLayoutManager = new LinearLayoutManagerPlus(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            holder.recyclerProduct.setLayoutManager(linearLayoutManager);
            productGalleryAdapter = new ProductGalleryAdapter(context, listPost.get(position).getMagazine_goods_list());

            productGalleryAdapter.setOnItemClickListener(new ProductGalleryAdapter.OnItemClickListener() {
                @Override
                public void onProductItemClick(View view, String goods_id, String relation_id) {
                    if (listener != null) {
                        listener.onProductItemClick(view, goods_id, relation_id, listPost.get(position).getPost_id());
                    }
                }

                @Override
                public void onMoreItemClick(View view) {
                    //跳转到帖子详情页
                    Intent intent = new Intent(context, CollectForumDetailActivity.class);
                    intent.putExtra("post_id", listPost.get(position).getPost_id());
                    context.startActivity(intent);
                }
            });
            holder.recyclerProduct.setAdapter(productGalleryAdapter);
        }
        ImageLoader.getInstance().displayImage(listPost.get(position).getMagazine_pic(), holder.imgMagazinePic, ImageOptionsUtils.getOptions(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (loadedImage == null) {
                    super.onLoadingComplete(imageUri, view, loadedImage);
                } else {
                    float es = (float) (DipUtils.getScreenWidth(context) - context.getResources().getDimensionPixelSize(R.dimen.mstore_home_head_icon_left) * 2) / (float) loadedImage.getWidth();
                    int height = (int) (loadedImage.getHeight() * es);
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
                    params.height = height;
                    view.setLayoutParams(params);
                }
            }
        });
        holder.imgMagazinePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onPostItemClick(v, listPost.get(position).getPost_id());
                }
            }
        });
        return view;
    }

    static class ViewHolder {
        ImageView imgMagazinePic;//帖子杂志封面图
        RecyclerView recyclerProduct;//滚动商品列表
    }
}
