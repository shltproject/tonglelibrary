package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/17
 * Time: 16:32
 * Email: jqleng@isoftstone.com
 * Desc: 作为对象参数，通过网络传回到应用的的密码找回信息类
 */
public class PWRecoveryResult implements Serializable{

    private BaseInfo head;
    private PWRecoveryInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public PWRecoveryInfo getBody() {
        return body;
    }

    public void setBody(PWRecoveryInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }

}
