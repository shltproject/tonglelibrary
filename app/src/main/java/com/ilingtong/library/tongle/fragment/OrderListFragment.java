package com.ilingtong.library.tongle.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.TotalOrderListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.OrderListItemData;
import com.ilingtong.library.tongle.protocol.OrderRequestParam;
import com.ilingtong.library.tongle.protocol.UserOrdersResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2016/2/2.
 * mail: wuqian@ilingtong.com
 * Description: 全部/待付款/待收货/待评价  的fragment页面。共用页面，根据type区分
 * 继承LazyFragment，实时加载fragment
 */
public class OrderListFragment extends LazyFragment implements XListView.IXListViewListener {
    private XListView listview;
    private RelativeLayout rl_replace;
    private LinearLayout topview_layout;
    private TotalOrderListAdapter totalOrderListAdapter;
    private OrderRequestParam param;
    private String type;
    // 标志位，标志已经初始化完成。
    private boolean isPrepared;
    private Dialog dialog;
    private List<OrderListItemData> orderlist = new ArrayList<>(); //订单列表

    public static final int REFRESH_LIST = 0;  //刷新列表
    public static final int NO_DATA = 1; //隐藏listview，显示rl_replace
    public static final int CONFIRM_RECEIVE_SUCCESS = 2;  //确认收货成功
    public static final int PAY_SUCCESS = 3;   //支付成功
    public static final int EVALUATE_COMPLETE = 4;   //全部商品都已评价完成
    private boolean loadMoreFlag = true; //是否能加载更多。
    /**********
     * 注意：这里类别的值和接口入口参数常量值保持一致是为了方便接口调用，无其他意义
     ***************************/
    public static final String TYPE_ALL = TongleAppConst.ORDER_FILTER_TOTAL; //表示列别为全部
    public static final String TYPE_WAIT_PAY = TongleAppConst.ORDER_FILTER_PENDING_PAY;//表示类别为待付款
    public static final String TYPE_WAIT_RECEIVE = TongleAppConst.ORDER_FILTER_PENGDING_DELIVERY;//表示类别为待收货
    public static final String TYPE_WAIT_EVALUATE = TongleAppConst.ORDER_FILTER_PENDING_COMMENT; //表示类别待评价

    public static final int REQUESTCODE_TO_PAY = 10001;   //activityForResult 支付
    public static final int REQUESTCODE_TO_EVALUATE = 10002;  //activityForResult 评价
    public static final int REQUESTCODE_TO_DETALI = 10003;  //activityForResult 订单详情

    private boolean clearFlag=true;//清空标志,true为清空


    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_LIST:  //刷新
                    listview.setRefreshTime();
                    totalOrderListAdapter.notifyDataSetChanged();
                    break;
                case NO_DATA:  //接口无数据返回时显示无数据画面
                    listview.setVisibility(View.GONE);
                    rl_replace.setVisibility(View.VISIBLE);
                    break;
                case CONFIRM_RECEIVE_SUCCESS:   //确认收货成功，改变订单状态。“确认收货”按钮改成“立即评价”
                    orderlist.get(totalOrderListAdapter.getOperatePosition()).status = TongleAppConst.ORDER_NO_COMMENTS;
                    totalOrderListAdapter.notifyDataSetChanged();
                    break;
                case PAY_SUCCESS:    //支付成功。隐藏立即支付按钮
                    orderlist.get(totalOrderListAdapter.getOperatePosition()).status = TongleAppConst.ORDER_PAID;
                    totalOrderListAdapter.notifyDataSetChanged();
                    break;
                case EVALUATE_COMPLETE:  //全部商品评价完成。隐藏立即评价按钮
                    orderlist.get(totalOrderListAdapter.getOperatePosition()).status = TongleAppConst.ORDER_HAVE_COMMENTS;
                    totalOrderListAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    public static OrderListFragment newInstance(String type) {

        Bundle args = new Bundle();
        args.putString("type", type);
        OrderListFragment fragment = new OrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.xlistview_comm_layout, null);
        initView(view);
        type = getArguments().getString("type", "");
        isPrepared = true;
        lazyLoad();
        return view;
    }

    /**
     * 初始化页面
     *
     * @param view
     */
    public void initView(View view) {
        dialog = DialogUtils.createLoadingDialog(getActivity());
        dialog.setCancelable(true);
        listview = (XListView) view.findViewById(R.id.xlistview);
        rl_replace = (RelativeLayout) view.findViewById(R.id.rl_replace);
        topview_layout = (LinearLayout) view.findViewById(R.id.topview_layout);
        topview_layout.setVisibility(View.GONE);
        orderlist.clear();
        totalOrderListAdapter = new TotalOrderListAdapter(OrderListFragment.this, orderlist,mHandler);
        listview.setPullLoadEnable(false);
        listview.setPullRefreshEnable(true );
        listview.setXListViewListener(this, 0);
        listview.setAdapter(totalOrderListAdapter);

        //设置订单支付消息处理
        TongleAppInstance.getInstance().setmPayHandler(mHandler);
    }

    /**
     * 调用接口请求数据
     *
     * @param type 订单状态
     */
    public void doRequest(String type, String orderNo) {
        Log.e("tag", "type:" + type);
        param = new OrderRequestParam();
        param.user_id = TongleAppInstance.getInstance().getUserID();
        param.order_status = type;
        param.order_date_from = "";
        param.order_date_to = "";
        param.order_no = orderNo;
        param.forward = TongleAppConst.FORWORD_DONW;
        param.fetch_count = TongleAppConst.FETCH_COUNT;
        ServiceManager.getUserOrdersRequest(param, successListener(), errorListener());
        dialog.show();
    }

    @Override
    public void onRefresh(int id) {
        clearFlag=true;
        doRequest(type, "");
    }

    @Override
    public void onLoadMore(int id) {
        if (loadMoreFlag) {
            clearFlag=false;
            doRequest(type, orderlist.get(orderlist.size() - 1).order_no);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.fragment_order_list_no_data), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<UserOrdersResult>() {
            @Override
            public void onResponse(UserOrdersResult userOrdersResult) {
                Log.e("tag", "type回调:" + type);
                dialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(userOrdersResult.getHead().getReturn_flag())) {
                    //刷新成功后清空list
                    if(orderlist!=null&&clearFlag){
                        orderlist.clear();
                    }
                    orderlist.addAll(userOrdersResult.getBody().getOrder_list());
                    if (Integer.parseInt(userOrdersResult.getBody().getData_total_count()) < 1) {
                        mHandler.sendEmptyMessage(NO_DATA);
                    } else {
                        if (Integer.parseInt(userOrdersResult.getBody().getData_total_count()) > orderlist.size()) {
                            loadMoreFlag = true;
                        } else {
                            loadMoreFlag = false;
                        }
                        listview.setPullLoadEnable(loadMoreFlag);
                        mHandler.sendEmptyMessage(REFRESH_LIST);
                    }
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + userOrdersResult.getHead().getReturn_message());
                }
            }

        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case REQUESTCODE_TO_PAY:   //支付成功
                    mHandler.sendEmptyMessage(PAY_SUCCESS);
                    break;
                case REQUESTCODE_TO_EVALUATE:   //全部商品评价完成
                    mHandler.sendEmptyMessage(EVALUATE_COMPLETE);
                    break;
                case REQUESTCODE_TO_DETALI:
                    onRefresh(0);
                    break;
            }
        }
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        };
    }
    @Override
    protected void lazyLoad() {
        Log.e("tag", "lazyLoad" + isVisible);
        if (!isPrepared || !isVisible) {
            return;
        } else {
            orderlist.clear();
            doRequest(type, "");
        }
    }
}

