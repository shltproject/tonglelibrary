package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * @author fengguowei
 * @ClassName: MStoreDetailsResult
 * @Description: 店铺主页及其中的首页信息 10004
 * @date 2017/1/12 18:13
 */

public class MStoreDetailsResult implements Serializable {
    private BaseInfo head;
    private MStoreDetailsBody body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public MStoreDetailsBody getBody() {
        return body;
    }

    public void setBody(MStoreDetailsBody body) {
        this.body = body;
    }
}
