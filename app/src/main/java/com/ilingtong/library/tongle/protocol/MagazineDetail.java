package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by fengguowei on 2017/1/12.
 * 魔店中杂志详情 10004
 */
public class MagazineDetail implements Serializable{
    private String goods_id;
    private String goods_name;
    private Double goods_price;
    private int goods_point;
    private String spe_mobile_pic_addr;
    private String relation_id;

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public Double getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(Double goods_price) {
        this.goods_price = goods_price;
    }

    public int getGoods_point() {
        return goods_point;
    }

    public void setGoods_point(int goods_point) {
        this.goods_point = goods_point;
    }

    public String getSpe_mobile_pic_addr() {
        return spe_mobile_pic_addr;
    }

    public void setSpe_mobile_pic_addr(String spe_mobile_pic_addr) {
        this.spe_mobile_pic_addr = spe_mobile_pic_addr;
    }

    public String getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(String relation_id) {
        this.relation_id = relation_id;
    }
}
