package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.AvailableMoneyResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * @ClassName: AvailableMoneyActivity
 * @Package: com.ilingtong.library.tongle.activity
 * @Description: 账户使用金额
 * @author: liuting
 * @Date: 2017/9/6 17:38
 */
public class AvailableMoneyActivity extends BaseActivity implements View.OnClickListener {
    private ImageView imgBack;//返回
    private TextView tvTitle;//标题
    private TextView tvAvailable;//可用金额
    private TextView tvAmount;//订单金额
    private EditText edtUse;//使用金额
    private Button btnConfirm;//确认使用
    private Dialog dialogLoading;//加载对话框
    private Double mProdAmount;//订单总金额
    private Double mAvailableMoney;//可用金额
    private static final String PROD_AMOUNT_EXTRA_KEY = "prod_amount";//订单金额
    public static final String USE_MONEY_EXTRA_KEY = "use_money";//使用可用金额

    /**
     * 页面跳转
     *
     * @param activity    Activity
     * @param prod_amount 订单金额
     * @param requestCode 请求码
     */
    public static void launcher(Activity activity, Double prod_amount, int requestCode) {
        Intent intent = new Intent(activity, AvailableMoneyActivity.class);
        intent.putExtra(PROD_AMOUNT_EXTRA_KEY, prod_amount);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_money_layout);
        initView();
    }

    /**
     * 初始化
     */
    private void initView() {
        mProdAmount = getIntent().getExtras().getDouble(PROD_AMOUNT_EXTRA_KEY);
        mAvailableMoney = 0.0;

        imgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        tvTitle = (TextView) findViewById(R.id.top_name);
        tvAvailable = (TextView) findViewById(R.id.available_money_tv_available);
        tvAmount = (TextView) findViewById(R.id.available_money_tv_amount);
        edtUse = (EditText) findViewById(R.id.available_money_edt_use);
        btnConfirm = (Button) findViewById(R.id.available_money_btn_confirm);

        edtUse.addTextChangedListener(useTextWatcher);
        imgBack.setVisibility(View.VISIBLE);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.use_user_available_money));
        imgBack.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);

        dialogLoading = DialogUtils.createLoadingDialog(AvailableMoneyActivity.this);
        dialogLoading.setCancelable(true);
        dialogLoading.show();

        tvAmount.setText(FontUtils.priceFormat(mProdAmount));

        doRequest();
    }

    //账户金额文本框文本变化监听
    private TextWatcher useTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            //判断如果输入的使用金额大于了订单金额或者可用金额，则确认按钮不可用，否则可用
            Double use_money;
            if (!TextUtils.isEmpty(edtUse.getText().toString())) {
                use_money = Double.parseDouble(edtUse.getText().toString());
                if (use_money > mProdAmount || use_money > mAvailableMoney) {
                    btnConfirm.setEnabled(false);
                } else {
                    btnConfirm.setEnabled(true);
                }
            } else {
                btnConfirm.setEnabled(false);
            }
        }
    };

    /**
     * 6036请求获取我的可用账户金额
     */
    private void doRequest() {
        ServiceManager.getAvailableMoney(new Response.Listener<AvailableMoneyResult>() {
            @Override
            public void onResponse(AvailableMoneyResult availableMoneyResult) {
                dialogLoading.dismiss();
                if (TongleAppConst.SUCCESS.equals(availableMoneyResult.getHead().getReturn_flag())) {
                    tvAvailable.setText(FontUtils.priceFormat(availableMoneyResult.body.money_valid));
                    mAvailableMoney = availableMoneyResult.body.money_valid;
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + availableMoneyResult.getHead().getReturn_message());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                dialogLoading.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {//返回
            finish();
        } else if (v.getId() == R.id.available_money_btn_confirm) {//确认使用可用金额
            if (TextUtils.isEmpty(edtUse.getText().toString())) {
                ToastUtils.toastShort(getString(R.string.please_input_account_money));
            } else {
                Intent intent = new Intent();
                intent.putExtra(USE_MONEY_EXTRA_KEY, Double.parseDouble(edtUse.getText().toString()));
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}
