package com.ilingtong.library.tongle.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import com.ilingtong.library.tongle.R;

/**
 * Created by dell on 2015/10/15.
 */
public class Utils {
    private static Toast mToast = null;
    /**
     * 功能：手机号码验证正则表达式
     *
     * @param mobiles
     * @return
     */
    public static boolean isMobileNO(String mobiles) {
        String telRegex = "[1][3578]\\d{9}";
        if (TextUtils.isEmpty(mobiles))
            return false;
        else
            return mobiles.matches(telRegex);
    }

    /**
     * 功能：邮编号码验证正则表达式
     *
     * @param mobiles
     * @return
     */
    public static boolean isPostcode(String mobiles) {
        String telRegex = "[1-9]\\d{5}(?!\\d)";
        if (TextUtils.isEmpty(mobiles))
            return false;
        else
            return mobiles.matches(telRegex);
    }
    /**
     * 功能：身份证号验证（15位和18位）
     *
     * @param mobiles
     * @return
     */
    public static boolean isIdCardcode(String mobiles) {
        String telRegex = "(^\\d{15}$)|(^\\d{17}([0-9]|X)$)";
        if (TextUtils.isEmpty(mobiles))
            return false;
        else
            return mobiles.matches(telRegex);
    }

    /**
     * 功能 ：将string字串格式化成数列  中间加空格
     * @param str 要格式化的字串
     * @param length 表示几个字节一组
     * @return
     */
    public static String formatStringSequence(String str,int length){
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i < str.length() + 1; i++) {
            sb.append(str.charAt(i-1));
            if (i%4==0){
                sb.append("  ");
            }
        }
        return sb.toString();
    }

    /**
     * @param context
     * @param phone   电话号码
     */
    public static void callPhone(Context context,String phone){
        if (!TextUtils.isEmpty(phone)) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
                    + phone));
            context.startActivity(intent);
        }else{
            ToastUtils.toastShort(R.string.store_phone_null_msg);
        }
    }
}
