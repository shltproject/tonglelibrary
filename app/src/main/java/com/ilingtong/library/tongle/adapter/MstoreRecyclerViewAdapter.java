package com.ilingtong.library.tongle.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.ProductListItemData;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by wuqian on 2016/1/11.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class MstoreRecyclerViewAdapter extends RecyclerView.Adapter<MstoreRecyclerViewAdapter.ViewHolder> implements View.OnClickListener {

    private List<ProductListItemData> list;

    public MstoreRecyclerViewAdapter(List<ProductListItemData> list) {
        this.list = list;
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (int) v.getTag());
        }
    }

    //define interface
    public static interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mstore_recycler_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(view);
        view.setOnClickListener(this);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ImageLoader.getInstance().displayImage(list.get(position).prod_thumbnail_pic_url, viewHolder.img_pic, ImageOptionsUtils.getOptions());
        viewHolder.txt_title.setText(list.get(position).prod_name);
        viewHolder.txt_price.setText(FontUtils.priceFormat(Double.parseDouble(list.get(position).prod_price)));
        viewHolder.img_sale.setVisibility(TongleAppConst.YES.equals(list.get(position).coupon_flag) ? View.VISIBLE : View.GONE);
        viewHolder.itemView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_pic;  //商品缩略图
        public TextView txt_title;  //商品标题
        public TextView txt_price;  //商品价格
        public ImageView img_sale;  //团购商品标志

        public ViewHolder(View itemView) {
            super(itemView);
            img_pic = (ImageView) itemView.findViewById(R.id.mstore_recycler_item_img_productpic);
            txt_title = (TextView) itemView.findViewById(R.id.mstore_recycler_item_txt_title);
            txt_price = (TextView) itemView.findViewById(R.id.mstore_recycler_item_txt_price);
            img_sale = (ImageView) itemView.findViewById(R.id.mstore_recycler_item_img_sale);
        }
    }
}
