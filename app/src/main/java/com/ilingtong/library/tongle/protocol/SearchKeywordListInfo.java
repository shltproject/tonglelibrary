package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/2/14
 * Desc:6024接口 body 店铺搜索关键词列表信息类
 */

public class SearchKeywordListInfo implements Serializable{
    private List<SearchKeywordInfo> key_list;//关键词集合

    public List<SearchKeywordInfo> getKey_list() {
        return key_list;
    }

    public void setKey_list(List<SearchKeywordInfo> key_list) {
        this.key_list = key_list;
    }
}
