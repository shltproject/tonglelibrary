package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/18
 * Time: 11:54
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ResetPwdParam implements Serializable {
    public String phone_number;
    public String password;
    public String confirm_password;
    public String verification_code;
}
