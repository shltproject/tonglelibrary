package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/17
 * Time: 11:59
 * Email: jqleng@isoftstone.com
 * Desc: 注册请求需要的参数类
 */
public class RegistInputParam implements Serializable {
    public String phone_number;             //手机号码（必须指定）
    public String password;                 //密码（必须指定）
    public String verification_code;        //验证码（必须指定）
    public String user_nick_name;           //昵称（可为空）
    public String channel_no;               //渠道编码（可为空）
    public String mobile_os_version;        //平台系统版本号（必须指定）
    public String terminal_device;          //终端设备描述名（可为空）
    public String recommend_code;           //推荐码（可为空）
}
