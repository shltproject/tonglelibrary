package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/3/29
 * Time: 16:41
 * Email: liuting@ilingtong.com
 * Desc:6015入口参数类：订单商品
 */
public class MyCouponAvailableItem implements Serializable{
    public String goods_id;//商品编号
    public String spec_id1;//规格ID1
    public String spec_id2;//规格ID2
    public String buy_count;//购买数量
}
