package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/5/18.
 * mail: wuqian@ilingtong.com
 * Description:1086 生成会员动态二维码 接口返回json结构
 */
public class ExpertQRurlResult implements Serializable {
    public BaseInfo head;
    public ExpertQRurlInfo body;
}
