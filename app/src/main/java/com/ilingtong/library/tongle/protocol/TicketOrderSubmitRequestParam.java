package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: liuting
 * Date: 2016/3/17
 * Time: 16:09
 * Email: liuting@ilingtong.com
 * Desc: 提交订单入口参数类
 */
public class TicketOrderSubmitRequestParam extends TicketOrderSubmitParam implements Serializable {
    public String user_id;//用户ID
    public String amount;//总金额
    public String vouchers_number_id;//优惠券ID
    public String order_amount;//订单金额
    public String user_token;//令牌
}
