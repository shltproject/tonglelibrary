package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:6015接口（我的可用优惠券）返回json对应的entity
 *
 */
public class MyCouponAvailableResult extends BaseResult implements Serializable{
    private MyCouponAvailableInfo body;

    public MyCouponAvailableInfo getBody() {
        return body;
    }

    public void setBody(MyCouponAvailableInfo body) {
        this.body = body;
    }
}
