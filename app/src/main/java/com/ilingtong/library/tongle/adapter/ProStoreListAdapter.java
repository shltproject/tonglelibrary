package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.CouponStoreInfo;
import com.ilingtong.library.tongle.utils.Utils;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/17
 * Time: 13:29
 * Email: liuting@ilingtong.com
 * Desc:商户列表Adapter
 */
public class ProStoreListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<CouponStoreInfo> mLvStore;
    private Context mContext;
    private ICallPhoneListener listener;//拨打电话监听

    public interface ICallPhoneListener{
        void callPhone(View view,int position);
    }

    public ProStoreListAdapter(Context context, List<CouponStoreInfo> storeList,ICallPhoneListener listener) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvStore = storeList;
        this.listener=listener;
    }

    @Override
    public int getCount() {
        return mLvStore.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvStore.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.store_info_list_item, null);
            holder = new ViewHolder();
            holder.storeName = (TextView) view.findViewById(R.id.store_info_tv_name);
            holder.storePhone = (TextView) view.findViewById(R.id.store_info_tv_phone);
            holder.storeAddress = (TextView) view.findViewById(R.id.store_info_tv_address);
            holder.rlyPhone = (RelativeLayout) view.findViewById(R.id.store_info_rly_phone);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final CouponStoreInfo mStoreItem = (CouponStoreInfo) getItem(position);
        holder.storeName.setText(mStoreItem.getName());
        holder.storeAddress.setText(mStoreItem.getAddress());
        holder.storePhone.setText(mStoreItem.getPhone());
        //拨打电话
        holder.rlyPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    listener.callPhone(v,position);
                }
//                Utils.callPhone(mContext,mStoreItem.getPhone());
            }
        });

        return view;
    }

    static class ViewHolder {
        TextView storeName;//商户名称
        TextView storePhone;//商户信息电话
        TextView storeAddress;//商户信息地址
        RelativeLayout rlyPhone;//拨打电话
    }
}

