package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/24
 * Time: 11:36
 * Email: jqleng@isoftstone.com
 * Desc: 用户协议类，通过该对象，被用作网络请求参数
 */
public class AgreementResult implements Serializable {
    private BaseInfo head;
    private AgreementInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public AgreementInfo getBody() {
        return body;
    }

    public void setBody(AgreementInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
