package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.SearchKeywordInfo;

import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.adapter
 * author:liuting
 * Date:2017/2/15
 * Desc:搜索关键词Adapter
 */

public class StoreSearchKeywordGridAdapter extends BaseAdapter {
    private List<SearchKeywordInfo> listKeyword;//关键词集合
    private Context context;//上下文
    private LayoutInflater inflater;
    private IOnItemClickListener listener;//事件监听

    public StoreSearchKeywordGridAdapter( Context context, List<SearchKeywordInfo> listKeyword,IOnItemClickListener listener) {
        this.listKeyword = listKeyword;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    public interface IOnItemClickListener{
        void onItemClick(View v,String find_key);//点击事件，传入关键词
    }
    @Override
    public int getCount() {
        return listKeyword.size();
    }

    @Override
    public Object getItem(int position) {
        return listKeyword.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            convertView = inflater.inflate(R.layout.grid_item_store_search_layout,null);
            holder = new ViewHolder();
            holder.tvKeyWord = (TextView) convertView.findViewById(R.id.store_search_tv_keyword);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvKeyWord.setText(listKeyword.get(position).getContent());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onItemClick(v,listKeyword.get(position).getContent());
                }
            }
        });
        return convertView;
    }

    static class ViewHolder{
        TextView tvKeyWord;//分类名称
    }
}
