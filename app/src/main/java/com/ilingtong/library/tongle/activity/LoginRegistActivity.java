package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.CodeBaseResult;
import com.ilingtong.library.tongle.protocol.CodeResult;
import com.ilingtong.library.tongle.protocol.RegistInputParam;
import com.ilingtong.library.tongle.protocol.RegistResult;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;
import com.ilingtong.library.tongle.widget.SelectDialog;

/**
 * User: lengjiqiang
 * Date: 2015/5/4
 * Time: 14:41
 * Email: jqleng@isoftstone.com
 * Desc: 用户立即注册页
 */
public class LoginRegistActivity extends BaseActivity implements View.OnClickListener {
    String TAG = "REGIST_ACTIVITY";
    final int REGIST_SCAN_CODE = 1;

    private EditText phoneNumberEdit;
    private EditText pwdEdit;
    private EditText validateEdit; //验证码
    private Button registBtn;//注册
    private Button getCodeBtn;
    private ImageView textArraw;
    private TextView textTopName;
    private LinearLayout agreementLiner;//用户协议
    private EditText QRCodeEdit;//推荐码
    private CheckBox agreeCheck;
    private Button scan_code_btn;

    private String strPhone; //手机号
    private String userPassword;//密码
    private String validateCode;

    private String[] needPermissions = {Manifest.permission.CAMERA};//相机权限，扫码所需
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    /**
     * 继承Activity的onCreate()方法，实现本类的特殊需求
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_regist_layout);
        initView();
    }

    public void initView() {
        textTopName = (TextView) findViewById(R.id.top_name);
        phoneNumberEdit = (EditText) findViewById(R.id.pone_text);
        pwdEdit = (EditText) findViewById(R.id.pwdText);
        validateEdit = (EditText) findViewById(R.id.vilaCode);
        QRCodeEdit = (EditText) findViewById(R.id.qrcode_edit);
        registBtn = (Button) findViewById(R.id.regist_btn);
        getCodeBtn = (Button) findViewById(R.id.validation_btn);
        scan_code_btn = (Button) findViewById(R.id.scan_code_btn);
        textArraw = (ImageView) findViewById(R.id.left_arrow_btn);
        agreementLiner = (LinearLayout) findViewById(R.id.agreement_liner);
        agreeCheck = (CheckBox) findViewById(R.id.agree_check);

        phoneNumberEdit.setOnClickListener(this);
        pwdEdit.setOnClickListener(this);
        registBtn.setOnClickListener(this);
        registBtn.setEnabled(false);
        getCodeBtn.setOnClickListener(this);
        getCodeBtn.setEnabled(false);
        scan_code_btn.setOnClickListener(this);
        textArraw.setOnClickListener(this);
        agreementLiner.setOnClickListener(this);
        agreeCheck.setOnClickListener(this);

        textArraw.setVisibility(View.VISIBLE);
        textTopName.setText(getString(R.string.regist));
        textTopName.setVisibility(View.VISIBLE);
        //做手机号的改变监听，判断底部注册按钮的背景
        phoneNumberEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strPhone = phoneNumberEdit.getText().toString();
                userPassword = pwdEdit.getText().toString();
                validateCode = validateEdit.getText().toString();

                if (strPhone.equals("") || userPassword.equals("") || validateCode.equals("")) {
                    registBtn.setEnabled(false);
//                    registBtn.setBackgroundResource(R.drawable.button_forbid_style);
                } else {
                    registBtn.setEnabled(true);
//                    registBtn.setBackgroundResource(R.drawable.button_style);
                }

                if (strPhone.equals("")) {
                    getCodeBtn.setEnabled(false);
//                    getCodeBtn.setBackgroundResource(R.drawable.button_forbid_style);
                } else {
                    getCodeBtn.setEnabled(true);
//                    getCodeBtn.setBackgroundResource(R.drawable.button_style);
                }
            }
        });
        //做密码的改变监听，判断底部注册按钮的背景
        pwdEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strPhone = phoneNumberEdit.getText().toString();
                userPassword = pwdEdit.getText().toString();
                validateCode = validateEdit.getText().toString();
                //按钮密码找回，背景图片的操作
                if (strPhone.equals("") || userPassword.equals("") || validateCode.equals("")) {
                    registBtn.setEnabled(false);
//                    registBtn.setBackgroundResource(R.drawable.button_forbid_style);
                } else {
                    registBtn.setEnabled(true);
//                    registBtn.setBackgroundResource(R.drawable.button_style);
                }
            }
        });
        //做验证码的改变监听，判断底部注册按钮的背景
        validateEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strPhone = phoneNumberEdit.getText().toString();
                userPassword = pwdEdit.getText().toString();
                validateCode = validateEdit.getText().toString();
                //按钮密码找回，背景图片的操作
                if (strPhone.equals("") || userPassword.equals("") || validateCode.equals("")) {
                    registBtn.setEnabled(false);
//                    registBtn.setBackgroundResource(R.drawable.button_forbid_style);
                } else {
                    registBtn.setEnabled(true);
//                    registBtn.setBackgroundResource(R.drawable.button_style);
                }
            }
        });
    }

    /**
     * 该页中的所有点击事件处理方法
     */
    @Override
    public void onClick(View v) {
        Intent intent;
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.validation_btn) {// 获取验证码 按钮
            strPhone = phoneNumberEdit.getText().toString();
            if ("".equals(strPhone)) {
                ToastUtils.toastShort(getString(R.string.regist_phone_error));
                return;
            }
            if (!Utils.isMobileNO(strPhone) || (strPhone.length() != TongleAppConst.PHONE_NUMBER_LENGTH)) {
                ToastUtils.toastShort(getString(R.string.regist_phone_error));
                return;
            }
            ServiceManager.doCodeRequest(strPhone, codeListener(), errorListener());
        } else if (v.getId() == R.id.agreement_liner) {// 用户协议
            intent = new Intent(this, SettingAgreementActivity.class);
            startActivityForResult(intent, TongleAppConst.AGREEMENT_RESULT);
        } else if (v.getId() == R.id.scan_code_btn) {//扫描推荐码
            requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(LoginRegistActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                        @Override
                        public void requestSuccess() {
                            Intent intent = new Intent(LoginRegistActivity.this, CaptureActivity.class);
                            intent.setAction(Intents.Scan.ACTION);
                            //不传入指定的扫码类型，默认是同时支持一维码、二维码等编码的扫描
//                            intent.putExtra(Intents.Scan.MODE, Intents.Scan.QR_CODE_MODE);
                            intent.putExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS, 0L);
                            intent.putExtra("TopName", getString(R.string.regist));
                            startActivityForResult(intent, REGIST_SCAN_CODE);
                        }

                        @Override
                        public void requestFail() {
                            SelectDialog dialog = new SelectDialog(LoginRegistActivity.this, getString(R.string.please_open_camera_permission));
                            dialog.show();
                        }
                    });
            requestPermissionUtils.checkPermissions(LoginRegistActivity.this);

        } else if (v.getId() == R.id.regist_btn) {//注册按钮
            strPhone = phoneNumberEdit.getText().toString();
            userPassword = pwdEdit.getText().toString();
            validateCode = validateEdit.getText().toString();

            //手机号验证
            if ((!Utils.isMobileNO(strPhone) || (strPhone.length() != TongleAppConst.PHONE_NUMBER_LENGTH))) {
                ToastUtils.toastShort(getString(R.string.regist_phone_error));
                return;
            }

            //验证码长度验证
            if (validateCode.length() != TongleAppConst.CODE_LENGTH) {
                ToastUtils.toastShort(getString(R.string.regist_code_error));
                return;
            }
            //密码长度验证
            if (userPassword.length() < TongleAppConst.PASSWORD_LENGTH) {
                ToastUtils.toastShort(getString(R.string.regist_password_error));
                return;
            }
            //没有接受用户协议
            if (agreeCheck.isChecked() == false) {
                ToastUtils.toastShort(getString(R.string.regist_agreement_nocheck));
                return;
            }

            if (!"".equals(strPhone) && !"".equals(userPassword) && !"".equals(validateCode)) {
                RegistInputParam param = new RegistInputParam();
                param.phone_number = strPhone;
                param.password = userPassword;
                param.verification_code = validateCode;
                param.user_nick_name = "";
                param.channel_no = "";
                param.mobile_os_version = TongleAppConst.ANDROID_PHONE;
                param.terminal_device = "";
                param.recommend_code = QRCodeEdit.getText().toString();

                ServiceManager.doRegistRequest(param, registListener(), errorListener());
            } else {
                ToastUtils.toastShort(getString(R.string.register_txt_null));
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REGIST_SCAN_CODE == requestCode) {
            if (resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                String qrcode = b.getString("SCAN_RESULT");
                if (qrcode != null) {
                    ServiceManager.scanUserDecode(qrcode, CodeBaseRequest(), errorListener());
                    Log.e(TAG, qrcode);
                } else {
                    Log.e(TAG, getString(R.string.register_get_qrcode_error));
                }
            }
        } else if (TongleAppConst.AGREEMENT_RESULT == requestCode) {
            agreeCheck.setChecked(true);
        }
    }

    //计时器，计算多少秒后重新获取验证码
    private CountDownTimer timer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            getCodeBtn.setEnabled(false);
            getCodeBtn.setText((millisUntilFinished / 1000) + getString(R.string.common_validat_btn_txt_before));
        }

        @Override
        public void onFinish() {
            getCodeBtn.setEnabled(true);
            getCodeBtn.setText(getString(R.string.common_validat_btn_txt_after));
        }
    };

    /**
     * 功能：获取验证码网络响应成功，返回数据
     */
    private Response.Listener<CodeResult> codeListener() {
        return new Response.Listener<CodeResult>() {
            @Override
            public void onResponse(CodeResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.common_get_code_success));
                } else {
                    ToastUtils.toastLong(getString(R.string.common_get_code_error) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：注册网络响应成功，返回数据
     */
    private Response.Listener<RegistResult> registListener() {
        return new Response.Listener<RegistResult>() {
            @Override
            public void onResponse(RegistResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.resign_success));
                    finish();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：会员个人二维码解析响应成功，返回数据
     */
    private Response.Listener<CodeBaseResult> CodeBaseRequest() {
        return new Response.Listener<CodeBaseResult>() {
            @Override
            public void onResponse(CodeBaseResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    QRCodeEdit.setText(response.getBody().getUser_data().phone);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
