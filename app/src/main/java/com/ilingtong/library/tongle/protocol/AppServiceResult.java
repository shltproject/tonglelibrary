package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/23
 * Time: 11:21
 * Email: jqleng@isoftstone.com
 * Desc: 服务管家对象
 */
public class AppServiceResult implements Serializable{
    private BaseInfo head;
    private AppServiceInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public AppServiceInfo getBody() {
        return body;
    }

    public void setBody(AppServiceInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
