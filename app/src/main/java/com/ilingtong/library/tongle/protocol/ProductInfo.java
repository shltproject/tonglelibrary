package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;

/**
 * User: lengjiqiang
 * Date: 2015/5/21
 * Time: 17:01
 * Email: jqleng@isoftstone.com
 * Dest: [1022] 功能点返回参数类型
 */
public class ProductInfo implements Serializable {
    public String prod_id;
    public String prod_name;
    public String prod_resume;
    public ArrayList<ProdPicUrlListItem> prod_pic_url_list;
    public String prod_status;
    public String mstore_id;
    public String mstore_name;
    public String price;
    public String stock_qty;
    public ArrayList<ProductSpecListItem> prod_spec_list;
    public String good_rating_percent;
    public String points_rule;
    public String prod_favorited_by_me;
    public String trade_prod_favorited_by_me;
    public String relation_id;
    public String country_pic_url;
    public String import_info_desc;
    public String transfer_fee_desc;
    public String tariff_desc;
    public String import_goods_flag;
    public String sender;
    public String goods_return_info;
    public String point_rule_url;
    public String point_rule_title;
    public ArrayList<ProductStockListItem> stock_list;//库存列表
    public String price_interval;
    public String promotional_price;  //add on 2017/7/21 商品原价
    public String wx_profile;  //微信简介 add at 2016/3/21
    public String overseas_flg;    //是否是海淘商品的标志  add on 2017/7/19

    @Override
    public String toString() {
        return "ProductInfo{" +
                "prod_id='" + prod_id + '\'' +
                ", prod_name='" + prod_name + '\'' +
                ", prod_resume='" + prod_resume + '\'' +
                ", prod_pic_url_list=" + prod_pic_url_list +
                ", prod_status='" + prod_status + '\'' +
                ", mstore_id='" + mstore_id + '\'' +
                ", mstore_name='" + mstore_name + '\'' +
                ", price='" + price + '\'' +
                ", stock_qty='" + stock_qty + '\'' +
                ", prod_spec_list=" + prod_spec_list +
                ", good_rating_percent='" + good_rating_percent + '\'' +
                ", points_rule='" + points_rule + '\'' +
                ", prod_favorited_by_me='" + prod_favorited_by_me + '\'' +
                ", trade_prod_favorited_by_me='" + trade_prod_favorited_by_me + '\'' +
                ", relation_id='" + relation_id + '\'' +
                ", country_pic_url='" + country_pic_url + '\'' +
                ", import_info_desc='" + import_info_desc + '\'' +
                ", transfer_fee_desc='" + transfer_fee_desc + '\'' +
                ", tariff_desc='" + tariff_desc + '\'' +
                ", import_goods_flag='" + import_goods_flag + '\'' +
                ", sender='" + sender + '\'' +
                ", goods_return_info='" + goods_return_info + '\'' +
                ", point_rule_url='" + point_rule_url + '\'' +
                ", point_rule_title='" + point_rule_title + '\'' +
                ", stock_list='" + stock_list + '\'' +
                ", price_interval='" + price_interval + '\'' +
                '}';
    }
}
