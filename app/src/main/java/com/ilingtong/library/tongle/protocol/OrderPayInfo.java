package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:订单支付情报 pay_type      1：支付宝 3：微信 4：网银
 *                          pay_status    0：未支付   1：已支付
 * use by 6012
 */
public class OrderPayInfo implements Serializable {
    private String pay_type;    //支付方式
    private String pay_status;    //支付状态

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getPay_status() {
        return pay_status;
    }

    public void setPay_status(String pay_status) {
        this.pay_status = pay_status;
    }
}
