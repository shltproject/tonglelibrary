package com.ilingtong.library.tongle.fragment;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.OrderConfirmActivity;
import com.ilingtong.library.tongle.adapter.CartListAdaper;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.model.CartModel;
import com.ilingtong.library.tongle.protocol.CartResult;
import com.ilingtong.library.tongle.protocol.CartSettlementResult;
import com.ilingtong.library.tongle.protocol.DeleteInCartResult;
import com.ilingtong.library.tongle.protocol.ProductListInfo;
import com.ilingtong.library.tongle.protocol.ShopCartListItem;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/4/24
 * Time: 16:38
 * Email: jqleng@isoftstone.com
 * Desc: 购物车属性页
 */
public class CartFragment extends BaseFragment implements XListView.IXListViewListener, View.OnClickListener {
    private XListView mCartList;
    private CheckBox selectAllCheck;
    private TextView totalPriceTxt;
    private Button checkoutBtn;
    //  private ImageButton deleteBtn;
    private Button btn_delete;  //删除按钮
    private Button btn_comfirm; //确定按钮
    private RelativeLayout rl_replace;
    private TextView tv_replace;
    private TextView txt_edit; //右上角编辑按钮
    private LinearLayout rl_edit; //编辑模式。显示“确认”“删除”按钮一栏
    private LinearLayout rl_checkout; //查看模式。显示结算按钮

    private CartModel cartModel;
    private CartListAdaper mCartListAdaper;
    public static float totalPrice;//价格
    public static int checkNum; // 记录选中的条目数量
    public static CartFragment instance = null;
    public static boolean CARTFRAGMENT_UPDATE_FLAG = true; //购物车刷新标志

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (cartModel == null)
            cartModel = new CartModel();
        View rootView = inflater.inflate(R.layout.fragment_cart_layout, null);
        CARTFRAGMENT_UPDATE_FLAG = true;
        instance = this;
        totalPrice = 0;
        initView(rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void initView(View rootView) {
        mCartList = (XListView) rootView.findViewById(R.id.cart_listview);
        selectAllCheck = (CheckBox) rootView.findViewById(R.id.select_all_checkbox);
        totalPriceTxt = (TextView) rootView.findViewById(R.id.total_price);
        checkoutBtn = (Button) rootView.findViewById(R.id.checkout_btn);
        //deleteBtn = (ImageButton) rootView.findViewById(R.id.cart_delete);
        btn_delete = (Button) rootView.findViewById(R.id.cart_btn_delete);
        btn_comfirm = (Button) rootView.findViewById(R.id.cart_btn_comfirm);
        rl_replace = (RelativeLayout) rootView.findViewById(R.id.rl_replace);
        tv_replace = (TextView) rootView.findViewById(R.id.tv_replace);
        txt_edit = (TextView) rootView.findViewById(R.id.cart_txt_edit);
        rl_checkout = (LinearLayout) rootView.findViewById(R.id.cart_rl_checkout);
        rl_edit = (LinearLayout) rootView.findViewById(R.id.cart_rl_edit);

        selectAllCheck.setOnClickListener(this);
        checkoutBtn.setOnClickListener(this);
        //deleteBtn.setOnClickListener(this);
        btn_delete.setOnClickListener(this);
        btn_comfirm.setOnClickListener(this);

        txt_edit.setOnClickListener(this);
        txt_edit.setVisibility(View.GONE);

//        checkoutBtn.setBackgroundResource(R.drawable.button_forbid_style);
        checkoutBtn.setEnabled(false);
        if ((mCartList != null) && (mCartList.getCount() != 0)) {
            selectAllCheck.setOnCheckedChangeListener(new SelectAllListener());
        }
    }

    /**
     * 显示页面时刷新购物车
     *
     * @param hidden
     */
    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden && CARTFRAGMENT_UPDATE_FLAG) {
            doRequest();
        }
        super.onHiddenChanged(hidden);
    }

    /**
     * 去结算后返回，刷新购物车
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10010 && CARTFRAGMENT_UPDATE_FLAG) {
            doRequest();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 刷新购物车数据
     */
    public void doRequest() {
        CARTFRAGMENT_UPDATE_FLAG = false;
        ServiceManager.doCartRequest(TongleAppInstance.getInstance().getUserID(), successListener(), cartRequestErrorListener());
    }

    @Override
    public void onRefresh(int id) {
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {

    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<CartResult> successListener() {
        return new Response.Listener<CartResult>() {
            @Override
            public void onResponse(CartResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    cartModel.my_shopping_cart = response.getBody().getMy_shopping_cart();
                    selectAllCheck.setOnCheckedChangeListener(new SelectAllListener());
                    updateListView();
                    //默认全选
                    if (cartModel.my_shopping_cart.size() > 0) {
                        selectAllCheck.setChecked(false);
                        selectAllCheck.setChecked(true);
                        txt_edit.setVisibility(View.VISIBLE);  //购物车存在商品时显示编辑按钮
                    } else {
                        selectAllCheck.setChecked(false);
                        setViewIfnoData();
                    }
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + response.getHead().getReturn_message());
                    setViewIfnoData();

                }
            }
        };
    }

    /**
     * 购物车中没有商品时，隐藏编辑按钮，隐藏编辑模式，显示结算模式。下拉刷新可用
     */
    private void setViewIfnoData() {
        txt_edit.setText(getString(R.string.fragment_cart_edit));
        txt_edit.setVisibility(View.GONE);
        rl_checkout.setVisibility(View.VISIBLE);
        rl_edit.setVisibility(View.GONE);
        mCartList.setPullRefreshEnable(true);
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        };
    }

    /**
     * 功能：购物车查询网络响应失败
     */
    private Response.ErrorListener cartRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
                txt_edit.setVisibility(View.GONE);  //购物车不存在商品时隐藏编辑按钮
            }
        };
    }

    public void updateListView() {
        mCartListAdaper = new CartListAdaper(getActivity(), handler, cartModel.my_shopping_cart, this);
        mCartList.setPullLoadEnable(false);
        mCartList.setPullRefreshEnable(true);
        mCartList.setXListViewListener(this, 0);
        mCartList.setRefreshTime();
        mCartList.setAdapter(mCartListAdaper);
    }

    private android.os.Handler handler;

    {
        handler = new android.os.Handler(new android.os.Handler.Callback() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)

            public boolean handleMessage(Message msg) {

                //显示总金额
                float fPrice = (float) msg.obj;
//                totalPriceTxt.setText(fPrice + "");
                totalPriceTxt.setText(FontUtils.priceFormat(fPrice));
                checkoutBtn.setText(String.format(getString(R.string.fragment_cart_checkout_txt),checkNum));

                if (msg.what == TongleAppConst.UPDATE_TOTALPRICE) {
                    //结算按钮判断
                    if (fPrice != 0) {
                        checkoutBtn.setEnabled(true);
//                        checkoutBtn.setBackgroundResource(R.drawable.button_style);
                        //设置全选box为勾选状态
                        if (CartModel.checkList.size() == cartModel.my_shopping_cart.size())
                            selectAllCheck.setChecked(true);
                        else
                            selectAllCheck.setChecked(false);
                    } else {
                        selectAllCheck.setChecked(false);
                        checkoutBtn.setEnabled(false);
//                        checkoutBtn.setBackgroundResource(R.drawable.button_forbid_style);
                    }
                } else if (msg.what == TongleAppConst.SELECT_UPDATE_TOTALPRICE) {
                    //结算按钮判断
                    if (fPrice != 0) {
                        checkoutBtn.setEnabled(true);
//                        checkoutBtn.setBackgroundResource(R.drawable.button_style);
                    } else {
                        checkoutBtn.setEnabled(false);
//                        checkoutBtn.setBackgroundResource(R.drawable.button_forbid_style);
                    }
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.checkout_btn) {   //结算按钮点击事件
            if (CartModel.checkList.size() != 0) {
                ArrayList obj = new ArrayList();
                for (int i = 0; i < CartModel.checkList.size(); i++) {
                    obj.add(CartModel.checkList.get(i).seq_no);
                }
                ServiceManager.addCartRequest(TongleAppInstance.getInstance().getUserID(), obj, CartSuccessListener(), errorListener());
            }
        } else if (v.getId() == R.id.cart_btn_delete) {  //编辑模式下删除按钮点击事件
            if (CartModel.checkList.size() != 0) {
                ArrayList<ProductListInfo> deleteList = new ArrayList<>();
                for (int i = 0; i < CartModel.checkList.size(); i++) {
                    ShopCartListItem cartItem = CartModel.checkList.get(i);
                    ProductListInfo item = new ProductListInfo();
                    item.seq_no = cartItem.seq_no;
                    item.product_id = cartItem.prod_id;
                    deleteList.add(item);
                }
                ServiceManager.doDeleteInCartRequest(TongleAppInstance.getInstance().getUserID(), deleteList, delSuccessListener(), errorListener());
            }
        } else if (v.getId() == R.id.cart_txt_edit) {  //右上角编辑/完成按钮事件
            if (txt_edit.getText().toString().equals(getString(R.string.fragment_cart_edit))) {
                rl_checkout.setVisibility(View.GONE);
                rl_edit.setVisibility(View.VISIBLE);
                txt_edit.setText(getString(R.string.fragment_cart_complete));
                mCartList.setPullRefreshEnable(false);
                mCartListAdaper.setType(CartListAdaper.TYPE_EDTT);
                mCartListAdaper.notifyDataSetChanged();
            } else if (txt_edit.getText().toString().equals(getString(R.string.fragment_cart_complete))) {
                setViewbyComplete();
            }
        } else if (v.getId() == R.id.cart_btn_comfirm) {  //编辑模式下 底部确定按钮事件
            setViewbyComplete();
        }
    }

    /**
     * 编辑模式下 确定/完成 按钮事件 （注：右上角“完成”事件和底部 确定按钮点击事件相同）
     */
    private void setViewbyComplete() {
        rl_checkout.setVisibility(View.VISIBLE);
        rl_edit.setVisibility(View.GONE);
        txt_edit.setText(getString(R.string.fragment_cart_edit));
        mCartList.setPullRefreshEnable(true);
        mCartListAdaper.setType(CartListAdaper.TYPE_CHECKOUT);
        mCartListAdaper.notifyDataSetChanged();
        selectAllCheck.setChecked(false);
        selectAllCheck.setChecked(true);
    }

    private class SelectAllListener implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ShopCartListItem cartDataItem;
            if (isChecked) {
                checkoutBtn.setEnabled(true);
//                checkoutBtn.setBackgroundResource(R.drawable.button_style);
                if (CartModel.checkList.size() == cartModel.my_shopping_cart.size())
                    return;
                CartModel.checkList.removeAll(CartModel.checkList);
                totalPrice = 0;
                checkNum = 0;
                // i = 0被认为是listview 的headview，所以从1开始, getChildCount()-1是减去footview
                for (int i = 0; i < cartModel.my_shopping_cart.size(); i++) {
                    // 遍历list的长度，将MyAdapter中的map值全部设为true
                    CartListAdaper.getIsSelected().put(i, true);
                    cartDataItem = (ShopCartListItem) mCartListAdaper.getItem(i);
                    CartModel.checkList.add(cartDataItem);

                    BigDecimal b1 = new BigDecimal(Float.toString(totalPrice));
                    BigDecimal b2 = new BigDecimal(Float.toString(Float.parseFloat(cartDataItem.price)));
                    BigDecimal b3 = new BigDecimal(Float.toString(Float.parseFloat(cartDataItem.order_qty)));
                    totalPrice = b1.add(b2.multiply(b3)).floatValue();

                    checkNum = checkNum + Integer.parseInt(cartDataItem.order_qty);
                }
                // 刷新listview和TextView的显示
                mCartListAdaper.notifyDataSetChanged();

                checkoutBtn.setText(String.format(getString(R.string.fragment_cart_checkout_txt),checkNum));
            } else {
                //设置全不选,mCartList - 2是因为减去head,foot项
                if (CartModel.checkList.size() < cartModel.my_shopping_cart.size())
                    return;

                if (CartModel.checkList.size() == 0) {
                    checkNum = 0;
                    totalPrice = 0;

                } else {
                    checkoutBtn.setEnabled(false);
//                    checkoutBtn.setBackgroundResource(R.drawable.button_forbid_style);
                    for (int i = 0; i < cartModel.my_shopping_cart.size(); i++) {
                        if (CartListAdaper.getIsSelected().get(i)) {
                            CartListAdaper.getIsSelected().put(i, false);

                            //更新选定列表
                            cartDataItem = (ShopCartListItem) mCartListAdaper.getItem(i);
                            CartModel.checkList.remove(cartDataItem);

                            BigDecimal b1 = new BigDecimal(Float.toString(totalPrice));
                            BigDecimal b2 = new BigDecimal(Float.toString(Float.parseFloat(cartDataItem.price)));
                            BigDecimal b3 = new BigDecimal(Float.toString(Float.parseFloat(cartDataItem.order_qty)));
                            totalPrice = b1.subtract(b2.multiply(b3)).floatValue();//更新总金额
                            checkNum = checkNum - Integer.parseInt(cartDataItem.order_qty);
                            //checkNum--;// 数量减1
                        }
                        mCartListAdaper.notifyDataSetChanged();
                    }
                }
            }
            checkoutBtn.setText(String.format(getString(R.string.fragment_cart_checkout_txt),checkNum));
            Message message = handler.obtainMessage();
            message.what = TongleAppConst.SELECT_UPDATE_TOTALPRICE;
            message.obj = totalPrice;
            handler.sendMessage(message);
        }
    }

    /**
     * 功能：删除购物车中的商品网络响应成功，返回数据
     */
    private Response.Listener<DeleteInCartResult> delSuccessListener() {
        return new Response.Listener<DeleteInCartResult>() {
            @Override
            public void onResponse(DeleteInCartResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.common_order_delete_success));
                    //重新加载数据，刷新界面
                    ServiceManager.doCartRequest(TongleAppInstance.getInstance().getUserID(), successListener(), errorListener());
                } else {
                    ToastUtils.toastLong(getString(R.string.sys_exception));
                }
            }
        };
    }

    /**
     * 功能：购物车结算网络响应成功，返回数据
     */
    private Response.Listener<CartSettlementResult> CartSuccessListener() {
        return new Response.Listener<CartSettlementResult>() {
            @Override
            public void onResponse(CartSettlementResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    OrderConfirmActivity.launcher(CartFragment.this,response,OrderConfirmActivity.TYPE_CART,10010);
                } else {
                    ToastUtils.toastLong(response.getHead().getReturn_message());
                }
            }
        };
    }

}