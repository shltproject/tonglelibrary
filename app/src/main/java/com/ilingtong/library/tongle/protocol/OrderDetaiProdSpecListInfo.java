package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class OrderDetaiProdSpecListInfo implements Serializable {
    public String prod_spec_id;
    public String prod_spec_name;
    public String spec_detail_id;
    public String spec_detail_name;

    @Override
    public String toString() {
        return "prod_spec_id:" + prod_spec_id + "\r\n" +
                "prod_spec_name:" + prod_spec_name + "\r\n" +
                "spec_detail_id:" + spec_detail_id + "\r\n" +
                "spec_detail_name:" + spec_detail_name;
    }
}
