package com.ilingtong.library.tongle.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.external.myswitchbutton.SwitchButton;

/**
 * User: lengjiqiang
 * Date: 2015/6/18
 * Time: 16:42
 * Email: jqleng@isoftstone.com
 * Desc: 我的->我的设置 页
 */
public class SettingActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;
    private ImageView left_arrow_btn;
    private LinearLayout aboutLiner;
    private LinearLayout addrManagerLiner;//收货地址管理
    private LinearLayout introLiner;
    private LinearLayout modifyPwdLiner;
    private Button exitBtn;//退出应用
    private SwitchButton ifRember;
    private SwitchButton ifLogin;
    public static SettingActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_layout);
        instance = this;
        initView();
    }
    public void initView(){
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        aboutLiner = (LinearLayout) findViewById(R.id.about_liner);
        addrManagerLiner = (LinearLayout) findViewById(R.id.delivery_liner);
        introLiner = (LinearLayout) findViewById(R.id.intro_liner);
        modifyPwdLiner = (LinearLayout) findViewById(R.id.modify_pwd_liner);
        exitBtn = (Button) findViewById(R.id.exit_btn);
        ifRember = (SwitchButton) findViewById(R.id.my_switch_button1);
        ifLogin = (SwitchButton) findViewById(R.id.my_switch_button);

        left_arrow_btn.setOnClickListener(this);
        aboutLiner.setOnClickListener(this);
        addrManagerLiner.setOnClickListener(this);
        introLiner.setOnClickListener(this);
        modifyPwdLiner.setOnClickListener(this);
        exitBtn.setOnClickListener(this);
        ifRember.setOnClickListener(this);
        ifLogin.setOnClickListener(this);

        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(R.string.setting);

        //设置监听是否保存用户名与密码.

        ifRember = (SwitchButton) findViewById(R.id.my_switch_button1);
        SharedPreferences sp = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        ifRember.setChecked(sp.getBoolean(TongleAppInstance.getInstance().getIfRemberKeyName(),false));
        ifLogin.setChecked(sp.getBoolean(TongleAppInstance.getInstance().getIfAutoLoginKeyName(),false));

        ifRember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SharedPreferences sp = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
                    sp.edit().putBoolean(TongleAppInstance.getInstance().getIfRemberKeyName(),true).commit();
                    ifRember.setChecked(true);
                    //选中
                }else{
                    SharedPreferences sp = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
                    sp.edit().putBoolean(TongleAppInstance.getInstance().getIfRemberKeyName(),false).commit();
                    ifRember.setChecked(false);
                }
            }
        });
        ifLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences sp = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
                    sp.edit().putBoolean(TongleAppInstance.getInstance().getIfAutoLoginKeyName(),true).commit();
                    ifLogin.setChecked(true);
                    ifRember.setChecked(true);
                } else {
                    SharedPreferences sp = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
                    sp.edit().putBoolean(TongleAppInstance.getInstance().getIfAutoLoginKeyName(),false).commit();
                    ifLogin.setChecked(false);
                }
            }
        });
    }
    public void cleanData(){
        TongleAppInstance.getInstance().setToken("");
        TongleAppInstance.getInstance().setUser_phone("");
        TongleAppInstance.getInstance().setUser_photo_url("");
        TongleAppInstance.getInstance().setUser_qr_code_url("");
        TongleAppInstance.getInstance().setUser_account_balance("");
        TongleAppInstance.getInstance().setUser_coupons("");
        TongleAppInstance.getInstance().setUser_fans("");
        TongleAppInstance.getInstance().setUser_follows("");
        TongleAppInstance.getInstance().setUser_name("");
        TongleAppInstance.getInstance().setUser_nick_name("");
        TongleAppInstance.getInstance().setUser_rebate_point("");
        TongleAppInstance.getInstance().setUser_sales_point("");
        TongleAppInstance.getInstance().setUserID("");
        TongleAppInstance.getInstance().setUser_id_no("");
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        if (v.getId() == R.id.left_arrow_btn){
            finish();
        }else if (v.getId() == R.id.modify_pwd_liner){//修改密码
            intent = new Intent(SettingActivity.this, LoginResetPwdSecActivity.class);
            startActivity(intent);
        }else if(v.getId() == R.id.delivery_liner){ //收货地址管理
//            intent = new Intent(SettingActivity.this, AddressManageActivity.class);
//            startActivity(intent);
            AddressManageActivity.launcher(SettingActivity.this, TongleAppConst.TYPE_SETTING);
        }else if (v.getId() == R.id.intro_liner){//功能介绍
            intent = new Intent(SettingActivity.this, SettingIntroActivity.class);
            startActivity(intent);
        }else if (v.getId() == R.id.about_liner){//关于通乐
            intent = new Intent(SettingActivity.this, SettingAboutActivity.class);
            startActivity(intent);
        }
        else if (v.getId() ==  R.id.exit_btn){//退出应用
            SharedPreferences sp = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
            sp.edit().putString(TongleAppInstance.getInstance().getLoginUseridKeyName(), TongleAppInstance.getInstance().getUser_phone()).commit();
            sp.edit().putString(TongleAppInstance.getInstance().getLoginPwdKeyName(), "").commit();

            intent = new Intent(SettingActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(TongleAppInstance.getInstance().getIfAutoLoginKeyName(),false);
            startActivity(intent);
            cleanData();
            finish();
            MainActivity.instance.finish();
        }
    }
}
