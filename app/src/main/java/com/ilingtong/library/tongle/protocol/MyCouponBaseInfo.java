package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:优惠券基本信息
 * use by 6014 6015
 */
public class MyCouponBaseInfo implements Serializable {
    private String vouchers_number_id;  //抵用券券号
    private String vouchers_name;   //抵用券名称
    private String issue_flg;    //发行区分
    private String issue_store_name;   //发行店铺名
    private double money;   //金额
    private double use_conditions;   //使用条件
    private String use_conditions_memo;   //使用条件说明
    private String expiration_date_begin;    //有效期开始日
    private String expiration_date_end;    //有效期结束日
    private String vouchers_status;   //抵用券状态  1-未领取  2-已领取  3-已使用   4-已过期

    public String getVouchers_number_id() {
        return vouchers_number_id;
    }

    public void setVouchers_number_id(String vouchers_number_id) {
        this.vouchers_number_id = vouchers_number_id;
    }

    public String getVouchers_name() {
        return vouchers_name;
    }

    public void setVouchers_name(String vouchers_name) {
        this.vouchers_name = vouchers_name;
    }

    public String getIssue_flg() {
        return issue_flg;
    }

    public void setIssue_flg(String issue_flg) {
        this.issue_flg = issue_flg;
    }

    public String getIssue_store_name() {
        return issue_store_name;
    }

    public void setIssue_store_name(String issue_store_name) {
        this.issue_store_name = issue_store_name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getUse_conditions() {
        return use_conditions;
    }

    public void setUse_conditions(double use_conditions) {
        this.use_conditions = use_conditions;
    }

    public String getUse_conditions_memo() {
        return use_conditions_memo;
    }

    public void setUse_conditions_memo(String use_conditions_memo) {
        this.use_conditions_memo = use_conditions_memo;
    }

    public String getExpiration_date_begin() {
        return expiration_date_begin;
    }

    public void setExpiration_date_begin(String expiration_date_begin) {
        this.expiration_date_begin = expiration_date_begin;
    }

    public String getExpiration_date_end() {
        return expiration_date_end;
    }

    public void setExpiration_date_end(String expiration_date_end) {
        this.expiration_date_end = expiration_date_end;
    }

    public String getVouchers_status() {
        return vouchers_status;
    }

    public void setVouchers_status(String vouchers_status) {
        this.vouchers_status = vouchers_status;
    }
}
