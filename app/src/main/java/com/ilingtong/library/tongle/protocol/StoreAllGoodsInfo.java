package com.ilingtong.library.tongle.protocol;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/1/11
 * Desc:10005接口 body 魔店所有商品信息
 */

public class StoreAllGoodsInfo {
    private StoreGoodsListInfo store_all_goods_info;//魔店全部商品

    public StoreGoodsListInfo getStore_all_goods_info() {
        return store_all_goods_info;
    }

    public void setStore_all_goods_info(StoreGoodsListInfo store_all_goods_info) {
        this.store_all_goods_info = store_all_goods_info;
    }
}
