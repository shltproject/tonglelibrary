package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * @ClassName: uploadIdCardInfo
 * @Package: com.ilingtong.library.tongle.protocol
 * @Description: 6028接口 body 身份证审核信息类
 * @author: liuting
 * @Date: 2017/6/12 15:11
 */

public class UploadIdCardInfo implements Serializable{
    private String apply_status;//审核状态

    public String getApply_status() {
        return apply_status;
    }

    public void setApply_status(String apply_status) {
        this.apply_status = apply_status;
    }
}
