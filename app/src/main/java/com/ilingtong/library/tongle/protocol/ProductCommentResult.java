package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/26
 * Time: 14:16
 * Email: leishuai@isoftstone.com
 * Desc: 产品评价
 */
public class ProductCommentResult implements Serializable {
    private BaseInfo head;
    private ProdCommentListInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ProdCommentListInfo getBody() {
        return body;
    }

    public void setBody(ProdCommentListInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
