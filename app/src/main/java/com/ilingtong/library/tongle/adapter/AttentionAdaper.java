package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.FriendListItem;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * User: shuailei
 * Date: 2015/6/25
 * Time: 16:13
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class AttentionAdaper extends BaseAdapter {
    private LayoutInflater inflater;
    private List<FriendListItem> list;
    FriendListItem expertDataItem = new FriendListItem();
    private Context mContext;

    public AttentionAdaper(Context context, ArrayList list) {
        mContext = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();

            holder.expertIcon = (ImageView) view.findViewById(R.id.expert_icon);
            holder.expertName = (TextView) view.findViewById(R.id.expert_name);
            holder.expertSign = (TextView) view.findViewById(R.id.expert_sign);
            holder.post_update_time = (TextView) view.findViewById(R.id.post_update_time);
            holder.latest_post_count = (TextView) view.findViewById(R.id.new_post_count);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        expertDataItem = list.get(position);
        ImageLoader.getInstance().displayImage(expertDataItem.user_head_photo_url, holder.expertIcon, ImageOptionsUtils.getHeadIconOptions());
        holder.expertName.setText(expertDataItem.user_nick_name);
        holder.expertSign.setText(expertDataItem.latest_post_info);
        if(expertDataItem.latest_post_count==null||expertDataItem.latest_post_count.equals("")||expertDataItem.latest_post_count.equals("0")){
            holder.latest_post_count.setVisibility(View.INVISIBLE);
        }else{
            holder.latest_post_count.setText(expertDataItem.latest_post_count);
        }
        holder.post_update_time.setText(expertDataItem.post_update_time);
        return view;
    }

    static class ViewHolder {
        TextView expertName, post_update_time;
        TextView expertSign;
        ImageView expertIcon;
        TextView latest_post_count;
    }
}
