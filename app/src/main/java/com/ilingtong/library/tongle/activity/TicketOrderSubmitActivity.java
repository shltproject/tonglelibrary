package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.GroupOrderGreatResult;
import com.ilingtong.library.tongle.protocol.MyCouponAvailableItem;
import com.ilingtong.library.tongle.protocol.MyCouponBaseInfo;
import com.ilingtong.library.tongle.protocol.TicketOrderSubmitParam;
import com.ilingtong.library.tongle.protocol.TicketOrderSubmitRequestParam;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/9
 * Time: 10:00
 * Email: liuting@ilingtong.com
 * Desc:提交订单
 */
public class TicketOrderSubmitActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标

    private TextView mTxtName;//商品名称
    private TextView mTxtPrice;//商品单价
    private ImageView mImgMiuns;//数量减
    private ImageView mImgAdd;//数量加
    private EditText mEdtNum;//数量
    private TextView mTxtSum;//小计
    private TextView mTxtCoupon;//使用优惠券
    private RelativeLayout mRlyCoupon;//优惠券块
    private TextView mTxtResult;//合计
    private Button mBtnSubmit;//提交订单按钮

    private TicketOrderSubmitParam mParam;//传递到提交订单的参数类
    private MyCouponBaseInfo mMyCouponBaseInfo = new MyCouponBaseInfo();//优惠券实体类
    private double mUseConditions;//优惠券限制条件
    private double mMoney;//金额
    private int mEditNum;//商品数量
    private TicketOrderSubmitRequestParam mRequestParam;//请求参数类
    public static TicketOrderSubmitActivity instance;
    private double mPrice;

    private Dialog mDialog;//加载对话框

    /**
     * @param context
     * @param ticketOrderSubmitParam 参数类
     */
    public static void launcher(Activity context, TicketOrderSubmitParam ticketOrderSubmitParam) {
        Intent intent = new Intent(context, TicketOrderSubmitActivity.class);
        intent.putExtra("order_submit_param", (Serializable) ticketOrderSubmitParam);
        context.startActivityForResult(intent, TongleAppConst.REQUEST_CODE_SUBMIT);
    }

    /**
     * @param context
     * @param ticketOrderSubmitParam 参数类
     */
    public static void launcher(Fragment context, TicketOrderSubmitParam ticketOrderSubmitParam, int requestCode, MyCouponBaseInfo voucher_base) {
        Intent intent = new Intent(context.getActivity(), TicketOrderSubmitActivity.class);
        intent.putExtra("order_submit_param", (Serializable) ticketOrderSubmitParam);
        intent.putExtra("voucher_base", (Serializable) voucher_base);
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_order_submit);
        initView();
        initData();
        instance = this;
    }

    public void initView() {
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle.setText(getResources().getString(R.string.ticket_order_submit_top_name));
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);

        mTxtName = (TextView) findViewById(R.id.ticket_order_submit_tv_name);
        mTxtPrice = (TextView) findViewById(R.id.ticket_order_submit_tv_price);
        mImgMiuns = (ImageView) findViewById(R.id.ticket_order_submit_img_numMiuns);
        mImgAdd = (ImageView) findViewById(R.id.ticket_order_submit_img_numAdd);
        mEdtNum = (EditText) findViewById(R.id.ticket_order_submit_edt_num);
        mTxtSum = (TextView) findViewById(R.id.ticket_order_submit_tv_sum);
        mTxtCoupon = (TextView) findViewById(R.id.ticket_order_submit_tv_coupon);
        mRlyCoupon = (RelativeLayout) findViewById(R.id.ticket_order_submit_rly_coupon);
        mTxtResult = (TextView) findViewById(R.id.ticket_order_submit_tv_result);
        mBtnSubmit = (Button) findViewById(R.id.ticket_order_submit_btn_submit);
        mImgMiuns.setOnClickListener(this);
        mImgAdd.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mRlyCoupon.setOnClickListener(this);

        mParam = (TicketOrderSubmitParam) getIntent().getSerializableExtra("order_submit_param");
        if (getIntent().getSerializableExtra("voucher_base") != null) {
            mMyCouponBaseInfo = (MyCouponBaseInfo) getIntent().getSerializableExtra("voucher_base");
        }

        mUseConditions = mMyCouponBaseInfo.getUse_conditions();//选择的优惠券满减条件。未选择优惠券时该值为0
        mMoney = mMyCouponBaseInfo.getMoney();//优惠券优惠的金额。未选择优惠券时该值为0

        mRequestParam = new TicketOrderSubmitRequestParam();
        mPrice = Double.parseDouble(mParam.price);

        mDialog = DialogUtils.createLoadingDialog(TicketOrderSubmitActivity.this);
        mDialog.setCancelable(false);
    }

    /**
     * 初始化数据
     */
    public void initData() {
        mTxtName.setText(mParam.product_name);
//        mTxtPrice.setText(getResources().getString(R.string.RMB) + FontUtils.setTwoDecimal(mPrice));
        mTxtPrice.setText(FontUtils.priceFormat(mPrice));
        if (mParam.order_qty != null) {//如果之前有购买数量，则显示购买数量，否则为1
            mEdtNum.setText(mParam.order_qty);
            mEditNum = Integer.parseInt(mEdtNum.getText().toString());
            updateView();
        } else {
            mEditNum = Integer.parseInt(mEdtNum.getText().toString());
//            mTxtSum.setText(getResources().getString(R.string.RMB) + FontUtils.setTwoDecimal(mPrice));
            mTxtSum.setText(FontUtils.priceFormat(mPrice));
//            mTxtResult.setText(getResources().getString(R.string.RMB) + FontUtils.setTwoDecimal(mPrice));
            mTxtResult.setText(FontUtils.priceFormat(mPrice));
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.ticket_order_submit_img_numMiuns) {//减数量
            if (mEditNum > 1) {//大于1的时候减
                mEditNum -= 1;
                mEdtNum.setText(mEditNum + "");
                updateView();
            } else {//等于1时提示不能再减了
                ToastUtils.toastShort(getString(R.string.common_number_min));
            }
        } else if (v.getId() == R.id.ticket_order_submit_img_numAdd) {//加数量
            mEditNum += 1;
            mEdtNum.setText(mEditNum + "");
            updateView();
        } else if (v.getId() == R.id.ticket_order_submit_btn_submit) {//提交订单到后台，前往选择支付界面
            mDialog.show();

            mRequestParam.user_id = TongleAppInstance.getInstance().getUserID();
            mRequestParam.user_token = TongleAppInstance.getInstance().getToken();
            mRequestParam.amount = FontUtils.setTwoDecimal((mPrice * mEditNum));//总金额
            if (mUseConditions != 0 && mPrice * mEditNum >= mUseConditions) {
                mRequestParam.vouchers_number_id = mMyCouponBaseInfo.getVouchers_number_id();
            } else {
                mRequestParam.vouchers_number_id = "";
            }
            mRequestParam.order_amount = FontUtils.setTwoDecimal((mPrice * mEditNum - mMoney));//订单金额
            mRequestParam.product_id = mParam.product_id;
            mRequestParam.price = mParam.price;
            mRequestParam.relation_id = mParam.relation_id;
            mRequestParam.mstore_id = mParam.mstore_id;
            mRequestParam.post_id = mParam.post_id;
            mRequestParam.order_qty = mEditNum + "";
            mRequestParam.order_no = mParam.order_no;
            mRequestParam.prod_spec_list = new ArrayList<>();
            mRequestParam.prod_spec_list.addAll(mParam.prod_spec_list);
            ServiceManager.doTicketOrderSubmitRequest(mRequestParam, successListener(), errorListener());
        } else if (v.getId() == R.id.ticket_order_submit_rly_coupon) {//优惠券块
            List<MyCouponAvailableItem> goods_info = new ArrayList<>();
            MyCouponAvailableItem myCouponAvailableItem = new MyCouponAvailableItem();
            myCouponAvailableItem.goods_id = mParam.product_id;
            myCouponAvailableItem.spec_id1 = mParam.prod_spec_list.get(0).spec_detail_id;
            myCouponAvailableItem.buy_count = mEditNum + "";
            if (mParam.prod_spec_list.size() > 1) {
                myCouponAvailableItem.spec_id2 = mParam.prod_spec_list.get(1).spec_detail_id;
            } else {
                myCouponAvailableItem.spec_id2 = "";
            }
            goods_info.add(myCouponAvailableItem);

            SelectCouponActivity.launcher(TicketOrderSubmitActivity.this, goods_info, mMyCouponBaseInfo.getVouchers_number_id());
        }
    }

    /**
     * 功能：提交订单网络响应成功，返回数据
     *
     * @return
     */
    private Response.Listener successListener() {
        return new Response.Listener<GroupOrderGreatResult>() {
            @Override
            public void onResponse(GroupOrderGreatResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    SelectPayTypeActivity.launcher(TicketOrderSubmitActivity.this, response.getBody().getOrder_no(), TongleAppConst.GROUP_PAY_MODE);
                    //提交成功，返回刷新团购订单列表
                    //  MyGroupOrderActivity.UPDATE_LIST_FLAG = true;
                    ToastUtils.toastShort(getString(R.string.ticket_order_submit_success_msg));
                    setResult(RESULT_OK);
                } else {
                    ToastUtils.toastLong(response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     *
     * @return
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mDialog.dismiss();
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    /**
     * 更新view
     */
    public void updateView() {
//        mTxtSum.setText(getResources().getString(R.string.RMB) + FontUtils.setTwoDecimal((mPrice * mEditNum)));
        mTxtSum.setText(FontUtils.priceFormat((mPrice * mEditNum)));
        if (mUseConditions != 0 && mPrice * mEditNum >= mUseConditions) {//如果使用优惠券且达到优惠条件，则减
//            mTxtResult.setText(getResources().getString(R.string.RMB) + FontUtils.setTwoDecimal((mPrice * mEditNum - mMoney)));
            mTxtResult.setText(FontUtils.priceFormat((mPrice * mEditNum - mMoney)));
            mTxtCoupon.setText(mMyCouponBaseInfo.getVouchers_name());//显示优惠券信息
        } else {
//            mTxtResult.setText(getResources().getString(R.string.RMB) + FontUtils.setTwoDecimal(mPrice * mEditNum));
            mTxtResult.setText(FontUtils.priceFormat(mPrice * mEditNum));
            mTxtCoupon.setText(getString(R.string.ticket_order_submit_unused_txt));//显示不使用
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TongleAppConst.REQUEST_CODE_SELECT_COUPON) {//选择优惠券的返回值
            if (resultCode == TongleAppConst.REQUEST_OK_SELECT_COUPON) {
                mMyCouponBaseInfo = (MyCouponBaseInfo) data.getSerializableExtra("my_coupon_base_info");
                mUseConditions = mMyCouponBaseInfo.getUse_conditions();
                mMoney = mMyCouponBaseInfo.getMoney();
                updateView();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
