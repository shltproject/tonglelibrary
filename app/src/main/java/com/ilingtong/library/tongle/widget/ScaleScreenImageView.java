package com.ilingtong.library.tongle.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by dell on 2015/10/23.
 */
public class ScaleScreenImageView  extends ImageView {

    public ScaleScreenImageView(Context context) {
        super(context);
    }

    public ScaleScreenImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScaleScreenImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (getWidth() > 0) {
            float es = (float) getWidth() / (float)bm.getWidth();
            int height = (int) (bm.getHeight() * es);
            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = height;
            setLayoutParams(params);
        }
    }

}

