package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.APPpromotionInfo;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;

import java.io.Serializable;
import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * Created by wuqian on 2016/10/14.
 * mail: wuqian@ilingtong.com
 * Description: 将app分享到微信或者朋友圈
 */
public class ShareAppDialogActivity extends BaseActivity implements View.OnClickListener{
    private TextView dialogSelectShareTxtWeixin;
    private TextView dialogSelectShareTxtMoment;
    private TextView dialogSelectShareTxtCancel;
    private APPpromotionInfo mAppPromotionInfo;

    private final int shareSuccess = 0;
    private final int shareFailure = 1;
    private final int shareCancel = 2;

    private String[] needPermissions={Manifest.permission.WRITE_EXTERNAL_STORAGE};//存储权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case shareSuccess:
                    ToastUtils.toastShort(getString(R.string.share_success));
                    ShareAppDialogActivity.this.finish();
                    break;
                case shareFailure:
                    ToastUtils.toastLong(getString(R.string.share_failure) + msg.obj);
                    break;
                case shareCancel:
                    ToastUtils.toastShort(getString(R.string.share_cancel));
                    break;
            }
        }
    };

    public static void launcher(Activity activity,APPpromotionInfo info){
        Intent intent = new Intent(activity,ShareAppDialogActivity.class);
        intent.putExtra("apppromotion_info",(Serializable) info);
        activity.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);;//去掉标题栏
        setContentView(R.layout.dialog_select_share_layout);
        window.getDecorView().setPadding(0, 0, 0, 0);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);
        mAppPromotionInfo = (APPpromotionInfo) getIntent().getSerializableExtra("apppromotion_info");
        init();
    }

    private void init() {

        dialogSelectShareTxtWeixin = (TextView) findViewById(R.id.dialog_select_share_txt_weixin);
        dialogSelectShareTxtMoment = (TextView) findViewById(R.id.dialog_select_share_txt_moment);
        dialogSelectShareTxtCancel = (TextView) findViewById(R.id.dialog_select_share_txt_cancel);

        dialogSelectShareTxtCancel.setOnClickListener(this);
        dialogSelectShareTxtMoment.setOnClickListener(this);
        dialogSelectShareTxtWeixin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.dialog_select_share_txt_weixin){
            requestPermissionUtils= RequestPermissionUtils.getRequestPermissionUtils(ShareAppDialogActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                        @Override
                        public void requestSuccess() {
                            ShareSDK.initSDK(ShareAppDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                            HashMap<String, Object> hasMap1 = new HashMap<String, Object>();
                            hasMap1.put("Id", "4");
                            hasMap1.put("SortId", "4");
                            hasMap1.put("AppId", TongleAppInstance.getInstance().getWECHAT_APPID());
                            hasMap1.put("AppSecret", TongleAppInstance.getInstance().getWECHAT_SECRET());
                            hasMap1.put("BypassApproval", "false");
                            hasMap1.put("Enable", "true");
                            ShareSDK.setPlatformDevInfo(Wechat.NAME, hasMap1);

                            Platform platform_weinxin = ShareSDK.getPlatform(Wechat.NAME);

                            //设置分享的参数：
                            Wechat.ShareParams sp = new Wechat.ShareParams();
                            sp.setShareType(Platform.SHARE_WEBPAGE);
                            sp.setTitle(mAppPromotionInfo.getWx_title());
                            sp.setText(mAppPromotionInfo.getWx_memo());
                            sp.setUrl(mAppPromotionInfo.getWx_link_url());
                            sp.setImageUrl(mAppPromotionInfo.getWx_pic_url());
                            // 分享
                            platform_weinxin.share(sp);
                            setPlatformActionListener(platform_weinxin);
                        }

                        @Override
                        public void requestFail() {
                            SelectDialog dialog=new SelectDialog(ShareAppDialogActivity.this,getString(R.string.please_open_the_storage_permission));
                            dialog.show();
                        }
                    });
            requestPermissionUtils.checkPermissions(ShareAppDialogActivity.this);

        }else if (v.getId() == R.id.dialog_select_share_txt_moment){

            requestPermissionUtils= RequestPermissionUtils.getRequestPermissionUtils(ShareAppDialogActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                        @Override
                        public void requestSuccess() {
                            ShareSDK.initSDK(ShareAppDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                            HashMap<String, Object> hasMap2 = new HashMap<String, Object>();
                            hasMap2.put("Id", "5");
                            hasMap2.put("SortId", "5");
                            hasMap2.put("AppId", TongleAppInstance.getInstance().getWECHAT_APPID());
                            hasMap2.put("AppSecret", TongleAppInstance.getInstance().getWECHAT_SECRET());
                            hasMap2.put("BypassApproval", "false");
                            hasMap2.put("Enable", "true");
                            ShareSDK.setPlatformDevInfo(WechatMoments.NAME, hasMap2);
                            Platform platformMoments = ShareSDK.getPlatform(WechatMoments.NAME);
                            //设置分享的参数：
                            Wechat.ShareParams shareParams = new Wechat.ShareParams();
                            shareParams.setTitle(mAppPromotionInfo.getWx_title());
                            shareParams.setText(mAppPromotionInfo.getWx_memo());
                            shareParams.setUrl(mAppPromotionInfo.getWx_link_url());
                            shareParams.setImageUrl(mAppPromotionInfo.getWx_pic_url());
                            shareParams.setShareType(Platform.SHARE_WEBPAGE);
                            // 分享
                            platformMoments.share(shareParams);
                            setPlatformActionListener(platformMoments);
                        }

                        @Override
                        public void requestFail() {
                            SelectDialog dialog=new SelectDialog(ShareAppDialogActivity.this,getString(R.string.please_open_the_storage_permission));
                            dialog.show();
                        }
                    });
            requestPermissionUtils.checkPermissions(ShareAppDialogActivity.this);

        }else if (v.getId() == R.id.dialog_select_share_txt_cancel){
            this.finish();
        }
    }
    /**
     * 分享回调监听
     *
     * @param platform
     */
    private void setPlatformActionListener(Platform platform) {
        platform.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                handler.sendEmptyMessage(shareSuccess);
                Log.e("tag", "complete");
            }
            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                Message msg = new Message();
                msg.what = shareFailure;
                msg.obj = throwable.getLocalizedMessage();
                handler.sendMessage(msg);
                Log.e("tag", "error");
            }
            @Override
            public void onCancel(Platform platform, int i) {
                handler.sendEmptyMessage(shareCancel);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode,permissions,paramArrayOfInt);
    }
}
