package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: 率磊
 * Date: 2015/8/7
 * Time: 9:36
 * Email: leishuai@isoftstone.com
 * Desc:  微信result
 */
public class WeChatPayResult implements Serializable {

    private BaseInfo head;
    private WeChatPayObjInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public WeChatPayObjInfo getBody() {
        return body;
    }

    public void setBody(WeChatPayObjInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }

}
