package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class OrderDetailInvoiceInfo implements Serializable {
    public String invoice_type_no;
    public String invoice_type_name;
    public String invoice_content_flag;
    public String invoice_content_flag_name;
    public String invoice_title;

    @Override
    public String toString() {
        return "invoice_type_no:" + invoice_type_no + "\r\n" +
                "invoice_type_name:" + invoice_type_name + "\r\n" +
                "invoice_content_flag:" + invoice_content_flag + "\r\n" +
                "invoice_content_flag_name:" + invoice_content_flag_name + "\r\n" +
                "invoice_title:" + invoice_title;
    }
}
