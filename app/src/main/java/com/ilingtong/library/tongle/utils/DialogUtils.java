
package com.ilingtong.library.tongle.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ilingtong.library.tongle.R;
import com.nostra13.universalimageloader.core.ImageLoader;

public class DialogUtils {
    /**
     * 创建自定义ProgressDialog
     *
     * @param context
     * @return
     */
    public static Dialog createLoadingDialog(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.layout_loading_dialog, null); // 得到加载view
        LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_view); // 加载布局
        Dialog loadingDialog = new Dialog(context, R.style.loading_dialog); // 创建自定义样式dialog
        loadingDialog.setCancelable(false); // 不可以用"返回键"取消
        loadingDialog.setContentView(layout, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        return loadingDialog;
    }

    /**
     * 创建并且显示一维码和二维码dialog
     *
     * @param context
     * @param coupon_1d_qrcode_url 一维码url
     * @param coupon_2d_qrcode_url  二维码url
     * @return AlertDialog
     */
    public static AlertDialog showQrDialog(Context context,String coupon_1d_qrcode_url,String coupon_2d_qrcode_url){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.ticket_code_dialog_layout, null);
        builder.setView(view);
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
        ImageView imgOneDimensional = (ImageView) view.findViewById(R.id.ticket_code_img_one_dimensional);//一维码
        ImageView imgQr = (ImageView) view.findViewById(R.id.ticket_code_img_qr);//二维码
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imgOneDimensional.getLayoutParams();
        float es = (float) (DipUtils.getScreenWidth(context)-DipUtils.px2dip(40*2));
        int height = (int) ( 70*es/200);
        params.height = height;
        imgOneDimensional.setLayoutParams(params);
        ImageLoader.getInstance().displayImage(coupon_1d_qrcode_url, imgOneDimensional, ImageOptionsUtils.getOptions());
        ImageLoader.getInstance().displayImage(coupon_2d_qrcode_url, imgQr, ImageOptionsUtils.getOptions());
        return alert;
    }

    /**
     * 设置垂直虚线的高度
     *
     * @param relativeLayout
     * @param pxValue
     */
    public static void setLineHeight(RelativeLayout relativeLayout, float pxValue) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) relativeLayout.getLayoutParams();
        params.height = DipUtils.px2dip(pxValue);
        relativeLayout.setLayoutParams(params);
    }
}
