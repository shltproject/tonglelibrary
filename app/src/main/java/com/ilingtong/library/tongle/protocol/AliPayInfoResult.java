package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/8/9
 * Time: 13:01
 * Email: ycshi@isoftstone.com
 * Desc: 支付宝支付
 */
public class AliPayInfoResult implements Serializable {
    public String alipay_info;

    public String getAlipay_info() {
        return alipay_info;
    }

    @Override
    public String toString() {
        return "alipay_info:" + alipay_info;
    }
}
