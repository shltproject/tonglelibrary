package com.ilingtong.library.tongle.protocol;

/**
 * Created by wuqian on 2017/1/5.
 * mail: wuqian@ilingtong.com
 * Description: 12003入口参数 order_info 参数结构
 */
public class CreateOrderParams {

    //订单信息（商品明细列表（明细序号，商品ID)
    public CreateOrderParamsProductInfo product_info;
    //收货地址编号
    public String address_no;
    //支付方式编号
    public String pay_type;
    //是否需要发票
    public String invoice_type;
    //配送信息（配送方式编号，配送时间）
    public ShippingMethodInfo shipping_method;
    //发票信息（发票类型编号，发票内容区分，发票抬头备注）
    public InvoiceInfo invoice_info;
    //订单备注
    public String order_memo;
    //关税
    public String tariff;
    //优惠券号
    public String vouchers_number_id;

    public String post_id;
    public String wifi_id;
    public String relation_id;
    public double account_pay_money;  //账户支付金额  add 2017/9/6

    public CreateOrderParams() {
    }

    public CreateOrderParams(CreateOrderParamsProductInfo product_info, String address_no, String invoice_type, ShippingMethodInfo shipping_method, InvoiceInfo invoice_info, String order_memo, String tariff, String vouchers_number_id, String relation_id, String post_id, double account_pay_money) {
        this.product_info = product_info;
        this.address_no = address_no;
        this.pay_type = "3";
        this.invoice_type = invoice_type;
        this.shipping_method = shipping_method;
        this.invoice_info = invoice_info;
        this.order_memo = order_memo;
        this.tariff = tariff;
        this.vouchers_number_id = vouchers_number_id;
        this.post_id = post_id;
        this.relation_id = relation_id;
        this.wifi_id = "";
        this.account_pay_money = account_pay_money;
    }
}
