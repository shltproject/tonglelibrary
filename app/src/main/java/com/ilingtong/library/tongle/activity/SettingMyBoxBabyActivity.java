package com.ilingtong.library.tongle.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.CollectProductListAdaper;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.model.CollectProductModel;
import com.ilingtong.library.tongle.protocol.CollectProductResult;
import com.ilingtong.library.tongle.protocol.MStoreDetailCollectResult;
import com.ilingtong.library.tongle.protocol.ProductListItemData;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc: 我的--》百宝箱--》我的宝贝
 */
public class SettingMyBoxBabyActivity extends BaseActivity implements XListView.IXListViewListener,View.OnClickListener{
    private TextView top_name;
    private ImageView left_arrow_btn;
    private XListView listview;
    private RelativeLayout rl_replace;
    private CollectProductListAdaper productListAdaper;
    private CollectProductModel collectProductModel;
    private AlertDialog alert;
    private String prodID;//id为本界面listview里的产品id，不是上个界面传下来的（长按listview里获得）
    private int pos;
    private boolean flag = true;
    public ArrayList<ProductListItemData> prod_list =new ArrayList<ProductListItemData>();
    public static SettingMyBoxBabyActivity instance = null;
    private int listIndex = -1; //表示是从 position位置跳转到M客详情的
    public static boolean UPDATE_LIST_FLAG = false; //是否刷新列表的标志
    private boolean clearFlag=true;//清空标志,true为清空
    Handler mHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0){
                productListAdaper.notifyDataSetChanged();
                return;
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xlistview_comm_layout);
        instance = this;
        initView();
        doRequest();
    }
    public void initView(){
        prod_list.clear();
        if (collectProductModel == null)
            collectProductModel = new CollectProductModel();
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        listview = (XListView)findViewById(R.id.xlistview);
        rl_replace = (RelativeLayout)findViewById(R.id.rl_replace);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(getString(R.string.my_baby));

        productListAdaper = new CollectProductListAdaper(this, prod_list);
        listview.setAdapter(productListAdaper);
        listview.setXListViewListener(this, 0);
        listview.setRefreshTime();
        listview.setPullLoadEnable(false);

        //listview的长安事件
        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //position做成全局变量，下面删除每条数据要用
                pos=position;
                //长按弹出一个dialog（删除按钮）
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingMyBoxBabyActivity.this);
                View view2 = LayoutInflater.from(SettingMyBoxBabyActivity.this).inflate(R.layout.mybaby_listview_dialog_layout, null);
                builder.setView(view2);
                alert = builder.create();
                alert.setCanceledOnTouchOutside(true);//点击dialog以外的地方，dialog消失
                //dialog里面删除按钮的初始化和点击事件
                TextView delete_alert=(TextView)view2.findViewById(R.id.te_delete);
                delete_alert.setOnClickListener(SettingMyBoxBabyActivity.this);
                return true;
            }
        });
        //listview的每条item的点击事件
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //点击item进行跳转，并把产品id传到产品详情界面
                if (!TextUtils.isEmpty(prod_list.get(position-1).prod_id)) {
                    if(prod_list.get(position-1).coupon_flag.equals(TongleAppConst.YES)) {//团购商品
                        ProductTicketDetailActivity.launchForResult(SettingMyBoxBabyActivity.this, prod_list.get(position - 1).prod_id, TongleAppConst.ACTIONID_MY_BABY, prod_list.get(position-1).relation_id, "", "", TongleAppConst.MYBABY_INTO, 10001);
                    }else{
                        //普通商品
                        CollectProductDetailActivity.launchForResult(SettingMyBoxBabyActivity.this, prod_list.get(position - 1).prod_id, TongleAppConst.ACTIONID_MY_BABY, prod_list.get(position-1).relation_id, "", "", TongleAppConst.MYBABY_INTO, 10001);
                    }
                    listIndex = position;
                }
            }
        });
    }

    /**
     * 去到商品详情后，返回如果有取消收藏操作则刷新页面
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10001 && UPDATE_LIST_FLAG) {
            if (listIndex > 0) {
                prod_list.remove(listIndex - 1);
                mHandler.sendEmptyMessage(0);
            }
            UPDATE_LIST_FLAG = false;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void doRequest(){
        ServiceManager.mineBaby(TongleAppInstance.getInstance().getUserID(),"","1",TongleAppConst.FETCH_COUNT, successListener(), errorListener());
    }
    //响应所有控件的点击事件
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn){
            finish();
        }else if (v.getId() == R.id.te_delete){
            //删除选中宝贝
            prodID=collectProductModel.my_prod_list.get(pos-1).prod_id;
            ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), prodID, "1", collectListener(), errorListener());
        }
    }
    @Override
    public void onRefresh(int id) {
        clearFlag=true;
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {
        //加载更多
        if (flag) {
            clearFlag=false;
            ServiceManager.mineBaby(TongleAppInstance.getInstance().getUserID(), prod_list.get(prod_list.size()-1).prod_id, "1",TongleAppConst.FETCH_COUNT, successListener(), errorListener());
        }else{
            ToastUtils.toastShort(getString(R.string.common_list_end));
            listview.setPullLoadEnable(false);
        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<CollectProductResult>() {
            @Override
            public void onResponse(CollectProductResult response) {
                //判断返回数据是否成功
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    //刷新成功后，清空list
                    if (prod_list!=null&&clearFlag){
                        prod_list.clear();
                    }
                    collectProductModel.data_total_count = response.getBody().getData_total_count();
                    collectProductModel.my_prod_list = response.getBody().getMy_prod_list();
                    updateListView();
                    //第一次请求或刷新页面时候判断是否隐藏底部加载更多按钮
                    if(response.getBody().getMy_prod_list().size()< Integer.parseInt(TongleAppConst.FETCH_COUNT)){
                        flag = false;
                        listview.setPullLoadEnable(false);
                    } else {
                        flag = true;
                        listview.setPullLoadEnable(true);
                    }
                    prod_list.addAll(response.getBody().getMy_prod_list());
                    //调用handler，发送消息
                    mHandler.sendEmptyMessage(0);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception)+response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        };
    }
    //更新listview数据
    public void updateListView() {
        if (collectProductModel.data_total_count.equals("0")){
            listview.setVisibility(View.GONE);
            rl_replace.setVisibility(View.VISIBLE);
        }
    }
    /**
     * 功能：删除选中宝贝请求网络响应成功，返回数据
     */
    private Response.Listener collectListener() {
        return new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.delete_success));
                    alert.dismiss();
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception)+response.getHead().getReturn_message());
                }
            }
        };
    }
}