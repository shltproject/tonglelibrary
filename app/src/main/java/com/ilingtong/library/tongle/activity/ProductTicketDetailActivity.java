package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.ProInfoListAdapter;
import com.ilingtong.library.tongle.adapter.ProNoticeListAdapter;
import com.ilingtong.library.tongle.adapter.TLPagerAdapter;
import com.ilingtong.library.tongle.external.MyListView;
import com.ilingtong.library.tongle.fragment.MProductFragment;
import com.ilingtong.library.tongle.protocol.FavoriteListItem;
import com.ilingtong.library.tongle.protocol.GroupProductResult;
import com.ilingtong.library.tongle.protocol.MStoreDetailCollectResult;
import com.ilingtong.library.tongle.protocol.ProdDetailQRResult;
import com.ilingtong.library.tongle.protocol.ProdDetailRequestParam;
import com.ilingtong.library.tongle.protocol.ProdSpecListInfo;
import com.ilingtong.library.tongle.protocol.TicketOrderSubmitParam;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;
import com.ilingtong.library.tongle.widget.SelectDialog;
import com.ilingtong.library.tongle.widget.SmartScrollView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * author: liuting
 * Date: 2016/3/11
 * Time: 9:02
 * Email: liuting@ilingtong.com
 * Desc:团购券商品详情页
 */
public class ProductTicketDetailActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标
    private ImageView mImgCollect;//收藏图标
    private ImageView mImgQR;//二维码图标
    private ViewPager mVpPicture;//轮播图片
    private LinearLayout mLlyBottom;//轮播图片导航

    private TextView mTxtName;//商品名称
    private TextView mTxtPrice;//商品价格
    private TextView mTxtDetail;//详情
    private ImageButton mImgShare;//分享
    private TextView mTxtService;//服务
    //private TextView mTxtNotify;//提示
    private TextView mTxtRetire;//随时退
    private TextView mTxtRefund;//过期退
    private TextView mTxtNoRefund;//不可退

    private TextView mTxtSold;//已售数量
    private TextView mTxtPercent;//好评度
    private LinearLayout mLlyGoodQty;//评价数块
    private TextView mTxtGoodQty;//评价数

    private RelativeLayout mRlyStoreCount;//适用商户块
    private TextView mTxtStoreCount;//适用商户数
    private TextView mTxtStoreName;//商户名称
    private TextView mTxtStoreAddress;//商户地址
    private ImageButton mImgStorePhone;//商户电话
    private RelativeLayout mRlyStoreAddress;//商户地址一栏
    private LinearLayout mLlyStoreLine;//商户地址间隔线

    private LinearLayout mLlyDetail;//图文详情块
    private TextView mTxtInfoHead;//团购详情标题
    private MyListView mLvInfo;//团购详情明细列表
    private TextView mTxtInfoTotal;//团购详情总价
    private TextView mTxtInfoActual;//团购详情团购价
    private TextView mTxtFooter;//脚注

    private MyListView mLvNotice;//购买须知列表
    private TextView mTxtOldPrice;//原价
    private TextView mTxtActualPrice;//现价
    private Button mBtnBuy;//立即抢购按钮
    private RelativeLayout mRlyBottom;//立即抢购底部一栏

    private RelativeLayout mRlyReplace;//无相关信息显示
    private TextView mTxtReplace;//显示无相关信息或者错误信息
    private SmartScrollView mSvMain;//有信息时显示的

    private GroupProductResult mGroupProductResult;
    private ArrayList<View> mProdPicList;//轮播图
    private TLPagerAdapter mPagerAdapter;
    private TextView mPreSelectedBtn;//选中后
    private View mTouchTarget;
    private ProInfoListAdapter mProInfoListAdapter;//团购信息明细Adapter
    private ProNoticeListAdapter mProNoticeListAdapter;//购买须知Adapter

    private Dialog mDialog;//加载对话框
    private String mProductId;//商品ID
    private String mStoreID;//魔店ID
    private String mRelationId;//关联ID
    private String mPostId;//帖子ID
    private String action;  //入口

    private int intoType;//进入方式的key
    private String mCollectType;//收藏类型
    private TicketOrderSubmitParam mParam;//传递到提交订单的参数类
    private ProdDetailRequestParam mRequestParam;//请求商品详情信息参数类
    private ScheduledExecutorService scheduledExecutorService;//图片切换Service
    private int currentItem = 0;//当前页面
    public static final String COLLECTED_BY_ME = "0";      //已收藏
    private boolean bIsFavorate = false;//是否已收藏标示

    private CheckBox[] mCheckBox = new CheckBox[2];
    private PopupWindow mPopupWindow;//收藏宝贝view
//    private EditText mEditCommon;//分享评论
//    private AlertDialog mAlert;//分享dialog
//    private ProgressDialog mPDailog;//分享加载对话框
//    private Platform mWXPlatform, mFriendPlatform, mSinaPlatform;//平台
    private String[] needPermissions={Manifest.permission.CALL_PHONE};//拨打电话权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    private float y = 0.0f;//按下位置
    private boolean isScrollTobottom;//是否滑动到底部
    private final float SCROLL_DISTANCE = 120;//手势向上滑动距离
    private boolean isFlip;//是否可翻页


    /**
     * 启动团购券详情activity
     *
     * @param activity
     * @param product_id  商品id
     * @param action      商品明细入口
     *                    1 首页活动
     *                    2 帖子链接
     *                    4	扫描条码
     *                    5	购物车
     *                    6	魔店
     * @param relation_id 二维码关联id
     * @param mstore_id   魔店id
     * @param post_id     帖子id
     */
    public static void launch(Activity activity, String product_id, String action, String relation_id, String mstore_id, String post_id) {
        Intent detailIntent = new Intent(activity, ProductTicketDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("product_id", product_id);
        bundle.putString("action", action);
        bundle.putString("relationId", relation_id);
        bundle.putString("mstore_id", mstore_id);
        bundle.putString("mypostId", post_id);
        detailIntent.putExtras(bundle);
        activity.startActivity(detailIntent);
    }

    /**
     * 启动团购券详情activity
     *
     * @param activity
     * @param product_id  商品id
     * @param action      商品明细入口
     *                    1 首页活动
     *                    2 帖子链接
     *                    4	扫描条码
     *                    5	购物车
     *                    6	魔店
     * @param relation_id 二维码关联id
     * @param mstore_id   魔店id
     * @param post_id     帖子id
     * @param intoType
     */
    public static void launch(Activity activity, String product_id, String action, String relation_id, String mstore_id, String post_id, int intoType) {
        Intent detailIntent = new Intent(activity, ProductTicketDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("product_id", product_id);
        bundle.putString("action", action);
        bundle.putString("relationId", relation_id);
        bundle.putString("mstore_id", mstore_id);
        bundle.putString("mypostId", post_id);
        bundle.putInt(TongleAppConst.INTO_TYPE, intoType);
        detailIntent.putExtras(bundle);
        activity.startActivity(detailIntent);
    }

    /**
     * 启动团购券详情activity
     *
     * @param activity
     * @param product_id  商品id
     * @param action      商品明细入口
     *                    1 首页活动
     *                    2 帖子链接
     *                    4	扫描条码
     *                    5	购物车
     *                    6	魔店
     * @param relation_id 二维码关联id
     * @param mstore_id   魔店id
     * @param post_id     帖子id
     * @param intoType
     */
    public static void launchForResult(Activity activity, String product_id, String action, String relation_id, String mstore_id, String post_id, int intoType, int requestCode) {
        Intent detailIntent = new Intent(activity, ProductTicketDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("product_id", product_id);
        bundle.putString("action", action);
        bundle.putString("relationId", relation_id);
        bundle.putString("mstore_id", mstore_id);
        bundle.putString("mypostId", post_id);
        bundle.putInt(TongleAppConst.INTO_TYPE, intoType);
        detailIntent.putExtras(bundle);
        activity.startActivityForResult(detailIntent, requestCode);
    }

    /**
     * 启动团购券详情activity
     *
     * @param fragment    Fragment
     * @param product_id  商品id
     * @param action      商品明细入口
     *                    1 首页活动
     *                    2 帖子链接
     *                    4	扫描条码
     *                    5	购物车
     *                    6	魔店
     * @param relation_id 二维码关联id
     * @param mstore_id   魔店id
     * @param post_id     帖子id
     * @param intoType
     */
    public static void launchForResult(Fragment fragment, String product_id, String action, String relation_id, String mstore_id, String post_id, int intoType, int requestCode) {
        Intent detailIntent = new Intent(fragment.getActivity(), ProductTicketDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("product_id", product_id);
        bundle.putString("action", action);
        bundle.putString("relationId", relation_id);
        bundle.putString("mstore_id", mstore_id);
        bundle.putString("mypostId", post_id);
        bundle.putInt(TongleAppConst.INTO_TYPE, intoType);
        detailIntent.putExtras(bundle);
        fragment.startActivityForResult(detailIntent, requestCode);
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (bIsFavorate) {
                        mImgCollect.setBackgroundResource(R.drawable.discollect_button_style);
                    } else {
                        mImgCollect.setBackgroundResource(R.drawable.collect_button_style);
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_ticket_detail);
        initView();
        getData();
    }

    public void initView() {
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle.setText(getResources().getString(R.string.product_ticket_detail_top_name));
        mImgCollect = (ImageView) findViewById(R.id.collect_btn);
        mImgQR = (ImageView) findViewById(R.id.QR_share_btn);

//        mImgCollect.setVisibility(View.VISIBLE);
//        mImgQR.setVisibility(View.VISIBLE);
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);
        mImgCollect.setOnClickListener(this);
        mImgQR.setOnClickListener(this);

        mVpPicture = (ViewPager) findViewById(R.id.product_ticket_detail_vp_pic);
        mLlyBottom = (LinearLayout) findViewById(R.id.product_ticket_detail_banner_bottom);

        mTxtName = (TextView) findViewById(R.id.product_ticket_detail_tv_name);
        mTxtPrice = (TextView) findViewById(R.id.product_ticket_detail_tv_price);
        mTxtDetail = (TextView) findViewById(R.id.product_ticket_detail_tv_detail);
        mImgShare = (ImageButton) findViewById(R.id.product_ticket_detail_btn_share);
        mTxtDetail.setOnClickListener(this);
        mImgShare.setOnClickListener(this);

        mTxtService = (TextView) findViewById(R.id.product_ticket_detail_tv_service);
        //mTxtNotify = (TextView) findViewById(R.id.product_ticket_detail_tv_notify);
        mTxtRetire = (TextView) findViewById(R.id.product_ticket_detail_tv_retire);
        mTxtRefund = (TextView) findViewById(R.id.product_ticket_detail_tv_refund);
        mTxtNoRefund = (TextView) findViewById(R.id.product_ticket_detail_tv_no_refund);

        mTxtSold = (TextView) findViewById(R.id.product_ticket_detail_tv_sold);
        mTxtPercent = (TextView) findViewById(R.id.product_ticket_detail_tv_percent);
        mLlyGoodQty = (LinearLayout) findViewById(R.id.product_ticket_detail_lly_qty);
        mTxtGoodQty = (TextView) findViewById(R.id.product_ticket_detail_tv_qty);
        mLlyGoodQty.setOnClickListener(this);

        mRlyStoreCount = (RelativeLayout) findViewById(R.id.ticket_detail_rly_store_count);
        mTxtStoreCount = (TextView) findViewById(R.id.ticket_detail_tv_store_count);
        mTxtStoreName = (TextView) findViewById(R.id.ticket_detail_tv_store_name);
        mTxtStoreAddress = (TextView) findViewById(R.id.ticket_detail_tv_store_address);
        mImgStorePhone = (ImageButton) findViewById(R.id.ticket_detail_btn_phone);
        mRlyStoreAddress=(RelativeLayout)findViewById(R.id.ticket_detail_rly_store_address);
        mLlyStoreLine=(LinearLayout)findViewById(R.id.ticket_detail_lly_store_line);
        mRlyStoreCount.setOnClickListener(this);
        mImgStorePhone.setOnClickListener(this);

        mLlyDetail = (LinearLayout) findViewById(R.id.ticket_detail_lly_detail);
        mTxtInfoHead = (TextView) findViewById(R.id.ticket_detail_tv_head);
        mLvInfo = (MyListView) findViewById(R.id.ticket_detail_lv_info);
        mTxtInfoTotal = (TextView) findViewById(R.id.ticket_detail_tv_price_total);
        mTxtInfoActual = (TextView) findViewById(R.id.ticket_detail_tv_price_actual);
        mTxtFooter = (TextView) findViewById(R.id.ticket_detail_tv_footer);
        mLlyDetail.setOnClickListener(this);

        mLvNotice = (MyListView) findViewById(R.id.ticket_detail_lv_notice);

        mTxtOldPrice = (TextView) findViewById(R.id.product_ticket_detail_tv_old_price);
        mTxtActualPrice = (TextView) findViewById(R.id.product_ticket_detail_tv_actual_price);
        mBtnBuy = (Button) findViewById(R.id.product_ticket_detail_btn_buy_now);
        mBtnBuy.setOnClickListener(this);
        mBtnBuy.setEnabled(false);//默认为不可用
        mRlyBottom = (RelativeLayout) findViewById(R.id.product_ticket_detail_rly_bottom);

        mRlyReplace = (RelativeLayout) findViewById(R.id.rl_replace);
        mTxtReplace = (TextView) findViewById(R.id.tv_replace);
        mSvMain = (SmartScrollView) findViewById(R.id.product_ticket_detail_sv_main);
        mSvMain.setScrollBottomListener(new SmartScrollView.ScrollBottomListener() {
            @Override
            public void scrollBottom(boolean isScrollBottom) {
                isScrollTobottom = isScrollBottom;
            }

        });

        mProdPicList = new ArrayList<View>();
        mPagerAdapter = new TLPagerAdapter(mProdPicList);

        mDialog = DialogUtils.createLoadingDialog(ProductTicketDetailActivity.this);
        mDialog.setCancelable(false);
        mDialog.show();

        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ViewPagerTask(), 1, 3, TimeUnit.SECONDS);

        mParam = new TicketOrderSubmitParam();

    }

    /**
     * 取得数据
     */
    public void getData() {
        mProductId = getIntent().getExtras().getString("product_id", "");
        mStoreID = getIntent().getExtras().getString("mstore_id", "");
        action = getIntent().getExtras().getString("action", "");
        mRelationId = getIntent().getExtras().getString("relationId", "");
        mPostId = getIntent().getExtras().getString("mypostId", "");
        intoType = getIntent().getIntExtra(TongleAppConst.INTO_TYPE, 0);
        if (TongleAppConst.ACTIONID_MY_BABY.equals(action)) {
            mCollectType = "baby";
        }

        mRequestParam = new ProdDetailRequestParam();
        mRequestParam.user_id = TongleAppInstance.getInstance().getUserID();
        mRequestParam.product_id = mProductId;
        mRequestParam.mstore_id = mStoreID;
        mRequestParam.post_id = mPostId;
        mRequestParam.action = action;
        mRequestParam.relation_id = mRelationId;

        mGroupProductResult = new GroupProductResult();
        ServiceManager.doProductTicketDetailRequest(mRequestParam, successListener(), requestErrorListener());

    }

    /**
     * 功能：商品详情网络响应成功，返回数据
     *
     * @return
     */
    private Response.Listener successListener() {
        return new Response.Listener<GroupProductResult>() {
            @Override
            public void onResponse(GroupProductResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mStoreID = response.getBody().getProd_base_info().getMstore_id();
                    mGroupProductResult = response;
                    mRlyReplace.setVisibility(View.GONE);
                    mSvMain.setVisibility(View.VISIBLE);
                    mImgCollect.setVisibility(View.VISIBLE);
                    mImgQR.setVisibility(View.VISIBLE);
                    mRlyBottom.setVisibility(View.VISIBLE);
                    addProductPicView();
                    initData();
                } else {
                    mRlyReplace.setVisibility(View.VISIBLE);
                    mTxtReplace.setVisibility(View.VISIBLE);
                    mSvMain.setVisibility(View.GONE);
                    mImgCollect.setVisibility(View.GONE);
                    mImgQR.setVisibility(View.GONE);
                    mRlyBottom.setVisibility(View.GONE);
                    mTxtReplace.setText(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：请求数据时网络响应失败
     *
     * @return
     */
    private Response.ErrorListener requestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mSvMain.setVisibility(View.GONE);
                mImgCollect.setVisibility(View.GONE);
                mImgQR.setVisibility(View.GONE);
                mRlyBottom.setVisibility(View.GONE);
                ToastUtils.toastLong(getString(R.string.sys_exception));
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     *
     * @return
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    /**
     * 初始化数据
     */
    public void initData() {
        mTxtName.setText(mGroupProductResult.getBody().getProd_base_info().getProd_name());
//        mTxtPrice.setText(getResources().getString(R.string.RMB) + FontUtils.setTwoDecimal(mGroupProductResult.getBody().getProd_base_info().getPrice()));
        mTxtPrice.setText(FontUtils.priceFormat(mGroupProductResult.getBody().getProd_base_info().getPrice()));
        mTxtService.setText(mGroupProductResult.getBody().getProd_base_info().getSender());
        if (mGroupProductResult.getBody().getCoupon_flag().getReady_to_retire_flg().equals(TongleAppConst.PRODUCT_REFUNDABLE)) {
            mTxtRetire.setVisibility(View.VISIBLE);
        }
        if (mGroupProductResult.getBody().getCoupon_flag().getExpired_refund_flg().equals(TongleAppConst.PRODUCT_REFUNDABLE)) {
            mTxtRefund.setVisibility(View.VISIBLE);
        }
        if (mGroupProductResult.getBody().getCoupon_flag().getNon_refundable_flg().equals(TongleAppConst.PRODUCT_REFUNDABLE)) {
            mTxtNoRefund.setVisibility(View.VISIBLE);
        }

        mTxtSold.setText(String.format(getString(R.string.ticket_detail_sold_txt), mGroupProductResult.getBody().getProd_base_info().getSold_qty()));
        mTxtPercent.setText(String.format(getString(R.string.ticket_detail_percent_txt), mGroupProductResult.getBody().getProd_base_info().getGood_rating_percent()));
        mTxtGoodQty.setText(String.format(getString(R.string.ticket_detail_qty_txt), mGroupProductResult.getBody().getProd_base_info().getGood_rating_qty()));

        mTxtStoreCount.setText(String.format(getResources().getString(R.string.ticket_detail_store_count_txt), mGroupProductResult.getBody().getCoupon_store_info().getStore_total_count()));

        if(mGroupProductResult.getBody().getCoupon_store_info().getStore_list().size()>0){//存在适用商户则默认显示第一条数据
            mRlyStoreAddress.setVisibility(View.VISIBLE);
            mLlyStoreLine.setVisibility(View.VISIBLE);
            mTxtStoreName.setText(mGroupProductResult.getBody().getCoupon_store_info().getStore_list().get(0).getName());
            mTxtStoreAddress.setText(mGroupProductResult.getBody().getCoupon_store_info().getStore_list().get(0).getAddress());
        }else{//没有商户则不显示
            mRlyStoreAddress.setVisibility(View.GONE);
            mLlyStoreLine.setVisibility(View.GONE);
        }

        mTxtInfoHead.setText(mGroupProductResult.getBody().getCoupon_purchase_info().getHead());
        mProInfoListAdapter = new ProInfoListAdapter(ProductTicketDetailActivity.this, mGroupProductResult.getBody().getCoupon_purchase_info().getContent_list());
        mLvInfo.setAdapter(mProInfoListAdapter);
        mTxtInfoTotal.setText(String.format(getResources().getString(R.string.ticket_detail_total_txt), FontUtils.setTwoDecimal(mGroupProductResult.getBody().getCoupon_purchase_info().getContent_total_price())));
        mTxtInfoActual.setText(String.format(getResources().getString(R.string.ticket_detail_total_txt), FontUtils.setTwoDecimal(mGroupProductResult.getBody().getCoupon_purchase_info().getContent_actually_price())));
        mTxtFooter.setText(mGroupProductResult.getBody().getCoupon_purchase_info().getFooter());

        mProNoticeListAdapter = new ProNoticeListAdapter(ProductTicketDetailActivity.this, mGroupProductResult.getBody().getCoupon_purchase_notice_list());
        mLvNotice.setAdapter(mProNoticeListAdapter);

//        mTxtOldPrice.setText(getResources().getString(R.string.RMB) + FontUtils.setTwoDecimal(mGroupProductResult.getBody().getProd_base_info().getShow_price()));
        mTxtOldPrice.setText(FontUtils.priceFormat(mGroupProductResult.getBody().getProd_base_info().getShow_price()));
        mTxtOldPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);//中间加横线
//        mTxtActualPrice.setText(FontUtils.setTwoDecimal(mGroupProductResult.getBody().getProd_base_info().getPrice()));
        mTxtActualPrice.setText(FontUtils.priceFormat(mGroupProductResult.getBody().getProd_base_info().getPrice()));
        mBtnBuy.setText(mGroupProductResult.getBody().getUi_btn_control_list().get(0).getBtn_name());
        if (mGroupProductResult.getBody().getUi_btn_control_list().get(0).getBtn_status().equals(TongleAppConst.PRODUCT_FLAG_ON_SALE)) {//按钮状态为0时，按钮状态为normal
            mBtnBuy.setEnabled(true);
        } else {//按钮状态为1、2时，按钮状态为disable
            mBtnBuy.setEnabled(false);
        }

        if (!TextUtils.isEmpty(mCollectType) && mGroupProductResult.getBody().getProd_base_info().getTrade_prod_favorited_by_me().equals(COLLECTED_BY_ME)) {//我的宝贝进入，并且我的宝贝收藏了
            //0已收藏
            mImgCollect.setBackgroundResource(R.drawable.discollect_button_style);
            bIsFavorate = true;
        } else if (mGroupProductResult.getBody().getProd_base_info().getProd_favorited_by_me().equals(COLLECTED_BY_ME)) {//商品已收藏
            mImgCollect.setBackgroundResource(R.drawable.discollect_button_style);
            bIsFavorate = true;
        }
    }

    public void addProductPicView() {
        if ((mProdPicList == null) || (mGroupProductResult.getBody().getProd_base_info().getProd_pic_url_list().size() == 0))
            return;
        mProdPicList.clear();
        try {
            for (int i = 0; i < mGroupProductResult.getBody().getProd_base_info().getProd_pic_url_list().size(); i++) {
                String url = mGroupProductResult.getBody().getProd_base_info().getProd_pic_url_list().get(i).pic_url;
                ImageView aPieceOfView = (ImageView) LayoutInflater.from(this).inflate(R.layout.banner_cell_layout, null);
                ImageLoader.getInstance().displayImage(url, aPieceOfView, ImageOptionsUtils.getOptions());
                mProdPicList.add(aPieceOfView);
            }
            mLlyBottom.removeAllViews();
            //定义viewpager下的圆点
            for (int i = 0; i < mProdPicList.size(); i++) {
                TextView bt = new TextView(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(16, 16);
                params.setMargins(10, 0, 0, 0);
                bt.setLayoutParams(params);
                bt.setBackgroundResource(R.color.main_bgcolor);
                mLlyBottom.addView(bt);
                mPagerAdapter.notifyDataSetChanged();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        mVpPicture.setAdapter(mPagerAdapter);
        mVpPicture.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int mPreviousState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (mPreSelectedBtn != null) {
                    mPreSelectedBtn.setBackgroundResource(R.color.main_bgcolor);
                }

                TextView currentBt = (TextView) mLlyBottom.getChildAt(position);
                currentBt.setBackgroundResource(R.color.topview_bgcolor);
                mPreSelectedBtn = currentBt;

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // All of this is to inhibit any scrollable container from consuming our touch events as the user is changing pages
                if (mPreviousState == ViewPager.SCROLL_STATE_IDLE) {
                    if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                        mTouchTarget = mVpPicture;
                    }
                } else {
                    if (state == ViewPager.SCROLL_STATE_IDLE || state == ViewPager.SCROLL_STATE_SETTLING) {
                        mTouchTarget = null;
                    }
                }
                mPreviousState = state;
            }
        });
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.left_arrow_btn) {
            //返回
            finish();
        } else if (v.getId() == R.id.collect_btn) {
            //收藏
            if (ToastUtils.isFastClick()) {
                return;
            } else {
                if (bIsFavorate == false) {
                    showFavDlg(v);
                } else {
                    if (mCollectType != null) {//取消收藏判断，如果是“我的宝贝”进入则取消我的宝贝
                        ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), mProductId, TongleAppConst.MYPRODUCT, cancelCollectListener(), errorListener());
                    } else {
                        ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), mProductId, TongleAppConst.PRODUCT, cancelCollectListener(), errorListener());
                    }
                }
            }
        } else if (v.getId() == R.id.QR_share_btn) {
            //二维码
            ServiceManager.doProdDetailQRRequest(TongleAppInstance.getInstance().getUserID(), mProductId, mGroupProductResult.getBody().getProd_base_info().getRelation_id(), QRListener(), errorListener());
        } else if (v.getId() == R.id.product_ticket_detail_btn_share) {
            //分享
//            showShareDialog();//分享dialog
            ShareDialogActivity.launch(ProductTicketDetailActivity.this, ShareDialogActivity.TYPE_SHARE_PRODUCT, mProductId, mGroupProductResult.getBody().getProd_base_info().getRelation_id());
        } else if (v.getId() == R.id.product_ticket_detail_lly_qty) {
            //查看评价
            toProductMore("comment");
        } else if (v.getId() == R.id.ticket_detail_rly_store_count) {
            //查看商户列表
            StoreInfoActivity.launcher(ProductTicketDetailActivity.this, mGroupProductResult.getBody().getProd_base_info().getProd_id());
        } else if (v.getId() == R.id.ticket_detail_btn_phone) {
            requestPermissionUtils=RequestPermissionUtils.getRequestPermissionUtils(ProductTicketDetailActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                @Override
                public void requestSuccess() {
                    //拨打电话
                    if(mGroupProductResult.getBody().getCoupon_store_info().getStore_list().size()>0){
                        Utils.callPhone(ProductTicketDetailActivity.this, mGroupProductResult.getBody().getCoupon_store_info().getStore_list().get(0).getPhone());
                    }
                }
                @Override
                public void requestFail() {
                    SelectDialog dialog=new SelectDialog(ProductTicketDetailActivity.this,getString(R.string.please_open_call_phone_permission));
                    dialog.show();
                }
            });
            requestPermissionUtils.checkPermissions(ProductTicketDetailActivity.this);


            //                if (!TextUtils.isEmpty(mGroupProductResult.getBody().getCoupon_store_info().getStore_list().get(0).getPhone())) {
            //                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
            //                            + mGroupProductResult.getBody().getCoupon_store_info().getStore_list().get(0).getPhone()));
            //                    ProductTicketDetailActivity.this.startActivity(intent);
            //                }
        } else if (v.getId() == R.id.ticket_detail_lly_detail) {
            //图文详情
            toProductMore("");
        } else if (v.getId() == R.id.product_ticket_detail_btn_buy_now) {
            //立即购买
            if (mGroupProductResult.getBody().getUi_btn_control_list().get(0).getBtn_status().equals(TongleAppConst.PRODUCT_FLAG_ON_SALE)) {
                mParam.mstore_id = mGroupProductResult.getBody().getProd_base_info().getMstore_id();
                mParam.post_id = mGroupProductResult.getBody().getProd_base_info().getPost_id();
                mParam.product_id = mGroupProductResult.getBody().getProd_base_info().getProd_id();
                mParam.relation_id = mGroupProductResult.getBody().getProd_base_info().getRelation_id();
                mParam.price = mGroupProductResult.getBody().getProd_base_info().getPrice() + "";
                mParam.product_name = mGroupProductResult.getBody().getProd_base_info().getProd_name();
                mParam.order_no = "";
                ProdSpecListInfo prodSpecListInfo = new ProdSpecListInfo();
                prodSpecListInfo.prod_spec_id = mGroupProductResult.getBody().getProd_base_info().getSpec_detail_list().get(0).prod_spec_id;
                prodSpecListInfo.prod_spec_name = mGroupProductResult.getBody().getProd_base_info().getSpec_detail_list().get(0).prod_spec_name;
                prodSpecListInfo.spec_detail_id = mGroupProductResult.getBody().getProd_base_info().getSpec_detail_list().get(0).spec_detail_list.get(0).spec_detail_id;
                prodSpecListInfo.spec_detail_name = mGroupProductResult.getBody().getProd_base_info().getSpec_detail_list().get(0).spec_detail_list.get(0).spec_detail_name;
                mParam.prod_spec_list = new ArrayList<>();
                mParam.prod_spec_list.add(prodSpecListInfo);
                TicketOrderSubmitActivity.launcher(ProductTicketDetailActivity.this, mParam);
            }
        } else if (v.getId() == R.id.product_ticket_detail_tv_detail) {
            //详情
            toProductMore("");
        }
    }

    /**
     * 前往图文详情
     */
    public void toProductMore(String type) {
        if (!TextUtils.isEmpty(mProductId)) {
            CollectProductMoreActivity.launcher(this, mProductId, mStoreID, bIsFavorate,false, intoType, mGroupProductResult.getBody().getProd_base_info().getRelation_id(), type, 10001);
        }
    }

    /**
     * 收藏弹出view
     *
     * @param v
     */
    public void showFavDlg(View v) {
        View view = LayoutInflater.from(ProductTicketDetailActivity.this).inflate(R.layout.prod_collect_layout, null);
        // 创建PopupWindow实例
        mPopupWindow = new PopupWindow(view, WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT, false);
        //设置可以获取焦点
        mPopupWindow.setFocusable(true);
        //防止弹出菜单获取焦点之后，点击activity的其他组件没有响应
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        //防止虚拟软键盘被弹出菜单遮住
        mPopupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //在底部显示
        mPopupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);

        // 点击其他地方消失
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mPopupWindow != null && mPopupWindow.isShowing()) {
                    mPopupWindow.dismiss();
                    mPopupWindow = null;
                }
                return false;
            }
        });

        //弹出dialog里面的控件
        mCheckBox[0] = (CheckBox) view.findViewById(R.id.my_favorite);
        mCheckBox[1] = (CheckBox) view.findViewById(R.id.my_product);
        Button cancle = (Button) view.findViewById(R.id.cancle);
        Button collect = (Button) view.findViewById(R.id.collect);
        //复选框点击以及确定取消点击请求的数据
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.dismiss();

            }
        });
        collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FavoriteListItem> favList = new ArrayList<FavoriteListItem>();
                for (int i = 0; i < 2; i++) {
                    if (mCheckBox[i].isChecked()) {
                        FavoriteListItem item = new FavoriteListItem();
                        if (i == 0) {
                            item.collection_type = TongleAppConst.PRODUCT;// 0 商品
                        } else {
                            item.collection_type = TongleAppConst.MYPRODUCT;// 4 宝贝
                        }
                        item.key_value = mProductId;
                        favList.add(item);
                    }
                }
                if (favList.isEmpty())
                    ToastUtils.toastShort(getString(R.string.no_favorite_select));
                else {
                    ServiceManager.doMStoreCollectRequest(TongleAppInstance.getInstance().getUserID(), favList, mGroupProductResult.getBody().getProd_base_info().getRelation_id(), collectListener(), errorListener());
                }
            }
        });
    }

    /**
     * 功能：收藏网络响应成功，返回数据
     *
     * @return
     */
    private Response.Listener collectListener() {
        return new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mImgCollect.setBackgroundResource(R.drawable.discollect_button_style);
                    ToastUtils.toastShort(getString(R.string.favorite_success));
                    bIsFavorate = true;
                    if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                        MProductFragment.UPDATE_LIST_FLAG = false;  //从收藏页进来的，表示刚刚取消了收藏之后又重新收藏。回收藏页时不需要刷新
                    } else if (intoType == TongleAppConst.MYBABY_INTO) {
                        SettingMyBoxBabyActivity.UPDATE_LIST_FLAG = false;  //从我的宝贝页面进入，表示刚刚取消了收藏之后又重新收藏。回宝贝列表页时不需刷新
                    } else {
                        MProductFragment.MPRODUCTFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新M品列表
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                mPopupWindow.dismiss();
            }
        };
    }

    /**
     * 功能：取消收藏请求网络响应成功，返回数据
     *
     * @return
     */
    private Response.Listener cancelCollectListener() {
        return new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mImgCollect.setBackgroundResource(R.drawable.collect_button_style);
                    ToastUtils.toastShort(getString(R.string.favorite_cancel));
                    bIsFavorate = false;
                    if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                        MProductFragment.UPDATE_LIST_FLAG = true;  //从收藏页进来的，表示刚刚取消了收藏。回收藏页时需要刷新
                    } else if (intoType == TongleAppConst.MYBABY_INTO) {
                        SettingMyBoxBabyActivity.UPDATE_LIST_FLAG = true;  //从我的宝贝页面进入，表示刚刚取消了收藏。回宝贝列表页时需刷新
                    } else {
                        MProductFragment.MPRODUCTFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新M品列表
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：商品二维码网络响应成功，返回数据
     *
     * @return
     */
    private Response.Listener QRListener() {
        return new Response.Listener<ProdDetailQRResult>() {
            @Override
            public void onResponse(ProdDetailQRResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
//                    ShowQRCodeDialog dialog = new ShowQRCodeDialog(ProductTicketDetailActivity.this,response.getBody().getProd_qr_code_url(),response.getBody().getLatest_relation_id(),TongleAppConst.STRRELATE_PRODUCT);
//                    dialog.show();
                    ShowQRCodeActivity.launcher(ProductTicketDetailActivity.this,response.getBody().getProd_qr_code_url(),response.getBody().getLatest_relation_id(),TongleAppConst.STRRELATE_PRODUCT);
//                    View view = LayoutInflater.from(ProductTicketDetailActivity.this).inflate(R.layout.expert_popwindow_layout, null);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductTicketDetailActivity.this, R.style.Dialog_FS);
//                    builder.setView(view);
//                    final AlertDialog alert = builder.create();
//                    alert.setCanceledOnTouchOutside(true);
//                    ImageView coverImage = (ImageView) view.findViewById(R.id.qr_user);
//
//                    view.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alert.dismiss();
//                        }
//                    });
//
//                    ImageLoader.getInstance().displayImage(response.getBody().getProd_qr_code_url(), coverImage, ImageOptionsUtils.getOptions());
//                    alert.show();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 用来完成图片切换的任务
     */
    private class ViewPagerTask implements Runnable {
        public void run() {
            //实现我们的操作
            //改变当前页面
            currentItem = (currentItem + 1) % mProdPicList.size();
            //Handler来实现图片切换
            handler.obtainMessage().sendToTarget();
        }
    }

    private Handler handler = new Handler() {

        public void handleMessage(Message msg) {
            //设定viewPager当前页面
            mVpPicture.setCurrentItem(currentItem);
        }
    };

    /**
     * 从图文详情返回后刷新顶部收藏图标
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10001) {
            if (resultCode == RESULT_OK) {
                boolean flag = data.getBooleanExtra("bIsFavorate", false);
                //如果传到图文详情的图标和传回的标识不同，说明在图文详情页做了收藏或者取消收藏的操作。需要改变头部收藏图标
                if (flag != bIsFavorate) {
                    bIsFavorate = flag;
                    //如果在图文详情页面取消了收藏，回到收藏/宝贝列表页都需要刷新列表
                    if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                        MProductFragment.UPDATE_LIST_FLAG = !MProductFragment.UPDATE_LIST_FLAG;
                    } else if (intoType == TongleAppConst.MYBABY_INTO) {
                        SettingMyBoxBabyActivity.UPDATE_LIST_FLAG = !SettingMyBoxBabyActivity.UPDATE_LIST_FLAG;
                    } else {
                        MProductFragment.MPRODUCTFRAGMENT_UPDATE_FLAG = true;
                    }
                    mHandler.sendEmptyMessage(0);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /***
     * 停止图片切换
     */
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        //停止图片切换
        scheduledExecutorService.shutdown();
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ViewPagerTask(), 1, 3, TimeUnit.SECONDS);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                y = ev.getY();
                if (isScrollTobottom) {
                    isFlip = true;//翻页
                } else {
                    isFlip = false;//不翻页
                }
                break;
            case MotionEvent.ACTION_UP:
                if (isScrollTobottom) {
                    if (ev.getY() - y < 0 && Math.abs(ev.getY() - y) > SCROLL_DISTANCE && isFlip) {
                        toProductMore("");
                    }
                } else {
                    isFlip = false;
                }
                break;
            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode,permissions,paramArrayOfInt);
    }

}
