package com.ilingtong.library.tongle.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectExpertDetailActivity;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.MStoreActivity;
import com.ilingtong.library.tongle.activity.ProdIntegralActivity;
import com.ilingtong.library.tongle.activity.ProductTicketDetailActivity;
import com.ilingtong.library.tongle.model.HomeModel;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/5/22
 * Time: 10:45
 * Email: leishuai@isoftstone.com
 * Dest:首页banner adapter
 */
public class HomePagerAdapter extends PagerAdapter {
    private ArrayList<View> bannerListView;
    private Activity mContext;
    private HomeModel homeModel;
    public HomePagerAdapter(Activity context,ArrayList<View> bannerListView,HomeModel homeModel){
        this.mContext = context;
        this.homeModel = homeModel;
        this.bannerListView = bannerListView;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return bannerListView.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(bannerListView.get(position));//删除页卡
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {    //这个方法用来实例化页卡
        container.addView(bannerListView.get(position), 0);//添加页卡
        View view = bannerListView.get(position);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = homeModel.bannerList.get(position).promotion_mode;
                //关联类型是商品
                if (type.equals(TongleAppConst.ACTIVITY_PRODUCT)) {
                    if (TongleAppConst.YES.equals(homeModel.bannerList.get(position).coupon_flag)){
                        ProductTicketDetailActivity.launch(mContext, homeModel.bannerList.get(position).object_id, TongleAppConst.ACTIONID_SALES, homeModel.bannerList.get(position).relation_id, "", "", TongleAppConst.HOME_INTO);
                    }else {
                        CollectProductDetailActivity.launch(mContext,homeModel.bannerList.get(position).object_id,TongleAppConst.ACTIONID_SALES,homeModel.bannerList.get(position).relation_id,"","",TongleAppConst.HOME_INTO);
                    }
//                    Intent i = new Intent(mContext, CollectProductDetailActivity.class);
//                    i.putExtra("product_id", homeModel.bannerList.get(position).object_id);
//                    i.putExtra("tag","homefragment");
//                    i.putExtra("relationId", homeModel.bannerList.get(position).relation_id);
//                    i.putExtra(TongleAppConst.INTO_TYPE,TongleAppConst.HOME_INTO);
//                    mContext.startActivity(i);
                    //关联类型是会员
                } else if (type.equals(TongleAppConst.ACTIVITY_MEMBER)) {
                    Intent i = new Intent(mContext, CollectExpertDetailActivity.class);
                    i.putExtra("user_id", homeModel.bannerList.get(position).object_id);
                    i.putExtra(TongleAppConst.INTO_TYPE,TongleAppConst.HOME_INTO);
                    //mContext.startActivity(i);
                    mContext.startActivityForResult(i,10002);
                    //关联类型是魔店
                } else if (type.equals(TongleAppConst.ACTIVITY_MSTORE)) {
                    Intent i = new Intent(mContext, MStoreActivity.class);
                    i.putExtra("mstore_id", homeModel.bannerList.get(position).object_id);
                    i.putExtra(TongleAppConst.INTO_TYPE,TongleAppConst.HOME_INTO);
                    //mContext.startActivity(i);
                    mContext.startActivityForResult(i,10002);
                    //关联类型是帖子
                } else if (type.equals(TongleAppConst.ACTIVITY_FORUM)) {
                    Intent i = new Intent(mContext, CollectForumDetailActivity.class);
                    i.putExtra("post_id", homeModel.bannerList.get(position).object_id);
                    i.putExtra(TongleAppConst.INTO_TYPE,TongleAppConst.HOME_INTO);
                    mContext.startActivity(i);
                    //关联类型是链接
                } else if (type.equals(TongleAppConst.ACTIVITY_LINK)){
                    String myurl = homeModel.bannerList.get(position).object_id + "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken()+"&app_inner_no="+TongleAppInstance.getInstance().getApp_inner_no();
                    Intent expertIntent = new Intent(mContext, ProdIntegralActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("prod_url", myurl);
                    bundle.putString("myTitle", homeModel.bannerList.get(position).promotion_title);
                    expertIntent.putExtras(bundle);
                    mContext.startActivity(expertIntent);
                }
            }
        });
        return bannerListView.get(position);
    }
}
