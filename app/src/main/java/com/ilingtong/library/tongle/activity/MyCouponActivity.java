package com.ilingtong.library.tongle.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.MyCouponListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.MyCouponInfo;
import com.ilingtong.library.tongle.protocol.MyCouponInfoResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/8
 * Time: 10:27
 * Email: liuting@ilingtong.com
 * Desc:我的优惠券
 */
public class MyCouponActivity extends BaseActivity implements View.OnClickListener, XListView.IXListViewListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标
    private XListView mLvCoupon;//我的优惠券列表
    private RelativeLayout mRlyReplace;//没有优惠券时

    private MyCouponInfoResult mMyCouponInfoResult;//我的优惠券列表返回结果
    private List<MyCouponInfo> mCouponList;//优惠券列表
    private Dialog mDialog;//加载对话框
    private MyCouponListAdapter mMyCouponListAdapter;//优惠券Adapter

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xlistview_common_layout);
        initView();
        getData();
    }

    public void initView() {
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle.setText(getResources().getString(R.string.my_coupon_top_name));
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);

        mLvCoupon = (XListView) findViewById(R.id.xlistview_common_lv_list);
        mRlyReplace = (RelativeLayout) findViewById(R.id.xlistview_common_rly_replace);
        mLvCoupon.setPullLoadEnable(false);
        mLvCoupon.setPullRefreshEnable(false);
        mLvCoupon.setXListViewListener(this, 0);
        mCouponList = new ArrayList<>();

        mDialog = DialogUtils.createLoadingDialog(MyCouponActivity.this);
        mDialog.setCancelable(false);
    }

    public void getData() {
        mMyCouponInfoResult = new MyCouponInfoResult();
//        mMyCouponInfoResult= (MyCouponInfoResult)TestInterface.parseJson(MyCouponInfoResult.class, "6014.txt");
        ServiceManager.doMyCouponListRequest(TongleAppInstance.getInstance().getUserID(), successListener(), errorListener());
    }

    /**
     * 功能：提交订单网络响应成功，返回数据
     *
     * @return
     */
    private Response.Listener successListener() {
        return new Response.Listener<MyCouponInfoResult>() {
            @Override
            public void onResponse(MyCouponInfoResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mMyCouponInfoResult = response;
                    initData();
                } else {
                    ToastUtils.toastLong(response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     *
     * @return
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                mDialog.dismiss();
            }
        };
    }

    public void initData() {
        if (mMyCouponInfoResult.getBody().getVoucher().size() > 0) {//有数据时显示数据列表
            mRlyReplace.setVisibility(View.GONE);
            mLvCoupon.setVisibility(View.VISIBLE);
            mCouponList.addAll(mMyCouponInfoResult.getBody().getVoucher());
            mMyCouponListAdapter = new MyCouponListAdapter(MyCouponActivity.this, mCouponList);
            mLvCoupon.setAdapter(mMyCouponListAdapter);
        } else {//无数据是提示没有相关数据
            mRlyReplace.setVisibility(View.VISIBLE);
            mLvCoupon.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.left_arrow_btn:
//                finish();
//                break;
//            default:break;
//        }
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        }
    }

    @Override
    public void onRefresh(int id) {

    }

    @Override
    public void onLoadMore(int id) {

    }
}
