package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/31
 * Time: 14:43
 * Email: jqleng@isoftstone.com
 * Desc:广告位列表中的元素类
 */
public class PromotionListItem implements Serializable {
    public String promotion_no;
    public String promotion_mode;
    public String object_id;
    public String mobile_pic_url;
    public String relation_id;
    public String coupon_flag;
    public String promotion_title;
}
