package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.VersionResult;
import com.ilingtong.library.tongle.service.VersionService;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * User: lengjiqiang
 * Date: 2015/6/19
 * Time: 17:28
 * Email: jqleng@isoftstone.com
 * Desc: 我的--》我的设置--》关于通乐
 */
public class SettingAboutActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;//标题栏
    private ImageView left_arrow_btn;

    private Button agreementBtn;//版权申报
    private Button updateBtn;//版本更新

    private TextView last_ver_txt;//最新版本号
    private TextView now_ver_txt;//当前版本号

    String mVersionCode, mVersionName, hasUpdate, url;
    private ProgressBar pb;
    private TextView tv;
    public static int loading_process;
    private String[] needPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};//存储权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_about_layout);
        initView();
        doRequest();
    }

    public void initView() {
        loading_process = 0;
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        agreementBtn = (Button) findViewById(R.id.agreement_btn);
        updateBtn = (Button) findViewById(R.id.update_btn);
        last_ver_txt = (TextView) findViewById(R.id.last_ver_txt);
        now_ver_txt = (TextView) findViewById(R.id.now_ver_txt);

        left_arrow_btn.setOnClickListener(this);
        agreementBtn.setOnClickListener(this);
        updateBtn.setOnClickListener(this);
        //标题栏
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(R.string.app_name);

        mVersionCode = getVersionCode(this);
        mVersionName = getVersionName(this);
        now_ver_txt.setText(mVersionName);
    }

    //请求获取
    public void doRequest() {
        //内部编号，01代表通乐
        ServiceManager.doVersionRequest(TongleAppConst.ANDROID_PHONE, mVersionCode, TongleAppInstance.getInstance().getApp_inner_no(), successListener(), errorListener());
    }

    /**
     * 该页中的所有点击事件处理方法
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        }
        if (v.getId() == R.id.agreement_btn) {
            Intent intent = new Intent(this, SettingAgreementActivity.class);
            startActivity(intent);
        }
        if (v.getId() == R.id.update_btn) {
//            if ( hasUpdate.equals("1") ) {
//                ToastUtils.toastShort(getString(R.string.setting_about_no_update));
//            }else{
            //不是最新版本，点击按钮。用handler发送请求.（不check当前版本，直接下载更新服务器上的apk）
            requestPermissionUtils=RequestPermissionUtils.getRequestPermissionUtils(SettingAboutActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                @Override
                public void requestSuccess() {
                    Message msg = BroadcastHandler.obtainMessage();
                    BroadcastHandler.sendMessage(msg);
                }

                @Override
                public void requestFail() {
                    SelectDialog dialog=new SelectDialog(SettingAboutActivity.this,getString(R.string.please_open_the_storage_permission));
                    dialog.show();
                }
            });
            requestPermissionUtils.checkPermissions(SettingAboutActivity.this);

        //}
    }

}

    /**
     * 获取本地应用版本号
     */
    public String getVersionCode(Context context) {
        String versionCode = "0.0";
        int iVer = 0;
        try {
            // 获取软件版本号，对应AndroidManifest.xml下android:versionCode
            iVer = context.getPackageManager().getPackageInfo(TongleAppInstance.getInstance().getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionCode = String.valueOf(iVer);
        return versionCode;
    }

    /**
     * 获取本地应用版本名称
     */
    public String getVersionName(Context context) {
        String versionName = "1.0";
        try {
            versionName = context.getPackageManager().getPackageInfo(TongleAppInstance.getInstance().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    /**
     * 功能：请求网络响应成功，返回数据
     */
    private Response.Listener<VersionResult> successListener() {
        return new Response.Listener<VersionResult>() {
            @Override
            public void onResponse(VersionResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    if (response.getBody() != null) {
                        last_ver_txt.setText(response.getBody().getRecently_version_no() + "");
                        hasUpdate = response.getBody().getHas_update();
                        url = response.getBody().getRecently_version_link();
                        if (hasUpdate.equals("1")) {
                            updateBtn.setBackgroundResource(R.drawable.update_forbid);
                        }
                    }
                } else {
                    ToastUtils.toastLong(response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(volleyError.toString());
            }
        };
    }

    //创建handler
    private Handler BroadcastHandler = new Handler() {
        public void handleMessage(Message msg) {
            Beginning();
        }
    };

    //开始加载文件
    public void Beginning() {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(SettingAboutActivity.this).inflate(
                R.layout.layout_loadapk, null);
        pb = (ProgressBar) ll.findViewById(R.id.down_pb);
        tv = (TextView) ll.findViewById(R.id.tv);
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingAboutActivity.this);
        builder.setView(ll);
        builder.setTitle(getString(R.string.common_update_alert_title));
        builder.setNegativeButton(getString(R.string.common_update_alert_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(SettingAboutActivity.this, VersionService.class);
                        startService(intent);
                        dialog.dismiss();
                    }
                });

        builder.show();
        //开启线程根据url请求apk
        new Thread() {
            public void run() {
                loadFile(url);
            }
        }.start();
    }

    //下载apk
    public void loadFile(String url) {
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);
        HttpResponse response;
        try {
            response = client.execute(get);

            HttpEntity entity = response.getEntity();
            float length = entity.getContentLength();

            InputStream is = entity.getContent();
            FileOutputStream fileOutputStream = null;
            if (is != null) {
                File file = new File(Environment.getExternalStorageDirectory(),
                        "Tongle.apk");
                fileOutputStream = new FileOutputStream(file);
                byte[] buf = new byte[1024];
                int ch = -1;
                float count = 0;
                while ((ch = is.read(buf)) != -1) {
                    fileOutputStream.write(buf, 0, ch);
                    count += ch;
                    sendMsg(1, (int) (count * 100 / length));
                }
            }
            sendMsg(2, 0);
            fileOutputStream.flush();
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        } catch (Exception e) {
            sendMsg(-1, 0);
        }
    }

    private void sendMsg(int flag, int c) {
        Message msg = new Message();
        msg.what = flag;
        msg.arg1 = c;
        handler.sendMessage(msg);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {// 定义一个Handler，用于处理下载线程与UI间通讯
            if (!Thread.currentThread().isInterrupted()) {
                if (msg.what == 1) {
                    //进度条
                    pb.setProgress(msg.arg1);
                    loading_process = msg.arg1;
                    tv.setText(String.format(getString(R.string.common_update_loading_txt),loading_process));
                }else if (msg.what == 2){
                    //安装APK
                    File file = new File(Environment.getExternalStorageDirectory(), "Tongle.apk");
                    installApk(file);
                } else if (msg.what == -1) {
                    //报错
                    ToastUtils.toastShort(msg.getData().getString("error"));
                }
            }
            super.handleMessage(msg);
        }
    };

    //安装APK
    private void installApk(File file) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
