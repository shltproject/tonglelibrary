package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/24
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class CatrReturnInfo implements Serializable {
    public String amount_payable;     //订单总金额减去优惠金额后的应付金额     update  on 2017/9/7
    public String total_fee;
    public String total_tariff;
    public String total_goods_count;
    public String total_goods_price;
    public String vouchers_number_id;       // add at 2016/04/06  优惠券号
    public String vouchers_name;      // add at 2016/04/06  券名
    public String money;      // add at 2016/04/06   优惠金额
    public String use_conditions;      // add at 2016/04/06   使用条件
    public String order_total_amount;      // add at 2016/04/06   订单总金额 update on 2017/9/7
    public ArrayList<ShopCartListItem> my_shopping_cart;

    @Override
    public String toString() {
        return "total_amount:" + amount_payable + "\r\n" +
                "total_fee:" + total_fee + "\r\n" +
                "total_tariff:" + total_tariff + "\r\n" +
                "total_goods_count:" + total_goods_count + "\r\n" +
                "total_goods_price:" + total_goods_price + "\r\n" +
                "my_shopping_cart:" + my_shopping_cart;
    }
}
