package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/13
 * Time: 18:18
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class DefaultAddressInfo implements Serializable {
    private ArrayList<AddressListItem> my_address_list;

    public ArrayList getMy_address_list() {
        return my_address_list;
    }

    @Override
    public String toString() {
        return "my_address_list:" + my_address_list;
    }
}
