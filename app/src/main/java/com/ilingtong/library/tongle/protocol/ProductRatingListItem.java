package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/26
 * Time: 10:08
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ProductRatingListItem implements Serializable {
    public String rating_doc_no;
    public String level;
    public String rating_date;
    public String user_nick_name;
    public String head_photo_url;
    public String memo;
}
