package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: syc
 * Date: 2015/6/10
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc:  UserOrdersResult--->
 */
public class UserOrdersInfo implements Serializable {

    private String data_total_count;
    private ArrayList<OrderListItemData> order_list;

    public String getData_total_count() {
        return data_total_count;
    }

    public ArrayList<OrderListItemData> getOrder_list() {
        return order_list;
    }

    @Override
    public String toString() {
        return "data_total_count:" + data_total_count + "\r\n" +
                "order_list:" + order_list;
    }
}
