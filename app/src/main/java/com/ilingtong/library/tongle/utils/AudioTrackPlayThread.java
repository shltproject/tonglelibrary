package com.ilingtong.library.tongle.utils;

import android.media.AudioFormat;
import android.os.Handler;

public class AudioTrackPlayThread implements Runnable{
	final int EVENT_PLAY_OVER = 0x100;
	public static boolean flag;

	byte []data;
	Handler mHandler;
	int repeatTimes;

	public AudioTrackPlayThread(byte[] data, Handler handler, int repeatTimes) {
		// TODO Auto-generated constructor stub
		this.data = data;
		mHandler = handler;
		this.repeatTimes = repeatTimes;
	}

	public void run() {
		flag = true;
		if (data == null || data.length == 0){
			return ;
		}
		// MyAudioTrack:   对AudioTrack进行简单封装的类
		MyAudioTrack myAudioTrack = new MyAudioTrack(44100,
				AudioFormat.CHANNEL_OUT_MONO,
				AudioFormat.ENCODING_PCM_16BIT);

		myAudioTrack.init();
		int playSize = myAudioTrack.getPrimePlaySize();
		int index = 0;
		int offset = 0;
		while(flag && repeatTimes >=0){
			try {
				Thread.sleep(0);
				offset = index * playSize;
				if (offset >= data.length){
					offset = 0;
					index = 0;
					continue;
				}
				myAudioTrack.playAudioTrack(data, offset, playSize);
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			index++;
			repeatTimes --;
		}
		myAudioTrack.release();
		mHandler.sendEmptyMessage(EVENT_PLAY_OVER);
	}

}