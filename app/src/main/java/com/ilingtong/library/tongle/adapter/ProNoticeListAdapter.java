package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.CouponPurchaseNoticeInfo;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/17
 * Time: 9:25
 * Email: liuting@ilingtong.com
 * Desc:商品团购购买须知列表Adapter
 */
public class ProNoticeListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<CouponPurchaseNoticeInfo> mLvNotice;
//    private CouponPurchaseNoticeInfo mNoticeItem = new CouponPurchaseNoticeInfo();
    private Context mContext;

    public ProNoticeListAdapter(Context context, List<CouponPurchaseNoticeInfo> noticeList) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvNotice = noticeList;
    }

    @Override
    public int getCount() {
        return mLvNotice.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvNotice.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.ticket_notice_list_item, null);
            holder = new ViewHolder();
            holder.noticeTitle = (TextView) view.findViewById(R.id.ticket_notice_tv_title);
            holder.noticeDesc = (TextView) view.findViewById(R.id.ticket_notice_tv_desc);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        CouponPurchaseNoticeInfo mNoticeItem = (CouponPurchaseNoticeInfo) getItem(position);
        holder.noticeTitle.setText(mNoticeItem.getTitle());
        holder.noticeDesc.setText(mNoticeItem.getDesc());

        return view;
    }

    static class ViewHolder {
        TextView noticeTitle;//须知项目名称
        TextView noticeDesc;//须知项目内容
    }
}

