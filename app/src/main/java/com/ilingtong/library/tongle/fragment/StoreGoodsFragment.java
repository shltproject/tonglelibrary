package com.ilingtong.library.tongle.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.adapter.StoreGoodsGridAdapter;
import com.ilingtong.library.tongle.protocol.StoreGoodsInfo;
import com.ilingtong.library.tongle.protocol.StoreGoodsListResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * Package:com.ilingtong.library.tongle.fragment
 * author:liuting
 * Date:2017/1/11
 * Desc:店铺主题商品列表展示，全部商品和热销商品共用，根据传入的theme_id和theme_type区分
 */

public class StoreGoodsFragment extends LazyFragment implements AbsListView.OnScrollListener {
    private String mThemeId;//主题id;
    private String mThemeType;//主题类型
    private String mStoreId;//店铺id
    private ArrayList<StoreGoodsInfo> listGoods;//商品列表
    private boolean loadMoreFlag;//加载更多标志
    private boolean isLoadEnd;//是否到达底部
    private StoreGoodsGridAdapter storeGoodsGridAdapter;//商品列表Adapter
    private boolean isPrepared;// 标志位，标志已经初始化完成
    private Dialog dialog;//提示Dialog
    private GridView gvList;//商品网格列表
    private RelativeLayout rlyReplace;//无相关信息时的替代界面
    private static final int NO_DATA = 0;//无数据
    private static final int REFRESH_LIST = 1;//刷新，获取初始数据
    private static final int LOAD_MORE = 2;//加载更多
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case NO_DATA://没有数据显示无相关数据
                    rlyReplace.setVisibility(View.VISIBLE);
                    gvList.setVisibility(View.GONE);
                    break;
                case REFRESH_LIST://刷新有数据显示列表
                    rlyReplace.setVisibility(View.GONE);
                    gvList.setVisibility(View.VISIBLE);
                    storeGoodsGridAdapter.setItems(listGoods);
                    break;
                case LOAD_MORE://加载更多
                    storeGoodsGridAdapter.addItems(listGoods);
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * 创建StoreGoodsFragment
     *
     * @param theme_id   主题id
     * @param theme_type 主题类型
     * @param store_id   店铺id
     * @return StoreGoodsFragment
     */
    public static StoreGoodsFragment newInstance(String theme_id, String theme_type, String store_id) {
        Bundle args = new Bundle();
        args.putString("theme_id", theme_id);
        args.putString("theme_type", theme_type);
        args.putString("store_id", store_id);
        StoreGoodsFragment fragment = new StoreGoodsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store_goods_layout, null);
        mThemeId = getArguments().getString("theme_id");
        mThemeType = getArguments().getString("theme_type");
        mStoreId = getArguments().getString("store_id");
        initView(view);
        isPrepared = true;
        lazyLoad();
        return view;
    }

    /**
     * 初始化控件
     *
     * @param view view
     */
    public void initView(View view) {
        listGoods = new ArrayList<>();
        loadMoreFlag = false;
        isLoadEnd = false;
        dialog = DialogUtils.createLoadingDialog(getActivity());
        dialog.setCancelable(true);
        storeGoodsGridAdapter = new StoreGoodsGridAdapter(getActivity(), new StoreGoodsGridAdapter.IOnItemClickListener() {
            @Override
            public void onItemClick(View view, String goods_id) {
                //跳转到商品详情页
                CollectProductDetailActivity.launch(getActivity(), goods_id, TongleAppConst.ACTIONID_MSTORE, "", mStoreId, "");
            }
        });
        gvList = (GridView) view.findViewById(R.id.store_goods_gv_list);
        gvList.setAdapter(storeGoodsGridAdapter);
        gvList.setOnScrollListener(this);
        rlyReplace = (RelativeLayout) view.findViewById(R.id.rl_replace);
    }

    /**
     * 请求数据
     *
     * @param show_order 显示顺序，初始为空，翻页时为最后一项返回的show_order
     * @param goods_id   商品id，初始为空，翻页时为最后一项返回的goods_id
     */
    public void doRequest(final String show_order, final String goods_id) {
        showDialog();
        ServiceManager.getStoreProductList(mThemeId, mThemeType, mStoreId, show_order, goods_id, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, new Response.Listener<StoreGoodsListResult>() {
            @Override
            public void onResponse(StoreGoodsListResult storeGoodsListResult) {
                dismissDialog();
                if (TongleAppConst.SUCCESS.equals(storeGoodsListResult.getHead().getReturn_flag())) {
                    if (TextUtils.isEmpty(show_order) && TextUtils.isEmpty(goods_id)) {//初始获取数据
                        if (listGoods != null) {
                            listGoods.clear();
                        }
                        if (storeGoodsListResult.getBody().getStore_all_goods_info().getData_total_count() <= 0) {//没有数据显示无相关数据
                            handler.sendEmptyMessage(NO_DATA);
                        } else {//有数据显示列表
                            listGoods = storeGoodsListResult.getBody().getStore_all_goods_info().getStore_goods_list();
                            handler.sendEmptyMessage(REFRESH_LIST);
                        }
                    } else {//加载更多
                        listGoods = storeGoodsListResult.getBody().getStore_all_goods_info().getStore_goods_list();
                        handler.sendEmptyMessage(LOAD_MORE);
                    }
                    //判断是否可以加载更多，如果返回的列表数小于请求的列表数，说明加载完成，否则继续加载
                    if (storeGoodsListResult.getBody().getStore_all_goods_info().getStore_goods_list().size() < Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                        loadMoreFlag = false;
                    } else {
                        loadMoreFlag = true;
                    }
                } else {//加载失败
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + storeGoodsListResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dismissDialog();
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && isLoadEnd) {
            // 判断是否已加载所有数据
            if (loadMoreFlag) {//未加载完所有数据，加载数据，并且还原isLoadEnd值为false，重新定位列表底部
                doRequest(listGoods.get(listGoods.size() - 1).getShow_order(), listGoods.get(listGoods.size() - 1).getGoods_id());
                isLoadEnd = false;
            } else {//加载完了所有的数据
                ToastUtils.toastShort(getString(R.string.common_list_end));
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
            View lastVisibleItemView = gvList.getChildAt(gvList.getChildCount() - 1);
            //滑到列表底部
            if (lastVisibleItemView != null && lastVisibleItemView.getBottom() == gvList.getHeight()) {
                isLoadEnd = true;
            } else {
                isLoadEnd = false;
            }
        }
    }

    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        } else {
            doRequest("", "");
        }
    }

    /**
     * dismiss dialog
     */
    public void dismissDialog(){
        if (dialog!=null&&dialog.isShowing()){
            dialog.dismiss();
        }
    }

    /**
     * 显示Dialog
     */
    public void showDialog(){
        if (dialog!=null&&!dialog.isShowing()){
            dialog.show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissDialog();
    }
}
