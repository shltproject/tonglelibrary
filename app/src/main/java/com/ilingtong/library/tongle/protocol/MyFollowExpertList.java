package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/5/18.
 * mail: wuqian@ilingtong.com
 * Description:1012接口的body
 * use by 1012接口
 */
public class MyFollowExpertList implements Serializable {
    public String data_total_count;
    public ArrayList<FriendListItem> user_follow_friend_list;  //关注人列表
}
