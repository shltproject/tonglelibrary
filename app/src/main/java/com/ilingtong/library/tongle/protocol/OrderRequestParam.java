package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/13
 * Time: 16:57
 * Email: jqleng@isoftstone.com
 * Desc: 订单请求参数类
 */
public class OrderRequestParam implements Serializable {
    public String user_id;
    public String order_status;
    public String order_date_from;
    public String order_date_to;
    public String order_no;
    public String forward;
    public String fetch_count;
}
