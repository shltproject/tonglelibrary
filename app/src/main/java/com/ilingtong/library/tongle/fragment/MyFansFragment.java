package com.ilingtong.library.tongle.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.FansAdaper;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.FriendListItem;
import com.ilingtong.library.tongle.protocol.MyFansInfo;
import com.ilingtong.library.tongle.protocol.MyFansResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/25
 * Time: 10:45
 * Email: leishaui@isoftstone.com
 * Dest:  我的粉丝
 */
public class MyFansFragment extends BaseFragment implements XListView.IXListViewListener {
    private XListView mExpertList;
    private FansAdaper mExpertListAdaper;
    private ProgressDialog pDlg;
    private MyFansInfo fans;
    private ArrayList<FriendListItem> user_fans_list = new ArrayList<>();
    private boolean flag = true;
    private boolean clearFlag=true;//清空标志,true为清空

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    mExpertListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.collect_forum_layout, container, false);
        initView(mainView);
        user_fans_list.clear();
        updateListView();
        doRequest();
        flag = true;
        return mainView;
    }

    public void initView(View mainView) {
        if (fans == null) {
            fans = new MyFansInfo();
        }
        //创建并弹出“加载中Dialog”
        pDlg = new ProgressDialog(getActivity());
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.loading));
        pDlg.show();
        mExpertList = (XListView) mainView.findViewById(R.id.product_listview);
    }

    public void doRequest() {
        //请求数据
        ServiceManager.mineFanstRequest("", "",TongleAppConst.FETCH_COUNT, successListener(), errorListener());
    }

    @Override
    public void onRefresh(int id) {
        clearFlag=true;
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {
        //加载更多
        if (flag) {
            clearFlag=false;
            ServiceManager.mineFanstRequest( user_fans_list.get(user_fans_list.size() - 1).user_id, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, successListener(), errorListener());
        } else {
            ToastUtils.toastShort(getString(R.string.common_list_end));
            mExpertList.setPullLoadEnable(false);
        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<MyFansResult> successListener() {
        return new Response.Listener<MyFansResult>() {
            @Override
            public void onResponse(MyFansResult response) {
                //判断返回数据是否成功
                if (TongleAppConst.SUCCESS.equals(response.head.getReturn_flag())) {
                    if (user_fans_list!=null&&clearFlag) {
                        user_fans_list.clear();
                    }
                    fans.data_total_count = response.body.data_total_count;
                    fans.user_fans_list = response.body.user_fans_list;
                    //第一次请求或刷新页面时候判断是否隐藏底部加载更多按钮
                    if ((response.body.user_fans_list.size())< Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                        flag = false;
                        mExpertList.setPullLoadEnable(false);
                    } else {
                        flag = true;
                        mExpertList.setPullLoadEnable(true);
                    }
                    user_fans_list.addAll(response.body.user_fans_list);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.head.getReturn_message());
                }
                mHandler.sendEmptyMessage(0);
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    private void updateListView() {
        //实例化适配器，并把listview传值过去
        mExpertListAdaper = new FansAdaper(getActivity(), user_fans_list);
        mExpertList.setPullLoadEnable(false);
        mExpertList.setPullRefreshEnable(true);
        mExpertList.setXListViewListener(this, 0);
        mExpertList.setRefreshTime();
        mExpertList.setAdapter(mExpertListAdaper);
    }
}
