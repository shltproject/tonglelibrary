package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.ConfirmOrderCartListAdapter;
import com.ilingtong.library.tongle.fragment.CartFragment;
import com.ilingtong.library.tongle.model.AddressModel;
import com.ilingtong.library.tongle.model.CartModel;
import com.ilingtong.library.tongle.model.ConfirmOrderModel;
import com.ilingtong.library.tongle.protocol.AddressListItem;
import com.ilingtong.library.tongle.protocol.AddressResult;
import com.ilingtong.library.tongle.protocol.BaseDataListItem;
import com.ilingtong.library.tongle.protocol.BaseDataResult;
import com.ilingtong.library.tongle.protocol.CartSettlementResult;
import com.ilingtong.library.tongle.protocol.CreateOrderParams;
import com.ilingtong.library.tongle.protocol.CreateOrderParamsProductInfo;
import com.ilingtong.library.tongle.protocol.DataListItem;
import com.ilingtong.library.tongle.protocol.InvoiceInfo;
import com.ilingtong.library.tongle.protocol.MyCouponAvailableItem;
import com.ilingtong.library.tongle.protocol.MyCouponBaseInfo;
import com.ilingtong.library.tongle.protocol.OrderInfoResult;
import com.ilingtong.library.tongle.protocol.OrderJSONInfo;
import com.ilingtong.library.tongle.protocol.ProductListInfo;
import com.ilingtong.library.tongle.protocol.ShippingMethodInfo;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * User: lengjiqiang
 * Date: 2015/6/7
 * Time: 22:41
 * Email: jqleng@isoftstone.com
 * Desc: 确认订单 页
 */
public class OrderConfirmActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;
    private ImageView left_arrow_btn;
    private ListView listview;
    ConfirmOrderCartListAdapter listAdapter;
    final private String DEFAULT_ADDRESS = "0";
    View headview;
    View footview;
    LinearLayout headview_linear;
    LinearLayout deliveryModeLine;
    LinearLayout deliveryTimeLine;
    TextView totalPriceTxt;  //应付金额
    TextView tv_addaddress;
    TextView deliveryTimeTxt;
    TextView deliveryModeTxt, guanshui_pay, yunfei_pay, all_pay, pro_totle, yunfei_desc;
    TextView toNameTxt;
    TextView toNumberTxt;
    TextView toAddrTxt;
    Button commitBtn;
    String shipping_method_no;
    String shipping_time_memo;
    private ArrayList<ProductListInfo> product_list;
    private String address_no;
    EditText order_memo;
    String invoice_content_flag, invoice_type_no;
    private ProgressDialog pDlg;
    CartSettlementResult response;

    private TextView txt_coupon;  //抵用券
    private TextView txt_selected_coupon;  //选择的优惠券名称
    private LinearLayout delivery_coupon_linear;  //去选择优惠券

    private int type;  //表示从购物车结算下单或从商品详情立即购买类别
    public final static int TYPE_CART = 1;  //从购物车结算下单
    public final static int TYPE_BUYNOW = 2;  //从商品详情立即购买

    private LinearLayout llyAvailableMoney;//使用可用酬金
    private TextView tvAvailableMoney;//使用可用酬金金额
    private TextView tvUseMoney;//使用酬金抵扣
    private static final int AVAILABLE_MONEY_REQUEST_CODE = 1000;//获取可用金额请求码
    private Double mAccountPayMoney = 0.0;//账户支付金额
    private Double mCouponMoney = 0.0;//优惠券抵扣金额
    private Double mActualAmount = 0.0;//实付总额

    /**
     * 跳转到该页面
     *
     * @param activity
     * @param cartSettlementResult
     */
    public static void launcher(Activity activity, CartSettlementResult cartSettlementResult, int type,int requestCode) {
        Intent intent = new Intent(activity, OrderConfirmActivity.class);
        intent.putExtra("response", cartSettlementResult);
        intent.putExtra("type", type);
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 购物车页面跳转
     *
     * @param fragment              Fragment
     * @param cartSettlementResult  CartSettlementResult 2042接口返回购物车信息结果类
     * @param type                   标识位
     * @param requestCode           请求码
     */
    public static void launcher(Fragment fragment, CartSettlementResult cartSettlementResult, int type, int requestCode) {
        Intent intent = new Intent(fragment.getActivity(), OrderConfirmActivity.class);
        intent.putExtra("response", cartSettlementResult);
        intent.putExtra("type", type);
        fragment.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_comm_layout);
        initView();
    }

    public void initView() {
        response = (CartSettlementResult) getIntent().getSerializableExtra("response");
        type = getIntent().getIntExtra("type", 0);

        ConfirmOrderModel.checkDeliveryMode = new DataListItem();
        ConfirmOrderModel.checkDeliveryTime = new DataListItem();
        //创建并弹出“加载中Dialog”
        pDlg = new ProgressDialog(this);
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.loading));
        pDlg.show();
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        listview = (ListView) findViewById(R.id.listview_comm);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(R.string.confirm_order);

        headview = LayoutInflater.from(this).inflate(R.layout.confirm_order_headview, null);
        listview.addHeaderView(headview);

        headview_linear = (LinearLayout) findViewById(R.id.headview_linear);
        tv_addaddress = (TextView) findViewById(R.id.tv_addaddress);
        headview_linear.setOnClickListener(this);
        tv_addaddress.setOnClickListener(this);

        footview = LayoutInflater.from(this).inflate(R.layout.confirm_order_footview, null);
        deliveryTimeTxt = (TextView) footview.findViewById(R.id.delivery_time);
        deliveryModeTxt = (TextView) footview.findViewById(R.id.delivery_mode);
        guanshui_pay = (TextView) footview.findViewById(R.id.guanshui_pay);
        yunfei_pay = (TextView) footview.findViewById(R.id.yunfei_pay);
        all_pay = (TextView) footview.findViewById(R.id.all_pay);
        pro_totle = (TextView) footview.findViewById(R.id.pro_totle);
        yunfei_desc = (TextView) footview.findViewById(R.id.yunfei_desc);
        txt_coupon = (TextView) footview.findViewById(R.id.confirm_order_footview_txt_coupon);  //结算区域 优惠券金额
        txt_selected_coupon = (TextView) footview.findViewById(R.id.delivery_coupon);  //选择的优惠券名称
        delivery_coupon_linear = (LinearLayout) footview.findViewById(R.id.delivery_coupon_linear);  //去选择优惠券
        delivery_coupon_linear.setOnClickListener(this);

        llyAvailableMoney = (LinearLayout) footview.findViewById(R.id.confirm_order_lly_available_money);
        tvAvailableMoney = (TextView) footview.findViewById(R.id.confirm_order_tv_available_money);
        tvUseMoney = (TextView) footview.findViewById(R.id.confirm_order_tv_use_money);
        llyAvailableMoney.setOnClickListener(this);
        //默认使用可用酬金为0
        tvAvailableMoney.setText(FontUtils.priceFormat(0));
        tvUseMoney.setText(FontUtils.priceFormat(0));

        commitBtn = (Button) footview.findViewById(R.id.commit_order_btn);
        commitBtn.setOnClickListener(this);
        listview.addFooterView(footview);

        order_memo = (EditText) findViewById(R.id.editText2);
        deliveryModeLine = (LinearLayout) footview.findViewById(R.id.delivery_mode_linear);
        deliveryModeLine.setOnClickListener(this);
        deliveryTimeLine = (LinearLayout) footview.findViewById(R.id.delivery_time_linear);
        deliveryTimeLine.setOnClickListener(this);

        totalPriceTxt = (TextView) findViewById(R.id.total_price_num);
        toNameTxt = (TextView) headview.findViewById(R.id.to_name);
        toNumberTxt = (TextView) headview.findViewById(R.id.to_number);
        toAddrTxt = (TextView) headview.findViewById(R.id.to_address);

        listAdapter = new ConfirmOrderCartListAdapter(this, CartModel.checkList);
        //确认订单时商品列表
        product_list = new ArrayList<ProductListInfo>();
        for (int i = 0; i < CartModel.checkList.size(); i++) {
            ProductListInfo listinfo = new ProductListInfo();
            listinfo.seq_no = CartModel.checkList.get(i).seq_no;
            listinfo.product_id = CartModel.checkList.get(i).prod_id;
            product_list.add(listinfo);
        }

        //选择的优惠券的名称
        if (!TextUtils.isEmpty(response.getBody().vouchers_name)) {
            txt_selected_coupon.setText(response.getBody().vouchers_name);
        }

        //运费
        yunfei_pay.setText(FontUtils.priceFormat(Double.parseDouble(response.getBody().total_fee)));
        float yunfei = Float.parseFloat(response.getBody().total_fee);
        if (yunfei > 0) {
            yunfei_desc.setVisibility(View.GONE);
        } else {
            yunfei_desc.setVisibility(View.VISIBLE);
        }
        //关税
        guanshui_pay.setText(FontUtils.priceFormat(Double.parseDouble(response.getBody().total_tariff)));
        //商品总额
        pro_totle.setText(FontUtils.priceFormat(Double.parseDouble(response.getBody().total_goods_price)));

        //订单总额 （商品总额加关税加运费）
        all_pay.setText(FontUtils.priceFormat(Double.parseDouble(response.getBody().order_total_amount)));

        //抵用券
        txt_coupon.setText("-"+FontUtils.priceFormat(Double.parseDouble(response.getBody().money)));
        mCouponMoney = Double.parseDouble(response.getBody().money);

        //应付金额
        totalPriceTxt.setText(FontUtils.priceFormat(Double.parseDouble(response.getBody().amount_payable)));
        mActualAmount = Double.parseDouble(response.getBody().amount_payable);

        ServiceManager.doAddressRequest(TongleAppInstance.getInstance().getUserID(), addrListener(), errorListener());

        listview.setAdapter(listAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("orderComfirm");
        MobclickAgent.onResume(getApplicationContext());
        ServiceManager.doAddressRequest(TongleAppInstance.getInstance().getUserID(), addrListener(), addressErrorListener());

        //进入其他页面回来时，需要更新地址的显示
        changeAddress();
    }

    /**
     * 更新配送方式和配送时间信息
     */
    private void updateDeliveryInfo() {
        if ((ConfirmOrderModel.checkDeliveryTime != null) && (ConfirmOrderModel.checkDeliveryTime.name != null)) {
            deliveryTimeTxt.setText(ConfirmOrderModel.checkDeliveryTime.name);
            shipping_time_memo = ConfirmOrderModel.checkDeliveryTime.code;
        }
        if ((ConfirmOrderModel.checkDeliveryMode != null) && (ConfirmOrderModel.checkDeliveryMode.name != null)) {
            deliveryModeTxt.setText(ConfirmOrderModel.checkDeliveryMode.name);
            shipping_method_no = ConfirmOrderModel.checkDeliveryMode.code;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageStart("productDetail");
        MobclickAgent.onPause(getApplicationContext());
    }

    /**
     * 该页中的所有点击事件处理方法
     */
    @Override
    public void onClick(View v) {
        Intent intent;
        Bundle bundle = new Bundle();
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.headview_linear) {
            intent = new Intent(this, AddressSelectActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.tv_addaddress) {   //表示没有默认地址，点击去新增收货地址
            AddressNewActivity.launcher(OrderConfirmActivity.this,TongleAppConst.TYPE_ORDER_ADD,10001);
        } else if (v.getId() == R.id.delivery_mode_linear) {
            intent = new Intent(this, OrderDeliveryModeActivity.class);
            if (ConfirmOrderModel.checkDeliveryMode.name == null) {
                //第一次进入确认订单页，
                bundle.putString("checkDeliveryMode", ConfirmOrderModel.deliveryModeList.get(0).name);
            } else {
                bundle.putString("checkDeliveryMode", ConfirmOrderModel.checkDeliveryMode.name);
            }
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (v.getId() == R.id.delivery_time_linear) {
            intent = new Intent(this, OrderDeliveryTimeActivity.class);
            if (ConfirmOrderModel.checkDeliveryTime.name == null) {
                //第一次进入确认订单页，
                bundle.putString("checkDeliveryTime", ConfirmOrderModel.deliveryTimeList.get(0).name);
            } else {
                bundle.putString("checkDeliveryTime", ConfirmOrderModel.checkDeliveryTime.name);
            }
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (v.getId() == R.id.commit_order_btn) {
            if (address_no == null) {
                Toast.makeText(OrderConfirmActivity.this, getString(R.string.order_confirm_address_null), Toast.LENGTH_SHORT).show();
                return;
            }
            if (shipping_method_no == null) {
                Toast.makeText(OrderConfirmActivity.this, getString(R.string.order_confirm_method_null), Toast.LENGTH_SHORT).show();
                return;
            }
            if (shipping_time_memo == null) {
                Toast.makeText(OrderConfirmActivity.this, getString(R.string.order_confirm_time_null), Toast.LENGTH_SHORT).show();
                return;
            }
            if (type == TYPE_BUYNOW) {
                buyNowCreateOrder();
            } else if (type == TYPE_CART) {
                cartCreateOrder();
            }
        } else if (v.getId() == R.id.delivery_coupon_linear) {  //去选择优惠券

            List<MyCouponAvailableItem> goods_info = new ArrayList<>();
            for (int i = 0; i < response.getBody().my_shopping_cart.size(); i++) {
                MyCouponAvailableItem myCouponAvailableItem = new MyCouponAvailableItem();
                myCouponAvailableItem.goods_id = response.getBody().my_shopping_cart.get(i).prod_id;
                myCouponAvailableItem.spec_id1 = response.getBody().my_shopping_cart.get(i).prod_spec_list.get(0).spec_detail_id;
                myCouponAvailableItem.buy_count = response.getBody().my_shopping_cart.get(i).order_qty;
                if (response.getBody().my_shopping_cart.get(i).prod_spec_list.size() > 1) {
                    myCouponAvailableItem.spec_id2 = response.getBody().my_shopping_cart.get(i).prod_spec_list.get(1).spec_detail_id;
                } else {
                    myCouponAvailableItem.spec_id2 = "";
                }

                goods_info.add(myCouponAvailableItem);
            }
            SelectCouponActivity.launcher(OrderConfirmActivity.this, goods_info, response.getBody().vouchers_number_id);
        }else if(v.getId() == R.id.confirm_order_lly_available_money){//使用可用金额
            Double order_max_can_use_account = mActualAmount + mAccountPayMoney;  //重新填写账户金额支付时用加上原来填写的酬金金额
            AvailableMoneyActivity.launcher(OrderConfirmActivity.this,order_max_can_use_account,AVAILABLE_MONEY_REQUEST_CODE);
        }
    }
    /**
     * 购物车购买流程的创建订单
     */
    private void cartCreateOrder() {
        String invoice_type = "";//是否需要发票
        invoice_type_no = "";
        invoice_content_flag = "";
        invoice_type = "0";

        //配送信息类实例化（配送方式，配送时间）
        ShippingMethodInfo shipping_method = new ShippingMethodInfo();
        shipping_method.shipping_method_no = shipping_method_no;
        shipping_method.shipping_time_memo = shipping_time_memo;

        //发票信息实例化（发票类型编号，发票内容区分，发票抬头备注）
        InvoiceInfo invoice_info = new InvoiceInfo();
        invoice_info.invoice_type_no = invoice_type_no;
        invoice_info.invoice_content_flag = invoice_content_flag;
        invoice_info.invoice_title = "";

        OrderJSONInfo order_info_json = new OrderJSONInfo();
        order_info_json.product_list = product_list;
        order_info_json.address_no = address_no;
        order_info_json.invoice_type = invoice_type;
        order_info_json.invoice_info = invoice_info;
        order_info_json.shipping_method = shipping_method;
        order_info_json.order_memo = order_memo.getText().toString();
        order_info_json.pay_type = "3";  //默认设置为微信支付
        order_info_json.tariff = response.getBody().total_tariff;
        order_info_json.vouchers_number_id = response.getBody().vouchers_number_id;  //优惠券号
        order_info_json.account_pay_money = mAccountPayMoney;//账户支付金额

        ServiceManager.addOrderRequest(TongleAppInstance.getInstance().getUserID(), order_info_json, addorderListener(), errorListener());
    }
    /**
     * 立即购买流程的下单
     */
    private void buyNowCreateOrder() {
        String invoice_type = "";//是否需要发票
        invoice_type_no = "";
        invoice_content_flag = "";
        invoice_type = "0";

        //配送信息类实例化（配送方式，配送时间）
        ShippingMethodInfo shipping_method = new ShippingMethodInfo();
        shipping_method.shipping_method_no = shipping_method_no;
        shipping_method.shipping_time_memo = shipping_time_memo;

        //发票信息实例化（发票类型编号，发票内容区分，发票抬头备注）
        InvoiceInfo invoice_info = new InvoiceInfo();
        invoice_info.invoice_type_no = invoice_type_no;
        invoice_info.invoice_content_flag = invoice_content_flag;
        invoice_info.invoice_title = "";


        CreateOrderParamsProductInfo productInfo = new CreateOrderParamsProductInfo(response.getBody().my_shopping_cart.get(0).prod_id,
                response.getBody().my_shopping_cart.get(0).order_qty,
                response.getBody().my_shopping_cart.get(0).prod_spec_list);

        CreateOrderParams creatOrderParams = new CreateOrderParams(productInfo, address_no, invoice_type, shipping_method, invoice_info,
                order_memo.getText().toString(), response.getBody().total_tariff, response.getBody().vouchers_number_id,
                response.getBody().my_shopping_cart.get(0).relation_id,
                response.getBody().my_shopping_cart.get(0).post_id,mAccountPayMoney);
        Map params = new HashMap();
        params.put("order_info", creatOrderParams);
        Gson gson = new Gson();
        pDlg.show();
        ServiceManager.creatOrderNow(gson.toJson(params), addorderListener(), errorListener());
    }
    /**
     * @param requestCode  请求码
     * @param resultCode   结果码
     * @param data         Intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TongleAppConst.REQUEST_CODE_SELECT_COUPON://选择优惠券的返回值
                if (resultCode == TongleAppConst.REQUEST_OK_SELECT_COUPON) {
                    MyCouponBaseInfo mMyCouponBaseInfo = (MyCouponBaseInfo) data.getSerializableExtra("my_coupon_base_info");
                    txt_selected_coupon.setText(mMyCouponBaseInfo.getVouchers_name());
                    txt_coupon.setText("-"+FontUtils.priceFormat(mMyCouponBaseInfo.getMoney()));
                    response.getBody().vouchers_number_id = mMyCouponBaseInfo.getVouchers_number_id();    //保存新的优惠券id

                    mCouponMoney = mMyCouponBaseInfo.getMoney();
                    mActualAmount = Double.parseDouble(response.getBody().order_total_amount) - mCouponMoney - mAccountPayMoney;

                    //选择完优惠券之后要重新判断，账户支付的金额是否小于等于实付金额，如果大于重置成最多可用值
                    if(mActualAmount < 0){
                        mAccountPayMoney = Double.parseDouble(response.getBody().order_total_amount) - mCouponMoney;
                        tvAvailableMoney.setText(FontUtils.priceFormat(mAccountPayMoney));
                        tvUseMoney.setText(FontUtils.priceFormat(mAccountPayMoney));
                        mActualAmount = Double.parseDouble(response.getBody().order_total_amount) - mCouponMoney - mAccountPayMoney;
                    }

                    totalPriceTxt.setText(FontUtils.priceFormat(mActualAmount));
                }
                break;
            case AVAILABLE_MONEY_REQUEST_CODE://使用可用金额的返回值
                if(resultCode == RESULT_OK){
                    if(data != null){
                        mAccountPayMoney = data.getExtras().getDouble(AvailableMoneyActivity.USE_MONEY_EXTRA_KEY);
                        tvAvailableMoney.setText(FontUtils.priceFormat(mAccountPayMoney));
                        tvUseMoney.setText(FontUtils.priceFormat(mAccountPayMoney));
                        mActualAmount = Double.parseDouble(response.getBody().order_total_amount) - mCouponMoney - mAccountPayMoney;
                        totalPriceTxt.setText(FontUtils.priceFormat(mActualAmount));
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
        }
        return false;
    }

    /**
     * 功能：请求地址列表网络响应成功，返回数据
     */
    private Response.Listener<AddressResult> addrListener() {
        return new Response.Listener<AddressResult>() {
            @Override
            public void onResponse(AddressResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    //存储地址列表到内存中
                    AddressModel.my_address_list = response.getBody().getMy_address_list();
                    //存储地址列表到数据库中，并更新控件
                    if ((AddressModel.my_address_list != null) && (AddressModel.my_address_list.size() != 0)) {
                        for (int i = 0; i < AddressModel.my_address_list.size(); i++) {
                            //判断是否有默认地址
                            if (AddressModel.my_address_list.size() > 0) {
                                tv_addaddress.setVisibility(View.GONE);
                                headview_linear.setVisibility(View.VISIBLE);
                                break;
                            } else {
                                tv_addaddress.setVisibility(View.VISIBLE);
                                headview_linear.setVisibility(View.GONE);
                            }
                        }

                    } else {
                        tv_addaddress.setVisibility(View.VISIBLE);
                        headview_linear.setVisibility(View.GONE);
                    }
                    //无论之前是否已经获取过基础数据，都需要拿到所有数据之后更新UI，并且判断地址的显示
                    getBaseData();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                updateDeliveryInfo();
            }
        };
    }

    /**
     * 获取到基础数据
     */
    public void getBaseData(){
        if (ConfirmOrderModel.base_data_list == null) {
            ServiceManager.doBaseDataRequest("", baseListener(), errorListener());
            Log.e("TAG","run---->base_data");
        } else {
            updateControl();
            Log.e("TAG","run--->get data");
        }
    }

    /**
     * 功能：获取基础数据网络响应成功，返回数据
     */
    private Response.Listener<BaseDataResult> baseListener() {
        return new Response.Listener<BaseDataResult>() {
            @Override
            public void onResponse(BaseDataResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ConfirmOrderModel.base_data_list = response.getBody().getBase_data_list();
                    updateControl();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：我的添加订单网络响应成功，返回数据
     */
    private Response.Listener<OrderInfoResult> addorderListener() {
        return new Response.Listener<OrderInfoResult>() {
            @Override
            public void onResponse(OrderInfoResult response) {
                pDlg.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    Toast.makeText(OrderConfirmActivity.this, getString(R.string.order_confirm_save_success), Toast.LENGTH_SHORT).show();
                    CartFragment.CARTFRAGMENT_UPDATE_FLAG = true; //下单成功，下次进入购物车页面时需刷新购物车页面

                    //订单无须再支付，直接跳转到订单列表页面，还需要支付则跳转到支付方式选择页面。
                    if (TongleAppConst.ORDER_PAID.equals(response.getBody().order_info.order_status)){
//                        Intent intent = new Intent(OrderConfirmActivity.this, OrderDetailActivity.class);
//                        intent.putExtra("order_no", response.getBody().order_info.order_no);
//                        startActivity(intent);
                        MyOrderMainActivity.launchActivity(OrderConfirmActivity.this);
                    }else {
                        SelectPayTypeActivity.launcher(OrderConfirmActivity.this, response.getBody().order_info.order_no, TongleAppConst.PAY_MODE,SelectPayTypeActivity.ORDER_CONFIRM_REQUEST);
                }
                finish();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    /**
     * 获取地址信息网络请求失败
     *
     * @return Response.ErrorListener
     */
    private Response.ErrorListener addressErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                updateDeliveryInfo();
                ToastUtils.toastLong(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    /**
     *
     * 改变地址
     * 1、存在地址列表项时，需要考虑情况分为已经选择过地址、没有选择过地址：
     * 选择过地址：分为该地址存在则显示该地址，该地址已经被删除，则显示默认地址或者第一条地址；
     * 没有选择过地址：显示默认地址或者第一条地址。
     * 2、没有地址，则显示填写地址txt
     *
     */
    public void changeAddress(){
        //选择了地址之后，获取到选择的地址
        if (AddressModel.checkAddress != null) {
            if (AddressModel.checkAddress.consignee == null || AddressModel.checkAddress.consignee.equals("")) {
                //这个是判断针对从别的界面返回（例如配送方式界面返回）
                if (AddressModel.my_address_list != null && AddressModel.my_address_list.size() > 0) {
                    toNameTxt.setText(AddressModel.my_address_list.get(0).consignee);
                    toNumberTxt.setText(AddressModel.my_address_list.get(0).tel);
                    toAddrTxt.setText(AddressModel.my_address_list.get(0).province_name + AddressModel.my_address_list.get(0).city_name + AddressModel.my_address_list.get(0).area_name + AddressModel.my_address_list.get(0).address);
                    address_no = AddressModel.my_address_list.get(0).address_no;
                    Log.e("TAG","run--->1");
                } else {
                    tv_addaddress.setVisibility(View.VISIBLE);
                    headview_linear.setVisibility(View.GONE);
                }
            } else {
                Log.e("TAG","run--->2");
                tv_addaddress.setVisibility(View.GONE);
                headview_linear.setVisibility(View.VISIBLE);

                toNameTxt.setText(AddressModel.checkAddress.consignee);
                toNumberTxt.setText(AddressModel.checkAddress.tel);
                toAddrTxt.setText(AddressModel.checkAddress.province_name + AddressModel.checkAddress.city_name + AddressModel.checkAddress.area_name + AddressModel.checkAddress.address);
                //确认订单时地址编码
                if (AddressModel.checkAddress.address_no != null)
                    address_no = AddressModel.checkAddress.address_no;
            }
        }else{//没有选择过地址，则使用默认的地址
            Log.e("TAG","run--->default");
            //存在收货地址时，显示默认的地址
            if (AddressModel.my_address_list != null && AddressModel.my_address_list.size() > 0) {
                Iterator iterator = AddressModel.my_address_list.iterator();

                while (iterator.hasNext()) {
                    AddressListItem item = new AddressListItem();
                    item = (AddressListItem) iterator.next();
                    if (item.default_flag.equals(DEFAULT_ADDRESS)) {
                        toNameTxt.setText(item.consignee);
                        toNumberTxt.setText(item.tel);
                        toAddrTxt.setText(item.province_name + item.city_name + item.area_name + item.address);
                        address_no = item.address_no;
                    } else {
                        toNameTxt.setText(AddressModel.my_address_list.get(0).consignee);
                        toNumberTxt.setText(AddressModel.my_address_list.get(0).tel);
                        toAddrTxt.setText(AddressModel.my_address_list.get(0).province_name + AddressModel.my_address_list.get(0).city_name + AddressModel.my_address_list.get(0).area_name + AddressModel.my_address_list.get(0).address);
                        address_no = AddressModel.my_address_list.get(0).address_no;
                    }
                }
            }else{
                //没有地址的时候则显示填写地址的txt
                tv_addaddress.setVisibility(View.VISIBLE);
                headview_linear.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 功能：更新动态控件UI，如发票类型，发票内容，支付方式需要动态创建控件
     */
    public void updateControl() {
        pDlg.dismiss();
        //更新UI控件
        //更新地址的显示
        changeAddress();

        for (int i = 0; i < ConfirmOrderModel.base_data_list.size(); i++) {
            BaseDataListItem item = new BaseDataListItem();
            item = ConfirmOrderModel.base_data_list.get(i);
            if (item.base_data_type.equals(TongleAppConst.DELIVERY_MODE)) {
                ConfirmOrderModel.deliveryModeList = item.data_list;
                deliveryModeTxt.setText(ConfirmOrderModel.deliveryModeList.get(0).name);
                //确认地址时，默认的送货方式
                shipping_method_no = ConfirmOrderModel.deliveryModeList.get(0).code;
            }
            //配送时间
            else if (item.base_data_type.equals(TongleAppConst.DELIVERY_TIME)) {
                ConfirmOrderModel.deliveryTimeList = item.data_list;
                deliveryTimeTxt.setText(ConfirmOrderModel.deliveryTimeList.get(0).name);
                //确认地址时，默认的送货时间
                shipping_time_memo = ConfirmOrderModel.deliveryTimeList.get(0).code;
            }
        }
    }
}