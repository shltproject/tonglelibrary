package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc: 订单详情类
 */
public class OrderDetailResult implements Serializable {
    private BaseInfo head;
    private OrderDetailOrderInfoResult body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public OrderDetailOrderInfoResult getBody() {
        return body;
    }

    public void setBody(OrderDetailOrderInfoResult body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
