package com.ilingtong.library.tongle.protocol;

import java.util.ArrayList;

/**
 * Created by wuqian on 2017/1/4.
 * mail: wuqian@ilingtong.com
 * Description:12002 结算结构入口参数实体
 */
public class BillingNowParam {
    public String product_id;
    public String order_qty;
    public ArrayList<ProdSpecListItem> prod_spec_list;

    public BillingNowParam(String product_id, String order_qty, ArrayList<ProdSpecListItem> prod_spec_list) {
        this.product_id = product_id;
        this.order_qty = order_qty;
        this.prod_spec_list = prod_spec_list;
    }
}
