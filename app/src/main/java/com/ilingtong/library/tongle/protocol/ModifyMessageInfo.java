package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/8/7
 * Time: 9:30
 * Email: leishuai@isoftstone.com
 * Desc: 微信信息类
 */
public class ModifyMessageInfo implements Serializable {
    private String  user_id;
    private String  user_name;
    private String  user_nick_name;
    private String  user_phone;
    private String  user_photo_url;
    private String  user_sex;
    private String  user_id_no;
    private String  user_sales_point;
    private String  user_rebate_point;
    private String  user_account_balance;
    private String  user_coupons;
    private String  user_fans;
    private String  user_follows;
    private String  user_qr_code_url;
    private String  user_customs_flag;

    public String getUser_id() {
        return user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_nick_name() {
        return user_nick_name;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public String getUser_photo_url() {
        return user_photo_url;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public String getUser_id_no() {
        return user_id_no;
    }

    public String getUser_sales_point() {
        return user_sales_point;
    }

    public String getUser_rebate_point() {
        return user_rebate_point;
    }

    public String getUser_account_balance() {
        return user_account_balance;
    }

    public String getUser_coupons() {
        return user_coupons;
    }

    public String getUser_fans() {
        return user_fans;
    }

    public String getUser_follows() {
        return user_follows;
    }

    public String getUser_qr_code_url() {
        return user_qr_code_url;
    }

    public String getUser_customs_flag() {
        return user_customs_flag;
    }

    @Override
    public String toString() {
        return "ModifyMessageInfo{" +
                "user_id='" + user_id + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_nick_name='" + user_nick_name + '\'' +
                ", user_phone='" + user_phone + '\'' +
                ", user_photo_url='" + user_photo_url + '\'' +
                ", user_sex='" + user_sex + '\'' +
                ", user_id_no='" + user_id_no + '\'' +
                ", user_sales_point='" + user_sales_point + '\'' +
                ", user_rebate_point='" + user_rebate_point + '\'' +
                ", user_account_balance='" + user_account_balance + '\'' +
                ", user_coupons='" + user_coupons + '\'' +
                ", user_fans='" + user_fans + '\'' +
                ", user_follows='" + user_follows + '\'' +
                ", user_qr_code_url='" + user_qr_code_url + '\'' +
                ", user_customs_flag='" + user_customs_flag + '\'' +
                '}';
    }
}
