package com.ilingtong.library.tongle.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.utils.DipUtils;

/**
 * @ClassName: DoubleSelectDialog
 * @Package: com.ilingtong.library.tongle.widget
 * @Description: 双选对话框
 * @author: liuting
 * @Date: 2017/6/12 17:05
 */

public class DoubleSelectDialog extends Dialog {
    private TextView txtMsg;//提示文本
    private Button btnCancel;//取消按钮
    private Button btnSure;//确定按钮
    private View.OnClickListener cancelListener;//取消监听事件
    private View.OnClickListener sureListener;//确定监听事件
    private String strMessage;//提示内容
    private String strCancel;//取消
    private String strSure;//确认
    private View viewDivider;//分隔线

    public DoubleSelectDialog(Context context) {
        super(context);
    }

    public DoubleSelectDialog(Context context,String cancel,String sure,String message, View.OnClickListener cancelListener,View.OnClickListener sureListener) {
        super(context, R.style.select_dialog_style);
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.CENTER;
        params.width= DipUtils.px2dip(280);
        window.setAttributes(params);
        this.strCancel=cancel;
        this.strSure=sure;
        this.strMessage=message;
        this.cancelListener=cancelListener;
        this.sureListener=sureListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_double_select_layout);
        initView();
    }

    /**
     * 初始化控件
     */
    private void initView() {
        txtMsg = (TextView) findViewById(R.id.select_txt_msg);
        btnCancel = (Button) findViewById(R.id.select_btn_cancel);
        btnSure = (Button) findViewById(R.id.select_btn_sure);
        viewDivider=(View)findViewById(R.id.select_view_divider);
        //根据赋值来确定是否显示取消按钮
        if(TextUtils.isEmpty(strCancel)){
            //为空时则不显示取消按钮，主要用于忘记密码和注册模块dialog
            viewDivider.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
        }else{//不为空时，用于购物车删除dialog等
            viewDivider.setVisibility(View.VISIBLE);
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(strCancel);
        }
        btnSure.setText(strSure);
        txtMsg.setText(strMessage);
        btnCancel.setOnClickListener(cancelListener);
        btnSure.setOnClickListener(sureListener);
    }
}
