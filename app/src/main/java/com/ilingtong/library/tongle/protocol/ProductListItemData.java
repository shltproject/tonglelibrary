package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/14
 * Time: 10:22
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ProductListItemData implements Serializable {
    public String prod_id;
    public String prod_name;
    public String prod_thumbnail_pic_url;
    public String prod_price;
    public String prod_points_rule;
    public String post_id;
    public String mstore_id;
    public String prod_favorited_by_me;
    public String trade_prod_favorited_by_me;
    public String relation_id;
    public String coupon_flag;  //团购商品标志  add  2016/3/29
    public String rownum;     //行号  分页用  add on 2017/10/19
}
