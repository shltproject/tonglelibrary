package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/1/11
 * Desc:店铺主题商品信息类
 * use by 10005
 */

public class StoreGoodsInfo implements Serializable{
    private String goods_id;//商品ID
    private String goods_name;//商品名称
    private String spe_mobile_pic_addr;//商品缩略图地址
    private Double goods_price;//售出价格
    private int goods_point;//售出积分
    private String goods_cate_id;//商品分类ID
    private String goods_cate_name;//商品分类名称
    private String show_order;//显示顺序

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public Double getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(Double goods_price) {
        this.goods_price = goods_price;
    }

    public String getSpe_mobile_pic_addr() {
        return spe_mobile_pic_addr;
    }

    public void setSpe_mobile_pic_addr(String spe_mobile_pic_addr) {
        this.spe_mobile_pic_addr = spe_mobile_pic_addr;
    }

    public int getGoods_point() {
        return goods_point;
    }

    public void setGoods_point(int goods_point) {
        this.goods_point = goods_point;
    }

    public String getGoods_cate_id() {
        return goods_cate_id;
    }

    public void setGoods_cate_id(String goods_cate_id) {
        this.goods_cate_id = goods_cate_id;
    }

    public String getGoods_cate_name() {
        return goods_cate_name;
    }

    public void setGoods_cate_name(String goods_cate_name) {
        this.goods_cate_name = goods_cate_name;
    }

    public String getShow_order() {
        return show_order;
    }

    public void setShow_order(String show_order) {
        this.show_order = show_order;
    }
}
