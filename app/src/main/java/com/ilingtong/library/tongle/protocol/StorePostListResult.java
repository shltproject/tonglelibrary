package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/1/11
 * Desc:10015接口返回json 对应entity
 */

public class StorePostListResult extends BaseResult implements Serializable {
    private StorePostListInfo body;//店铺动态列表body部分

    public StorePostListInfo getBody() {
        return body;
    }

    public void setBody(StorePostListInfo body) {
        this.body = body;
    }
}
