package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.MStoreListItem;
import com.ilingtong.library.tongle.protocol.PromotionListItem;
import com.ilingtong.library.tongle.protocol.UserFollowPostList;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/2
 * Time: 13:15
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class HomeModel {
    public ArrayList<PromotionListItem> bannerList;
    public String forum_data_total_count;
    public ArrayList<UserFollowPostList> user_follow_post_list;
    public static String mstore_data_total_count;
    public static ArrayList<MStoreListItem> mstore_list;
}
