package com.ilingtong.library.tongle.listener;

public interface Task<T> {
	void run(T t);
}
