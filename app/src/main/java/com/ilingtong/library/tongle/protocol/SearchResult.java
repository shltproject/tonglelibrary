package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/6/24
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc: 【1090】
 */
public class SearchResult implements Serializable {

    private BaseInfo head;
    private SearchInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public SearchInfo getBody() {
        return body;
    }

    public void setBody(SearchInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }

}
