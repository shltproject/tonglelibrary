package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:团购券详情之商品详情。
 * use by 6011
 */
public class CouponDetailProdInfo implements Serializable {
    private String prod_id;   //商品编号
    private String prod_name;    //商品名称
    private String prod_resume;            //商品简述
    private String prod_property;           //商品属性 2：代金券 3：服务券 4：提货券
    private List<ProdPicUrlListItem> prod_pic_url_list;        //	商品图片URL列表
    private String out_of_date;       //过期/未消费标志  0：未消费   1：过期
    private String coupon_code;     //	团购券券号
    private String expired_date;          //	团购券过期日期
    private String relation_id;           //	二维码关联ID
    private String refund_qty;         // 可退数量
    private String goods_return_url; //退货申请url Add 2016/04/07
    private String goods_return_title; //退货申请title Add 2016/04/07

    public String getProd_id() {
        return prod_id;
    }

    public void setProd_id(String prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getProd_resume() {
        return prod_resume;
    }

    public void setProd_resume(String prod_resume) {
        this.prod_resume = prod_resume;
    }

    public String getProd_property() {
        return prod_property;
    }

    public void setProd_property(String prod_property) {
        this.prod_property = prod_property;
    }

    public List<ProdPicUrlListItem> getProd_pic_url_list() {
        return prod_pic_url_list;
    }

    public void setProd_pic_url_list(List<ProdPicUrlListItem> prod_pic_url_list) {
        this.prod_pic_url_list = prod_pic_url_list;
    }

    public String getOut_of_date() {
        return out_of_date;
    }

    public void setOut_of_date(String out_of_date) {
        this.out_of_date = out_of_date;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }

    public String getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(String relation_id) {
        this.relation_id = relation_id;
    }

    public String getRefund_qty() {
        return refund_qty;
    }

    public void setRefund_qty(String refund_qty) {
        this.refund_qty = refund_qty;
    }

    public String getGoods_return_url() {
        return goods_return_url;
    }

    public void setGoods_return_url(String goods_return_url) {
        this.goods_return_url = goods_return_url;
    }

    public String getGoods_return_title() {
        return goods_return_title;
    }

    public void setGoods_return_title(String goods_return_title) {
        this.goods_return_title = goods_return_title;
    }
}
