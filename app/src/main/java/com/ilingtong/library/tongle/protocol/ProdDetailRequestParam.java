package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/5
 * Time: 11:09
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ProdDetailRequestParam implements Serializable {
    public String user_id;
    public String product_id;
    public String post_id;
    public String action;
    public String mstore_id;
    public String relation_id;
}
