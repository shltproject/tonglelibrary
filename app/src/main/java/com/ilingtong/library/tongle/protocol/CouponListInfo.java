package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:6010接口 body 团购券列表信息  注：团购券是按照订单封装列表。可以理解为团购券的订单列表。
 */
public class CouponListInfo implements Serializable{
   private int  data_total_count;   //数据总条数
   private List<CouponOrderInfo>  coupon_list; //团购券列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<CouponOrderInfo> getCoupon_list() {
        return coupon_list;
    }

    public void setCoupon_list(List<CouponOrderInfo> coupon_list) {
        this.coupon_list = coupon_list;
    }
}
