package com.ilingtong.library.tongle.external;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;

/**
 * 流式布局的RadioGroup
 */
public class FlowRadioGroup extends RadioGroup {
    private int mMaxWidth = 0;//最大宽度
    private int mItemWidth = 0;//每一项的宽度
    private int mItemMinWidth = 0;//每一项最小的宽度
    private int mItemHeight = 0;//每一项的高度
    private int mHorizontalPadding = 0;//水平间隔距离
    private int mVerticalPadding = 0;//垂直间隔距离
    private ArrayList<Position> mPosition = null;
    public FlowRadioGroup(Context context) {
        super(context);
        initData(context,null);
    }

    public FlowRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData(context,attrs);
    }

    private void initData(Context context,AttributeSet attrs){
        TypedArray ta = context.obtainStyledAttributes(attrs,R.styleable.FlowRadioGroup);
        mMaxWidth = ta.getDimensionPixelSize(R.styleable.FlowRadioGroup_frg_max_width, 0);
        mItemWidth = ta.getDimensionPixelSize(R.styleable.FlowRadioGroup_frg_item_width, 0);
        mItemHeight = ta.getDimensionPixelSize(R.styleable.FlowRadioGroup_frg_item_height,0);
        mHorizontalPadding = ta.getDimensionPixelSize(R.styleable.FlowRadioGroup_frg_horizontal_padding, 0);
        mVerticalPadding = ta.getDimensionPixelSize(R.styleable.FlowRadioGroup_frg_vertical_padding,0);
        ta.recycle();
        mPosition = new ArrayList<FlowRadioGroup.Position>();
        Log.e("TAG", "mHorizontalPadding="+mHorizontalPadding+",mVerticalPadding="+mVerticalPadding);
        setWillNotDraw(false);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int maxWidth = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        if(mMaxWidth != 0){
            maxWidth = mMaxWidth;
        }
        Log.e("TAG", "maxWidth="+maxWidth);
        int childCount = getChildCount();
        int viewWidth = 0, viewHeight = 0;//记录当前view的宽度和高度
        int remainWidth = maxWidth;
        int left = 0;
        int top = 0 ;
        View view = null;
        int currentRow = 0;//记录当前位于第几行
        int currentColumn = 0;//记录当前位于第几列
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        for (int i = 0; i < childCount; i++) {
            view = getChildAt(i);
            viewWidth = view.getMeasuredWidth();
            viewHeight = view.getMeasuredHeight();
            Log.e("TAG", "viewWidth="+viewWidth+",viewHeight="+viewHeight);
            mItemHeight = mItemHeight == 0 ? viewHeight : mItemHeight;
            if(remainWidth >= viewWidth + mHorizontalPadding){
                left = maxWidth - remainWidth + getPaddingLeft() + (currentColumn > 0 ? mHorizontalPadding : 0);
                top = getPaddingTop() + currentRow * (mItemHeight + mVerticalPadding);
                mPosition.add(new Position(left, top, left + viewWidth, top + mItemHeight,currentRow,currentColumn));
                remainWidth -= (currentColumn > 0 ? mHorizontalPadding : 0) + viewWidth;
                currentColumn ++;
            }else{
                remainWidth = maxWidth;
                currentRow ++;
                currentColumn = 0;
                left = maxWidth - remainWidth + getPaddingLeft();
                top = getPaddingTop() + currentRow * (mItemHeight + mVerticalPadding);
                mPosition.add(new Position(left, top, left + viewWidth, top + mItemHeight,currentRow,currentColumn));
                remainWidth -= (currentColumn > 0 ? mHorizontalPadding : 0) + viewWidth;
                currentColumn ++;
            }
        }
        // 设置容器所需的宽度和高度
        setMeasuredDimension(maxWidth, (currentRow + 1) * mItemHeight + currentRow * mVerticalPadding + getPaddingTop() + getPaddingBottom());
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();
        int lastLineStart = 0;
        Position position = null;
        View view = null;
        int preRow = -1;//前一行
        int lastCloumnRemainSpace = 0;
        int width = 0;
        for (int i = 0; i < count; i++) {
            view = getChildAt(i);
            position = mPosition.get(i);
            if(preRow != position.row){
                preRow = position.row;
            }
            position.left += lastLineStart;
            position.right += lastLineStart;
            position.right += lastCloumnRemainSpace;
            view.layout(position.left, position.top, position.right, position.bottom);
            ((TextView)view).setGravity(Gravity.CENTER);
        }
    }

    public void setItemPadding(int horizontal,int vertical){
        mHorizontalPadding = horizontal;
        mVerticalPadding = vertical;
        invalidate();
    }

    private class Position{
        /**
         *
         * @param left 左边开始位置
         * @param top 顶部位置
         * @param right 右边位置
         * @param bottom 底部位置
         * @param row 位于第几行
         * @param cloumn 位于第几列
         */
        public Position(int left,int top,int right,int bottom,int row,int cloumn){
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.row = row;
            this.cloumn = cloumn;
        }
        int left;
        int top;
        int right;
        int bottom;
        int row;//行
        int cloumn;//列
    }
}
