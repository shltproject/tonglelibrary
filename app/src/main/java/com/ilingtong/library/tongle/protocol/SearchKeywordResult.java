package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/2/14
 * Desc:6024接口返回json 对应entity
 */

public class SearchKeywordResult extends BaseResult implements Serializable {
    private SearchKeywordListInfo body;//搜索关键词集合body

    public SearchKeywordListInfo getBody() {
        return body;
    }

    public void setBody(SearchKeywordListInfo body) {
        this.body = body;
    }
}
