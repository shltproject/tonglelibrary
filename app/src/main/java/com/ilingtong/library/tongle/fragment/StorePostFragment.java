package com.ilingtong.library.tongle.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.adapter.StorePostListAdapter;
import com.ilingtong.library.tongle.protocol.StorePostInfo;
import com.ilingtong.library.tongle.protocol.StorePostListResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * Package:com.ilingtong.library.tongle.fragment
 * author:liuting
 * Date:2017/1/11
 * Desc:店铺动态帖子展示
 */

public class StorePostFragment extends LazyFragment implements AbsListView.OnScrollListener {
    private String mStoreUserId;//店铺会员ID
    private ArrayList<StorePostInfo> listPost;//动态帖子列表
    private boolean loadMoreFlag;//加载更多标志
    private boolean isLoadEnd;//listView是否到达底部
    private StorePostListAdapter storePostListAdapter;//帖子列表Adapter
    private boolean isPrepared;// 标志位，标志已经初始化完成
    private Dialog dialog;//提示Dialog
    private ListView lvList;//动态帖子列表
    private RelativeLayout rlyReplace;//无相关信息时的替代界面
    private static final int NO_DATA = 0;//无数据
    private static final int REFRESH_LIST = 1;//刷新，获取初始数据
    private static final int LOAD_MORE = 2;//加载更多
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case NO_DATA://没有数据显示无相关数据
                    rlyReplace.setVisibility(View.VISIBLE);
                    lvList.setVisibility(View.GONE);
                    break;
                case REFRESH_LIST://刷新有数据显示列表
                    rlyReplace.setVisibility(View.GONE);
                    lvList.setVisibility(View.VISIBLE);
                    storePostListAdapter.setItems(listPost);
                    break;
                case LOAD_MORE://加载更多
                    storePostListAdapter.addItems(listPost);
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * 创建StorePostFragment
     *
     * @param store_user_id 店铺会员ID
     * @return StorePostFragment
     */
    public static StorePostFragment newInstance(String store_user_id) {
        Bundle args = new Bundle();
        args.putString("store_user_id", store_user_id);
        StorePostFragment fragment = new StorePostFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store_post_layout, null);
        mStoreUserId = getArguments().getString("store_user_id");
        initView(view);
        isPrepared = true;
        lazyLoad();
        return view;
    }

    /**
     * 初始化控件
     *
     * @param view view
     */
    public void initView(View view) {
        listPost = new ArrayList<>();
        loadMoreFlag = false;
        isLoadEnd = false;
        dialog = DialogUtils.createLoadingDialog(getActivity());
        dialog.setCancelable(true);
        storePostListAdapter = new StorePostListAdapter(getActivity(), new StorePostListAdapter.IOnItemClickListener() {
            @Override
            public void onItemClick(View view, String post_id) {
                //跳转到帖子详情页
                Intent intent = new Intent(getActivity(), CollectForumDetailActivity.class);
                intent.putExtra("post_id", post_id);
                startActivity(intent);
            }
        });
        lvList = (ListView) view.findViewById(R.id.store_post_lv_list);
        lvList.setAdapter(storePostListAdapter);
        lvList.setOnScrollListener(this);
        rlyReplace = (RelativeLayout) view.findViewById(R.id.rl_replace);
    }

    /**
     * 请求数据
     *
     * @param row_num 行号，初始为空，翻页时为最后一项返回的row_num
     */
    public void doRequest(final String row_num) {
        showDialog();
        ServiceManager.getStorePostList(mStoreUserId, row_num, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, new Response.Listener<StorePostListResult>() {
            @Override
            public void onResponse(StorePostListResult storePostListResult) {
                dismissDialog();
                if (TongleAppConst.SUCCESS.equals(storePostListResult.getHead().getReturn_flag())) {
                    if (TextUtils.isEmpty(row_num)) {//初始获取数据
                        if (listPost != null) {
                            listPost.clear();
                        }
                        if (storePostListResult.getBody().getData_total_count() <= 0) {//没有数据显示无相关数据
                            handler.sendEmptyMessage(NO_DATA);
                        } else {//有数据显示列表
                            listPost = storePostListResult.getBody().getUser_post_list();
                            handler.sendEmptyMessage(REFRESH_LIST);
                        }
                    } else {//加载更多
                        listPost = storePostListResult.getBody().getUser_post_list();
                        handler.sendEmptyMessage(LOAD_MORE);
                    }
                    //判断是否可以加载更多，如果返回的列表数小于请求的列表数，说明加载完成，否则继续加载
                    if (storePostListResult.getBody().getUser_post_list().size() < Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                        loadMoreFlag = false;
                    } else {
                        loadMoreFlag = true;
                    }
                } else {//加载失败
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + storePostListResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dismissDialog();
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && isLoadEnd) {
            // 判断是否已加载所有数据
            if (loadMoreFlag) {//未加载完所有数据，加载数据，并且还原isLoadEnd值为false，重新定位列表底部
                doRequest(String.valueOf(listPost.get(listPost.size() - 1).getRow_num()));
                isLoadEnd = false;
            } else {//加载完了所有的数据
                ToastUtils.toastShort(getString(R.string.common_list_end));
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
            View lastVisibleItemView = lvList.getChildAt(lvList.getChildCount() - 1);
            //滑到列表底部
            if (lastVisibleItemView != null && lastVisibleItemView.getBottom() == lvList.getHeight()) {
                isLoadEnd = true;
            } else {
                isLoadEnd = false;
            }
        }
    }

    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        } else {
            doRequest("");
        }
    }

    /**
     * dismiss dialog
     */
    public void dismissDialog(){
        if (dialog!=null&&dialog.isShowing()){
            dialog.dismiss();
        }
    }

    /**
     * 显示Dialog
     */
    public void showDialog(){
        if (dialog!=null&&!dialog.isShowing()){
            dialog.show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissDialog();
    }
}
