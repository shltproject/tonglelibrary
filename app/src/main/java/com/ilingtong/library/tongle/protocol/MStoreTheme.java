package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by fengguowei on 2017/1/12.
 * 魔店主题信息 10004
 */
public class MStoreTheme implements Serializable {
    private String theme_id;
    private String theme_name;
    private String theme_type;
    private String theme_pic1;
    private String theme_pic2;

    public String getTheme_id() {
        return theme_id;
    }

    public void setTheme_id(String theme_id) {
        this.theme_id = theme_id;
    }

    public String getTheme_name() {
        return theme_name;
    }

    public void setTheme_name(String theme_name) {
        this.theme_name = theme_name;
    }

    public String getTheme_type() {
        return theme_type;
    }

    public void setTheme_type(String theme_type) {
        this.theme_type = theme_type;
    }

    public String getTheme_pic1() {
        return theme_pic1;
    }

    public void setTheme_pic1(String theme_pic1) {
        this.theme_pic1 = theme_pic1;
    }

    public String getTheme_pic2() {
        return theme_pic2;
    }

    public void setTheme_pic2(String theme_pic2) {
        this.theme_pic2 = theme_pic2;
    }
}
