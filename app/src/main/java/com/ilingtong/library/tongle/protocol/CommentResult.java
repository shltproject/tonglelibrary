package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/15
 * Time: 16:25
 * Email: jqleng@isoftstone.com
 * Desc: 商品评价类
 */
public class CommentResult implements Serializable {
    private BaseInfo head;
    private CommentInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public CommentInfo getBody() {
        return body;
    }

    public void setBody(CommentInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
