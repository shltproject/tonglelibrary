package com.ilingtong.library.tongle;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ilingtong.library.tongle.protocol.AddressListItem;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/30
 * Time: 10:24
 * Email: jqleng@isoftstone.com
 * Desc: 数据库工具类，该类和具体业务相关
 */
public class DBOperator {
    private DatabaseHelper sqliteDBHelper;
    private SQLiteDatabase db;
    private ArrayList<AddressListItem> mAddressList = new ArrayList<>();
    private Cursor cursor;

    public DBOperator(Context context) {
        this.sqliteDBHelper = new DatabaseHelper(context);
        db = sqliteDBHelper.getWritableDatabase();
    }

    /**
     * 写数据库操作
     * @param strSQL
     * @return
     */
    public boolean execWriteDB(final String strSQL) {
//        db.beginTransaction();  //开始事务
        try {
            System.out.println("strSQL" + strSQL);
            db.execSQL(strSQL);
//            db.setTransactionSuccessful();  //设置事务成功完成
//            db.close();
            return true;
        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
//        }finally {
//            db.endTransaction();    //结束事务
        }
    }

    /**
     * 读数据库操作，并保存到内存中
     */
    public void execQuery( ) {
        try {
//            System.out.println("strSQL>" + strSQL);
//            Cursor cursor = db.query(DatabaseHelper.TABLE_NAME, null,null,null,null,null,null);
            cursor = db.rawQuery("select * from Address_table", null);
            // 始终让cursor指向数据库表的第1行记录
            cursor.moveToFirst();
            // 循环游标，如果不是最后一项记录
            while (!cursor.isAfterLast()) {
                AddressListItem item = new AddressListItem();
                item.address_no = cursor.getString(cursor.getColumnIndex("address_no"));
                item.consignee = cursor.getString(cursor.getColumnIndex("name"));
                item.tel = cursor.getString(cursor.getColumnIndex("phone"));
                item.province_id = cursor.getString(cursor.getColumnIndex("province_id"));
                item.province_name = cursor.getString(cursor.getColumnIndex("province_name"));
                item.area_id = cursor.getString(cursor.getColumnIndex("area_id"));
                item.city_id = cursor.getString(cursor.getColumnIndex("city_id"));
                item.city_name = cursor.getString(cursor.getColumnIndex("city_name"));
                item.area_name = cursor.getString(cursor.getColumnIndex("area_name"));
                item.address = cursor.getString(cursor.getColumnIndex("address"));
                item.post_code = cursor.getString(cursor.getColumnIndex("post_code"));
                item.default_flag = cursor.getString(cursor.getColumnIndex("default_flag"));
                mAddressList.add(item);
                cursor.moveToNext();
            }
//            db.close();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }finally {
                cursor.close();

        }
    }

    /**
     * 返回数据库中的记录总数
     * @return
     */
    public int getRecordCount() {
        int count = 0;
        try {
            Cursor cursor = db.rawQuery("select * from Address_table", null);
            count = cursor.getCount();
//            db.close();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * 功能：关闭数据库
     */
    public void closeDB() {
        try {
            db.close();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    /**
     * 功能：返回地址列表
     * @return
     */
    public ArrayList getAddressList() {
        return mAddressList;
    }
}
