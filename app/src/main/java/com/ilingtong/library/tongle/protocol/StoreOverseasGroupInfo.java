package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/7/14.
 * mail: wuqian@ilingtong.com
 * Description:
 */

public class StoreOverseasGroupInfo implements Serializable {

    public String background_pic_url;     //	活动背景图URL
    public String activity_begin_date;    //	活动开始日
    public String activity_end_date;      //	活动结束日
    public int order_qty;              //当前购买商品件数
    public int limit_qty;              //成团商品件数
    public String group_show_msg;         //海淘显示信息
    public String group_info_url;         //活动说明URL
    public String mstore_name;            //	店铺名称

}
