package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.StoreFieldInfo;

import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.adapter
 * author:liuting
 * Date:2017/2/14
 * Desc:店铺子分类列表Adapter
 */

public class StoreSubFieldGridAdapter extends BaseAdapter{
    private List<StoreFieldInfo> listField;//分类列表
    private Context context;//上下文
    private LayoutInflater inflater;
    private IOnItemClickListener listener;//事件监听

    public StoreSubFieldGridAdapter(Context context,List<StoreFieldInfo> listField,IOnItemClickListener listener) {
        this.listField = listField;
        this.context = context;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    public interface IOnItemClickListener{
        void onItemClick(View v,String field_id);//点击事件，传入分类id
    }

    @Override
    public int getCount() {
        return listField.size();
    }

    @Override
    public Object getItem(int position) {
        return listField.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
       ViewHolder holder;
        if(convertView==null){
            convertView = inflater.inflate(R.layout.grid_item_sub_field_layout,null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.sub_field_tv_name);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvName.setText(listField.get(position).getField_name());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onItemClick(v,listField.get(position).getField_id());
                }
            }
        });
        return convertView;
    }

    static class ViewHolder{
        TextView tvName;//分类名称
    }
}
