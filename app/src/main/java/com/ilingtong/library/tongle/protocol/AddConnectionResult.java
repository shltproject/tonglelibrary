package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/26
 * Time: 10:54
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class AddConnectionResult implements Serializable {
    private BaseInfo head;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

}
