package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/2/14
 * Desc:6023接口返回json 对应entity
 */

public class StoreFieldResult extends BaseResult implements Serializable {
    private StoreFieldListInfo body;//店铺分类列表body

    public StoreFieldListInfo getBody() {
        return body;
    }

    public void setBody(StoreFieldListInfo body) {
        this.body = body;
    }
}
