package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.fragment.ProductCommentFragment;
import com.ilingtong.library.tongle.fragment.ProductDetailFragment;
import com.ilingtong.library.tongle.fragment.ProductSpecFragment;
import com.ilingtong.library.tongle.model.ProductDetailModel;
import com.ilingtong.library.tongle.protocol.FavoriteListItem;
import com.ilingtong.library.tongle.protocol.MStoreDetailCollectResult;
import com.ilingtong.library.tongle.protocol.ProdDetailQRResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 9:48
 * Email: jqleng@isoftstone.com
 * Dest: 产品更多
 */
public class CollectProductMoreActivity extends BaseFragmentActivity implements View.OnClickListener {
    private ImageView returnBtn;
    private ImageView collectBtn;
    private ImageView shareBtn;
    private TextView top_name;

    private String prod_favorited_by_me;
    private String rela_id;
    private boolean bIsFavorate = false;
    private ProductDetailModel productDetailModel;
    public static String prodID;
    public static String mStoreID;
    public static boolean isOverseasFlag;    //是否是海淘团购商品
    private int intoType = 0;  //表示从哪个页面跳转来

    private TextView detailText;
    private TextView specText;
    private TextView commentText;
    private RelativeLayout detailFrame;
    private RelativeLayout specFrame;
    private RelativeLayout commentFrame;
    private ImageView prod_detail_cursor1;
    private ImageView prod_detail_cursor2;
    private ImageView prod_detail_cursor3;
    ProductDetailFragment detailFragment;
    ProductSpecFragment specFragment;
    ProductCommentFragment commentFragment;
    private String mType;//类型，用于区分商品详情跳转时显示图文详情，还是评价

    /**
     * 跳转到当前activity
     * @param activity
     * @param productId 商品id
     * @param isFavorate 是否被收藏
     * @param intoType  入口
     * @param mStoreId 魔店id
     * @param relationId
     * @param isOverseas  是否是海淘商品
     * @param type
     * @param requestCode
     */
    public static void launcher(Activity activity, String productId,String mStoreId, boolean isFavorate, boolean isOverseas,int intoType, String relationId, String type, int requestCode) {
        Intent moreIntent = new Intent(activity, CollectProductMoreActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("product_id", productId);
        bundle.putString("mStoreId",mStoreId);
        bundle.putBoolean("prod_favorited_by_me", isFavorate);
        bundle.putBoolean("isOverseas",isOverseas);
        bundle.putInt(TongleAppConst.INTO_TYPE, intoType);
        bundle.putString("rela_id", relationId);
        bundle.putString("type", type);
        moreIntent.putExtras(bundle);
        activity.startActivityForResult(moreIntent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_product_more_layout);
        getData();
        initView();
    }

    public void getData() {
        Bundle bundle = getIntent().getExtras();
        prodID = bundle.getString("product_id");
        mStoreID = bundle.getString("mStoreId");
        bIsFavorate = bundle.getBoolean("prod_favorited_by_me", false);
        intoType = getIntent().getIntExtra(TongleAppConst.INTO_TYPE, 0);
        rela_id = bundle.getString("rela_id");
        mType = bundle.getString("type");
        isOverseasFlag = bundle.getBoolean("isOverseas",false);
    }

    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        returnBtn = (ImageView) findViewById(R.id.left_arrow_btn);
        shareBtn = (ImageView) findViewById(R.id.QR_share_btn);
        collectBtn = (ImageView) findViewById(R.id.collect_btn);
        returnBtn.setOnClickListener(this);
        shareBtn.setOnClickListener(this);
        collectBtn.setOnClickListener(this);

        top_name.setText(R.string.product_detail);
        top_name.setVisibility(View.VISIBLE);
        collectBtn.setVisibility(View.VISIBLE);
        returnBtn.setVisibility(View.VISIBLE);
        shareBtn.setVisibility(View.VISIBLE);
        if (bIsFavorate) {
            collectBtn.setBackgroundResource(R.drawable.discollect_button_style);
        } else {
            collectBtn.setBackgroundResource(R.drawable.collect_button_style);
        }

        if (productDetailModel == null)
            productDetailModel = new ProductDetailModel();

        detailText = (TextView) findViewById(R.id.detail_text);
        specText = (TextView) findViewById(R.id.spec_text);
        commentText = (TextView) findViewById(R.id.comment_text);
        detailFrame = (RelativeLayout) findViewById(R.id.detail_frame);
        specFrame = (RelativeLayout) findViewById(R.id.spec_frame);
        commentFrame = (RelativeLayout) findViewById(R.id.comment_frame);
        prod_detail_cursor1 = (ImageView) findViewById(R.id.prod_detail_cursor1);
        prod_detail_cursor2 = (ImageView) findViewById(R.id.prod_detail_cursor2);
        prod_detail_cursor3 = (ImageView) findViewById(R.id.prod_detail_cursor3);

        detailFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTabSelected("detail_tab");
            }
        });
        specFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTabSelected("spec_tab");
            }
        });
        commentFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTabSelected("comment_tab");
            }
        });
        if (mType.equals("comment")) {//评价
            onTabSelected("comment_tab");
        } else {//图文详情
            onTabSelected("detail_tab");
        }
    }

    public void onTabSelected(String tabName) {
        Resources resource = (Resources) getResources();
        final ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.expert_select_color);
        final ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.expert_unselect_color);
        if (tabName == "detail_tab") {
            if (null == detailFragment) {
                detailFragment = new ProductDetailFragment();
            }
            replaceFragment(R.id.product_more_fragment_container, detailFragment);
            detailText.setTextColor(selectedTextColor);
            specText.setTextColor(unselectedTextColor);
            commentText.setTextColor(unselectedTextColor);

            prod_detail_cursor1.setVisibility(View.VISIBLE);
            prod_detail_cursor2.setVisibility(View.INVISIBLE);
            prod_detail_cursor3.setVisibility(View.INVISIBLE);
        } else if (tabName == "spec_tab") {
            if (null == specFragment) {
                specFragment = new ProductSpecFragment();
            }
            replaceFragment(R.id.product_more_fragment_container, specFragment);
            detailText.setTextColor(unselectedTextColor);
            specText.setTextColor(selectedTextColor);
            commentText.setTextColor(unselectedTextColor);

            prod_detail_cursor1.setVisibility(View.INVISIBLE);
            prod_detail_cursor2.setVisibility(View.VISIBLE);
            prod_detail_cursor3.setVisibility(View.INVISIBLE);
        } else if (tabName == "comment_tab") {
            if (null == commentFragment) {
                commentFragment = new ProductCommentFragment();
            }
            replaceFragment(R.id.product_more_fragment_container, commentFragment);
            detailText.setTextColor(unselectedTextColor);
            specText.setTextColor(unselectedTextColor);
            commentText.setTextColor(selectedTextColor);

            prod_detail_cursor1.setVisibility(View.INVISIBLE);
            prod_detail_cursor2.setVisibility(View.INVISIBLE);
            prod_detail_cursor3.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 功能：fragment切换
     *
     * @param viewId
     * @param fragment
     */
    protected void replaceFragment(int viewId, Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(viewId, fragment).commit();
    }

    /**
     * 重写手机物理返回键事件。按下返回键时，把数据传回调用的Activity
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            intent.putExtra("bIsFavorate", bIsFavorate);
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            Intent intent = new Intent();
            intent.putExtra("bIsFavorate", bIsFavorate);
            setResult(RESULT_OK, intent);
            finish();
        } else if (v.getId() == R.id.QR_share_btn) {
            if (ToastUtils.isFastClick()) {
                return;
            } else {
                ServiceManager.doProdDetailQRRequest(TongleAppInstance.getInstance().getUserID(), prodID, "", QRListener(), errorListener());
            }
        } else if (v.getId() == R.id.collect_btn) {
            if (ToastUtils.isFastClick()) {
                return;
            } else {
                if (bIsFavorate == false) {
                    //产品收藏
                    ArrayList<FavoriteListItem> favList = new ArrayList<FavoriteListItem>();
                    FavoriteListItem favListItem = new FavoriteListItem();
                    favListItem.collection_type = intoType == TongleAppConst.MYBABY_INTO ? TongleAppConst.MYPRODUCT : TongleAppConst.PRODUCT;
                    favListItem.key_value = prodID;
                    favList.add(favListItem);
                    ServiceManager.doMStoreCollectRequest(TongleAppInstance.getInstance().getUserID(), favList, rela_id, collectListener(), errorListener());
                } else {
                    String collection_type = intoType == TongleAppConst.MYBABY_INTO ? TongleAppConst.MYPRODUCT : TongleAppConst.PRODUCT;
                    //取消收藏
                    ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), prodID, collection_type, collectListener(), errorListener());
                }
            }
        }
    }

    /**
     * 功能：添加收藏请求网络响应成功，返回数据
     */
    private Response.Listener collectListener() {
        return new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult response) {
                if (TongleAppConst.ADD_FAVORITE.equals(response.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                        collectBtn.setBackgroundResource(R.drawable.discollect_button_style);
                        ToastUtils.toastShort(getString(R.string.favorite_success));
                        bIsFavorate = true;
//                        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
//                            MProductFragment.UPDATE_LIST_FLAG = false;  //从收藏页进来的，表示刚刚取消了收藏之后又重新收藏。回收藏页时不需要刷新
//                        } if (intoType == TongleAppConst.MYBABY_INTO){
//                            SettingMyBoxBabyActivity.UPDATE_LIST_FLAG = false;
//                        }else {
//                            MProductFragment.MPRODUCTFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新M品列表
//                        }
                    } else {
                        ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getFunction_id() + response.getHead().getReturn_message());
                    }
                } else if (TongleAppConst.CANCEL_FAVORITE.equals(response.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                        collectBtn.setBackgroundResource(R.drawable.collect_button_style);
                        ToastUtils.toastShort(getString(R.string.favorite_cancel));
                        bIsFavorate = false;
//                        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
//                            MProductFragment.UPDATE_LIST_FLAG = true;  //从收藏页进来的，表示刚刚取消了收藏之后又重新收藏。回收藏页时不需要刷新
//                        } if (intoType == TongleAppConst.MYBABY_INTO){
//                            SettingMyBoxBabyActivity.UPDATE_LIST_FLAG = true;
//                        } else {
//                            MProductFragment.MPRODUCTFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新M品列表
//                        }
                    } else {
                        ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                    }
                }
            }
        };
    }

    /**
     * 功能：魔店详情请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    /**
     * 功能：商品二维码网络响应成功，返回数据
     */
    private Response.Listener QRListener() {
        return new Response.Listener<ProdDetailQRResult>() {
            @Override
            public void onResponse(ProdDetailQRResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
//                    ShowQRCodeDialog dialog = new ShowQRCodeDialog(CollectProductMoreActivity.this,response.getBody().getProd_qr_code_url(),response.getBody().getLatest_relation_id(),TongleAppConst.STRRELATE_PRODUCT);
//                    dialog.show();
                    ShowQRCodeActivity.launcher(CollectProductMoreActivity.this, response.getBody().getProd_qr_code_url(), response.getBody().getLatest_relation_id(), TongleAppConst.STRRELATE_PRODUCT);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }
}
