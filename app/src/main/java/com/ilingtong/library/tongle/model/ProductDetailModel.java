package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.PostInfo;
import com.ilingtong.library.tongle.protocol.ProductInfo;

/**
 * User: lengjiqiang
 * Date: 2015/5/21
 * Time: 14:27
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ProductDetailModel {
    public ProductInfo productInfo;
    public PostInfo postInfo;
    public String prod_qr_code_url;
    public String latest_relation_id;
}
