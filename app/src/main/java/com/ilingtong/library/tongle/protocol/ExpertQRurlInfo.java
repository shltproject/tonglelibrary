package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/5/18.
 * mail: wuqian@ilingtong.com
 * Description:1086接口的body
 */
public class ExpertQRurlInfo implements Serializable {
    public String user_qr_code_url;   //二维码图片url
}
