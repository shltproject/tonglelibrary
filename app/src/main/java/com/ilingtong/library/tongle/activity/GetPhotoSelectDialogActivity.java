package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.utils.FileUtils;
import com.ilingtong.library.tongle.utils.PictureUtil;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;

import java.io.File;

/**
 * @ClassName: GetPhotoSelectDialogActivity
 * @Package: com.ilingtong.library.tongle.activity
 * @Description: 拍照或选择图片
 * @author: liuting
 * @Date: 2017/6/12 14:06
 */

public class GetPhotoSelectDialogActivity extends BaseActivity implements View.OnClickListener {
    private TextView txtCamera;//拍照
    private TextView txtPhotos;//选择图片
    private TextView txtCancel;//取消
    private Uri outputFileUri;//文件 uri
    public static final int REQUESTCODE_TAKE_PIC = 1000;//拍照请求
    public static final int REQUESTCODE_CHOOSE_PIC = 1001;//选择图片请求
    private String mFileName;//文件名称
    private SelectDialog mTipsDialog;//提示对话框
    private RequestPermissionUtils requestPermissionUtils;//权限请求
    private String[] needPermissions = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE};//相机权限
    private String[] picPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE};//读取文件的权限

    /**
     * 显示选择拍照or相册获取dialog
     *
     * @param activity
     * @param requestCode
     * @param fileName    拍照后保存到sd卡的图片名称
     */
    public static void launcher(Activity activity, String fileName, int requestCode) {
        Intent intent = new Intent(activity, GetPhotoSelectDialogActivity.class);
        intent.putExtra("filename", fileName);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = this.getWindow();
        setContentView(R.layout.dialog_getpic_layout);
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);
        mFileName = getIntent().getStringExtra("filename");
        init();
    }

    /**
     * 初始化
     */
    private void init() {
        txtCamera = (TextView) findViewById(R.id.dialog_getpic_txt_camera);
        txtPhotos = (TextView) findViewById(R.id.dialog_getpic_txt_photos);
        txtCancel = (TextView) findViewById(R.id.dialog_getpic_txt_cancel);

        txtCancel.setOnClickListener(this);
        txtCamera.setOnClickListener(this);
        txtPhotos.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        int i = view.getId();
        if (i == R.id.dialog_getpic_txt_cancel) {
            this.finish();

        } else if (i == R.id.dialog_getpic_txt_photos) {
            GetPicFromPhotoLibrary();

        } else if (i == R.id.dialog_getpic_txt_camera) {
            takePhoto();
        }
    }

    /**
     * @author: liuting
     * @date: 2017/6/13 11:06
     * @MethodName: takePhoto
     * @Description: 拍照
     * @param:
     * @return: void
     * @throws
     */
    private void takePhoto() {
        requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(GetPhotoSelectDialogActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
            @Override
            public void requestSuccess() {
                startCamera(mFileName);
            }

            @Override
            public void requestFail() {
                mTipsDialog = new SelectDialog(GetPhotoSelectDialogActivity.this, getString(R.string.camera_permission_tips), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mTipsDialog.dismiss();
                        requestPermissionUtils.checkPermissions(GetPhotoSelectDialogActivity.this);
                    }
                });
                mTipsDialog.setCancelable(false);
                mTipsDialog.show();
            }
        });
        requestPermissionUtils.checkPermissions(GetPhotoSelectDialogActivity.this);
    }

    /**
     * @author: liuting
     * @date: 2017/6/13 11:06
     * @MethodName: GetPicFromPhotoLibrary
     * @Description: 从图库中获取图片
     * @param:
     * @return: void
     * @throws
     */
    private void GetPicFromPhotoLibrary() {
        requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(GetPhotoSelectDialogActivity.this, picPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
            @Override
            public void requestSuccess() {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUESTCODE_CHOOSE_PIC);
            }

            @Override
            public void requestFail() {
                mTipsDialog = new SelectDialog(GetPhotoSelectDialogActivity.this, getString(R.string.please_open_permission_read_external_storage), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mTipsDialog.dismiss();
                        requestPermissionUtils.checkPermissions(GetPhotoSelectDialogActivity.this);
                    }
                });
                mTipsDialog.setCancelable(false);
                mTipsDialog.show();
            }
        });
        requestPermissionUtils.checkPermissions(GetPhotoSelectDialogActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUESTCODE_TAKE_PIC:   //拍照完成后
                    Intent intent = new Intent();
                    intent.putExtra("outputFileUri", FileUtils.getFile(mFileName).getAbsolutePath());
                    setResult(RESULT_OK, intent);
                    finish();

                    break;
                case REQUESTCODE_CHOOSE_PIC:  //从相册中选择照片后
                    if (data != null && data.getData() != null) {
                            Intent intent2 = new Intent();
                            String path = PictureUtil.getRealPathFromURI(this, data.getData());
                            intent2.putExtra("outputFileUri", path);
                            setResult(RESULT_OK, intent2);
                            finish();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 启动相机拍照
     */
    private void startCamera(String fileName) {
        File file = FileUtils.getFile(fileName);
        outputFileUri = Uri.fromFile(file);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUESTCODE_TAKE_PIC);
        } else {
            ToastUtils.toastShort(getString(R.string.do_not_support_carmera));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode,permissions,paramArrayOfInt);
    }

}

