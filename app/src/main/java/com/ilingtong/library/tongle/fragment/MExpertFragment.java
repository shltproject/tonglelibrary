package com.ilingtong.library.tongle.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectExpertDetailActivity;
import com.ilingtong.library.tongle.activity.SearchActivity;
import com.ilingtong.library.tongle.adapter.XListAdaper;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.ExpertRequestParam;
import com.ilingtong.library.tongle.protocol.FriendListItem;
import com.ilingtong.library.tongle.protocol.MyFollowExpertListResult;
import com.ilingtong.library.tongle.protocol.SearchResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * Created by wuqian on 2015/10/29.
 * mail: wuqian@ilingtong.com
 * Description: M客fragment
 */
public class MExpertFragment extends LazyFragment implements XListView.IXListViewListener, SearchActivity.setKeywordsListener {
    private XListView listView;
    private XListAdaper mExpertListAdaper;
    private ExpertRequestParam param;
    private ArrayList<FriendListItem> user_follow_friend_list = new ArrayList<FriendListItem>();
    private int intoType = 0;

    public static boolean UPDATE_LIST_FLAG = false; //是否刷新列表的标准
    public static boolean MEXPERTFRAGMENT_UPDATE_FLAG = false; //进入页面刷新标志。为ture时表示刷新，反之不刷新
    private int listIndex = -1; //表示是从 position位置跳转到M客详情的
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    listView.setRefreshTime();
                    mExpertListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    // 标志位，标志已经初始化完成。
    private boolean isPrepared;
    private boolean isLoadData;    //是否已经加载过数据
    private String key;  //搜索关键字
    private Dialog progressDialog;

    /**
     * 静态工厂方法需要一个int型的值来初始化fragment的参数，然后返回新的fragment到调用者
     *
     * @param intoType 在哪个页面创建
     * @return
     */
    public static MExpertFragment newInstance(int intoType) {
        MExpertFragment fragment = new MExpertFragment();
        Bundle args = new Bundle();
        args.putInt(TongleAppConst.INTO_TYPE, intoType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.collect_expert_layout, null);
        initView(view);
        isLoadData = false;
        isPrepared = true;
        lazyLoad();
        return view;
    }

    /**
     * 初始化view
     *
     * @param view
     */
    private void initView(View view) {
        progressDialog = DialogUtils.createLoadingDialog(getActivity());
        intoType = getArguments().getInt(TongleAppConst.INTO_TYPE);
        user_follow_friend_list.clear();

        listView = (XListView) view.findViewById(R.id.expert_listview);
        listView.setXListViewListener(this, 0);
        listView.setPullLoadEnable(false);
        mExpertListAdaper = new XListAdaper(getActivity(), user_follow_friend_list);
        listView.setAdapter(mExpertListAdaper);

        //点击列表Item，进入达人详情
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent expertIntent = new Intent(getActivity(), CollectExpertDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("user_id", user_follow_friend_list.get(position - 1).user_id);
                bundle.putString("user_favorited_by_me", user_follow_friend_list.get(position - 1).user_favorited_by_me);
                bundle.putInt(TongleAppConst.INTO_TYPE, intoType);
                expertIntent.putExtras(bundle);
                listIndex = position;
                startActivityForResult(expertIntent, 10001);
            }
        });
    }

    /**
     * 点击M客列表进入详情后，返回的回调。如果在详情页面有取消关注，回到该页面时刷新列表。没有则不刷新
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO && requestCode == 10001 && UPDATE_LIST_FLAG) {
            if (listIndex > 0) {
                user_follow_friend_list.remove(listIndex - 1);
                UPDATE_LIST_FLAG = false;
                mHandler.sendEmptyMessage(0);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 调用接口，请求数据
     */
    public void doRequest() {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {  //收藏
            param = new ExpertRequestParam();
            param.user_id = TongleAppInstance.getInstance().getUserID();
            param.forward = "1";
            param.fetch_count = TongleAppConst.FETCH_COUNT;
            ServiceManager.requestCollectExpert(param, successListener(true), errorListener());
            MEXPERTFRAGMENT_UPDATE_FLAG = false;
            UPDATE_LIST_FLAG = false;
        } else if (intoType == TongleAppConst.FINDFRAGMENT_INTO) {  //热门搜索
            ServiceManager.hotSearchByTypeRequest(TongleAppConst.FIND_TYPE_EXPERT, "", TongleAppConst.FETCH_COUNT, hotSearchSuccessListener(true), errorListener());
        } else if (intoType == TongleAppConst.SEARCH_INTO) {  //关键字搜索
            if (!TextUtils.isEmpty(key)) {
                progressDialog.show();
                ServiceManager.searchRequest(TongleAppConst.FIND_TYPE_EXPERT, key, "", TongleAppConst.FETCH_COUNT, hotSearchSuccessListener(true), errorListener());
            }
        }
        isLoadData = true;  //表示已经加载了数据，重新切回该fragment的时候无需再重新调用接口
    }

    @Override
    public void onRefresh(int id) {
        doRequest();
    }

    /**
     * 加载更多
     *
     * @param id
     */
    @Override
    public void onLoadMore(int id) {
        if (intoType == TongleAppConst.FINDFRAGMENT_INTO) {
            ServiceManager.hotSearchByTypeRequest(TongleAppConst.FIND_TYPE_EXPERT, user_follow_friend_list.get(user_follow_friend_list.size() - 1).rownum, TongleAppConst.FETCH_COUNT, hotSearchSuccessListener(false), errorListener());
        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
            //第二个参数user_id是list里最后一个参数的id
            ExpertRequestParam param = new ExpertRequestParam();
            param.user_id = TongleAppInstance.getInstance().getUserID();
            param.expert_user_id = user_follow_friend_list.get(user_follow_friend_list.size() - 1).user_id;
            param.fetch_count = TongleAppConst.FETCH_COUNT;
            param.forward = "1";
            ServiceManager.requestCollectExpert(param, successListener(false), errorListener());
        } else if (intoType == TongleAppConst.SEARCH_INTO) {
            ServiceManager.searchRequest(TongleAppConst.FIND_TYPE_EXPERT, key, user_follow_friend_list.get(user_follow_friend_list.size() - 1).user_id, TongleAppConst.FETCH_COUNT, hotSearchSuccessListener(false), errorListener());
        }
    }

    /**
     * 获取热门搜索的会员 网络响应成功
     *
     * @return
     */
    private Response.Listener<SearchResult> hotSearchSuccessListener(final boolean clearListFlag) {
        return new Response.Listener<SearchResult>() {
            @Override
            public void onResponse(SearchResult searchResult) {
                progressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(searchResult.getHead().getReturn_flag())) {
                    if (user_follow_friend_list != null && clearListFlag) {
                        user_follow_friend_list.clear();
                    }
                    user_follow_friend_list.addAll(searchResult.getBody().getUser_list());
                    listView.setPullLoadEnable(searchResult.getBody().getData_total_count() > user_follow_friend_list.size() ? true : false);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + searchResult.getHead().getReturn_message());
                }
                //调用handler，发送消息
                mHandler.sendEmptyMessage(0);
            }
        };
    }

    /**
     * 功能：网络响应成功，返回数据 收藏
     */
    private Response.Listener<MyFollowExpertListResult> successListener(final boolean clearFlag) {
        return new Response.Listener<MyFollowExpertListResult>() {
            @Override
            public void onResponse(MyFollowExpertListResult response) {
                if (TongleAppConst.SUCCESS.equals(response.head.getReturn_flag())) {
                    //成功获取到数据后，清空list
                    if (user_follow_friend_list != null && clearFlag) {
                        user_follow_friend_list.clear();
                    }
                    user_follow_friend_list.addAll(response.body.user_follow_friend_list);
                    listView.setPullLoadEnable(Integer.parseInt(response.body.data_total_count) > user_follow_friend_list.size() ? true : false);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.head.getReturn_message());
                }
                //调用handler，发送消息
                mHandler.sendEmptyMessage(0);
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        }
        //防止每次显示fragment都去加载数据. intoType为收藏时例外，是否刷新由MEXPERTFRAGMENT_UPDATE_FLAG 和 UPDATE_LIST_FLAG决定
        if (!isLoadData) {
            doRequest();
        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
            if (MEXPERTFRAGMENT_UPDATE_FLAG || UPDATE_LIST_FLAG) {
                onRefresh(0);
            }
        }

    }

    /**
     * intotype为 关键字搜索时调用。
     *
     * @param keywords
     */
    @Override
    public void setKeywords(String keywords) {
        if (!TextUtils.isEmpty(keywords) && !keywords.equals(key)) {
            isLoadData = false;
        }
        key = keywords;
        lazyLoad();
    }
}
