package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/1/11
 * Desc:店铺主题全部商品列表
 * used by 10005
 */

public class StoreGoodsListInfo implements Serializable {
    private int data_total_count;//商品总件数
    private ArrayList<StoreGoodsInfo> store_goods_list;//商品列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public ArrayList<StoreGoodsInfo> getStore_goods_list() {
        return store_goods_list;
    }

    public void setStore_goods_list(ArrayList<StoreGoodsInfo> store_goods_list) {
        this.store_goods_list = store_goods_list;
    }
}
