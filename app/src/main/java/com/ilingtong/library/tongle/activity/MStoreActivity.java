package com.ilingtong.library.tongle.activity;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.MStoreFragmentAdapter;
import com.ilingtong.library.tongle.fragment.HomeFragment;
import com.ilingtong.library.tongle.fragment.MStoreFragment;
import com.ilingtong.library.tongle.fragment.StoreGoodsFragment;
import com.ilingtong.library.tongle.fragment.StoreHomeFragment;
import com.ilingtong.library.tongle.fragment.StorePostFragment;
import com.ilingtong.library.tongle.listener.ImageLoadListener;
import com.ilingtong.library.tongle.protocol.FavoriteListItem;
import com.ilingtong.library.tongle.protocol.MStoreDetailCollectResult;
import com.ilingtong.library.tongle.protocol.MStoreDetailsResult;
import com.ilingtong.library.tongle.protocol.MStoreTheme;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SearchEditText;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fengguowei
 * @ClassName: MStoreActivity
 * @Description: 魔店主页
 * @date 2017/1/12 17:06
 */

public class MStoreActivity extends BaseFragmentActivity implements SearchEditText.ISearchListener {
    private ImageLoader imageLoader;
    private LinearLayout backLayout;
    private TextView txStoreName;
    private TextView txFansNum;
    private TextView txProductNum;
    private ImageView storeBgImageView;
    private ImageView storeLogoImageView;
    private TextView txAddAttention;
    private int intoType;//标识由何页面进入
    private String originalAttention;//进入页面时的关注状态
    private String finalAttention;//最终的关注状态
    private SearchEditText edtSearch;
    private ImageView prodClassImg;
    private View cursor;
    private int mCursorWidth;//每个标签页所占的宽度
    private int mOffset;//游标偏移量
    private int mCurrentIndex;
    private RadioGroup mRadioGroup;
    private List<RadioButton> mRadioButtonList;
    private List<Map<String, Map<String, Drawable>>> mThemeMapList;
    private int screenWidth;
    private MStoreFragmentAdapter mFragmentAdapter;
    private ViewPager mViewPager;
    private List<Fragment> mListFragment;
    private FragmentManager mFragmentManager;
    private Dialog dialog;//提示Dialog
    private String mStoreId;
    private final int THEME_LOADED = 0;//服务器返回多个theme
    private final int THEME_NONE = 1;//服务器返回0个theme
    private final String NORMAL_KEY = "normal";
    private final String PRESS_KEY = "press";
    private ImageView img_overseas_flag;   //海淘店铺标志
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case THEME_LOADED:
                    boolean isLoaded = true;//主题icon是否加载完毕
                    for (Map<String, Map<String, Drawable>> mapThemeName : mThemeMapList) {
                        for (Map.Entry<String, Map<String, Drawable>> mapThemeState : mapThemeName.entrySet()) {
                            for (Map.Entry<String, Drawable> themeIcon : mapThemeState.getValue().entrySet()) {
                                isLoaded = isLoaded && (themeIcon.getValue() != null);
                            }
                        }
                    }
                    if (isLoaded) {
                        Map<String, Map<String, Drawable>> mapDynamic = new HashMap<String, Map<String, Drawable>>();
                        Map<String, Drawable> mapDynamicIcon = new HashMap<String, Drawable>();
                        mapDynamicIcon.put(NORMAL_KEY, getResources().getDrawable(R.drawable.ms_dynamic_icon_normal));
                        mapDynamicIcon.put(PRESS_KEY, getResources().getDrawable(R.drawable.ms_dynamic_icon_pressed));
                        mapDynamic.put(getString(R.string.ms_dynamic), mapDynamicIcon);
                        mThemeMapList.add(mapDynamic);
                        addRadioButtons();
                    }
                    break;
                case THEME_NONE:
                    addRadioButtons();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mstore_layout);
        mStoreId = getIntent().getStringExtra("mstore_id");
        intoType = getIntent().getIntExtra(TongleAppConst.INTO_TYPE, 0);
        dialog = DialogUtils.createLoadingDialog(this);
        imageLoader = ImageLoader.getInstance();
        initView();
        doRequest();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initView() {
        backLayout = (LinearLayout) findViewById(R.id.back_layout);
        edtSearch = (SearchEditText) findViewById(R.id.search_edt);
        prodClassImg = (ImageView) findViewById(R.id.prod_class_img);

        edtSearch.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //禁止长按出现粘贴情形
                return true;
            }
        });
        edtSearch.setSearchListener(this);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StoreSearchActivity.launcher(MStoreActivity.this, mStoreId);
            }
        });
        prodClassImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StoreFieldActivity.launcher(MStoreActivity.this, mStoreId);
            }
        });
        txStoreName = (TextView) findViewById(R.id.mstore_name);
        txFansNum = (TextView) findViewById(R.id.fans_num);
        txProductNum = (TextView) findViewById(R.id.product_num);
        storeBgImageView = (ImageView) findViewById(R.id.store_bg_img);
        storeLogoImageView = (ImageView) findViewById(R.id.store_logo_img);
        txAddAttention = (TextView) findViewById(R.id.add_attention);
        mRadioGroup = (RadioGroup) findViewById(R.id.mstore_rg);
        img_overseas_flag = (ImageView) findViewById(R.id.store_img_overseas_flag);
        txAddAttention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txAddAttention.getText().toString().equals(getString(R.string.add_attention))) {
                    addAttention();
                } else if (txAddAttention.getText().toString().equals(getString(R.string.add_attention_already))) {
                    cancelAttention();
                }
            }
        });
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i = 0; i < mRadioButtonList.size(); i++) {
                    if (mRadioButtonList.get(i).isChecked()) {
                        mViewPager.setCurrentItem(i);
                    }
                }
            }
        });
        // 获取手机屏幕分辨率(宽度)
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        screenWidth = dm.widthPixels;
    }

    /**
     * @param
     * @return
     * @throws
     * @author fengguowei
     * @date 2017/1/12 17:05
     * @Title: ${enclosing_method}
     * @Description: 请求魔店主页以及首页数据
     */
    private void doRequest() {
        dialog.show();
        ServiceManager.getMStoreDetails(mStoreId, null, TongleAppConst.FORWORD_DONW, "10", new Response.Listener<MStoreDetailsResult>() {
            @Override
            public void onResponse(MStoreDetailsResult result) {
                dialog.dismiss();
                img_overseas_flag.setVisibility(TongleAppConst.YES.equals(result.getBody().getStore_info().overseas_flg) ? View.VISIBLE : View.GONE);
                txStoreName.setText(result.getBody().getStore_info().getStore_name());
                txFansNum.setText(String.format(getString(R.string.fans_num), result.getBody().getStore_info().getFans_count()));
                txProductNum.setText(String.format(getString(R.string.product_num), result.getBody().getStore_info().getGoods_count()));
                if (result.getBody().getStore_info().getFavorite_flg().equals(TongleAppConst.Add_ATTENTION_YES)) {//已关注
                    txAddAttention.setText(getString(R.string.add_attention_already));
                } else if (result.getBody().getStore_info().getFavorite_flg().equals(TongleAppConst.Add_ATTENTION_NO)) {//未关注
                    txAddAttention.setText(getString(R.string.add_attention));
                }
                originalAttention = result.getBody().getStore_info().getFavorite_flg();
                imageLoader.displayImage(result.getBody().getStore_info().getStore_hp_pic(), storeBgImageView, ImageOptionsUtils.getOptions());
                imageLoader.displayImage(result.getBody().getStore_info().getStore_logo(), storeLogoImageView, ImageOptionsUtils.getHeadIconOptions());
                //加载主题icon
                loadThemeIcon(result.getBody().getStore_theme_list());
                initFragment(result);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();

            }
        });
    }

    /**
     * @param themeList 主题列表
     * @author fengguowei
     * @date 2017/1/13 10:15
     * @Title: ${enclosing_method}
     * @Description: 获取主题图片资源
     * @ret @throws
     */
    private void loadThemeIcon(List<MStoreTheme> themeList) {
        mRadioButtonList = new ArrayList<RadioButton>();
        RadioButton radioButtonHome = (RadioButton) LayoutInflater.from(MStoreActivity.this).inflate(R.layout.ms_radio_button, null);
        radioButtonHome.setId(Integer.valueOf(1));//设置Id实现互斥
        radioButtonHome.setText(getString(R.string.ms_home));

        mRadioButtonList.add(radioButtonHome);
        for (int i = 0; i < themeList.size(); i++) {
            RadioButton radioButton = (RadioButton) LayoutInflater.from(MStoreActivity.this).inflate(R.layout.ms_radio_button, null);
            radioButton.setId(Integer.valueOf(i + 2));
            radioButton.setText(themeList.get(i).getTheme_name());
            mRadioButtonList.add(radioButton);
        }
        RadioButton radioButtonDynamic = (RadioButton) LayoutInflater.from(MStoreActivity.this).inflate(R.layout.ms_radio_button, null);
        radioButtonDynamic.setId(Integer.valueOf(mRadioButtonList.size() + 1));
        radioButtonDynamic.setText(getString(R.string.ms_dynamic));
        mRadioButtonList.add(radioButtonDynamic);

        mThemeMapList = new ArrayList<Map<String, Map<String, Drawable>>>();
        Map<String, Map<String, Drawable>> mapHome = new HashMap<String, Map<String, Drawable>>();
        Map<String, Drawable> mapHomeIcon = new HashMap<String, Drawable>();
        mapHomeIcon.put(NORMAL_KEY, getResources().getDrawable(R.drawable.ms_home_icon_normal));
        mapHomeIcon.put(PRESS_KEY, getResources().getDrawable(R.drawable.ms_home_icon_pressed));
        mapHome.put(getString(R.string.ms_home), mapHomeIcon);
        mThemeMapList.add(mapHome);
        if (themeList.size() == 0) {
            Map<String, Map<String, Drawable>> mapDynamic = new HashMap<String, Map<String, Drawable>>();
            Map<String, Drawable> mapDynamicIcon = new HashMap<String, Drawable>();
            mapDynamicIcon.put(NORMAL_KEY, getResources().getDrawable(R.drawable.ms_dynamic_icon_normal));
            mapDynamicIcon.put(PRESS_KEY, getResources().getDrawable(R.drawable.ms_dynamic_icon_pressed));
            mapDynamic.put(getString(R.string.ms_dynamic), mapDynamicIcon);
            mThemeMapList.add(mapDynamic);
            handler.sendEmptyMessage(THEME_NONE);
            return;
        }
        for (final MStoreTheme theme : themeList) {
            Map<String, Map<String, Drawable>> mapTheme = new HashMap<String, Map<String, Drawable>>();
            Map<String, Drawable> mapThemeIcon = new HashMap<String, Drawable>();
            mapThemeIcon.put(NORMAL_KEY, null);
            mapThemeIcon.put(PRESS_KEY, null);
            mapTheme.put(theme.getTheme_name(), mapThemeIcon);
            mThemeMapList.add(mapTheme);
            //normal
            imageLoader.loadImage(theme.getTheme_pic1(), ImageOptionsUtils.getThemeOptions(), new ImageLoadListener(theme.getTheme_name()) {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    for (Map<String, Map<String, Drawable>> map : mThemeMapList) {
                        for (Map.Entry<String, Map<String, Drawable>> mapEntry : map.entrySet()) {
                            if (mapEntry.getKey().equals(getThemeName())) {
                                Drawable drawable = new BitmapDrawable(bitmap);
                                mapEntry.getValue().put(NORMAL_KEY, drawable);
                            }
                        }
                        handler.sendEmptyMessage(THEME_LOADED);
                    }
                }


                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
            //press
            imageLoader.loadImage(theme.getTheme_pic2(), ImageOptionsUtils.getThemeOptions(), new ImageLoadListener(theme.getTheme_name()) {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    for (Map<String, Map<String, Drawable>> map : mThemeMapList) {
                        for (Map.Entry<String, Map<String, Drawable>> mapEntry : map.entrySet()) {
                            if (mapEntry.getKey().equals(getThemeName())) {
                                Drawable drawable = new BitmapDrawable(bitmap);
                                mapEntry.getValue().put(PRESS_KEY, drawable);
                            }
                        }
                        handler.sendEmptyMessage(THEME_LOADED);

                    }
                    handler.sendEmptyMessage(THEME_LOADED);

                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
        }

    }

    /**
     * @param mStoreDetailsResult 店铺信息
     * @author fengguowei
     * @date 2017/1/16 9:49
     * @Title: ${enclosing_method}
     * @Description: 初始化Fragment
     */
    private void initFragment(MStoreDetailsResult mStoreDetailsResult) {
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mListFragment = new ArrayList<Fragment>();
        mFragmentManager = getSupportFragmentManager();
        mListFragment.add(StoreHomeFragment.newInstance(mStoreId, mStoreDetailsResult.getBody()));
        for (MStoreTheme theme : mStoreDetailsResult.getBody().getStore_theme_list()) {
            mListFragment.add(StoreGoodsFragment.newInstance(theme.getTheme_id(), theme.getTheme_type(), mStoreId));
        }
        mListFragment.add(StorePostFragment.newInstance(mStoreDetailsResult.getBody().getStore_info().getStore_user_id()));

        mFragmentAdapter = new MStoreFragmentAdapter(mFragmentManager, mListFragment);
        mViewPager.setAdapter(mFragmentAdapter);
        mViewPager.setOffscreenPageLimit(mRadioButtonList.size());
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // 获取每一个标签页所占宽度
                int section = mOffset * 2 + mCursorWidth;
                // currentIndex为上一次滑动所处标签页位置
                Animation animation = new TranslateAnimation(
                        section * mCurrentIndex, section * position, 0, 0);
                mCurrentIndex = position;
                animation.setDuration(500);
                animation.setFillAfter(true);// 动画结束后停留在当前所处位置
                cursor.startAnimation(animation);
                mRadioButtonList.get(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * @author fengguowei
     * @date 2017/1/16 10:06
     * @Title: ${enclosing_method}
     * @Description: 添加RadioButton
     */
    private void addRadioButtons() {
        for (Map<String, Map<String, Drawable>> mapThemeName : mThemeMapList) {
            for (Map.Entry<String, Map<String, Drawable>> mapThemeState : mapThemeName.entrySet()) {
                for (RadioButton radioButton : mRadioButtonList) {
                    if (mapThemeState.getKey().equals(radioButton.getText().toString())) {
                        setStateListDrawable(radioButton, mapThemeState.getValue().get(NORMAL_KEY), mapThemeState.getValue().get(PRESS_KEY));
                    }
                }
            }
        }
        mRadioGroup.removeAllViews();
        mRadioGroup.setWeightSum(mRadioButtonList.size());
        for (RadioButton radioButton : mRadioButtonList) {
            radioButton.setLayoutParams(new RadioGroup.LayoutParams(com.ilingtong.library.tongle.external.RadioGroup.LayoutParams.WRAP_CONTENT, com.ilingtong.library.tongle.external.RadioGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            radioButton.setPadding(0, 0, 0, DipUtils.dip2px(6));
            mRadioGroup.addView(radioButton);
        }
        initCursor();
        mRadioButtonList.get(0).setChecked(true);
        mViewPager.setCurrentItem(0);
    }

    /**
     * @author fengguowei
     * @date 2017/1/16 9:54
     * @Title: ${enclosing_method}
     * @Description: 初始化游标
     */
    private void initCursor() {
        // 初始化横向划线的位置，需要计算偏移量并移动View，有两个方法
        cursor = (View) findViewById(R.id.cursor);
        cursor.setVisibility(View.VISIBLE);
        mCursorWidth = screenWidth / 4 - 30;
        //设置游标宽度
        cursor.setLayoutParams(new LinearLayout.LayoutParams(mCursorWidth, 4));
        mOffset = (screenWidth / mRadioGroup.getChildCount() - mCursorWidth) / 2;
        //通过LayoutParams.setMargins属性设置偏移量
        LinearLayout.MarginLayoutParams lp = (LinearLayout.MarginLayoutParams) cursor.getLayoutParams();
        lp.setMargins(mOffset, 0, 0, 0);
        cursor.setLayoutParams(lp);
    }

    /**
     * @param radioButton 组件 drawableNormal正常图片 drawablePress选中图片
     * @author fengguowei
     * @date 2017/1/16 9:47
     * @Title: ${enclosing_method}
     * @Description: 为RadioButton设置顶部drawable
     */
    private void setStateListDrawable(RadioButton radioButton, Drawable drawableNormal, Drawable drawablePress) {
        StateListDrawable stalistDrawable = new StateListDrawable();
        int checked = android.R.attr.state_checked;//state_pressed无法实现选中  与state_pressed同时设置state_checked会无效
        stalistDrawable.addState(new int[]{-checked}, drawableNormal);
        stalistDrawable.addState(new int[]{checked}, drawablePress);
        stalistDrawable.addState(new int[]{}, null);
        stalistDrawable.setBounds(0, 0, DipUtils.dip2px(20), DipUtils.dip2px(20));
        radioButton.setCompoundDrawablePadding(DipUtils.dip2px(6));

        radioButton.setCompoundDrawables(null, stalistDrawable, null, null);
    }

    private void addAttention() {
        ArrayList<FavoriteListItem> favList = new ArrayList<FavoriteListItem>();
        FavoriteListItem favListItem = new FavoriteListItem();
        favListItem.collection_type = TongleAppConst.COLLECT_TYPE_MSTORE;// 1 魔店
        favListItem.key_value = mStoreId;
        favList.add(favListItem);
        ServiceManager.doMStoreCollectRequest(TongleAppInstance.getInstance().getUserID(), favList, "", new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult result) {
                if (TongleAppConst.ADD_FAVORITE.equals(result.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(result.getHead().getReturn_flag())) {
                        txAddAttention.setText(getString(R.string.add_attention_already));
                        finalAttention = TongleAppConst.Add_ATTENTION_YES;
                    } else {
                        ToastUtils.toastLong(result.getHead().getReturn_message());
                    }
                    if (!originalAttention.equals(finalAttention)) {//关注状态改变
                        if (intoType == TongleAppConst.HOME_INTO) {
                            HomeFragment.MSTORE_UPDATE_FLAG = true;
                            //更新收藏页
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;
                        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                            MStoreFragment.UPDATE_LIST_FLAG = false; //从收藏页进来的，表示刚刚取消了收藏之后又重新收藏。回收藏页时不需要刷新。首页同理
                            HomeFragment.MSTORE_UPDATE_FLAG = false;
                        } else {
                            HomeFragment.MSTORE_UPDATE_FLAG_OTHER = true; //如果是从发现等页面进入，则必须都在显示首页时刷仙首页魔店列表
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新魔店列表
                            //更新收藏页
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;
                            MStoreFragment.UPDATE_LIST_FLAG = true;
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        });
    }

    private void cancelAttention() {
        ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), mStoreId, TongleAppConst.COLLECT_TYPE_MSTORE, new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult result) {
                if (TongleAppConst.CANCEL_FAVORITE.equals(result.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(result.getHead().getReturn_flag())) {
                        txAddAttention.setText(getString(R.string.add_attention));
                        finalAttention = TongleAppConst.Add_ATTENTION_NO;
                    } else {
                        ToastUtils.toastLong(result.getHead().getReturn_message());
                    }
                    if (!originalAttention.equals(finalAttention)) {//关注状态改变
                        if (intoType == TongleAppConst.HOME_INTO) {
                            HomeFragment.MSTORE_UPDATE_FLAG = true;
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;
                            //更新收藏页
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;
                            MStoreFragment.UPDATE_LIST_FLAG = true;
                        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                            MStoreFragment.UPDATE_LIST_FLAG = true; //表示从收藏页过来，取消收藏该魔店。回页面时需刷新。显示首页时也需刷新
                            HomeFragment.MSTORE_UPDATE_FLAG = true;
                        } else {
                            HomeFragment.MSTORE_UPDATE_FLAG_OTHER = true; //如果是从发现等页面进入，则必须都在显示首页时刷仙首页魔店列表
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新魔店列表
                            //更新收藏页
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;
                            MStoreFragment.UPDATE_LIST_FLAG = true;
                        }
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        });
    }

    @Override
    public void search() {
    }
}
