package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.ProdDetailListItem;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * User: syc
 * Date: 2015/6/12
 * Time: 13:53
 * Email: ycshi@isoftstone.com
 * Desc:1040
 */
public class OrderFormListItemListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<ProdDetailListItem> list;
    private ProdDetailListItem Itemdata = new ProdDetailListItem();
    private Context mContext;

    public OrderFormListItemListAdapter(Context context, List<ProdDetailListItem> list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView ItemImageView;
        TextView prod_name;
        TextView prod_price;
        TextView prod_count;
        TextView txt_spec;  //规格参数
        convertView = inflater.inflate(R.layout.orderformlist_item_list, null);
        ItemImageView = (ImageView) convertView.findViewById(R.id.orderformlist_itemlist_image);
        prod_name = (TextView) convertView.findViewById(R.id.orderformlist_itemlist_name);
        prod_price = (TextView) convertView.findViewById(R.id.orderformlist_itemlist_price);
        prod_count = (TextView) convertView.findViewById(R.id.orderformlist_itemlist_count);
        txt_spec = (TextView) convertView.findViewById(R.id.orderformlist_itemlist_spec);


        Itemdata = list.get(position);

        prod_name.setText(Itemdata.prod_name);
//        prod_price.setText(mContext.getString(R.string.RMB) + Itemdata.price);
        prod_price.setText(FontUtils.priceFormat(Double.parseDouble(Itemdata.price)));
        prod_count.setText(mContext.getString(R.string.multiply) + Itemdata.quantity);
        String url = Itemdata.prod_pic_url;
        ImageLoader.getInstance().displayImage(url, ItemImageView, ImageOptionsUtils.getOptions());
        StringBuffer spec = new StringBuffer();
        if (Itemdata.prod_spec_list!=null &&Itemdata.prod_spec_list.size()>0){
            for (int i = 0; i < Itemdata.prod_spec_list.size(); i++) {
                spec.append(Itemdata.prod_spec_list.get(i).prod_spec_name+":"+Itemdata.prod_spec_list.get(i).spec_detail_name+";  ");
            }
        }
        txt_spec.setText(spec.toString());

        return convertView;

    }

    static class holder {
        TextView desc;
        ImageView listItemImageView;
        TextView txt_spec;  //规格参数
    }
}
