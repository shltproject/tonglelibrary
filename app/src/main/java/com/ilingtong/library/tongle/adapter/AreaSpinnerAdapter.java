package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.BaseDataNode;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/7/7
 * Time: 16:17
 * Email: jqleng@isoftstone.com
 * Desc: 行政区划下拉列表控件适配器
 */
public class AreaSpinnerAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<BaseDataNode> list;
    private TextView spinnerText;
    public AreaSpinnerAdapter(Context context,ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.spinner_item, null);
        if (convertView != null) {
            spinnerText = (TextView) convertView.findViewById(R.id.spinner_item_text);
            String name = ((BaseDataNode)getItem(position)).name;
            spinnerText.setText(name);
        }
        return convertView;
    }
}
