package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * @ClassName: AvailableMoneyInfo
 * @Package: com.ilingtong.library.tongle.protocol
 * @Description: used by 6036接口（我的可用余额取得） 可用余额信息类
 * @author: liuting
 * @Date: 2017/9/7 9:00
 */
public class AvailableMoneyInfo implements Serializable{
    public double money_valid; //可用余额
}
