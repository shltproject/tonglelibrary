package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.IdCardInfo;
import com.ilingtong.library.tongle.protocol.UploadIdCardInfoResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.ImageUtil;
import com.ilingtong.library.tongle.utils.PictureUtil;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.IOException;
import java.io.Serializable;

/**
 * @ClassName: SettingIdCardInfoActivity
 * @Package: com.ilingtong.library.tongle.activity
 * @Description: 设置我的身份证信息
 * @author: liuting
 * @Date: 2017/6/12 14:05
 */

public class SettingIdCardInfoActivity extends BaseActivity implements View.OnClickListener {
    private ImageView imgBack;//返回
    private TextView tvTitle;//标题
    private TextView tvSave;//保存
    private ImageView imgAddFront;//上传身份证正面照
    private ImageView imgAddOpposite;//上传身份证背面照
    private String mPositivePicName = "positive_pic.jpg";//身份证正面照片名称
    private String mOppositePicName = "opposite_pic.jpg";//身份证背面照片名称
    private static final int GET_POSITIVE_PIC_REQUEST_CODE = 1000;//上传身份证正面照请求码
    private static final int GET_OPPOSITE_PIC_REQUEST_CODE = 1001;//上传身份证背面照请求码
    private String mPositivePicPath;//身份证正面照片路径
    private String mOppositePicPath;//身份证背面照片路径
    private Dialog dialogLoading;//加载对话框
    private IdCardInfo mIdCardInfo;//身份证信息类
//    private String mDirPath = Environment.getExternalStorageDirectory().getPath() + TongleAppConst.FILE_FOLDER_NAME;//文件路径
    private RequestPermissionUtils requestPermissionUtils;//权限工具类
    private String[] needPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};//存储权限，保存图片都需要
    private SelectDialog mTipsDialog;//提示对话框

    /**
     * @throws
     * @author: liuting
     * @date: 2017/6/12 15:42
     * @MethodName: launcher
     * @Description: 页面跳转，主要是用于首页跳转
     * @param: fragment          Fragment
     * @param: idCardInfo       身份证信息类
     * @param: requestCode      请求码
     * @return: void
     */
    public static void launcher(Fragment fragment, IdCardInfo idCardInfo, int requestCode) {
        Intent intent = new Intent(fragment.getActivity(), SettingIdCardInfoActivity.class);
        intent.putExtra("idCardInfo", (Serializable) idCardInfo);
        fragment.startActivityForResult(intent, requestCode);
    }

    /**
     * @throws
     * @author: liuting
     * @date: 2017/6/12 15:42
     * @MethodName: launcher
     * @Description: 页面跳转，主要用于我的信息页面跳转
     * @param: activity          Activity
     * @param: idCardInfo       身份证信息类
     * @param: requestCode      请求码
     * @return: void
     */
    public static void launcher(Activity activity, IdCardInfo idCardInfo, int requestCode) {
        Intent intent = new Intent(activity, SettingIdCardInfoActivity.class);
        intent.putExtra("idCardInfo", (Serializable) idCardInfo);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_id_card_info_layout);
        initView();
    }

    /**
     * @throws
     * @author: liuting
     * @date: 2017/6/12 14:47
     * @MethodName: initView
     * @Description: 初始化控件
     * @param:
     * @return: void
     */
    private void initView() {
        imgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        tvTitle = (TextView) findViewById(R.id.top_name);
        tvSave = (TextView) findViewById(R.id.top_btn_text);
        imgAddFront = (ImageView) findViewById(R.id.id_card_info_img_add_front);
        imgAddOpposite = (ImageView) findViewById(R.id.id_card_info_img_add_opposite);

        imgBack.setVisibility(View.VISIBLE);
        tvTitle.setVisibility(View.VISIBLE);
        tvSave.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.id_card_info));
        tvSave.setText(getString(R.string.save));
        imgBack.setOnClickListener(this);
        tvSave.setOnClickListener(this);

        imgAddFront.setOnClickListener(this);
        imgAddOpposite.setOnClickListener(this);
        dialogLoading = DialogUtils.createLoadingDialog(SettingIdCardInfoActivity.this);

        mIdCardInfo = (IdCardInfo) getIntent().getSerializableExtra("idCardInfo");
//        mIdCardInfo.setId_card_opposite_pic("http://pic62.nipic.com/file/20150319/12632424_132215178296_2.jpg");
//        mIdCardInfo.setId_card_positive_pic("http://img1.imgtn.bdimg.com/it/u=1794894692,1423685501&fm=26&gp=0.jpg");

        saveImage();
    }

    /**
     * @author: liuting
     * @date: 2017/6/13 14:52
     * @MethodName: saveImage
     * @Description: 保存本地图片
     * @param:
     * @return: void
     * @throws
     */
    private void saveImage() {
        requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(SettingIdCardInfoActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
            @Override
            public void requestSuccess() {
                ImageLoader.getInstance().displayImage(mIdCardInfo.getId_card_positive_pic(), imgAddFront, ImageOptionsUtils.getIdCardPicOptions(), new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if (loadedImage == null) {
                            super.onLoadingComplete(imageUri, view, loadedImage);
                        } else {
                            //加载后的图片保存下来
                            if (!TextUtils.isEmpty(mIdCardInfo.getId_card_positive_pic())) {
                                try {
                                    mPositivePicPath = ImageUtil.saveImg(loadedImage, mPositivePicName).getPath();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
                ImageLoader.getInstance().displayImage(mIdCardInfo.getId_card_opposite_pic(), imgAddOpposite, ImageOptionsUtils.getIdCardPicOptions(), new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if (loadedImage == null) {
                            super.onLoadingComplete(imageUri, view, loadedImage);
                        } else {
                            //加载后的图片保存下来
                            if (!TextUtils.isEmpty(mIdCardInfo.getId_card_opposite_pic())) {
                                try {
                                    mOppositePicPath = ImageUtil.saveImg(loadedImage, mOppositePicName).getPath();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
            }

            @Override
            public void requestFail() {
                mTipsDialog = new SelectDialog(SettingIdCardInfoActivity.this, getString(R.string.please_open_the_storage_permission), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mTipsDialog.dismiss();
                        requestPermissionUtils.checkPermissions(SettingIdCardInfoActivity.this);
                    }
                });
                mTipsDialog.setCancelable(false);
                mTipsDialog.show();
            }
        });
        requestPermissionUtils.checkPermissions(SettingIdCardInfoActivity.this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.left_arrow_btn) {//返回
            finish();
        } else if (id == R.id.top_btn_text) {//保存
            doRequest();
        } else if (id == R.id.id_card_info_img_add_front) {//上传身份证正面照
            GetPhotoSelectDialogActivity.launcher(this, mPositivePicName, GET_POSITIVE_PIC_REQUEST_CODE);
        } else if (id == R.id.id_card_info_img_add_opposite) {//上传身份证背面照
            GetPhotoSelectDialogActivity.launcher(this, mOppositePicName, GET_OPPOSITE_PIC_REQUEST_CODE);
        }
    }

    /**
     * @throws
     * @author: liuting
     * @date: 2017/6/12 15:33
     * @MethodName: doRequest
     * @Description: 请求数据
     * @param:
     * @return: void
     */
    public void doRequest() {
        if (TextUtils.isEmpty(mPositivePicPath)) {
            ToastUtils.toastShort(getString(R.string.please_upload_positive_picture_of_id_card));
            return;
        }
        if (TextUtils.isEmpty(mOppositePicPath)) {
            ToastUtils.toastShort(getString(R.string.please_upload_opposite_picture_of_id_card));
            return;
        }
        dialogLoading.show();
        ServiceManager.uploadIdCardInfo(PictureUtil.bitmapToString(mPositivePicPath), PictureUtil.bitmapToString(mOppositePicPath), TongleAppInstance.getInstance().getUserID(), new Response.Listener<UploadIdCardInfoResult>() {
            @Override
            public void onResponse(UploadIdCardInfoResult uploadIdCardInfoResult) {
                dialogLoading.dismiss();
                if (uploadIdCardInfoResult.getHead().getReturn_flag().equals(TongleAppConst.SUCCESS)) {
                    ToastUtils.toastShort(getString(R.string.save_message));
                    setResult(RESULT_OK);
                    finish();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + uploadIdCardInfoResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialogLoading.dismiss();
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GET_POSITIVE_PIC_REQUEST_CODE://获取到身份证正面照
                    String positivePicPath = data.getStringExtra("outputFileUri");
                    imgAddFront.setImageBitmap(PictureUtil
                            .getSmallBitmap(positivePicPath, 480, 800));
                    mPositivePicPath = positivePicPath;
                    break;
                case GET_OPPOSITE_PIC_REQUEST_CODE://获取到身份证背面照
                    String oppositePicPath = data.getStringExtra("outputFileUri");
                    imgAddOpposite.setImageBitmap(PictureUtil
                            .getSmallBitmap(oppositePicPath, 480, 800));
                    mOppositePicPath = oppositePicPath;
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode,permissions,paramArrayOfInt);
    }

}
