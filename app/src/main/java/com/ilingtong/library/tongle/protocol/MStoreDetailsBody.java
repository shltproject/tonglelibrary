package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by fengguowei on 2017/1/12.
 */
public class MStoreDetailsBody implements Serializable{
    private MStoreBaseInfo  store_info;
    private MStorePost  store_post_info;
    private MStoreCoupon  store_voucher;
    private List<MStoreTheme> store_theme_list;

    public MStoreBaseInfo getStore_info() {
        return store_info;
    }

    public void setStore_info(MStoreBaseInfo store_info) {
        this.store_info = store_info;
    }

    public MStorePost getStore_post_info() {
        return store_post_info;
    }

    public void setStore_post_info(MStorePost store_post_info) {
        this.store_post_info = store_post_info;
    }

    public MStoreCoupon getStore_voucher() {
        return store_voucher;
    }

    public void setStore_voucher(MStoreCoupon store_voucher) {
        this.store_voucher = store_voucher;
    }

    public List<MStoreTheme> getStore_theme_list() {
        return store_theme_list;
    }

    public void setStore_theme_list(List<MStoreTheme> store_theme_list) {
        this.store_theme_list = store_theme_list;
    }
}
