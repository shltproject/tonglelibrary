package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/8/9
 * Time: 13:01
 * Email: ycshi@isoftstone.com
 * Desc: 支付宝支付
 */
public class AliPayResult implements Serializable {
    private BaseInfo head;
    private AliPayInfoResult body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public AliPayInfoResult getBody() {
        return body;
    }

    public void setBody(AliPayInfoResult body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
