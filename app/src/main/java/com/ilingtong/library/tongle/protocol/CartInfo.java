package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/3
 * Time: 17:48
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class CartInfo implements Serializable {
    private ArrayList<ShopCartListItem> my_shopping_cart;
    private ArrayList<ShopCartListItem> this_shopping_cart;

    public ArrayList<ShopCartListItem> getMy_shopping_cart() {
        return my_shopping_cart;
    }
    public ArrayList<ShopCartListItem> getThis_shopping_cart() {
        return this_shopping_cart;
    }

    @Override
    public String toString() {
        return "my_shopping_cart:" + my_shopping_cart + "\r\n" +
                "this_shopping_cart:" + this_shopping_cart;
    }
}
