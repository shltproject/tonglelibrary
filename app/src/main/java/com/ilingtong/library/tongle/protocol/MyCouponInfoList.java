package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:6014接口的body
 */
public class MyCouponInfoList implements Serializable {
    private List<MyCouponInfo> voucher;

    public List<MyCouponInfo> getVoucher() {
        return voucher;
    }

    public void setVoucher(List<MyCouponInfo> voucher) {
        this.voucher = voucher;
    }
}
