package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.adapter.CollectFragmentPagerAdapter;
import com.ilingtong.library.tongle.fragment.OrderListFragment;

/**
 * Created by wuqian on 2016/2/2.
 * mail: wuqian@ilingtong.com
 * Description: 我的订单模块mainActivity。继承fragmentActivity
 */
public class MyOrderMainActivity extends BaseFragmentActivity implements View.OnClickListener {

    private ImageView left_arrow_btn;
    private TextView top_name;
    private TextView totalorder;
    private TextView waitevaluate;
    private TextView waitpay;
    private TextView waitget;
    private ViewPager viewPager;

    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;

    private int currIndex = 0;
    private Fragment[] fragments = new Fragment[4];

    /**
     * 页面跳转
     *
     * @param activity  Activity
     */
    public static void launchActivity(Activity activity) {
        Intent intent = new Intent(activity, MyOrderMainActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_orderform_layout);
        initView();
    }

    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        totalorder = (TextView) findViewById(R.id.total_order);
        waitevaluate = (TextView) findViewById(R.id.wait_evaluate);
        waitpay = (TextView) findViewById(R.id.wait_pay);
        waitget = (TextView) findViewById(R.id.wait_get);
        viewPager = (ViewPager) findViewById(R.id.order_pager);
        imageView1 = (ImageView) findViewById(R.id.order_cursor1);
        imageView2 = (ImageView) findViewById(R.id.order_cursor2);
        imageView3 = (ImageView) findViewById(R.id.order_cursor3);
        imageView4 = (ImageView) findViewById(R.id.order_cursor4);

        top_name.setText(getString(R.string.my_order));
        top_name.setGravity(View.TEXT_ALIGNMENT_CENTER);
        top_name.setVisibility(View.VISIBLE);
        left_arrow_btn.setVisibility(View.VISIBLE);
        left_arrow_btn.setOnClickListener(this);

        totalorder.setOnClickListener(new TextOnClickListener(0));
        waitpay.setOnClickListener(new TextOnClickListener(1));
        waitget.setOnClickListener(new TextOnClickListener(2));
        waitevaluate.setOnClickListener(new TextOnClickListener(3));

        //初始化fragment并添加设置viewpagerAdapter
        viewPager = (ViewPager) findViewById(R.id.order_pager);
        fragments[0] = OrderListFragment.newInstance(OrderListFragment.TYPE_ALL);
        fragments[1] = OrderListFragment.newInstance(OrderListFragment.TYPE_WAIT_PAY);
        fragments[2] = OrderListFragment.newInstance(OrderListFragment.TYPE_WAIT_RECEIVE);
        fragments[3] = OrderListFragment.newInstance(OrderListFragment.TYPE_WAIT_EVALUATE);
        CollectFragmentPagerAdapter adapter = new CollectFragmentPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setOnPageChangeListener(new ViewPageOnPageChangeListener());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setCurrentItem(0);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.left_arrow_btn) {  //右上角小箭头返回
            this.finish();
        }

    }

    /**
     * TextOnClickListener监听
     * 点击顶部 “全部”，“待付款”，“待收货”，“待评价”时设置viewpager的选中页
     */
    private class TextOnClickListener implements View.OnClickListener {
        private int index = 0;

        public TextOnClickListener(int i) {
            index = i;
        }

        public void onClick(View v) {
            viewPager.setCurrentItem(index);
        }
    }

    /**
     * ViewPageOnPageChangeListener监听
     * viewpager选中页面改变时，设置选项卡title的字体和颜色
     */
    public class ViewPageOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int arg0) {
            currIndex = arg0;
            int i = currIndex + 1;
            setTabTitle(i);
        }
    }

    /**
     * 设置选项卡title的字体颜色
     *
     * @param tabId
     */
    public void setTabTitle(int tabId) {
        //viewpager顶部导航游标，并设置游标显示位置
        if (tabId == 1) {
            imageView1.setVisibility(View.VISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 2) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.VISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 3) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.VISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 4) {

            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.VISIBLE);
        }
        //viewpager顶部文字导航，并设置文字颜色
        Resources resource = (Resources) getResources();
        ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_selecttext_color);
        ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_text_color);

        if (tabId == 1) {
            totalorder.setTextColor(selectedTextColor);
            waitpay.setTextColor(unselectedTextColor);
            waitget.setTextColor(unselectedTextColor);
            waitevaluate.setTextColor(unselectedTextColor);

        } else if (tabId == 2) {
            totalorder.setTextColor(unselectedTextColor);
            waitpay.setTextColor(selectedTextColor);
            waitget.setTextColor(unselectedTextColor);
            waitevaluate.setTextColor(unselectedTextColor);
        } else if (tabId == 3) {
            totalorder.setTextColor(unselectedTextColor);
            waitpay.setTextColor(unselectedTextColor);
            waitget.setTextColor(selectedTextColor);
            waitevaluate.setTextColor(unselectedTextColor);
        } else if (tabId == 4) {
            totalorder.setTextColor(unselectedTextColor);
            waitpay.setTextColor(unselectedTextColor);
            waitget.setTextColor(unselectedTextColor);
            waitevaluate.setTextColor(selectedTextColor);
        }
    }
}
