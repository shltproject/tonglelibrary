package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by fengguowei on 2017/1/12.
 * 魔店优惠券信息 10004
 */
public class MStoreCoupon implements Serializable {
    private String voucher_pic;
    private String voucher_url;

    public String getVoucher_pic() {
        return voucher_pic;
    }

    public void setVoucher_pic(String voucher_pic) {
        this.voucher_pic = voucher_pic;
    }

    public String getVoucher_url() {
        return voucher_url;
    }

    public void setVoucher_url(String voucher_url) {
        this.voucher_url = voucher_url;
    }
}
