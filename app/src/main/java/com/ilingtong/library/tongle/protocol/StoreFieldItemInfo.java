package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/2/14
 * Desc:店铺分类信息类
 * used by 6023
 */

public class StoreFieldItemInfo implements Serializable{
    private String field_id;//分类ID
    private String field_name;//分类名称
    private String field_pic_url;//分类图片URL
    private List<StoreFieldInfo> sub_field_list;//子分类列表，结构为自嵌套

    public String getField_id() {
        return field_id;
    }

    public void setField_id(String field_id) {
        this.field_id = field_id;
    }

    public String getField_name() {
        return field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public String getField_pic_url() {
        return field_pic_url;
    }

    public void setField_pic_url(String field_pic_url) {
        this.field_pic_url = field_pic_url;
    }

    public List<StoreFieldInfo> getSub_field_list() {
        return sub_field_list;
    }

    public void setSub_field_list(List<StoreFieldInfo> sub_field_list) {
        this.sub_field_list = sub_field_list;
    }
}
