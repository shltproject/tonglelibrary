package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 22:05
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class MyFieldInfo implements Serializable {
    private  ArrayList<MyFieldTypeListInfo> type_list;

    public ArrayList getType_list() {
        return type_list;
    }

    public void setType_list(ArrayList<MyFieldTypeListInfo> type_list) {
        this.type_list = type_list;
    }

    @Override
    public String toString() {
        return  "type_list:" + type_list;
    }
}
