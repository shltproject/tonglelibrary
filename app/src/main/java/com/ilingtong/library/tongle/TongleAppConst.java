package com.ilingtong.library.tongle;

/**
 * User: lengjiqiang
 * Date: 2015/4/24
 * Time: 11:50
 * Email: jqleng@isoftstone.com
 * Desc: 应用中使用的所有常量 类
 */
public class TongleAppConst {
    //正式版
    //public static final String SERVER_ADDRESS = "http://www.tongler.cn/api/1.2/";

    //测试版
    public static final String SERVER_ADDRESS = "http://www.tongler.cn/api/2.0/";
    //public static final String SERVER_ADDRESS = "http://www.tongler.cn/taxapi/2.0/";


    public static final String SUCCESS = "0";
    public static final String CANCEL_FAVORITE = "1088";
    public static final String ADD_FAVORITE = "2021";
    public static final String FETCH_COUNT = "10";
    //带返回结果的activity参数
    public static final int AGREEMENT_RESULT = 2;
    //消息定义
    public static final int UPDATE_TOTALPRICE = 1;

    public static final int SELECT_UPDATE_TOTALPRICE = 2;

    //注册验证信息
    //手机号码长度
    public static final int PHONE_NUMBER_LENGTH = 11;
    //身份证号码长度(15)
    public static final int ID_NUMBER_LENGTH_five = 15;
    //身份证号码长度(18)
    public static final int ID_NUMBER_LENGTH_eight = 18;
    //手机验证码长度
    public static final int CODE_LENGTH = 6;
    //注册密码长度
    public static final int PASSWORD_LENGTH = 6;
    //邮编长度
    public static final int POST_LENGTH = 6;

    public static final String PRODUCT = "0";
    public static final String MSTORE = "1";
    public static final String MEMBER = "2";
    public static final String POST = "3";
    public static final String MYPRODUCT = "4";

    //基础数据类型
    public static final String ADMINISTRATIVE = "1";    //	省市区列表
    public static final String PAY_MODE = "2";          //	支付方式列表
    public static final String DELIVERY_MODE = "3";     //	配送方式列表
    public static final String BILL_TYPE = "4";         //	发票类型列表
    public static final String BILL_CONTENT = "5";      //	发票内容区分列表
    public static final String DELIVERY_TIME = "6";     //	配送时间
    public static final String GROUP_PAY_MODE = "9";    //团购券支付方式列表

    //移动平台编号
    public static final String IOS = "1";               //	Ios
    public static final String IPAD = "2";              //	iPad
    public static final String ANDROID_PHONE = "3";     //	android phone
    public static final String ANDROID_PAD = "4";       //	android pad

    //收益类型
    public static final String CONSUMPTIONINTEGRAL = "1";     //消费积分
    public static final String INTEGRALREBATE = "2";          //返利积分
    public static final String ACCOUNTBALANCE = "3";            //	账户余额
    //收藏类别
    public static final String COLLECT_TYPE_PRODUCT = "0";      //	商品
    public static final String COLLECT_TYPE_MSTORE = "1";      //	魔店
    public static final String COLLECT_TYPE_MEMBER = "2";       //	会员
    public static final String COLLECT_TYPE_FORUM = "3";        //	帖子
    public static final String COLLECT_TYPE_PRECIOUS = "4";      //	宝贝商品

    //订单状态过滤条件
    public static final String ORDER_FILTER_TOTAL = "0";    //	全部
    public static final String ORDER_FILTER_PENDING_REVIEW = "1";   //	待审核
    public static final String ORDER_FILTER_PENDING_PAY = "2";  //	待付款
    public static final String ORDER_FILTER_PENGDING_DELIVERY = "3";    //	待收货
    public static final String ORDER_FILTER_PENDING_COMMENT = "4";  //	待评价
    //订单状态
    public static final String ORDER_NEW = "0";             //	新单
    public static final String ORDER_REVIEWED = "1";        //	已审核
    public static final String ORDER_PAID = "2";            //	已付款
    public static final String ORDER_SENDING = "3";         //	发货中sssssssssss
    public static final String ORDER_NO_COMMENTS = "4";    //	完成未评价ssssssssssss
    public static final String ORDER_HAVE_COMMENTS = "5";   //	已评价
    public static final String ORDER_REJECT = "6";          //	拒收
    public static final String ORDER_NO_PAY = "7";          //	待付款ssssssssss
    public static final String ORDER_PAY_CONFIRM = "8";     //付款确认中(微信支付使用)
    public static final String ORDER_CANCEL = "12";         //订单取消  add on 2017/07/28
    //评价等级
    public static final String RATE_TOTAL = "0";            //	全部
    public static final String RATE_GOOD = "1";             //	好评
    public static final String RATE_GENERAL = "2";          //	中评
    public static final String RATE_BAD = "3";              //	差评
    //活动模式
    public static final String ACTIVITY_PRODUCT = "0";            //	商品
    public static final String ACTIVITY_FORUM = "1";             //	帖子
    public static final String ACTIVITY_MSTORE = "2";          //	魔店
    public static final String ACTIVITY_MEMBER = "3";              //	会员
    public static final String ACTIVITY_LINK = "4";     //链接

    //翻页
    public static final String FORWORD_DONW = "1";  //向下翻页，加载更多

    //进入页标志
    public static final String INTO_TYPE = "into_type"; //进入方式的key
    public static final int HOME_INTO = 1001; //首页进入
    public static final int COLLECTFRAGMENT_INTO = 1003; //收藏页进入
    public static final int FINDFRAGMENT_INTO = 1004; //发现页进入
    public static final int SEARCH_INTO = 1005;  //从关键字搜索页面进入
    public static final int MYBABY_INTO = 1006; //从我的宝贝页面进入

    //转发类别
    public static final String FORWARDTYPE_WEIXIN = "3";//微信转发类别
    public static final String FORWARDTYPE_INNER = "1"; //站内转发类别
    public static final String FORWARDTYPE_SINA = "2"; //新浪微博转发类别

    //sharesdk、微信，新浪id和密钥
//    public static final String SHARE_SDK_ID = "8890093a6278";//sharesdk注册平台id
//    public static final String WECHAT_APPID = "wxa44de924715e51b9";//微信平台appid
//    public static final String WECHAT_SECRET = "04f00d143acc9c7f52ee9eeab74275b8";//微信平台密钥
//
//    //全乐
////    public static final String WECHAT_APPID="wx1e36fa3419143338";
////    public static final String WECHAT_SECRET = "adea9a5e981b7cd6ca1d9d0e08a78dbc";//微信平台密钥
//
//    public static final String SINA_APPKEY = "162262167";//新浪平台app_key
//    public static final String SINA_APPSECRET = "7f5c6a9c120e91f9d072b99efe1a1126";//新浪平台密钥
//    public static final String SINA_REDIRECT = "https://127.0.0.1/oauth2/default.html";//新浪平台回调地址

    public static final String MAINACTIVITY_NEWINTENT_FLAG_NAME = "MainActiviyNewIntentFlag";  //启动MainActivity时intent Extra的name
    public static final String MAINACTIVITY_NEWINTENT_FLAG_CART = "launcherCart";  //为该值时表示跳转到购物车页面
    public static final String MAINACTIVITY_NEWINTENT_FLAG_MAIN = "launcherMain";  //为该值时表示跳转到首页
    public static final String MAINACTIVITY_NEWINTENT_FLAG_FIND = "launcherFind";  //为该值时表示跳转到发现

    //是否常量标识
    public static final String YES = "0";  //是
    public static final String NO = "1";   //否

    //通关状态
    public static final String CUSTOMS_FLAG_SUCCESS = "0";  //通关成功
    public static final String CUSTOMS_FLAG_FAILURE = "1";  //通关失败
    public static final String CUSTOMS_FLAG_OHTER = "2";  //其他 通关中等

    //是否可退
    public static final String PRODUCT_REFUNDABLE = "0";  //是
    public static final String PRODUCT_NO_REFUNDABLE = "1";  //否

    //团购商品状态
    public static final String PRODUCT_FLAG_ON_SALE = "0";  //立即购买
    public static final String PRODUCT_FLAG_SELL_OUT = "1";  //卖光了
    public static final String PRODUCT_FLAG_END = "2";  //已结束

    public static final int REQUEST_CODE_SELECT_COUPON = 1007; //去往选择优惠券请求码
    public static final int REQUEST_OK_SELECT_COUPON = 1111; //去往选择优惠券请求成功

    public static final int IMG_LINE_HEIGHT = 34; //垂直虚线高度

    //团购券状态：未消费/过期
    public static final String GROUP_TICKET_NOT_USED = "0"; //未消费
    public static final String GROUP_TICKET_OUT_OF = "1"; //过期
    public static final String TICKET_STATUS_USED = "2";  //已使用
    public static final String TICKET_STATUS_REFUND = "3";  //已退款
    public static final String TICKET_STATUS_REFUNDING = "4";  //退款中

    //团购券订单过滤条件
    public static final String GROUP_ORDER_ALL = "";  //全部
    public static final String GROUP_ORDER_UNUSED = "0";  //未消费
    public static final String GROUP_ORDER_REFUND = "1";  //退款单

    //支付方式：支付宝、网银、微信
    public static final String PAY_TYPE_ALIPAY = "1";  //支付宝
    public static final String PAY_TYPE_WECHAT = "3";  //微信
    public static final String PAY_TYPE_BANK = "4";  //网银

    public static final int PAY_TYPE_INT_ALIPAY = 1;  //支付宝
    public static final int PAY_TYPE_INT_WECHAT = 3;  //微信
    public static final int PAY_TYPE_INT_CASH = 0;  //货到付款

    //支付状态
    public static final String FLAG_NO_PAY = "0";  //未支付
    public static final String FLAG_PAID = "1";  //已支付

    //平台区分
    public static final String FLAG_ISSUE_ALL = "1";  //平台发行
    public static final String FLAG_ISSUE_STORE = "2";  //店铺发行

    //优惠券状态
    public static final String COUPON_STATUS_NO_RECEIVE = "1";  //未领取
    public static final String COUPON_STATUS_RECEIVED = "2";  //已领取
    public static final String COUPON_STATUS_USED = "3";  //已使用
    public static final String COUPON_STATUS_EXPIRED = "4";  //已过期

    //链接类型
    public static final String COUPON_LINK_TYPE_PRODUCT = "0";  //商品
    public static final String COUPON_LINK_TYPE_POST = "1";  //帖子
    public static final String COUPON_LINK_TYPE_STORE = "2";  //魔店
    public static final String COUPON_LINK_TYPE_MEMBER = "3";  //会员
    public static final String COUPON_LINK_TYPE_HOME = "4";  //首页
    public static final String COUPON_LINK_TYPE_HTML = "5";  //H5

    public static final int REQUEST_CODE_SUBMIT = 1008; //去往提交订单请求码
    public static final int REQUEST_OK_SUBMIT = 1112; //去往提交订单请求成功

    //商品入口明细ActionID
    public static final String ACTIONID_SALES = "1";     //首页活动(商品)
    public static final String ACTIONID_POST = "2"; // 帖子/杂志
    public static final String ACTIONID_ORDER = "3"; // 订单
    public static final String ACTIONID_SCAN_CODE = "4"; // 扫码
    public static final String ACTIONID_SHOPCART = "5"; // 购物车
    public static final String ACTIONID_MSTORE = "6"; // 魔店
    public static final String ACTIONID_COLLECT = "7"; // 收藏
    public static final String ACTIONID_MY_BABY = "8"; // 我的宝贝（本人）
    public static final String ACTIONID_EXPERT_BABY = "9"; // 我的宝贝（他人）
    public static final String ACTIONID_HOT_PRODUCT = "10";  //  热门商品
    public static final String ACTIONID_ORG_PRODUCT = "11";  //  组织推荐商品

    //团购券号格式化长度
    public static final int COUPON_SEQUENCE_LENGHT = 4; //团购券序列化分组每组字串个数。

    //二维码类别
    public static final String STREXPERT = "2";//达人详情
    public static final String STRSTORE = "4";//魔店详情
    public static final String STRPRODUCT = "1";//产品详情
    public static final String STRFORUM = "3";//帖子详情
    public static final String STRGROUP_PRODUCT = "5";  //团购商品
    public static final String STRORGANIZATION = "6";  //组织
    public static final String STRRELATE_PRODUCT = "7";  //商品  Add 2016/07/21


    /***********
     * add on 2016/12/27  简化收货地址操作流程
     *************/
    public static final String TYPE_SETTING = "0";  //设置界面标志
    public static final String TYPE_SELECT = "1";  //选择地址列表界面标志
    public static final String TYPE_ORDER_ADD = "2";  //确认订单页面直接新增地址
    /***********
     * add on 2016/12/27  简化收货地址操作流程  end
     *************/

    //是否关注常量
    public static final String Add_ATTENTION_YES = "0";
    public static final String Add_ATTENTION_NO = "1";

    //搜索区分
    public static final String SEARCH_KEY = "0";//关键字搜索
    public static final String SEARCH_CLASS = "1";//分类搜索

    //搜索结果排序区分
    public static final String MULTIPLE_SORT = "0";//综合排序
    public static final String SALES_SORT = "1";//销量排序
    public static final String NEW_PROD_SORT = "3";//新品
    public static final String PRICE_SORT = "2";//价格排序

    //排序顺序
    public static final String SORT_UP = "0";//升序
    public static final String SORT_DOWN = "1";//降序

    public static final String FILE_FOLDER_NAME = "/tongle";

    //身份证审核状态
    public static final String STATUS_NOT_APPLY = "0";//未提交
    public static final String STATUS_WAIT_CHECK = "1";//待审核
    public static final String STATUS_APPLY_PASS = "2";//审核通过
    public static final String STATUS_APPLY_REJECT = "3";//审核被拒

    //订单类别
    public static final String ORDER_TYPE_GENERAL ="0";       //一般订单
    public static final String ORDER_TYPE_GROUP ="1";       //团购订单
    public static final String ORDER_TYPE_OVERSEAS ="2";       //海淘订单

    //订单内商品是否成团
    public static final String ORDER_ALREADY ="0";       //已成团
    public static final String ORDER_SOON ="1";         //即将成团
    public static final String ORDER_NOT ="2";      //未成团

    //查找类型
    public static final String FIND_TYPE_PRODUCT  = "0";       //	商品
    public static final String FIND_TYPE_MSTORE  = "1";       //	魔店
    public static final String FIND_TYPE_EXPERT  = "2";       //	会员
    public static final String FIND_TYPE_POST  = "3";       //	帖子
}
