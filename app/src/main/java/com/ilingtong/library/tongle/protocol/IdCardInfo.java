package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * @ClassName: IdCardInfo
 * @Package: com.ilingtong.library.tongle.protocol
 * @Description: 身份证信息 used by 2012 获取我的基本信息
 * @author: liuting
 * @Date: 2017/6/12 14:56
 */

public class IdCardInfo  implements Serializable {
    private String id_card_positive_pic;//身份证照片正面
    private String id_card_opposite_pic;//身份证照片背面
    private String apply_status;//身份证审核状态，0：未提交，1：已提交待审核，2：已提交审核通过，3：已提交审核被拒

    public String getId_card_positive_pic() {
        return id_card_positive_pic;
    }

    public void setId_card_positive_pic(String id_card_positive_pic) {
        this.id_card_positive_pic = id_card_positive_pic;
    }

    public String getId_card_opposite_pic() {
        return id_card_opposite_pic;
    }

    public void setId_card_opposite_pic(String id_card_opposite_pic) {
        this.id_card_opposite_pic = id_card_opposite_pic;
    }

    public String getApply_status() {
        return apply_status;
    }

    public void setApply_status(String apply_status) {
        this.apply_status = apply_status;
    }
}
