package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.MyUsefulCouponAdapter;
import com.ilingtong.library.tongle.protocol.MyCouponAvailableItem;
import com.ilingtong.library.tongle.protocol.MyCouponAvailableResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.io.Serializable;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/9
 * Time: 11:38
 * Email: liuting@ilingtong.com
 * Desc:选择优惠券
 */
public class SelectCouponActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标
    private ListView mLvCoupon;//我的优惠券列表
    private MyCouponAvailableResult mMyCouponAvailableResult;//我的可用优惠券
    private MyUsefulCouponAdapter mMyUsefulCouponAdapter;//我的可用优惠券列表Adapter
    private String mProductId;//商品ID
    private int mProductNum;//商品数量
    private String mCouponId;//优惠券券号
    private String mSpecId;//规格ID

    private Dialog mDialog;//加载对话框
    private List<MyCouponAvailableItem> goodsInfos;  //订单商品列表

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_coupon);
        initView();//初始化控件
        getData();//取得数据
    }

    public void initView() {
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle.setText(getResources().getString(R.string.my_coupon_top_name));
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);

        mLvCoupon = (ListView) findViewById(R.id.select_coupon_lv_list);

        mDialog = DialogUtils.createLoadingDialog(SelectCouponActivity.this);
        mDialog.setCancelable(false);
        mDialog.show();

//        mProductId=(String)getIntent().getSerializableExtra("product_id");
//        mProductNum=(int)getIntent().getSerializableExtra("product_num");
        mCouponId = (String) getIntent().getSerializableExtra("coupon_id");
//        mSpecId=(String)getIntent().getSerializableExtra("spec_id");
        goodsInfos = (List<MyCouponAvailableItem>) getIntent().getSerializableExtra("goods_info");
    }

    public void getData() {
        mMyCouponAvailableResult = new MyCouponAvailableResult();
//        mMyCouponAvailableResult = (MyCouponAvailableResult) TestInterface.parseJson(MyCouponAvailableResult.class, "6015.txt");
//        MyCouponAvailableItem myCouponAvailableItem=new MyCouponAvailableItem();
//        ArrayList<MyCouponAvailableItem> goods_info=new ArrayList<>();
//        myCouponAvailableItem.goods_id=mProductId;
//        myCouponAvailableItem.spec_id1=mSpecId;
//        myCouponAvailableItem.buy_count=mProductNum+"";
//        myCouponAvailableItem.spec_id2="";
//        goods_info.add(myCouponAvailableItem);
        ServiceManager.doMyUsefulCouponRequest(goodsInfos, successListener(), errorListener());
    }

    /**
     * 功能：网络响应成功，返回数据
     *
     * @return
     */
    private Response.Listener successListener() {
        return new Response.Listener<MyCouponAvailableResult>() {
            @Override
            public void onResponse(MyCouponAvailableResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mMyCouponAvailableResult = response;
                    initData();
                } else {
                    ToastUtils.toastLong(response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     *
     * @return
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                mDialog.dismiss();
            }
        };
    }


    /**
     * 初始化数据
     */
    public void initData() {
        mMyUsefulCouponAdapter = new MyUsefulCouponAdapter(SelectCouponActivity.this, mMyCouponAvailableResult.getBody().getVoucher(), mCouponId);
        mLvCoupon.setAdapter(mMyUsefulCouponAdapter);
        mLvCoupon.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                View selectView = mLvCoupon.getChildAt(position);
                ImageView imgSelect = (ImageView) selectView.findViewById(R.id.select_coupon_list_item_img_select);
                imgSelect.setImageDrawable(getResources().getDrawable(R.drawable.check_box_press));
                Intent intent = new Intent();
                intent.putExtra("my_coupon_base_info", mMyCouponAvailableResult.getBody().getVoucher().get(position).getVoucher_base());
                /*
                 * 调用setResult方法表示我将Intent对象返回给之前的那个Activity，这样就可以在onActivityResult方法中得到Intent对象，
                 */
                setResult(TongleAppConst.REQUEST_OK_SELECT_COUPON, intent);
                finish();
            }
        });
    }

//    /**
//     * @param context
//     * @param product_id 商品ID
//     * @param product_num 商品数量
//     * @param coupon_id 优惠券券号：第一次进入时为空，如果之前选择过，则不为空
//     * @param spec_id   规格ID
//     */
//    public static void launcher(Activity context, String product_id,int product_num,String coupon_id,String spec_id) {
//        Intent intent = new Intent(context, SelectCouponActivity.class);
//        intent.putExtra("product_id", (Serializable) product_id);
//        intent.putExtra("product_num", (Serializable) product_num);
//        intent.putExtra("coupon_id", (Serializable) coupon_id);
//        intent.putExtra("spec_id", (Serializable) spec_id);
//        context.startActivityForResult(intent, TongleAppConst.REQUEST_CODE_SELECT_COUPON);
//    }

    public static void launcher(Activity context, List<MyCouponAvailableItem> goods_info, String coupon_id) {
        Intent intent = new Intent(context, SelectCouponActivity.class);
        intent.putExtra("goods_info", (Serializable) goods_info);
        intent.putExtra("coupon_id", (Serializable) coupon_id);
        context.startActivityForResult(intent, TongleAppConst.REQUEST_CODE_SELECT_COUPON);
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.left_arrow_btn:
//                finish();
//                break;
//            default:break;
//        }

        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        }
    }
}
