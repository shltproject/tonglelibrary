package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/1/11
 * Desc:10005接口返回json对应 entity
 */

public class StoreGoodsListResult extends BaseResult implements Serializable{
    private StoreAllGoodsInfo body;//魔店全部商品

    public StoreAllGoodsInfo getBody() {
        return body;
    }

    public void setBody(StoreAllGoodsInfo body) {
        this.body = body;
    }
}
