package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/3
 * Time: 16:55
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ForumInfo implements Serializable {
    private String data_total_count;
    private ArrayList<UserFollowPostList> user_follow_post_list;

    public ArrayList<UserFollowPostList> getUser_follow_post_list() {
        return user_follow_post_list;
    }

    public String getData_total_count() {
        return data_total_count;
    }

    @Override
    public String toString() {
        return "data_total_count:" + data_total_count + "\r\n" +
                "user_follow_post_list:" + user_follow_post_list;
    }
}
