package com.ilingtong.library.tongle.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.ProdIntegralActivity;
import com.ilingtong.library.tongle.adapter.StoreHomePostAdapter;
import com.ilingtong.library.tongle.protocol.MStoreDetailsBody;
import com.ilingtong.library.tongle.protocol.MStoreDetailsResult;
import com.ilingtong.library.tongle.protocol.PostDetail;
import com.ilingtong.library.tongle.protocol.StoreOverseasGroupInfo;
import com.ilingtong.library.tongle.protocol.StoreOverseasInfoResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.RoundProgressBar;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.fragment
 * author:liuting
 * Date:2017/1/13
 * Desc:店铺首页
 */

public class StoreHomeFragment extends LazyFragment implements AbsListView.OnScrollListener {
    private ImageView imgVoucher;//优惠券
    private ListView lvPost;//帖子列表
    private RelativeLayout rlyReplace;//替代布局，无相关信息时显示
    private String mStoreId;//店铺id
    private List<PostDetail> listPost = new ArrayList<>();//帖子列表
    private boolean loadMoreFlag;//加载更多标志
    private boolean isLoadEnd;//是否到达底部
    private StoreHomePostAdapter storeHomePostAdapter;//帖子列表Adapter
    private boolean isPrepared;// 标志位，标志已经初始化完成
    private Dialog dialog;//提示Dialog
    private static final int NO_DATA = 0;//无数据
    private static final int REFRESH_LIST = 1;//刷新，获取初始数据
    private static final int LOAD_MORE = 2;//加载更多
    private MStoreDetailsBody mStoreDetailsBody;//10004接口返回的body
    private LinearLayout layoutMain;//主控件
    private Boolean isFirst = true;//是否为第一次加载
    private View voucherView;//优惠券布局
    private LinearLayout llyVoucher;//优惠券布局，根布局下囊括一层，控制它的显示和隐藏，从而达到headView的完成隐藏
    private FrameLayout frameLayoutOverSeas;   //海淘团购主视图
    private TextView txt_overseas_date;  //团购日期
    private ImageView img_overseas_bg;   //团购活动背景图
    private TextView txt_overseas_number;  //已参团人数
    private TextView txt_overseas_memo;    //团购文字宣传语
    private RoundProgressBar roundProgressBar;   //环形进度条

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case NO_DATA://没有帖子列表
                    break;
                case REFRESH_LIST://刷新有数据显示列表，重置Adapter,修复实时刷新数据有时数据溢出的问题
                    rlyReplace.setVisibility(View.GONE);
                    layoutMain.setVisibility(View.VISIBLE);
                    initAdapter();
                    storeHomePostAdapter.setItems(listPost);
                    break;
                case LOAD_MORE://加载更多
                    storeHomePostAdapter.addItems(listPost);
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * 创建StoreHomeFragment
     *
     * @param store_id         店铺ID，由店铺Activity传入
     * @param storeDetailsBody 10004接口返回body，店铺基本信息类
     * @return StoreHomeFragment
     */
    public static StoreHomeFragment newInstance(String store_id, MStoreDetailsBody storeDetailsBody) {
        Bundle args = new Bundle();
        args.putString("store_id", store_id);
        args.putSerializable("storeDetailsBody", storeDetailsBody);
        StoreHomeFragment fragment = new StoreHomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store_home_layout, null);
        mStoreDetailsBody = (MStoreDetailsBody) getArguments().getSerializable("storeDetailsBody");
        mStoreId = getArguments().getString("store_id");
        initView(view);
        isPrepared = true;
        lazyLoad();
        return view;
    }

    /**
     * 初始化控件
     *
     * @param view
     */
    public void initView(View view) {
        loadMoreFlag = false;
        isLoadEnd = false;
        dialog = DialogUtils.createLoadingDialog(getActivity());
        dialog.setCancelable(true);

        lvPost = (ListView) view.findViewById(R.id.store_home_lv_post);
        layoutMain = (LinearLayout) view.findViewById(R.id.store_home_layout_main);

        voucherView = LayoutInflater.from(getActivity()).inflate(R.layout.view_store_home_voucher_layout, null);
        llyVoucher = (LinearLayout) voucherView.findViewById(R.id.store_home_lly_voucher);
        frameLayoutOverSeas = (FrameLayout) voucherView.findViewById(R.id.store_home_headview_fl_overseas);
        img_overseas_bg = (ImageView) voucherView.findViewById(R.id.store_home_headview_img_overseas_bg);
        txt_overseas_date = (TextView) voucherView.findViewById(R.id.store_home_overseas_txt_date);
        txt_overseas_number = (TextView) voucherView.findViewById(R.id.store_home_overseas_txt_number);
        txt_overseas_memo = (TextView) voucherView.findViewById(R.id.store_home_overseas_txt_memo);
        roundProgressBar = (RoundProgressBar) voucherView.findViewById(R.id.store_home_overseas_round_progressbar);
        voucherView.findViewById(R.id.store_home_headview_ll_small_bg).getBackground().setAlpha(230);  //设置不透明度
        //添加HeaderView
        if (voucherView != null && lvPost.getHeaderViewsCount() == 0) {
            lvPost.addHeaderView(voucherView);
        }
        initAdapter();
        lvPost.setOnScrollListener(this);
        imgVoucher = (ImageView) voucherView.findViewById(R.id.store_home_img_voucher);
        rlyReplace = (RelativeLayout) view.findViewById(R.id.rl_replace);
    }

    /**
     * 初始化Adapter
     */
    private void initAdapter() {
        storeHomePostAdapter = new StoreHomePostAdapter(getActivity(), new StoreHomePostAdapter.IOnItemClickListener() {
            @Override
            public void onPostItemClick(View view, String post_id) {
                //跳转到帖子详情页
                Intent intent = new Intent(getActivity(), CollectForumDetailActivity.class);
                intent.putExtra("post_id", post_id);
                startActivity(intent);
            }

            @Override
            public void onProductItemClick(View view, String goods_id, String relation_id, String postId) {
                //跳转到商品详情页
                CollectProductDetailActivity.launch(getActivity(), goods_id, TongleAppConst.ACTIONID_POST, relation_id, mStoreId, postId);
            }
        });
        lvPost.setAdapter(storeHomePostAdapter);
    }

    /**
     * 初始数据
     */
    public void initData() {
        if (mStoreDetailsBody != null) {
            if (mStoreDetailsBody.getStore_post_info().getData_total_count() <= 0) {//没有数据显示无相关数据
                handler.sendEmptyMessage(NO_DATA);
            } else {//有数据显示列表
                listPost = mStoreDetailsBody.getStore_post_info().getStore_post_list();
                handler.sendEmptyMessage(REFRESH_LIST);
            }
            initVoucher(mStoreDetailsBody.getStore_voucher().getVoucher_pic(), mStoreDetailsBody.getStore_voucher().getVoucher_url());
            if (TongleAppConst.YES.equals(mStoreDetailsBody.getStore_info().overseas_flg)) {
                getOverseasGruop(mStoreId);
            } else {
                frameLayoutOverSeas.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 数据请求
     *
     * @param show_order 显示顺序，初始为空，翻页时为最后一项返回的show_order
     */
    public void doRequest(final String show_order) {
        showDialog();
        ServiceManager.getMStoreDetails(mStoreId, show_order, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, new Response.Listener<MStoreDetailsResult>() {
            @Override
            public void onResponse(final MStoreDetailsResult mStoreDetailsResult) {
                dismissDialog();
                if (TongleAppConst.SUCCESS.equals(mStoreDetailsResult.getHead().getReturn_flag())) {
                    if (TextUtils.isEmpty(show_order)) {//初始获取数据
                        initVoucher(mStoreDetailsResult.getBody().getStore_voucher().getVoucher_pic(), mStoreDetailsResult.getBody().getStore_voucher().getVoucher_url());
                        if (TongleAppConst.YES.equals(mStoreDetailsResult.getBody().getStore_info().overseas_flg)) {
                            getOverseasGruop(mStoreId);
                        } else {
                            frameLayoutOverSeas.setVisibility(View.GONE);
                        }
                        if (listPost != null) {
                            listPost.clear();
                        }
                        if (mStoreDetailsResult.getBody().getStore_post_info().getData_total_count() <= 0) {//没有数据显示无相关数据
                            handler.sendEmptyMessage(NO_DATA);
                        } else {//有数据显示列表
                            listPost = mStoreDetailsResult.getBody().getStore_post_info().getStore_post_list();
                            handler.sendEmptyMessage(REFRESH_LIST);
                        }
                    } else {//加载更多
                        listPost = mStoreDetailsResult.getBody().getStore_post_info().getStore_post_list();
                        handler.sendEmptyMessage(LOAD_MORE);
                    }
                    //判断是否可以加载更多，如果返回的列表数小于请求的列表数，说明加载完成，否则继续加载
                    if (mStoreDetailsResult.getBody().getStore_post_info().getStore_post_list().size() < Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                        loadMoreFlag = false;
                    } else {
                        loadMoreFlag = true;
                    }
                } else {//加载失败
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + mStoreDetailsResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dismissDialog();
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        });
    }

    /**
     * 获取海淘团购活动
     *
     * @param mStoreId
     */
    private void getOverseasGruop(String mStoreId) {
        ServiceManager.getStoreOverseasGroup(mStoreId, new Response.Listener<StoreOverseasInfoResult>() {
            @Override
            public void onResponse(StoreOverseasInfoResult storeOverseasInfoResult) {

                if (TongleAppConst.YES.equals(storeOverseasInfoResult.getHead().getReturn_flag())) {
                    setOverseasData(storeOverseasInfoResult.body.group_info);
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + storeOverseasInfoResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        });
    }

    private void setOverseasData(final StoreOverseasGroupInfo group_info) {

        frameLayoutOverSeas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent overseasIntent = new Intent(getActivity(), ProdIntegralActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("prod_url", group_info.group_info_url);
                bundle.putString("myTitle", "活动说明");
                overseasIntent.putExtras(bundle);
                startActivity(overseasIntent);
            }
        });

        txt_overseas_date.setText(String.format(getResources().getString(R.string.overseas_date), group_info.activity_begin_date, group_info.activity_end_date));
        txt_overseas_number.setText(String.format(getResources().getString(R.string.overseas_group_number), group_info.order_qty, group_info.limit_qty));
        txt_overseas_memo.setText(group_info.group_show_msg);

        if (group_info.limit_qty > 0) {
            roundProgressBar.setMax(group_info.limit_qty);
            roundProgressBar.setProgress(group_info.order_qty);
        }
        if (!TextUtils.isEmpty(group_info.background_pic_url)) {
            ImageLoader.getInstance().displayImage(group_info.background_pic_url, img_overseas_bg, ImageOptionsUtils.overseasGroupBigBgOptions(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    if (loadedImage == null) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                    } else {
                        float es = (float) (DipUtils.getScreenWidth(getContext()) - getContext().getResources().getDimensionPixelSize(R.dimen.mstore_home_head_icon_left) * 2) / (float) loadedImage.getWidth();
                        int height = (int) (loadedImage.getHeight() * es);
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
                        params.height = height;
                        view.setLayoutParams(params);
                    }
                }
            });
        }
    }

    /**
     * 初始化优惠券信息
     *
     * @param voucher_pic 优惠券图片地址
     * @param voucher_url 优惠券url
     */
    private void initVoucher(String voucher_pic, final String voucher_url) {
        //优惠券的图片地址为空，则不显示，否则显示优惠券，并且点击优惠券跳转到指定链接
        if (!TextUtils.isEmpty(voucher_pic)) {
            llyVoucher.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(voucher_pic, imgVoucher, ImageOptionsUtils.getOptions());
            llyVoucher.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(voucher_url)) {
                        ToastUtils.toastShort(getString(R.string.url_is_null));
                    } else {
                        Intent expertIntent = new Intent(getActivity(), ProdIntegralActivity.class);
                        Bundle bundle = new Bundle();
                        String url = voucher_url + "?user_token=" + TongleAppInstance.getInstance().getToken()
                                + "&user_id=" + TongleAppInstance.getInstance().getUserID()
                                + "&app_no=" + TongleAppInstance.getInstance().getApp_inner_no()
                                + "&store_id=" + mStoreId
                                + "&store_name=" + mStoreDetailsBody.getStore_info().getStore_name();
                        Log.e("tag", "url:" + url);
                        bundle.putString("prod_url", url);
                        bundle.putString("myTitle", getString(R.string.store_voucher));
                        expertIntent.putExtras(bundle);
                        startActivity(expertIntent);
                    }
                }
            });
        } else {
            llyVoucher.setVisibility(View.GONE);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && isLoadEnd) {
            // 判断是否已加载所有数据
            if (loadMoreFlag) {//未加载完所有数据，加载数据，并且还原isLoadEnd值为false，重新定位列表底部
                doRequest(String.valueOf(listPost.get(listPost.size() - 1).getShow_order()));
                isLoadEnd = false;
            } else {//加载完了所有的数据
                ToastUtils.toastShort(getString(R.string.common_list_end));
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
            View lastVisibleItemView = lvPost.getChildAt(lvPost.getChildCount() - 1);
            //滑到列表底部
            if (lastVisibleItemView != null && lastVisibleItemView.getBottom() == lvPost.getHeight()) {
                isLoadEnd = true;
            } else {
                isLoadEnd = false;
            }
        }
    }

    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        } else {
            //第一次加载时从Activity传入赋值，再次实时刷新加载时则请求10004接口获取最新数据
            if (isFirst) {
                initData();
                isFirst = false;
            } else {
                doRequest("");
            }
        }
    }

    /**
     * dismiss dialog
     */
    public void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    /**
     * 显示Dialog
     */
    public void showDialog() {
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissDialog();
    }
}
