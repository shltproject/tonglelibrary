package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: 率磊
 * Date: 2015/7/3
 * Time: 09：48
 * Email: leishuai@isoftstone.com
 * Desc: 我的关注人返回信息
 */
public class MyAttentionInfo implements Serializable {
    public String data_total_count;
    public ArrayList<FriendListItem> user_follow_friend_list;
    @Override
    public String toString() {
        return "data_total_count:" + data_total_count + "\r\n" +
                "user_follow_friend_list:" + user_follow_friend_list;
    }
}
