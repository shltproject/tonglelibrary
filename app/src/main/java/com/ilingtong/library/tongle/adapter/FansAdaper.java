package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.AddConnectionResult;
import com.ilingtong.library.tongle.protocol.FriendListItem;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * User: shuailei
 * Date: 2015/6/25
 * Time: 16:13
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class FansAdaper extends BaseAdapter {
    private LayoutInflater inflater;
    private List<FriendListItem> list;
    FriendListItem expertDataItem = new FriendListItem();
    private Context mContext;

    public FansAdaper(Context context, ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_fans_item, null);
            holder = new ViewHolder();

            holder.expertIcon = (ImageView) view.findViewById(R.id.expert_icon);
            holder.expertName = (TextView) view.findViewById(R.id.expert_name);
            holder.add_fans_btn = (TextView) view.findViewById(R.id.add_fans_btn);
            holder.expertSign = (TextView) view.findViewById(R.id.expert_sign);
            holder.latest_post_count = (TextView) view.findViewById(R.id.new_post_count);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        expertDataItem = list.get(position);
        ImageLoader.getInstance().displayImage(expertDataItem.user_head_photo_url, holder.expertIcon, ImageOptionsUtils.getHeadIconOptions());
        holder.expertName.setText(expertDataItem.user_nick_name);
        holder.expertSign.setText(expertDataItem.latest_post_info);
        if(expertDataItem.latest_post_count==null||expertDataItem.latest_post_count.equals("")||expertDataItem.latest_post_count.equals("0")){
            holder.latest_post_count.setVisibility(View.INVISIBLE);
        }else{
            holder.latest_post_count.setText(expertDataItem.latest_post_count);
        }

        holder. add_fans_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ToastUtils.isFastClick()){
                    return;
                }else{
                    //首先获取选中的用户的UserID，当成参数进行请求
                    String expert_user_id = list.get(position).user_id;
                    //请求接口数据
                    ServiceManager.AddAttentionRequest(TongleAppInstance.getInstance().getUserID(), expert_user_id, successListener(), errorListener());
                }
            }
        });
        return view;
    }

    static class ViewHolder {
        TextView expertName;
        TextView expertSign;
        ImageView expertIcon;
        TextView latest_post_count;
        TextView add_fans_btn;
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<AddConnectionResult>() {
            @Override
            public void onResponse(AddConnectionResult response) {
                //判断返回数据是否成功
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort( mContext.getString(R.string.common_add_success));
                } else {
                    ToastUtils.toastLong(mContext.getString(R.string.para_exception)+response.getHead().getReturn_message());

                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(mContext.getString(R.string.sys_exception)+volleyError.toString());
            }
        };
    }
}
