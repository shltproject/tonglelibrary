package com.ilingtong.library.tongle.utils;

import android.os.Environment;

import com.ilingtong.library.tongle.TongleAppConst;

import java.io.File;
import java.io.IOException;

/**
 * @ClassName: FileUtils
 * @Package: com.ilingtong.library.tongle.utils
 * @Description: 文件工具类
 * @author: liuting
 * @Date: 2017/6/12 14:24
 */

public class FileUtils {

    /**
     * 获取到文件
     *
     * @param filename 文件名称
     * @return
     */
    public static File getFile(String filename) {
        String storageState = Environment.getExternalStorageState();// 获取sd卡的状态
        File file;
        File dir;
        if (Environment.MEDIA_MOUNTED.equals(storageState)) {// 如果已挂载状态
            //存储在SD卡文件夹image下
            dir = new File(Environment.getExternalStorageDirectory().getPath() + TongleAppConst.FILE_FOLDER_NAME);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            file = new File(Environment.getExternalStorageDirectory().getPath() + TongleAppConst.FILE_FOLDER_NAME + "/" + filename);
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            //存储在本地文件夹image下
            dir = new File(TongleAppConst.FILE_FOLDER_NAME);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            file = new File(TongleAppConst.FILE_FOLDER_NAME + "/" + filename);
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }
}
