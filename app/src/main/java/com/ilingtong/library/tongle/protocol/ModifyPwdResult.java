package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/25
 * Time: 14:16
 * Email: jqleng@isoftstone.com
 * Desc: 修改密码类，通过该对象，被用作网络请求参数
 */
public class ModifyPwdResult implements Serializable {
    private BaseInfo head;
    private ModifyPwdInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ModifyPwdInfo getBody() {
        return body;
    }

    public void setBody(ModifyPwdInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
