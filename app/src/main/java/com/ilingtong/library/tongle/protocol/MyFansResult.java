package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: 率磊
 * Date: 2015/7/3
 * Time: 09：48
 * Email: leishuai@isoftstone.com
 * Desc: 我的粉丝返回信息
 */
public class MyFansResult implements Serializable {
    public BaseInfo head;
    public MyFansInfo body;
}
