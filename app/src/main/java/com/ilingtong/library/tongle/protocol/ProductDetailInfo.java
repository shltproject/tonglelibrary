package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/5
 * Time: 11:27
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ProductDetailInfo implements Serializable {
    private ProductInfo prod_info;
    private PostInfo post_info;

    public ProductInfo getProd_info() {
        return prod_info;
    }

    public PostInfo getPost_info() {
        return post_info;
    }

    @Override
    public String toString() {
        return "prod_info:" + prod_info + "\r\n" +
                "post_info:" + post_info;
    }
}
