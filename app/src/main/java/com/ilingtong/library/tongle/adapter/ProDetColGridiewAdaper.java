//package com.ilingtong.app.tongle.adapter;
//
//import android.content.Context;
//import android.graphics.drawable.Drawable;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.RadioButton;
//import android.widget.TextView;
//
//import com.ilingtong.app.tongle.R;
//import com.ilingtong.app.tongle.protocol.ProductListItemData;
//import com.ilingtong.app.tongle.protocol.ProductSpecListItem;
//import com.ilingtong.app.tongle.protocol.ProductStockListItem;
//import com.ilingtong.app.tongle.protocol.SpecDetailListItem;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * User: shuailei
// * Date: 2015/5/27
// * Time: 13：29
// * Email: leishuai@isoftstone.com
// * Dest:产品详情选择颜色
// */
//public class ProDetColGridiewAdaper extends BaseAdapter {
//    private LayoutInflater inflater;
//    private List<SpecDetailListItem> list;
//    ProductListItemData dataItem = new ProductListItemData();
//    private ViewHolder viewHolder = null;
//    private int selectIndex = -1;
//    private ArrayList<ProductStockListItem> stockListItem;
//    private Context context;
//    private ArrayList<ProductSpecListItem> specListItems;
//    int i;
//    private Map<Integer,Boolean> selector = new HashMap<Integer,Boolean>();
//
//    public Map<Integer, Boolean> getSelector() {
//        return selector;
//    }
//
//    public void setSelector(Map<Integer, Boolean> selector) {
//        this.selector = selector;
//    }
//
//    public int getSelectIndex() {
//        return selectIndex;
//    }
//
//    public void setSelectIndex(int selectIndex) {
//        this.selectIndex = selectIndex;
//    }
//
//    public ProDetColGridiewAdaper(Context context, List<SpecDetailListItem> list,ArrayList<ProductStockListItem> stockListItem,ArrayList<ProductSpecListItem> specListItems,int i) {
//        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.list = list;
//        this.stockListItem=stockListItem;
//        this.context=context;
//        this.specListItems=specListItems;
//        this.i=i;
//    }
//    public ProDetColGridiewAdaper(Context context, List<SpecDetailListItem> list) {
//        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.list = list;
//        this.context=context;
//        for (int i = 0; i < list.size(); i++) {
//            selector.put(i,false);
//        }
//    }
//    @Override
//    public int getCount() {
//        return list.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return list.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        if (convertView == null) {
//            viewHolder = new ViewHolder();
//            convertView = inflater.inflate(R.layout.activity_procolor_gridview_layout, null);
//            viewHolder.tv_color = (TextView) convertView.findViewById(R.id.color_txt);
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }
//        //判断当前view的位置和gridview的item的position是否相等
//        if (selector.get(position)) {
//            viewHolder.tv_color.setBackgroundResource(R.drawable.border_press);
//        } else {
//            viewHolder.tv_color.setBackgroundResource(R.drawable.border);
//        }
//
//        viewHolder.tv_color.setText(list.get(position).spec_detail_name);
//        return convertView;
//    }
//
//    class ViewHolder {
//        TextView tv_color;
//    }
//}