package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: syc
 * Date: 2015/6/24
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc: 【1090】
 */
public class SearchInfo implements Serializable {



    private ArrayList<MStoreListItem> mstore_list;
    private ArrayList<FriendListItem> user_list;
    private ArrayList<UserFollowPostList> post_list;
    private ArrayList<ProductListItemData> prod_list;
    private ArrayList<OrganizationListItem> organization_list;   //组织列表  add at 2016/05/18
    private int data_total_count;    //当前类别列表条数  add on 2017/10/19


    public ArrayList<MStoreListItem> getMstore_list() {
        return mstore_list;
    }

    public ArrayList<FriendListItem> getUser_list() {
        return user_list;
    }

    public ArrayList<UserFollowPostList> getPost_list() {
        return post_list;
    }

    public ArrayList<ProductListItemData> getProd_list() {
        return prod_list;
    }

    public ArrayList<OrganizationListItem> getOrganization_list() {
        return organization_list;
    }

    public int getData_total_count() {
        return data_total_count;
    }

    @Override
    public String toString() {
        return "mstore_list:" + mstore_list + "\r\n" +
                        "user_list:" + user_list + "\r\n" +
                        "post_list:" + post_list + "\r\n" +
                        "prod_list:" + prod_list;
    }


}
