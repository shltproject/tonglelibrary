package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 11:06
 * Email: jqleng@isoftstone.com
 * Desc: 基础数据子列表中的元素基本类型
 */
public class DataListItem implements Serializable {
    public String code;
    public String name;
    public ArrayList<DataListItem> sub_list;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<DataListItem> getSub_list() {
        return sub_list;
    }

    public void setSub_list(ArrayList<DataListItem> sub_list) {
        this.sub_list = sub_list;
    }

    @Override
    public String toString() {
        return "DataListItem{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", sub_list=" + sub_list +
                '}';
    }
}
