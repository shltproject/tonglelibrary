package com.ilingtong.library.tongle.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.LoginPwdRecoveryActivity;
import com.ilingtong.library.tongle.activity.LoginRegistActivity;
import com.ilingtong.library.tongle.activity.MainActivity;
import com.ilingtong.library.tongle.protocol.LoginResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * User: lengjiqiang
 * Date: 2015/4/29
 * Time: 17:16
 * Email: jqleng@isoftstone.com
 * Desc: 用户登录页
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    public EditText phoneNumberEdit;
    public EditText passwordEdit;
    private ImageButton loginBtn;
    public String strNumber;
    public String strPassword;
    private TextView textForgot;
    private TextView textRegister;
    private TextView textTopName;
    public SharedPreferences sp, userSp;
    public Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getData();
        View rootView = inflater.inflate(R.layout.activity_login_layout, container, false);
        initView(rootView);
        isLogin();
        return rootView;
    }
    public void getData() {
        sp = getActivity().getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        userSp = getActivity().getApplicationContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
    }

    //判断是否自动登入
    public void isLogin() {
        //是否自动输入号码及密码
        if (sp.getBoolean(TongleAppInstance.getInstance().getIfRemberKeyName(), true)) {
            phoneNumberEdit.setText(sp.getString(TongleAppInstance.getInstance().getLoginUseridKeyName(), ""));
            passwordEdit.setText(sp.getString(TongleAppInstance.getInstance().getLoginPwdKeyName(), ""));
        } else {
            passwordEdit.setText("");
        }
        phoneNumberEdit.setSelection(phoneNumberEdit.getText().length());
        passwordEdit.setSelection(passwordEdit.getText().length());
    }

    public void initView(View rootView) {
        dialog = DialogUtils.createLoadingDialog(getActivity());
        rootView.findViewById(R.id.left_arrow_btn).setVisibility(View.GONE); //去掉返回箭头，让“登录”文字靠左显示

        phoneNumberEdit = (EditText) rootView.findViewById(R.id.number_edit);
        passwordEdit = (EditText) rootView.findViewById(R.id.password_edit);
        loginBtn = (ImageButton) rootView.findViewById(R.id.login_btn);
        textTopName = (TextView) rootView.findViewById(R.id.top_name);
        textTopName.setText(R.string.login);
        textTopName.setVisibility(View.VISIBLE);
        loginBtn.setOnClickListener(this);
        loginBtn.setEnabled(false);
        textForgot = (TextView) rootView.findViewById(R.id.forgot_text);
        textRegister = (TextView) rootView.findViewById(R.id.register_text);
        textForgot.setOnClickListener(this);
        textRegister.setOnClickListener(this);
        textTopName.setVisibility(View.VISIBLE);
        textTopName.setText(R.string.login);
        phoneNumberEdit.setText(sp.getString(TongleAppInstance.getInstance().getLoginUseridKeyName(), ""));


        //做手机号的改变监听，判断底部登录按钮的背景
        phoneNumberEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strNumber = phoneNumberEdit.getText().toString();
                strPassword = passwordEdit.getText().toString();

                if (strNumber.equals("") || strPassword.equals("")) {
//                    loginBtn.setBackgroundResource(R.drawable.button_forbid_style);
                    loginBtn.setEnabled(false);
                } else {
//                    loginBtn.setBackgroundResource(R.drawable.button_style);
                    loginBtn.setEnabled(true);
                }
            }
        });
        //做密码的改变监听，判断底部登录按钮的背景
        passwordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strNumber = phoneNumberEdit.getText().toString();
                strPassword = passwordEdit.getText().toString();

                if (strNumber.equals("") || strPassword.equals("")) {
                    loginBtn.setEnabled(false);
//                    loginBtn.setBackgroundResource(R.drawable.button_forbid_style);
                } else {
                    loginBtn.setEnabled(true);
//                    loginBtn.setBackgroundResource(R.drawable.button_style);
                }
            }
        });
        //做手机号的改变监听，判断底部登录按钮的背景
        phoneNumberEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strNumber = phoneNumberEdit.getText().toString();
                strPassword = passwordEdit.getText().toString();

                if (strNumber.equals("") || strPassword.equals("")) {
                    loginBtn.setEnabled(false);
//                    loginBtn.setBackgroundResource(R.drawable.button_forbid_style);
                } else {
                    loginBtn.setEnabled(true);
//                    loginBtn.setBackgroundResource(R.drawable.button_style);
                }
            }
        });
        //做密码的改变监听，判断底部登录按钮的背景
        passwordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strNumber = phoneNumberEdit.getText().toString();
                strPassword = passwordEdit.getText().toString();

                if (strNumber.equals("") || strPassword.equals("")) {
                    loginBtn.setEnabled(false);
//                    loginBtn.setBackgroundResource(R.drawable.button_forbid_style);
                } else {
                    loginBtn.setEnabled(true);
//                    loginBtn.setBackgroundResource(R.drawable.button_style);
                }
            }
        });
    }

    /**
     * 登入
     *
     * @param strNumber1   手机号
     * @param strPassword1 密码
     */
    private void login(String strNumber1, String strPassword1) {
        if (!"".equals(strNumber1) && !"".equals(strPassword1)) {
            TongleAppInstance.getInstance().setUser_phone(strNumber1);
            strNumber = strNumber1;
            strPassword = strPassword1;
            //创建并弹出“加载中Dialog”
            dialog.show();
            ServiceManager.doLogin(strNumber, strPassword, successListener(), errorListener());
        } else {
            ToastUtils.toastShort(getString(R.string.common_userid_pwd_null));
        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<LoginResult> successListener() {
        return new Response.Listener<LoginResult>() {
            @Override
            public void onResponse(LoginResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    TongleAppInstance.getInstance().setUserID(response.getBody().getUserID());
                    TongleAppInstance.getInstance().setToken(response.getBody().getToken());
                    TongleAppInstance.getInstance().setUser_photo_url(response.getBody().getUser_head_photo_url());
                    //记录登入名与密码
                    sp.edit().putString(TongleAppInstance.getInstance().getLoginUseridKeyName(), strNumber).commit();
                    sp.edit().putString(TongleAppInstance.getInstance().getLoginPwdKeyName(), strPassword).commit();

                    //缓存重要数据
                    userSp.edit().putString("token", response.getBody().getToken()).commit();
                    userSp.edit().putString("user_id", response.getBody().getUserID()).commit();// 用户ID
                    userSp.edit().putString("user_photo_url", response.getBody().getUser_head_photo_url()).commit();// 头像URL

                    Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mainIntent.putExtra(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME,"");
                    startActivity(mainIntent);
                    getActivity().finish();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                dialog.dismiss();
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                dialog.dismiss();
            }
        };
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_btn) {
            strNumber = phoneNumberEdit.getText().toString();
            strPassword = passwordEdit.getText().toString();
            login(strNumber, strPassword);
        } else if (v.getId() == R.id.forgot_text) {
            Intent recoveryIntent = new Intent(getActivity(), LoginPwdRecoveryActivity.class);
            startActivity(recoveryIntent);
        } else if (v.getId() == R.id.register_text) {
            Intent registIntent = new Intent(getActivity(), LoginRegistActivity.class);
            startActivity(registIntent);
        }
    }
}
