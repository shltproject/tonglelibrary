package com.ilingtong.library.tongle.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.ilingtong.library.tongle.TongleAppInstance;

/**
 * @author fengguowei
 * @ClassName: SearchEditText
 * @Description: ${todo}
 * @date 2017/2/14 19:15
 */

public class SearchEditText extends EditText implements View.OnTouchListener {
    private ISearchListener mSearchListener;

    public SearchEditText(Context context) {
        super(context);
        init();
    }

    public SearchEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SearchEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    //初始化控件，绑定监听器
    public void init() {
        setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //如果不是按下操作，就不做处理，如果是按下操作但是没有图片，也不做处理
        if (event.getAction() == MotionEvent.ACTION_UP && this.getCompoundDrawables()[2] != null) {//对右侧的图片进行处理
            //检测点击区域的X坐标是否在图片范围内
            if (event.getX() > this.getWidth()
                    - this.getPaddingRight()
                    - this.getCompoundDrawables()[2].getIntrinsicWidth()) {

                //在此做图片的点击处理
                mSearchListener.search();
                InputMethodManager imm = (InputMethodManager) TongleAppInstance.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(this.getWindowToken(), 0);
                }
                clearFocus();
                return true;
            }
            return false;
        }
        return false;
    }

    //为EditText设置监听
    public void setSearchListener(ISearchListener searchListener) {
        this.mSearchListener = searchListener;
    }

    //EditText右侧图片监听
    public interface ISearchListener {
        void search();
    }
}
