package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.ProductListItemData;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by wuqian on 2016/1/11.
 * mail: wuqian@ilingtong.com
 * Description:魔店详情中的展示商品列表的adapter
 */
public class MstoreDetaliGridAdapter extends BaseAdapter {
    private List<ProductListItemData> list;
    private Context mContext;

    public MstoreDetaliGridAdapter(Context mContext,List<ProductListItemData> list) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.mstore_recycler_item,null);
            holder.img_pic = (ImageView) convertView.findViewById(R.id.mstore_recycler_item_img_productpic);
            holder.txt_title = (TextView) convertView.findViewById(R.id.mstore_recycler_item_txt_title);
            holder.txt_price = (TextView) convertView.findViewById(R.id.mstore_recycler_item_txt_price);
            holder.img_sale = (ImageView) convertView.findViewById(R.id.mstore_recycler_item_img_sale);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        ImageLoader.getInstance().displayImage(list.get(position).prod_thumbnail_pic_url, holder.img_pic, ImageOptionsUtils.getOptions());
        holder.txt_title.setText(list.get(position).prod_name);
//        holder.txt_price.setText(list.get(position).prod_price);
        holder.txt_price.setText(FontUtils.priceFormat(Double.parseDouble(list.get(position).prod_price)));
        holder.img_sale.setVisibility(TongleAppConst.YES.equals(list.get(position).coupon_flag) ? View.VISIBLE : View.GONE);
        return convertView;
    }

    class ViewHolder {
        public ImageView img_pic;  //商品缩略图
        public TextView txt_title;  //商品标题
        public TextView txt_price;  //商品价格
        public ImageView img_sale;  //团购商品标志
    }
}
