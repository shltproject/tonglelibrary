package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/14
 * Time: 0:06
 * Email: jqleng@isoftstone.com
 * Desc: 删除收货地址信息类
 */
public class DelAddrInfo implements Serializable {
    private ArrayList<DefaultAddressListItem> my_address_list;

    public ArrayList getMy_address_list() {
        return my_address_list;
    }

    @Override
    public String toString() {
        return "my_address_list:" + my_address_list;
    }
}
