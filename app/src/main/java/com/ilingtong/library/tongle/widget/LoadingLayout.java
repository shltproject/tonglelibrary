package com.ilingtong.library.tongle.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.utils.DipUtils;

/**
 * 加载进度
 * 
 * @author GaiQS E-mail:gaiqs@sina.com
 * @Date 2015年4月9日
 * @Time 上午10:06:02
 */
public class LoadingLayout extends LinearLayout {
	private TextView textView;
	private TimeCount time;// 倒计时
	private String finish;// 完成计时显示提示
	private TypedArray typedArray;
	private int MILLIS = 500;// 毫秒计算
	private int MILLIS_TOTAL = 60000;
	private int circulation = 0;// 循环

	public LoadingLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TimeCount);
		this.finish = typedArray.getString(R.styleable.TimeCount_timer_text_finish);
		initView();
	}

	private void initView() {
		textView = new TextView(getContext());
//		 textView.setBackgroundResource(R.drawable.bg_gray_selected);
		textView.setText(typedArray.getString(R.styleable.TimeCount_timer_text_content));
		textView.setTextColor(getResources().getColor(R.color.w4193FD));
		textView.setTextSize(15);
		textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
		textView.setWidth(DipUtils.dip2px(100));
		addView(textView);
	}

	/**
	 * Start the countdown.
	 */
	public void startCountdown() {
		time = new TimeCount(typedArray.getInteger(R.styleable.TimeCount_timer_millis_total, MILLIS_TOTAL), MILLIS);// 构造CountDownTimer对象
		time.start();
	}

	/**
	 * Cancel the countdown
	 */
	public void cancelCountdown() {
		if (null != time) {
			time.cancel();
		}
	}
	/**
	 * 倒计时
	 * 
	 * @author GaiQS E-mail:gaiqs@sina.com
	 * @Date 2015年4月9日
	 * @Time 上午9:58:27
	 */
	class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);// 参数依次为总时长,和计时的时间间隔
		}

		@Override
		public void onFinish() {// 计时完毕时触发
			textView.setText(finish);
			textView.setSelected(false);
		}

		@Override
		public void onTick(long millisUntilFinished) {// 计时过程显示
			circulation = circulation >= 4 ? 0 : circulation;
			switch (circulation) {
			case 0:
				textView.setText(getContext().getString(R.string.loading_layout_message_01));
				break;
			case 1:
				textView.setText(getContext().getString(R.string.loading_layout_message_02));
				break;
			case 2:
				textView.setText(getContext().getString(R.string.loading_layout_message_03));
				break;
			case 3:
				textView.setText(getContext().getString(R.string.loading_layout_message_04));
				break;

			default:
				break;
			}
			++circulation;
		}
	}
}
