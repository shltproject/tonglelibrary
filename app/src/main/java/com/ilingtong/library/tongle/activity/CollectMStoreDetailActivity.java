package com.ilingtong.library.tongle.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.MstoreDetaliGridAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.fragment.HomeFragment;
import com.ilingtong.library.tongle.fragment.MStoreFragment;
import com.ilingtong.library.tongle.model.MStoreDetailModel;
import com.ilingtong.library.tongle.protocol.FavoriteListItem;
import com.ilingtong.library.tongle.protocol.MStoreDetailCollectResult;
import com.ilingtong.library.tongle.protocol.MStoreDetailQRResult;
import com.ilingtong.library.tongle.protocol.MStoreDetailResult;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * User: shuailei
 * Date: 2015/5/22
 * Time: 9:26
 * Email: leishuai@isoftstone.com
 * Desc: 魔店详情界面（魔店二级页面）
 */
public class CollectMStoreDetailActivity extends BaseActivity implements XListView.IXListViewListener, View.OnClickListener {
    private XListView xlistview;
    //  private MStoreDetailListAdaper productListAdaper;
    private MStoreDetailModel mStoreDetailModel;
    private ViewPager bannerViewPager;
    private ArrayList<View> bannerListView;
    private MyAdapter bannerPageAdapter;
    private View mTouchTarget;
    private View headView;
    private LinearLayout bannerView, banner_bottom;
    private ImageView QR_btn;
    private ImageView collect_btn;
    private ImageView left_arrow_btn;
    private TextView top_name;
    private boolean bIsFavorate = false;
    private TextView mPreSelectedBt;
    private ScheduledExecutorService scheduledExecutorService;
    private int currentItem = 0;//当前页面
    private String mStoreID;
    private ProgressDialog pDlg;
    private int intoType = 0;
    private GridView gridView;
    private MstoreDetaliGridAdapter gridAdapter;
    private TextView txt_vouchers;  //优惠活动一栏
    private MStoreDetailResult mResult;//返回结果

    /**
     * 继承Activity的onCreate()方法，实现本类的特殊需求
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xlistview_comm_layout);
        getData();
        doRequest();
        initView();
    }

    public void getData() {
        mStoreID = getIntent().getExtras().getString("mstore_id");
        intoType = getIntent().getIntExtra(TongleAppConst.INTO_TYPE, 0);
    }

    public void initView() {
        //魔店基本信息
        if (mStoreDetailModel == null)
            mStoreDetailModel = new MStoreDetailModel();
        pDlg = new ProgressDialog(this);
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.common_dialog_message));
        pDlg.show();

        mResult=new MStoreDetailResult();

        xlistview = (XListView) findViewById(R.id.xlistview);
        headView = LayoutInflater.from(this).inflate(R.layout.mstore_detailview_layout, null);
        bannerView = (LinearLayout) headView.findViewById(R.id.banner_view1);
        gridView = (GridView) headView.findViewById(R.id.mstore_detailview_gird);
        txt_vouchers = (TextView) headView.findViewById(R.id.mstore_detailview_tv_info);

        bannerViewPager = (ViewPager) bannerView.findViewById(R.id.banner_viewpager1);
        //设置图片比例为16:9
        ViewGroup.LayoutParams params1 = bannerViewPager.getLayoutParams();
        params1.height = (DipUtils.getScreenWidth(this) * 9 / 16);
        bannerViewPager.setLayoutParams(params1);

        bannerViewPager.setLayoutParams(params1);
        bannerListView = new ArrayList<View>();
        bannerPageAdapter = new MyAdapter();
        bannerViewPager.setAdapter(bannerPageAdapter);
        bannerViewPager.setCurrentItem(0);
        banner_bottom = (LinearLayout) bannerView.findViewById(R.id.banner_bottom);

        bannerViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int mPreviousState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                if (mPreSelectedBt != null) {
                    mPreSelectedBt.setBackgroundResource(R.color.main_bgcolor);
                }

                TextView currentBt = (TextView) banner_bottom.getChildAt(i);
                currentBt.setBackgroundResource(R.color.topview_bgcolor);
                mPreSelectedBt = currentBt;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (mPreviousState == ViewPager.SCROLL_STATE_IDLE) {
                    if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                        mTouchTarget = bannerViewPager;
                    }
                } else {
                    if (state == ViewPager.SCROLL_STATE_IDLE || state == ViewPager.SCROLL_STATE_SETTLING) {
                        mTouchTarget = null;
                    }
                }

                mPreviousState = state;
            }
        });


        xlistview.addHeaderView(headView);

        QR_btn = (ImageView) findViewById(R.id.QR_share_btn);
        collect_btn = (ImageView) findViewById(R.id.collect_btn);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        top_name = (TextView) findViewById(R.id.top_name);

        QR_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        left_arrow_btn.setVisibility(View.VISIBLE);
        collect_btn.setVisibility(View.VISIBLE);
        left_arrow_btn.setOnClickListener(this);
        collect_btn.setOnClickListener(this);
        QR_btn.setOnClickListener(this);

        //添加定时器，轮播图自动切换
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ViewPagerTask(), 1, 3, TimeUnit.SECONDS);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent;
                if(mResult.getBody().getProd_list().get(position).coupon_flag.equals(TongleAppConst.YES)){//团购商品
                    ProductTicketDetailActivity.launch(CollectMStoreDetailActivity.this,mStoreDetailModel.prod_list.get(position).prod_id,TongleAppConst.ACTIONID_MSTORE,mStoreDetailModel.prod_list.get(position).relation_id,mStoreID,"");
                }else{//普通商品
                    CollectProductDetailActivity.launch(CollectMStoreDetailActivity.this, mStoreDetailModel.prod_list.get(position).prod_id, TongleAppConst.ACTIONID_MSTORE, mStoreDetailModel.prod_list.get(position).relation_id, mStoreID, "");
                }

            }
        });

    }

    public void doRequest() {
        ServiceManager.doMStoreDetailRequest(TongleAppInstance.getInstance().getUserID(), mStoreID, successListener(), errorListener());
    }

    @Override
    public void onRefresh(int id) {
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {

    }

    public void updateListView() {
        //productListAdaper = new MStoreDetailListAdaper(this, mStoreDetailModel.prod_list);
        gridAdapter = new MstoreDetaliGridAdapter(this, mStoreDetailModel.prod_list);
        gridView.setAdapter(gridAdapter);
        xlistview.setPullLoadEnable(false);
        xlistview.setPullRefreshEnable(true);
        xlistview.setXListViewListener(this, 0);
        xlistview.setRefreshTime();
        xlistview.setAdapter(null);
        addBannerView();
        if (TextUtils.isEmpty(mStoreDetailModel.vouchers_text)) {
            txt_vouchers.setVisibility(View.GONE);
        } else {
            txt_vouchers.setText(mStoreDetailModel.vouchers_text);
            txt_vouchers.setOnClickListener(this);
        }
    }

    public void addBannerView() {
        if ((bannerListView == null) || (mStoreDetailModel.ad_list.size() == 0))
            return;

        bannerListView.clear();
        try {
            for (int i = 0; i < mStoreDetailModel.ad_list.size(); i++) {
                String url = mStoreDetailModel.ad_list.get(i).ad_pic_url;
                ImageView viewOne = (ImageView) LayoutInflater.from(this).inflate(R.layout.banner_cell_layout, null);
                ImageLoader.getInstance().displayImage(url, viewOne, ImageOptionsUtils.getOptions());
                bannerListView.add(viewOne);
            }
            banner_bottom.removeAllViews();
            //定义viewpager下的圆点
            for (int i = 0; i < bannerListView.size(); i++) {
                TextView bt = new TextView(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(12, 12);
                params.setMargins(10, 0, 0, 0);
                bt.setLayoutParams(params);
                bt.setBackgroundResource(R.color.main_bgcolor);
                banner_bottom.addView(bt);
                bannerPageAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        bannerViewPager.setAdapter(bannerPageAdapter);
    }


    //bannerListView
    class MyAdapter extends PagerAdapter {
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public int getCount() {
            return bannerListView.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(bannerListView.get(position));//删除页卡
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {    //这个方法用来实例化页卡
            container.addView(bannerListView.get(position), 0);//添加页卡
            View view = bannerListView.get(position);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String type = mStoreDetailModel.ad_list.get(position).promotion_mode;

                    //关联类型是商品
                    if (type.equals(TongleAppConst.ACTIVITY_PRODUCT)) {
                        CollectProductDetailActivity.launch(CollectMStoreDetailActivity.this,mStoreDetailModel.ad_list.get(position).object_id,TongleAppConst.ACTIONID_MSTORE,"",mStoreID,"");
                        //关联类型是会员
                    } else if (type.equals(TongleAppConst.ACTIVITY_MEMBER)) {
                        Intent i = new Intent(CollectMStoreDetailActivity.this, CollectExpertDetailActivity.class);
                        i.putExtra("user_id", mStoreDetailModel.ad_list.get(position).object_id);
                        startActivity(i);
                        //关联类型是魔店
                    } else if (type.equals(TongleAppConst.ACTIVITY_MSTORE)) {
                        Intent i = new Intent(CollectMStoreDetailActivity.this, CollectMStoreDetailActivity.class);
                        i.putExtra("mstore_id", mStoreDetailModel.ad_list.get(position).object_id);
                        startActivity(i);
                        //关联类型是帖子
                    } else if (type.equals(TongleAppConst.ACTIVITY_FORUM)) {
                        Intent i = new Intent(CollectMStoreDetailActivity.this, CollectForumDetailActivity.class);
                        i.putExtra("post_id", mStoreDetailModel.ad_list.get(position).object_id);
                        startActivity(i);
                    }
                }
            });
            return bannerListView.get(position);
        }
    }

    /**
     * 该页中的所有点击事件处理方法
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.QR_share_btn) {
            //魔店二维码
            ServiceManager.doMStoreQRRequest(TongleAppInstance.getInstance().getUserID(), mStoreID, QRListener(), errorListener());
        } else if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.collect_btn) {
            if (ToastUtils.isFastClick()) {
                return;
            } else {
                if (bIsFavorate == false) {
                    //魔店收藏
                    ArrayList<FavoriteListItem> favList = new ArrayList<FavoriteListItem>();
                    FavoriteListItem favListItem = new FavoriteListItem();
                    favListItem.collection_type = "1";// 1 魔店
                    favListItem.key_value = mStoreID;
                    favList.add(favListItem);
                    ServiceManager.doMStoreCollectRequest(TongleAppInstance.getInstance().getUserID(), favList, "", collectListener(), errorListener());
                } else {
                    //魔店取消收藏
                    ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), mStoreID, "1", collectListener(), errorListener());
                }
            }
        } else if (v.getId() == R.id.mstore_detailview_tv_info) {   //优惠券信息页面跳转

            if(!TextUtils.isEmpty(mStoreDetailModel.vouchers_url)){
                String myurl = mStoreDetailModel.vouchers_url + "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken()+"&app_inner_no="+TongleAppInstance.getInstance().getApp_inner_no();
                Intent expertIntent = new Intent(this, ProdIntegralActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("prod_url", myurl);
                bundle.putString("myTitle", mStoreDetailModel.vouchers_title);
                expertIntent.putExtras(bundle);
                startActivity(expertIntent);
            }else{
                ToastUtils.toastShort(getString(R.string.collect_store_detail_voucher_null_msg));
            }
        }
    }

    //用来完成图片切换的任务
    private class ViewPagerTask implements Runnable {

        public void run() {
            //实现我们的操作
            //改变当前页面
            currentItem = (currentItem + 1) % bannerListView.size();
            //Handler来实现图片切换
            handler.obtainMessage().sendToTarget();
        }
    }

    private android.os.Handler handler = new android.os.Handler() {

        public void handleMessage(Message msg) {
            //设定viewPager当前页面
            bannerViewPager.setCurrentItem(currentItem);
        }
    };

    @Override
    protected void onRestart() {
        super.onRestart();
        scheduledExecutorService.isTerminated();
    }

    /**
     * 功能：魔店详情网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<MStoreDetailResult>() {
            @Override
            public void onResponse(MStoreDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mResult=response;
                    mStoreDetailModel.mstore_id = response.getBody().getMstore_id();
                    mStoreDetailModel.mstore_name = response.getBody().getMstore_name();
                    mStoreDetailModel.mstore_favorited_by_me = response.getBody().getMstore_favorited_by_me();
                    mStoreDetailModel.ad_list = response.getBody().getAd_list();
                    mStoreDetailModel.prod_list = response.getBody().getProd_list();
                    mStoreDetailModel.vouchers_text = response.getBody().getVouchers_text();
                    mStoreDetailModel.vouchers_url = response.getBody().getVouchers_url();
                    mStoreDetailModel.vouchers_title = response.getBody().getVouchers_title();
                    updateListView();
                    top_name.setText(mStoreDetailModel.mstore_name);
                    //0已收藏
                    if (mStoreDetailModel.mstore_favorited_by_me.equals("0")) {
                        collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                        bIsFavorate = true;
                    } else {
                        collect_btn.setBackgroundResource(R.drawable.collect_button_style);
                        bIsFavorate = false;
                    }

                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：魔店详情请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：二维码请求网络响应成功，返回数据
     */
    private Response.Listener QRListener() {
        return new Response.Listener<MStoreDetailQRResult>() {
            @Override
            public void onResponse(MStoreDetailQRResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mStoreDetailModel.mstore_qr_code_url = response.getBody().getMstore_qr_code_url();
//                    ShowQRCodeDialog dialog = new ShowQRCodeDialog(CollectMStoreDetailActivity.this,mStoreDetailModel.mstore_qr_code_url,mStoreID,TongleAppConst.STRSTORE);
//                    dialog.show();
                    ShowQRCodeActivity.launcher(CollectMStoreDetailActivity.this,mStoreDetailModel.mstore_qr_code_url,mStoreID,TongleAppConst.STRSTORE);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：添加收藏请求网络响应成功，返回数据
     */
    private Response.Listener collectListener() {
        return new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult response) {
                if (TongleAppConst.ADD_FAVORITE.equals(response.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                        collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                        ToastUtils.toastShort(getString(R.string.favorite_success));
                        bIsFavorate = true;
                        if (intoType == TongleAppConst.HOME_INTO) {
                            HomeFragment.MSTORE_UPDATE_FLAG = false;
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;
                        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                            MStoreFragment.UPDATE_LIST_FLAG = false;  //从收藏页进来的，表示刚刚取消了收藏之后又重新收藏。回收藏页时不需要刷新。首页同理
                            HomeFragment.MSTORE_UPDATE_FLAG = false;
                        } else {
                            HomeFragment.MSTORE_UPDATE_FLAG_OTHER = true;   //如果是从发现等页面进入，则必须都在显示首页时刷仙首页魔店列表
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新魔店列表
                        }

                    } else {
                        ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                    }
                } else if (TongleAppConst.CANCEL_FAVORITE.equals(response.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                        collect_btn.setBackgroundResource(R.drawable.collect_button_style);
                        ToastUtils.toastShort(getString(R.string.favorite_cancel));
                        bIsFavorate = false;
                        if (CollectMStoreActivity.instance != null) {
                            CollectMStoreActivity.instance.onRefresh(1);
                        }
                        if (intoType == TongleAppConst.HOME_INTO) {
                            HomeFragment.MSTORE_UPDATE_FLAG = true;
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;
                        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                            MStoreFragment.UPDATE_LIST_FLAG = true; //表示从收藏页过来，取消收藏该魔店。回页面时需刷新。显示首页时也需刷新
                            HomeFragment.MSTORE_UPDATE_FLAG = true;
                        } else {
                            HomeFragment.MSTORE_UPDATE_FLAG_OTHER = true; //如果是从发现等页面进入，则必须都在显示首页时刷仙首页魔店列表
                            MStoreFragment.MSTOREFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新魔店列表
                        }
                    } else {
                        ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                    }
                }
            }
        };
    }
}
