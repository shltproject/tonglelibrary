package com.ilingtong.library.tongle.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectProductMoreActivity;
import com.ilingtong.library.tongle.activity.ProdIntegralActivity;
import com.ilingtong.library.tongle.adapter.PicTextDetailListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.PicTxtDetailResult;
import com.ilingtong.library.tongle.protocol.PicTxtEDetailModle;
import com.ilingtong.library.tongle.protocol.StoreOverseasGroupInfo;
import com.ilingtong.library.tongle.protocol.StoreOverseasInfoResult;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.RoundProgressBar;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 13:54
 * Email: jqleng@isoftstone.com
 * Desc: 商品图文详情页
 */
public class ProductDetailFragment extends FragmentAppBase {
    private XListView detailListView;
    private RelativeLayout rl_replace;
    private PicTextDetailListAdapter detailListAdapter;
    private PicTxtEDetailModle pictxtedetailmodle = new PicTxtEDetailModle();

    View mainView;
    private View headviewOverseas;
    private FrameLayout frameLayoutOverSeas;   //海淘团购主视图
    private TextView txt_overseas_date;  //团购日期
    private ImageView img_overseas_bg;   //团购活动背景图
    private TextView txt_overseas_number;  //已参团人数
    private TextView txt_overseas_memo;    //团购文字宣传语
    private RoundProgressBar roundProgressBar;   //环形进度条

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mainView == null) {
            mainView = inflater.inflate(R.layout.fragment_product_detail_layout, null);
            initView(mainView);
            doRequest();
        }
        return mainView;
    }

    public void initView(View mainView) {
        detailListView = (XListView) mainView.findViewById(R.id.product_detail_listview);
        rl_replace = (RelativeLayout) mainView.findViewById(R.id.rl_replace);

        headviewOverseas = LayoutInflater.from(getActivity()).inflate(R.layout.view_store_home_voucher_layout, null);
        frameLayoutOverSeas = (FrameLayout) headviewOverseas.findViewById(R.id.store_home_headview_fl_overseas);
        img_overseas_bg = (ImageView) headviewOverseas.findViewById(R.id.store_home_headview_img_overseas_bg);
        txt_overseas_date = (TextView) headviewOverseas.findViewById(R.id.store_home_overseas_txt_date);
        txt_overseas_number = (TextView) headviewOverseas.findViewById(R.id.store_home_overseas_txt_number);
        txt_overseas_memo = (TextView) headviewOverseas.findViewById(R.id.store_home_overseas_txt_memo);
        roundProgressBar = (RoundProgressBar) headviewOverseas.findViewById(R.id.store_home_overseas_round_progressbar);
        headviewOverseas.findViewById(R.id.store_home_headview_ll_small_bg).getBackground().setAlpha(230);  //设置不透明度

        detailListView.addHeaderView(headviewOverseas);

        detailListView.setPullLoadEnable(false);
        detailListView.setPullRefreshEnable(false);
    }

    public void doRequest() {
        if (CollectProductMoreActivity.isOverseasFlag) {
            getOverseasGruop(CollectProductMoreActivity.mStoreID);
        } else {
            frameLayoutOverSeas.setVisibility(View.GONE);
        }
        ServiceManager.productDetailRequest(TongleAppInstance.getInstance().getUserID(), CollectProductMoreActivity.prodID, successListener(), errorListener());
    }

    /**
     * 功能：商品图文详情网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<PicTxtDetailResult>() {
            @Override
            public void onResponse(PicTxtDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    showRequestCallbackDialog();
                    pictxtedetailmodle.prod_pic_text_list = response.getBody().prod_pic_text_list;
                    if (pictxtedetailmodle.prod_pic_text_list.size() == 0) {
                        detailListView.setVisibility(View.GONE);
                        rl_replace.setVisibility(View.VISIBLE);
                    } else {
                        detailListAdapter = new PicTextDetailListAdapter(getActivity(), pictxtedetailmodle.prod_pic_text_list);
                        detailListView.setAdapter(detailListAdapter);
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                cancelRequestCallbackDialog();
            }
        };
    }

    /**
     * 功能：商品图文详情请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                cancelRequestCallbackDialog();
            }
        };
    }

    /**
     * 获取海淘团购活动
     *
     * @param mStoreId
     */
    private void getOverseasGruop(String mStoreId) {
        ServiceManager.getStoreOverseasGroup(mStoreId, new Response.Listener<StoreOverseasInfoResult>() {
            @Override
            public void onResponse(StoreOverseasInfoResult storeOverseasInfoResult) {

                if (TongleAppConst.YES.equals(storeOverseasInfoResult.getHead().getReturn_flag())) {
                    setOverseasData(storeOverseasInfoResult.body.group_info);
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + storeOverseasInfoResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        });
    }

    private void setOverseasData(final StoreOverseasGroupInfo group_info) {
        frameLayoutOverSeas.setVisibility(View.VISIBLE);
        frameLayoutOverSeas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent overseasIntent = new Intent(getActivity(), ProdIntegralActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("prod_url", group_info.group_info_url);
                bundle.putString("myTitle", "活动说明");
                overseasIntent.putExtras(bundle);
                startActivity(overseasIntent);
            }
        });

        txt_overseas_date.setText(String.format(getResources().getString(R.string.overseas_date), group_info.activity_begin_date, group_info.activity_end_date));
        txt_overseas_number.setText(String.format(getResources().getString(R.string.overseas_group_number), group_info.order_qty, group_info.limit_qty));
        txt_overseas_memo.setText(group_info.group_show_msg);

        if (group_info.limit_qty > 0) {
            roundProgressBar.setMax(group_info.limit_qty);
            roundProgressBar.setProgress(group_info.order_qty);
        }

        if (!TextUtils.isEmpty(group_info.background_pic_url)) {
            ImageLoader.getInstance().displayImage(group_info.background_pic_url, img_overseas_bg, ImageOptionsUtils.overseasGroupBigBgOptions(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    if (loadedImage == null) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                    } else {
                        float es = (float) (DipUtils.getScreenWidth(getContext()) - getContext().getResources().getDimensionPixelSize(R.dimen.mstore_home_head_icon_left) * 2) / (float) loadedImage.getWidth();
                        int height = (int) (loadedImage.getHeight() * es);
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
                        params.height = height;
                        view.setLayoutParams(params);
                    }
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewParent parent = mainView.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(mainView);
        }
    }
}
