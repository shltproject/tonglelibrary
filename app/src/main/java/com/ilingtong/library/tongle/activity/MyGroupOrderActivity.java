package com.ilingtong.library.tongle.activity;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.CollectFragmentPagerAdapter;
import com.ilingtong.library.tongle.fragment.GroupOrderListFragment;

/**
 * author: liuting
 * Date: 2016/3/15
 * Time: 9:55
 * Email: liuting@ilingtong.com
 * Desc:团购订单
 */
public class MyGroupOrderActivity extends BaseFragmentActivity implements View.OnClickListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标
    private TextView mTxtAll;//全部
    private TextView mTxtUnused;//未消费
    private TextView mTxtRefund;//退款单
    private ImageView mImgAll;//全部
    private ImageView mImgUnused;//未消费
    private ImageView mImgRefund;//退款单
    private ViewPager mVpMain;//切换

    private int currIndex = 0;
    private Fragment[] fragments = new Fragment[3];

    // public static boolean UPDATE_LIST_FLAG = false; //是否刷新列表的标准

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_group_order);
        initView();
    }

    public void initView() {
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle.setText(getResources().getString(R.string.my_group_order_top_name));
        mTxtTitle.setGravity(View.TEXT_ALIGNMENT_CENTER);
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);

        mTxtAll = (TextView) findViewById(R.id.my_group_order_tv_all);
        mTxtUnused = (TextView) findViewById(R.id.my_group_order_tv_unused);
        mTxtRefund = (TextView) findViewById(R.id.my_group_order_tv_refund);
        mTxtAll.setOnClickListener(new TextOnClickListener(0));
        mTxtUnused.setOnClickListener(new TextOnClickListener(1));
        mTxtRefund.setOnClickListener(new TextOnClickListener(2));

        mImgAll = (ImageView) findViewById(R.id.my_group_order_img_all);
        mImgUnused = (ImageView) findViewById(R.id.my_group_order_img_unused);
        mImgRefund = (ImageView) findViewById(R.id.my_group_order_img_refund);

        mVpMain = (ViewPager) findViewById(R.id.my_group_order_vp_main);

        //初始化fragment并添加设置viewpagerAdapter
        fragments[0] = GroupOrderListFragment.newInstance(TongleAppConst.GROUP_ORDER_ALL);
        fragments[1] = GroupOrderListFragment.newInstance(TongleAppConst.GROUP_ORDER_UNUSED);
        fragments[2] = GroupOrderListFragment.newInstance(TongleAppConst.GROUP_ORDER_REFUND);
        CollectFragmentPagerAdapter adapter = new CollectFragmentPagerAdapter(getSupportFragmentManager(), fragments);
        mVpMain.setOnPageChangeListener(new ViewPageOnPageChangeListener());
        mVpMain.setAdapter(adapter);
        mVpMain.setOffscreenPageLimit(3);
        mVpMain.setCurrentItem(0);
    }

    /**
     * TextOnClickListener监听
     * 点击顶部 “全部”，“待付款”，“待收货”，“待评价”时设置viewpager的选中页
     */
    private class TextOnClickListener implements View.OnClickListener {
        private int index = 0;

        public TextOnClickListener(int i) {
            index = i;
        }

        public void onClick(View v) {
            mVpMain.setCurrentItem(index);
        }
    }

    /**
     * ViewPageOnPageChangeListener监听
     * viewpager选中页面改变时，设置选项卡title的字体和颜色
     */
    public class ViewPageOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int arg0) {
            currIndex = arg0;
            int i = currIndex + 1;
            setTabTitle(i);
        }
    }

    /**
     * 设置选项卡title的字体颜色
     *
     * @param tabId
     */
    public void setTabTitle(int tabId) {
        //viewpager顶部导航游标，并设置游标显示位置
        if (tabId == 1) {
            mImgAll.setVisibility(View.VISIBLE);
            mImgUnused.setVisibility(View.INVISIBLE);
            mImgRefund.setVisibility(View.INVISIBLE);
        } else if (tabId == 2) {
            mImgAll.setVisibility(View.INVISIBLE);
            mImgUnused.setVisibility(View.VISIBLE);
            mImgRefund.setVisibility(View.INVISIBLE);
        } else if (tabId == 3) {
            mImgAll.setVisibility(View.INVISIBLE);
            mImgUnused.setVisibility(View.INVISIBLE);
            mImgRefund.setVisibility(View.VISIBLE);
        }
        //viewpager顶部文字导航，并设置文字颜色
        Resources resource = (Resources) getResources();
        ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_text_color);
        ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_selecttext_color);

        if (tabId == 1) {
            mTxtAll.setTextColor(selectedTextColor);
            mTxtUnused.setTextColor(unselectedTextColor);
            mTxtRefund.setTextColor(unselectedTextColor);

        } else if (tabId == 2) {
            mTxtAll.setTextColor(unselectedTextColor);
            mTxtUnused.setTextColor(selectedTextColor);
            mTxtRefund.setTextColor(unselectedTextColor);
        } else if (tabId == 3) {
            mTxtAll.setTextColor(unselectedTextColor);
            mTxtUnused.setTextColor(unselectedTextColor);
            mTxtRefund.setTextColor(selectedTextColor);
        }
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.left_arrow_btn:
//                finish();
//                break;
//            default:break;
//        }

        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        }
    }
    //   @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            case TongleAppConst.REQUEST_CODE_SUBMIT:
//                if(UPDATE_LIST_FLAG==true){
//                    switch (currIndex){
//                        case 0:
//                            //刷新全部团购订单
//                            fragments[0].setUserVisibleHint(true);
//                            GroupOrderListFragment.newInstance(TongleAppConst.GROUP_ORDER_ALL).UPDATE_LIST_FLAG=true;
//                            break;
//                        case 1:
//                            //刷新未消费订单
//                            fragments[1].setUserVisibleHint(true);
//                            GroupOrderListFragment.newInstance(TongleAppConst.GROUP_ORDER_UNUSED).UPDATE_LIST_FLAG=true;
//                            break;
//                        case 2:
//                            //刷新未消费订单
//                            fragments[2].setUserVisibleHint(true);
//                            GroupOrderListFragment.newInstance(TongleAppConst.GROUP_ORDER_REFUND).UPDATE_LIST_FLAG=true;
//                            break;
//                    }
//                    UPDATE_LIST_FLAG=false;
//                }
//                break;
//        }
//    }
}
