package com.ilingtong.library.tongle.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectExpertDetailActivity;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.FindListenActivity;
import com.ilingtong.library.tongle.activity.HotSearchActivity;
import com.ilingtong.library.tongle.activity.MStoreActivity;
import com.ilingtong.library.tongle.activity.ProductTicketDetailActivity;
import com.ilingtong.library.tongle.activity.SearchActivity;
import com.ilingtong.library.tongle.protocol.ExpertBaseResult;
import com.ilingtong.library.tongle.protocol.QRData;
import com.ilingtong.library.tongle.protocol.ScanDataResult;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SearchEditText;
import com.ilingtong.library.tongle.widget.SelectDialog;

import java.lang.reflect.Field;

import static android.app.Activity.RESULT_OK;
import static com.ilingtong.library.tongle.R.string.search;

/**
 * User: lengjiqiang
 * Date: 2015/4/24
 * Time: 16:35
 * Email: jqleng@isoftstone.com
 * Desc: 发现属性页
 */
public class FindFragment extends BaseFragment implements View.OnClickListener {
    private ImageView scanImage;
    private ImageView listenImage;
    //private SearchView search; //搜索
    //private SearchEditText searchEditText;  //搜索
    private TextView txt_search;
    private LinearLayout expert;
    private LinearLayout store;
    private LinearLayout product;
    private LinearLayout item_forum;
    final int FIND_SCAN_CODE = 0;
    final int FIND_LISTEN_CODE = 10001;
    public ProgressDialog dialog;
    private String[] needPermissions = {Manifest.permission.CAMERA};//相机权限，扫码所需
    private String[] listenPermissions = {Manifest.permission.RECORD_AUDIO};//麦克风权限，听一听所需
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_find_layout, null);
        initView(rootView);
        return rootView;
    }

    public void initView(View rootView) {
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.fragment_find_dialog_message));
        scanImage = (ImageView) rootView.findViewById(R.id.image_scan);
        listenImage = (ImageView) rootView.findViewById(R.id.image_listen);
        //search = (SearchView) rootView.findViewById(R.id.search_view);
        //searchEditText = (SearchEditText) rootView.findViewById(R.id.fragment_find_search);
        txt_search = (TextView) rootView.findViewById(R.id.txt_search);
        expert = (LinearLayout) rootView.findViewById(R.id.item_expert);
        store = (LinearLayout) rootView.findViewById(R.id.item_store);
        product = (LinearLayout) rootView.findViewById(R.id.item_product);
        item_forum = (LinearLayout) rootView.findViewById(R.id.item_forum);

        scanImage.setOnClickListener(this);
        listenImage.setOnClickListener(this);
        expert.setOnClickListener(this);
        store.setOnClickListener(this);
        product.setOnClickListener(this);
        item_forum.setOnClickListener(this);
        
        txt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2017/10/13
                Intent intent = new Intent(getActivity(),SearchActivity.class);
                startActivity(intent);
            }
        });

//        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//
//            public boolean onQueryTextSubmit(String query) {
//                Intent intent = new Intent(getActivity().getApplicationContext(), SearchActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("KeyWords", query);
//                intent.putExtras(bundle);
//                startActivity(intent);
//                return true;
//            }
//
//            public boolean onQueryTextChange(String newText) {
//                return false;
//            }
//        });
//        setSearchViewBackground(search);
    }

    /**
     * android4.0 SearchView去掉（修改）搜索框的背景 修改光标
     */
    public void setSearchViewBackground(SearchView search) {
        try {
            Class<?> argClass = search.getClass();
            // 指定某个私有属性
            Field ownField = argClass.getDeclaredField("mSearchPlate"); // 注意mSearchPlate的背景是stateListDrawable(不同状态不同的图片)
            // 所以不能用BitmapDrawable
            // setAccessible 它是用来设置是否有权限访问反射类中的私有属性的，只有设置为true时才可以访问，默认为false
            ownField.setAccessible(true);
            View mView = (View) ownField.get(search);
            mView.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.input_bk));

            // 指定某个私有属性
            Field mQueryTextView = argClass.getDeclaredField("mQueryTextView");
            mQueryTextView.setAccessible(true);
            Class<?> mTextViewClass = mQueryTextView.get(search).getClass()
                    .getSuperclass().getSuperclass().getSuperclass();

            // mCursorDrawableRes光标图片Id的属性
            // 这个属性是TextView的属性，所以要用mQueryTextView（SearchAutoComplete）的父类（AutoCompleteTextView）的父
            // 类( EditText）的父类(TextView)
            Field mCursorDrawableRes = mTextViewClass
                    .getDeclaredField("mCursorDrawableRes");

            // setAccessible 它是用来设置是否有权限访问反射类中的私有属性的，只有设置为true时才可以访问，默认为false
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(mQueryTextView.get(search),
                    R.drawable.input_line);// 注意第一个参数持有这个属性(mQueryTextView)的对象(mSearchView)
            // 光标必须是一张图片不能是颜色，因为光标有两张图片，一张是第一次获得焦点的时候的闪烁的图片，一张是后边有内容时候的图片，如果用颜色填充的话，就会失去闪烁的那张图片，颜色填充的会缩短文字和光标的距离（某些字母会背光标覆盖一部分）。
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 该页中所有的点击事件处理方法
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        Intent intent;
        if (v.getId() == R.id.image_scan) {//扫一扫
            requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(getActivity(), needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                @Override
                public void requestSuccess() {
                    //跳转到扫码
                    Intent intent = new Intent(getActivity(), CaptureActivity.class);
                    intent.setAction(Intents.Scan.ACTION);
                    //不传入指定的扫码类型，默认是同时支持一维码、二维码等编码的扫描
//                    intent.putExtra(Intents.Scan.MODE, Intents.Scan.QR_CODE_MODE);
                    intent.putExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS, 0L);
                    intent.putExtra("TopName", getString(R.string.find));
                    startActivityForResult(intent, FIND_SCAN_CODE);
                }

                @Override
                public void requestFail() {
                    SelectDialog dialog = new SelectDialog(getActivity(), getString(R.string.please_open_camera_permission));
                    dialog.show();
                }
            });
            requestPermissionUtils.checkPermissions(FindFragment.this);

        } else if (v.getId() == R.id.image_listen) {//听一听
            requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(getActivity(), listenPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                @Override
                public void requestSuccess() {
                    //跳转到听一听
                    Intent intent = new Intent(getActivity(), FindListenActivity.class);
                    startActivityForResult(intent, FIND_LISTEN_CODE);
                }

                @Override
                public void requestFail() {
                    SelectDialog dialog = new SelectDialog(getActivity(), getString(R.string.please_open_audio_permission));
                    dialog.show();
                }
            });
            requestPermissionUtils.checkPermissions(FindFragment.this);

        } else if (v.getId() == R.id.item_expert) {//达人
            intent = new Intent(getActivity().getApplicationContext(), HotSearchActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("target", String.valueOf(0));
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (v.getId() == R.id.item_store) {//魔店
            Intent intent1 = new Intent(getActivity().getApplicationContext(), HotSearchActivity.class);
            Bundle bundle1 = new Bundle();
            bundle1.putString("target", String.valueOf(2));
            intent1.putExtras(bundle1);
            startActivity(intent1);
        } else if (v.getId() == R.id.item_product) {//产品
            Intent intent2 = new Intent(getActivity().getApplicationContext(), HotSearchActivity.class);
            Bundle bundle2 = new Bundle();
            bundle2.putString("target", String.valueOf(3));
            intent2.putExtras(bundle2);
            startActivity(intent2);
        } else if (v.getId() == R.id.item_forum) {//帖子
            Intent intent3 = new Intent(getActivity().getApplicationContext(), HotSearchActivity.class);
            Bundle bundle3 = new Bundle();
            bundle3.putString("target", String.valueOf(1));
            intent3.putExtras(bundle3);
            startActivity(intent3);
        }
    }

    /**
     * 处理带返回结果的activity方法
     *
     * @param requestCode 开发者定义的唯一请求码
     * @param resultCode  activity返回的结果码
     * @param data        activity返回的数据
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (FIND_SCAN_CODE == requestCode) {
            if (resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                String qrcode = b.getString("SCAN_RESULT");
                if (!TextUtils.isEmpty(qrcode)) {
                    dialog.show();
                    ServiceManager.doScanDataRequest(TongleAppInstance.getInstance().getUserID(), qrcode, successListener(), errorListener());
                }
            }
        } else if (FIND_LISTEN_CODE == requestCode) { //听一听返回
            if (resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                String qrcode = b.getString("LISTEN_RESULT");
                if (!TextUtils.isEmpty(qrcode)) {
                    ServiceManager.doListennDataRequest(TongleAppInstance.getInstance().getUserID(), qrcode, successListener(), errorListener());
                }
            }
        }
    }

    /**
     * 功能：注册网络响应成功，返回数据
     */
    private Response.Listener<ScanDataResult> successListener() {
        return new Response.Listener<ScanDataResult>() {
            @Override
            public void onResponse(ScanDataResult response) {
                dialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    QRData qrData = response.getBody().getQRData();
                    codeJump(qrData);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }


        };
    }

    /**
     * 扫二维码跳转
     */
    private void codeJump(QRData qrData) {
        if (!TextUtils.isEmpty(qrData.code_type)) {
            //达人详情
            if (TongleAppConst.STREXPERT.equals(qrData.code_type)) {
                ServiceManager.doExpertBaseRequest(qrData.user_id, ExpertBaseListener(), errorListener());
                //帖子详情
            } else if (TongleAppConst.STRFORUM.equals(qrData.code_type)) {
                Intent forumIntent = new Intent(getActivity(), CollectForumDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("post_id", qrData.post_id);
                forumIntent.putExtras(bundle);
                startActivity(forumIntent);
                //魔店详情
            } else if (TongleAppConst.STRSTORE.equals(qrData.code_type)) {
                Intent intent = new Intent(getActivity(), MStoreActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("mstore_id", qrData.store_id);
                intent.putExtras(bundle);
                startActivity(intent);
                //产品详情
            } else if (TongleAppConst.STRPRODUCT.equals(qrData.code_type)) {
                CollectProductDetailActivity.launch(getActivity(), qrData.prod_id, TongleAppConst.ACTIONID_SCAN_CODE, qrData.relation_id, "", "");
                //团购商品详情
            } else if (TongleAppConst.STRGROUP_PRODUCT.equals(qrData.code_type)) {
                ProductTicketDetailActivity.launch(getActivity(), qrData.prod_id, TongleAppConst.ACTIONID_SCAN_CODE, qrData.relation_id, "", "");
            }
        }
    }

    /**
     * 功能：注册网络响应成功，返回数据
     */
    private Response.Listener<ExpertBaseResult> ExpertBaseListener() {
        return new Response.Listener<ExpertBaseResult>() {
            @Override
            public void onResponse(ExpertBaseResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    Intent expertIntent = new Intent(getActivity(), CollectExpertDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("user_id", response.getBody().user_id);
                    bundle.putString("user_favorited_by_me", response.getBody().user_favorited_by_me);
                    expertIntent.putExtras(bundle);
                    startActivity(expertIntent);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }


        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}

