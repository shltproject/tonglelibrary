package com.ilingtong.library.tongle.protocol;

/**
 * Created by wuqian on 2016/5/18.
 * mail: wuqian@ilingtong.com
 * Description:组织信息 共用块9013
 * use by 1090接口
 */
public class OrganizationListItem {
    public String org_id;  //组织ID
    public String org_name; //组织名称
    public String org_summary; //组织简介
    public String org_photo_url; //组织图片
    public String latest_post_info;//最近发帖内容
    public String post_update_time;//帖子最近更新时间
    public String org_favorited_by_me;//是否被当前用户加入
}
