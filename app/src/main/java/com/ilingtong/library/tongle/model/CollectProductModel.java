package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.ProductListItemData;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 14:27
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CollectProductModel {
    // 收藏 -> 产品
    public String data_total_count;
    public ArrayList<ProductListItemData> prod_list;
    public ArrayList<ProductListItemData> my_prod_list;

}
