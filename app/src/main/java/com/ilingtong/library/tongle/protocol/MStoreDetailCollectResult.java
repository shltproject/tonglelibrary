package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 22:15
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class MStoreDetailCollectResult implements Serializable {
    private BaseInfo head;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }
}
