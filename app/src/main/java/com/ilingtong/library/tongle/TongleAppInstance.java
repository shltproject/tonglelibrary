package com.ilingtong.library.tongle;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.TextUtils;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * User: lengjiqiang
 * Date: 2015/5/6
 * Time: 10:06
 * Email: jqleng@isoftstone.com
 * Dest: application类。继承android Application类。初始化程序中使用到的所有全局变量，包括ImageLoader,Volley RequesQueue
 */
public class TongleAppInstance extends Application {
    private static RequestQueue requestQueue;
    private static TongleAppInstance instance;
    private static Context paramContext = null;

    public static float sWidth;
    public static float sHeight;

    public static TongleAppInstance getInstance() {
        return instance;
    }

//    public static DisplayImageOptions options;        // DisplayImageOptions是用于设置图片显示的类
//    public static DisplayImageOptions options_round;    //显示圆形图片
//    public static DisplayImageOptions headIcon_options;  //加载头像的opitions

    //根据用户ID取得用户的基本信息
    private String token;
    private String user_id; // 用户ID
    private String user_name; //用户名称
    private String user_nick_name;  // 用户昵称
    private String user_phone;  // 手机号
    private String user_photo_url;// 头像URL
    private String user_sex;// 性别
    private String user_id_no;// 身份证号
    private String user_sales_point;// 用户消费积分余额
    private String user_rebate_point;// 用户返利积分余额
    private String user_account_balance;//用户账户余额
    private String user_coupons;// 用户优惠券
    private String user_fans;// 用户粉丝数
    private String user_follows;// 用户关注数
    private String user_qr_code_url;// 用户二维码URL
    private String user_customs_flag;// 个人积分开关
    private String longitude;//GPS经度
    private String latitude;//GPS纬度

    private SharedPreferences sp;
    private SharedPreferences settingSp;  //该sharedprefences用来存储配置信息

    public LocationClient mLocationClient;//定义定位终端
    private boolean bIsShow;//是否已经提示定位失败
/*************************************配置数据 start ********************/
    /** *******项目配置信息。主项目需重新设置这些值********/
    private String PackageName = "com.ilingtong.app.tongle";  //当前project的包名
    private String app_inner_no ="01";
    private String ifRemberKeyName ="ifRemberKeyName";   //是否记住了密码SP存储KeyName
    private String loginUseridKeyName="loginUseridKeyName";  //登录用户名SP存储KeyName
    private String loginPwdKeyName="loginPwdKeyName";  //登录密码SP存储KeyName
    private String ifAutoLoginKeyName="ifAutoLoginKeyName";  //是否自动登录SP存储keyName
    private String SHARE_SDK_ID="8890093a6278";//sharesdk注册平台id
    private String WECHAT_APPID="wxa44de924715e51b9";//微信平台appid
    private String WECHAT_SECRET="04f00d143acc9c7f52ee9eeab74275b8";//微信平台密钥

    private String SINA_APPKEY="162262167" ;//新浪平台app_key
    private String SINA_APPSECRET="7f5c6a9c120e91f9d072b99efe1a1126";//新浪平台密钥
    private String SINA_REDIRECT="https://127.0.0.1/oauth2/default.html";//新浪平台回调地址

    /** *******项目配置信息。主项目需重新设置这些值********/
//    public void setSettings(){
////        setPackageName("com.ilingtong.app.tongle");
////        setApp_inner_no("01");
////        setIfRemberKeyName("ifRemberKeyName");
////        setLoginUseridKeyName("loginUseridKeyName");
////        setLoginPwdKeyName("loginPwdKeyName");
////        setIfAutoLoginKeyName("ifAutoLoginKeyName");
////        setSHARE_SDK_ID("8890093a6278");
////        setWECHAT_APPID("wxa44de924715e51b9");
////        setWECHAT_SECRET("04f00d143acc9c7f52ee9eeab74275b8");
////        setSINA_APPKEY("162262167");
////        setSINA_APPSECRET("7f5c6a9c120e91f9d072b99efe1a1126");
////        setSINA_REDIRECT("https://127.0.0.1/oauth2/default.html");
//    }

    private Handler mPayHandler;//订单支付消息处理

    /**
     * 获取对应的支付消息处理
     *
     * @return  mPayHandler
     */
    public Handler getmPayHandler() {
        return mPayHandler;
    }

    /**
     * 设置支付消息处理
     *
     * @param mPayHandler  支付消息处理
     */
    public void setmPayHandler(Handler mPayHandler) {
        this.mPayHandler = mPayHandler;
    }

    @Override
    public String getPackageName() {
        if (TextUtils.isEmpty(PackageName)){
           PackageName = settingSp.getString("packagename","");
        }
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
        settingSp.edit().putString("packagename",packageName).commit();
    }

    public String getApp_inner_no() {
        if (TextUtils.isEmpty(app_inner_no)){
            app_inner_no = settingSp.getString("app_inner_no","");
        }
        return app_inner_no;
    }

    public void setApp_inner_no(String app_inner_no) {
        this.app_inner_no = app_inner_no;
        settingSp.edit().putString("app_inner_no",app_inner_no).commit();
    }

    public String getIfRemberKeyName() {
        if (TextUtils.isEmpty(ifRemberKeyName)){
            ifRemberKeyName = settingSp.getString("ifRemberKeyName","ifRemberKeyName");
        }
        return ifRemberKeyName;
    }

    public void setIfRemberKeyName(String ifRemberKeyName) {
        this.ifRemberKeyName = ifRemberKeyName;
        settingSp.edit().putString("ifRemberKeyName",ifRemberKeyName).commit();
    }

    public String getLoginUseridKeyName() {
        if (TextUtils.isEmpty(loginUseridKeyName)){
            loginUseridKeyName = settingSp.getString("loginUserid","loginUserid");;
        }
        return loginUseridKeyName;
    }

    public void setLoginUseridKeyName(String loginUseridKeyName) {
        this.loginUseridKeyName = loginUseridKeyName;
        settingSp.edit().putString("loginUserid",loginUseridKeyName).commit();
    }

    public String getLoginPwdKeyName() {
        if (TextUtils.isEmpty(loginPwdKeyName)){
            loginPwdKeyName = settingSp.getString("loginPwdKeyName","loginPwdKeyName");
        }
        return loginPwdKeyName;
    }

    public void setLoginPwdKeyName(String loginPwdKeyName) {
        this.loginPwdKeyName = loginPwdKeyName;
        settingSp.edit().putString("loginPwdKeyName",loginPwdKeyName).commit();
    }

    public String getIfAutoLoginKeyName() {
        if (TextUtils.isEmpty(ifAutoLoginKeyName)){
            ifAutoLoginKeyName = settingSp.getString("ifAutoLoginKeyName","ifAutoLoginKeyName");
        }
        return ifAutoLoginKeyName;
    }

    public void setIfAutoLoginKeyName(String ifAutoLoginKeyName) {
        this.ifAutoLoginKeyName = ifAutoLoginKeyName;
        settingSp.edit().putString("ifAutoLoginKeyName",ifAutoLoginKeyName).commit();
    }

    public String getSHARE_SDK_ID() {
        if (TextUtils.isEmpty(SHARE_SDK_ID)){
            SHARE_SDK_ID = settingSp.getString("SHARE_SDK_ID","");
        }
        return SHARE_SDK_ID;
    }

    public void setSHARE_SDK_ID(String SHARE_SDK_ID) {
        this.SHARE_SDK_ID = SHARE_SDK_ID;
        settingSp.edit().putString("SHARE_SDK_ID",SHARE_SDK_ID).commit();
    }

    public String getWECHAT_APPID() {
        if (TextUtils.isEmpty(WECHAT_APPID)){
            WECHAT_APPID = settingSp.getString("WECHAT_APPID","");
        }
        return WECHAT_APPID;
    }

    public void setWECHAT_APPID(String WECHAT_APPID) {
        this.WECHAT_APPID = WECHAT_APPID;
        settingSp.edit().putString("WECHAT_APPID",WECHAT_APPID).commit();
    }

    public String getWECHAT_SECRET() {
        if (TextUtils.isEmpty(WECHAT_SECRET)){
            WECHAT_SECRET = settingSp.getString("WECHAT_SECRET","");
        }
        return WECHAT_SECRET;
    }

    public void setWECHAT_SECRET(String WECHAT_SECRET) {
        this.WECHAT_SECRET = WECHAT_SECRET;
        settingSp.edit().putString("WECHAT_SECRET",WECHAT_SECRET).commit();
    }

    public String getSINA_APPKEY() {
        if (TextUtils.isEmpty(SINA_APPKEY)){
            SINA_APPKEY = settingSp.getString("SINA_APPKEY","");
        }
        return SINA_APPKEY;
    }

    public void setSINA_APPKEY(String SINA_APPKEY) {
        this.SINA_APPKEY = SINA_APPKEY;
        settingSp.edit().putString("SINA_APPKEY",SINA_APPKEY).commit();
    }

    public String getSINA_APPSECRET() {
        if (TextUtils.isEmpty(SINA_APPSECRET)){
            SINA_APPSECRET = settingSp.getString("SINA_APPSECRET","");
        }
        return SINA_APPSECRET;
    }

    public void setSINA_APPSECRET(String SINA_APPSECRET) {
        this.SINA_APPSECRET = SINA_APPSECRET;
        settingSp.edit().putString("SINA_APPSECRET",SINA_APPSECRET).commit();
    }

    public String getSINA_REDIRECT() {
        if (TextUtils.isEmpty(SINA_REDIRECT)){
            SINA_REDIRECT = settingSp.getString("SINA_REDIRECT","");
        }
        return SINA_REDIRECT;
    }

    public void setSINA_REDIRECT(String SINA_REDIRECT) {
        this.SINA_REDIRECT = SINA_REDIRECT;
        settingSp.edit().putString("SINA_REDIRECT",SINA_REDIRECT).commit();
    }
/*************************************配置数据 end ********************/
    public String getLatitude() {
        if (TextUtils.isEmpty(latitude)) {
            latitude = sp.getString("latitude", "");
        }
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        if (TextUtils.isEmpty(longitude)) {
            longitude = sp.getString("longitude", "");
        }
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getToken() {
        if (TextUtils.isEmpty(token)) {
            token = sp.getString("token", "");
        }
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserID() {
        if (TextUtils.isEmpty(user_id)) {
            user_id = sp.getString("user_id", "");
        }
        return user_id;
    }

    public void setUserID(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        if (TextUtils.isEmpty(user_name)) {
            user_name = sp.getString("user_name", "");
        }
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_nick_name() {
        if (TextUtils.isEmpty(user_nick_name)) {
            user_nick_name = sp.getString("user_nick_name", "");
        }
        return user_nick_name;
    }

    public void setUser_nick_name(String user_nick_name) {
        this.user_nick_name = user_nick_name;
    }

    public String getUser_phone() {
        if (TextUtils.isEmpty(user_phone)) {
            user_phone = sp.getString("user_phone", "");
        }
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_photo_url() {
        if (TextUtils.isEmpty(user_photo_url)) {
            user_photo_url = sp.getString("user_photo_url", "");
        }
        return user_photo_url;
    }

    public void setUser_photo_url(String user_photo_url) {
        this.user_photo_url = user_photo_url;
    }

    public String getUser_sex() {
        if (TextUtils.isEmpty(user_sex)) {
            user_sex = sp.getString("user_sex", "");
        }
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public String getUser_id_no() {
        if (TextUtils.isEmpty(user_id_no)) {
            user_id_no = sp.getString("user_id_no", "");
        }
        return user_id_no;
    }

    public void setUser_id_no(String user_id_no) {
        this.user_id_no = user_id_no;
    }

    public String getUser_sales_point() {
        if (TextUtils.isEmpty(user_sales_point)) {
            user_sales_point = sp.getString("user_sales_point", "");
        }
        return user_sales_point;
    }

    public void setUser_sales_point(String user_sales_point) {
        this.user_sales_point = user_sales_point;
    }

    public String getUser_rebate_point() {
        if (TextUtils.isEmpty(user_rebate_point)) {
            user_rebate_point = sp.getString("user_rebate_point", "");
        }
        return user_rebate_point;
    }

    public void setUser_rebate_point(String user_rebate_point) {
        this.user_rebate_point = user_rebate_point;
    }

    public String getUser_account_balance() {
        if (TextUtils.isEmpty(user_account_balance)) {
            user_account_balance = sp.getString("user_account_balance", "");
        }
        return user_account_balance;
    }

    public void setUser_account_balance(String user_account_balance) {
        this.user_account_balance = user_account_balance;
    }

    public String getUser_coupons() {
        if (TextUtils.isEmpty(user_coupons)) {
            user_coupons = sp.getString("user_coupons", "");
        }
        return user_coupons;
    }

    public void setUser_coupons(String user_coupons) {
        this.user_coupons = user_coupons;
    }

    public String getUser_fans() {
        if (TextUtils.isEmpty(user_fans)) {
            user_fans = sp.getString("user_fans", "");
        }
        return user_fans;
    }

    public void setUser_fans(String user_fans) {
        this.user_fans = user_fans;
    }

    public String getUser_follows() {
        if (TextUtils.isEmpty(user_follows)) {
            user_follows = sp.getString("user_follows", "");
        }
        return user_follows;
    }

    public void setUser_follows(String user_follows) {
        this.user_follows = user_follows;
    }

    public String getUser_qr_code_url() {
        if (TextUtils.isEmpty(user_qr_code_url)) {
            user_qr_code_url = sp.getString("user_qr_code_url", "");
        }
        return user_qr_code_url;
    }

    public void setUser_qr_code_url(String user_qr_code_url) {
        this.user_qr_code_url = user_qr_code_url;
    }

    public String getUser_customs_flag() {
        if (TextUtils.isEmpty(user_customs_flag)) {
            user_customs_flag = sp.getString("user_customs_flag", "");
        }
        return user_customs_flag;
    }

    public void setUser_customs_flag(String user_customs_flag) {
        this.user_customs_flag = user_customs_flag;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        sp = getSharedPreferences("userInfo",Context.MODE_PRIVATE);
        settingSp = getSharedPreferences("settings", Context.MODE_PRIVATE);
        //setSettings();
        paramContext = getApplicationContext();
        sWidth = getResources().getDisplayMetrics().widthPixels;
        sHeight = getResources().getDisplayMetrics().heightPixels;

        requestQueue = Volley.newRequestQueue(this);
//        options = new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.default_image)            // 设置图片下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.default_image)    // 设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.default_image)        // 设置图片加载或解码过程中发生错误显示的图片
//                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
//                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
//                        //.displayer(new RoundedBitmapDisplayer(20))	// 设置成圆角图片
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .build();
//        options_round = new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.default_image)            // 设置图片下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.default_image)    // 设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.default_image)        // 设置图片加载或解码过程中发生错误显示的图片
//                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
//                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
//                .displayer(new RoundedBitmapDisplayer(200))    // 设置成圆角图片
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .build();
//        headIcon_options = new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.head_icon_defualt)            // 设置图片下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.head_icon_defualt)    // 设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.head_icon_defualt)        // 设置图片加载或解码过程中发生错误显示的图片
//                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
//                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
//                        //.displayer(new RoundedBitmapDisplayer(20))	// 设置成圆角图片
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .build();
        initImageLoader(getApplicationContext());

        /***
         * 初始化定位，建议在Application中创建
         */
        mLocationClient = new LocationClient(this.getApplicationContext());
        mLocationClient.registerLocationListener(mListener);
        initLocation();
        mLocationClient.start();
        bIsShow=false;
    }

    public static RequestQueue getRequestQueue() {
        return requestQueue;
    }

    /**
     * 获取上下文
     *
     * @return
     */
    public static Context getAppContext() {
        return paramContext;
    }

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)
                        // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        ImageLoader.getInstance().init(config);
    }

    private void initLocation(){
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy
        );//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType("bd09ll");//可选，默认gcj02，设置返回的定位结果坐标系
        int span=1000;
        option.setScanSpan(span);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);//可选，默认false,设置是否使用gps
        option.setLocationNotify(true);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIsNeedLocationDescribe(true);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(true);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setIgnoreKillProcess(false);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.SetIgnoreCacheException(false);//可选，默认false，设置是否收集CRASH信息，默认收集
        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        mLocationClient.setLocOption(option);
    }

    /*****
     * 定位结果回调，重写onReceiveLocation方法，可以直接拷贝如下代码到自己工程中修改
     */
    public BDLocationListener mListener = new BDLocationListener() {

        @Override
        public void onReceiveLocation(BDLocation location) {
            // TODO Auto-generated method stub
            if (null != location && location.getLocType() != BDLocation.TypeServerError) {
                TongleAppInstance.getInstance().setLongitude(location.getLongitude() + "");//获取经度
                TongleAppInstance.getInstance().setLatitude(location.getLatitude() + "");//获取纬度
                sp.edit().putString("longitude", location.getLongitude() + "").commit();
                sp.edit().putString("latitude", location.getLatitude() + "").commit();

//                Log.i("TAG", TongleAppInstance.getInstance().getLongitude());
//                Log.i("TAG", TongleAppInstance.getInstance().getLatitude());

                if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                    //Log.i("TAG", getString(R.string.gps_location_success_msg));
                } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                   // Log.i("TAG", getString(R.string.net_location_success_msg));
                } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                   // Log.i("TAG", getString(R.string.off_location_success_msg));
                } else if (location.getLocType() == BDLocation.TypeServerError) {
                    if(bIsShow){
                        return;
                    }else{
//                        ToastUtils.toastShort(getString(R.string.error_location_fail_msg));
                        bIsShow=true;
                    }
                } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                    if(bIsShow){
                        return;
                    }else{
//                        ToastUtils.toastShort(getString(R.string.error_location_fail_msg));
                        bIsShow=true;
                    }
                } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                    if(bIsShow){
                        return;
                    }else{
//                        ToastUtils.toastShort(getString(R.string.error_location_fail_msg));
                        bIsShow=true;
                    }
                }

            }
        }

    };

}
