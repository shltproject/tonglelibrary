package com.ilingtong.library.tongle.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * @ClassName: SmartScrollView
 * @Package: com.ilingtong.library.tongle.widget
 * @Description: 重写ScrollView 监听滑动事件
 * @author: liuting
 * @Date: 2017/6/21 10:45
 */

public class SmartScrollView extends ScrollView {

    private ScrollBottomListener scrollBottomListener;

    public SmartScrollView(Context context) {
        super(context);
    }

    public SmartScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SmartScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (t + getHeight() >= computeVerticalScrollRange()) {
            //ScrollView滑动到底部了
            scrollBottomListener.scrollBottom(true);
        } else {
            scrollBottomListener.scrollBottom(false);
        }
    }

    public void setScrollBottomListener(ScrollBottomListener scrollBottomListener) {
        this.scrollBottomListener = scrollBottomListener;
    }

    //滑动监听
    public interface ScrollBottomListener {
        public void scrollBottom(boolean isScrollBottom);
    }
}