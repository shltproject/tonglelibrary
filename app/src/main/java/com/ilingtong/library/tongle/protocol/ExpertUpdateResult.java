package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: ljq
 * Date: 2015/6/3
 * Time: 22:31
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ExpertUpdateResult implements Serializable {
    private BaseInfo head;
    private ExpertUpdateInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ExpertUpdateInfo getBody() {
        return body;
    }

    public void setBody(ExpertUpdateInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
