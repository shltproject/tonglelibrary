package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/10.
 * mail: wuqian@ilingtong.com
 * Description:团购商品购买须知
 * use by 6007 6011
 */
public class CouponPurchaseNoticeInfo implements Serializable {
    private String title;    //须知项目名
    private String desc;   //须知项目内容

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
