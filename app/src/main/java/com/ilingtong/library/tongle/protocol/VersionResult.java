package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/24
 * Time: 15:59
 * Email: jqleng@isoftstone.com
 * Desc: 版本号类，通过该对象，被用作网络请求参数
 */
public class VersionResult implements Serializable{
    private BaseInfo head;
    private VersionInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public VersionInfo getBody() {
        return body;
    }

    public void setBody(VersionInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
