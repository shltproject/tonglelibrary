package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.AddressListAdapter;
import com.ilingtong.library.tongle.model.AddressModel;
import com.ilingtong.library.tongle.protocol.AddressListItem;
import com.ilingtong.library.tongle.protocol.AddressNo;
import com.ilingtong.library.tongle.protocol.AddressResult;
import com.ilingtong.library.tongle.protocol.DefaultAddressResult;
import com.ilingtong.library.tongle.protocol.DelAddrResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/10
 * Time: 14:38
 * Email: jqleng@isoftstone.com
 * Desc: 用户收货地址管理页
 */
public class AddressManageActivity extends BaseActivity implements View.OnClickListener {
    private ImageView returnBtn;
    private TextView top_name;
    private ListView addressList;
    private Button newAddrBtn;

    private AddressListAdapter listAdapter;
    private Button set2DefaultBtn, deleteBtn;
    private View selectItemView;
    private AddressListItem itemToDefault = new AddressListItem();
    private AddressNo item;
    private ArrayList<AddressNo> addrNoList;
    public static AddressManageActivity instance;
    private String type;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:
                    listAdapter = new AddressListAdapter(AddressManageActivity.this, AddressModel.my_address_list);
                    addressList.setAdapter(listAdapter);
                    break;
            }
        }
    };

    /**
     * 页面跳转
     *
     * @param context
     * @param type  标志，用于区分是从选择地址列表进入，还是设置页面进入
     */
    public static void launcher(Activity context, String type, int requestCode){
        Intent intent = new Intent(context, AddressManageActivity.class);
        intent.putExtra("type", (Serializable) type);
        context.startActivityForResult(intent,requestCode);
    }

    /**
     * 页面跳转
     *
     * @param context
     * @param type  标志，用于区分是从选择地址列表进入，还是设置页面进入
     */
    public static void launcher(Activity context, String type){
        Intent intent = new Intent(context, AddressManageActivity.class);
        intent.putExtra("type", (Serializable) type);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_manage_layout);
        instance=this;
        type=getIntent().getExtras().getString("type");
        doRequest();
        initView();
    }

    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        returnBtn = (ImageView) findViewById(R.id.left_arrow_btn);
        newAddrBtn = (Button) findViewById(R.id.new_address_btn);
        addressList = (ListView) findViewById(R.id.manage_address_listView);
        returnBtn.setOnClickListener(this);
        newAddrBtn.setOnClickListener(this);

        returnBtn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(R.string.manage_address);

        //点击列表一项进入修改地址列表
        addressList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int addrID = (int) parent.getItemIdAtPosition(position);
                Intent intent = new Intent(AddressManageActivity.this, AddressModifyActivity.class);
                intent.putExtra("address_id", addrID);
                startActivityForResult(intent, 10001);
            }
        });

        //地址列表长按出现菜单
        addressList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                selectItemView = parent.getChildAt(position);
                itemToDefault = (AddressListItem) parent.getItemAtPosition(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(AddressManageActivity.this);
                View dlgView = LayoutInflater.from(AddressManageActivity.this).inflate(R.layout.address_dialog_layout, null);
                builder.setView(dlgView);
                final AlertDialog alert = builder.create();
                alert.setCanceledOnTouchOutside(true);
                //设置默认地址
                set2DefaultBtn = (Button) dlgView.findViewById(R.id.set_default_btn);
                set2DefaultBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                        ServiceManager.doSetDefaultAddrRequest(TongleAppInstance.getInstance().getUserID(), itemToDefault.address_no, setDefaultListener(), errorListener());
                    }
                });
                //删除地址
                deleteBtn = (Button) dlgView.findViewById(R.id.delete_btn);
                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                        addrNoList = new ArrayList<AddressNo>();
                        item = new AddressNo();
                        item.address_no = itemToDefault.address_no;

                        addrNoList.add(item);
                        //接口请求服务器删除
                        ServiceManager.doDeleteAddrRequest(TongleAppInstance.getInstance().getUserID(), addrNoList, deleteListener(), errorListener());
                    }
                });
                alert.show();
                return true;
            }
        });
    }

    public void doRequest() {
        ServiceManager.doAddressRequest(TongleAppInstance.getInstance().getUserID(), addrListener(), errorListener());
    }

    /**
     * 功能：设置默认收货地址网络响应成功，返回数据
     */
    private Response.Listener<DefaultAddressResult> setDefaultListener() {
        return new Response.Listener<DefaultAddressResult>() {
            @Override
            public void onResponse(DefaultAddressResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    for (int i = 0; i < addressList.getCount(); i++) {
                        View itemview = addressList.getChildAt(i);
                        ImageView selectIcon = (ImageView) itemview.findViewById(R.id.select_icon);
                        selectIcon.setVisibility(View.INVISIBLE);
                    }
                    ImageView defaultIcon = (ImageView) selectItemView.findViewById(R.id.select_icon);
                    defaultIcon.setVisibility(View.VISIBLE);
                    defaultIcon.setBackgroundResource(R.drawable.dingdanxiangqing_icon01);
                    //成功修改了默认地址之后，回到地址的选择界面时，也应该刷新默认地址的显示
                    if (AddressModel.my_address_list.size() != 0) {
                        for (int i = 0; i < AddressModel.my_address_list.size(); i++) {
                            AddressModel.my_address_list.get(i).default_flag=TongleAppConst.NO;
                            if (itemToDefault.address_no.equals(AddressModel.my_address_list.get(i).address_no)) {
                                AddressModel.my_address_list.get(i).default_flag=TongleAppConst.YES;
                            }
                        }
                    }
                    ToastUtils.toastShort(getString(R.string.address_manager_set_success));
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：删除收货地址网络响应成功，返回数据
     */
    private Response.Listener<DelAddrResult> deleteListener() {
        return new Response.Listener<DelAddrResult>() {
            @Override
            public void onResponse(DelAddrResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {

                    AddressModel.my_address_list.remove(item);
                    if (AddressModel.my_address_list.size() != 0) {
                        for (int i = 0; i < AddressModel.my_address_list.size(); i++) {
                            if (AddressModel.my_address_list.get(i).address_no.equals(addrNoList.get(0).address_no)) {
                                AddressModel.my_address_list.remove(AddressModel.my_address_list.get(i));
                            }
                        }
                    }
                    //刷新列表
                    if ((listAdapter != null) && (AddressModel.my_address_list != null)) {
                        mHandler.sendEmptyMessage(0);
                    }

                    //如果删除的是之前选中的收货地址，则也应该把保存的收货地址清除掉
                    if(AddressModel.checkAddress!=null){
                        if(!TextUtils.isEmpty(AddressModel.checkAddress.address_no)&&item.address_no.equals(AddressModel.checkAddress.address_no)){
                            AddressModel.checkAddress=null;
                            Log.e("TAG","run--->delete");
                        }
                    }

                    ToastUtils.toastShort(getString(R.string.common_delete_success));
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }

    /**
     * 功能：请求地址列表网络响应成功，返回数据
     */
    private Response.Listener<AddressResult> addrListener() {
        return new Response.Listener<AddressResult>() {
            @Override
            public void onResponse(AddressResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    //存储地址列表到内存中
                    AddressModel.my_address_list = response.getBody().getMy_address_list();
                    //刷新列表
                    mHandler.sendEmptyMessage(0);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.new_address_btn) {
            AddressNewActivity.launcher(AddressManageActivity.this,type,10001);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10001) {
            mHandler.sendEmptyMessage(0);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
