package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class OrderDetailHeadInfo implements Serializable {
    public String order_no;
    public String order_time;
    public String order_memo;
    public String quantity;
    public String order_total_amount;  //订单总金额  update on 2017/9/7
    public String fee_amount;
    public String status;
    public String pay_method;
    public String modify_flag;
    public String cancel_flag;
    public String evaluate_flag;
    public String tariff;
    public String customs_flag;
    public String customs_fail_reason;
    public String goods_amount;    //add on 2016/04/07  商品总额
    public String pay_amount;    //add on 2016/04/07    应付金额
    public String order_type;    // add on 2017/07/21  订单类别 值参见常量定义
    public String is_group;    //add on 2017/07/21  成团标志，值参见常量定义
    public String plan_send_date;   //预计发货日期  add on 2017/08/29
    public double account_pay_money;  //账户支付金额  add 2017/9/6

    @Override
    public String toString() {
        return "order_no:" + order_no + "\r\n" +
                "order_time:" + order_time + "\r\n" +
                "order_memo:" + order_memo + "\r\n" +
                "quantity:" + quantity + "\r\n" +
                "amount:" + order_total_amount + "\r\n" +
                "fee_amount:" + fee_amount + "\r\n" +
                "status:" + status + "\r\n" +
                "pay_method:" + pay_method + "\r\n" +
                "modify_flag:" + modify_flag + "\r\n" +
                "cancle_flag:" + cancel_flag + "\r\n" +
                "tariff:" + tariff + "\r\n" +
                "customs_fail_reason:" + customs_fail_reason + "\r\n" +
                "customs_flag:" + customs_flag + "\r\n" +
                "evaluate_flag:" + evaluate_flag;
    }
}
