package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.model.ShareParamModel;
import com.ilingtong.library.tongle.protocol.APPpromotionInfo;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;

import java.io.Serializable;
import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * Created by wuqian
 * mail: wuqian@ilingtong.com
 * Description: h5分享调起
 */
public class ShareH5PostDialogActivity extends BaseActivity implements View.OnClickListener {
    private TextView dialogSelectShareTxtWeixin;
    private TextView dialogSelectShareTxtMoment;
    private TextView dialogSelectShareTxtCancel;
    private TextView dialogSelectShareTxtSina;

    private ShareParamModel shareParam;

    private final int shareSuccess = 0;
    private final int shareFailure = 1;
    private final int shareCancel = 2;

    private String[] needPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};//存储权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case shareSuccess:
                    ToastUtils.toastShort(getString(R.string.share_success));
                    ShareH5PostDialogActivity.this.finish();
                    setResult(RESULT_OK);
                    break;
                case shareFailure:
                    ToastUtils.toastLong(getString(R.string.share_failure) + msg.obj);
                    break;
                case shareCancel:
                    ShareH5PostDialogActivity.this.finish();
                    ToastUtils.toastShort(getString(R.string.share_cancel));
                    break;
            }
        }
    };

    public static void launcherForResult(Activity activity, ShareParamModel shareParam, int requestCode) {
        Intent intent = new Intent(activity, ShareH5PostDialogActivity.class);
        intent.putExtra("shareParam", (Serializable) shareParam);
        activity.startActivityForResult(intent,requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        //去掉标题栏
        setContentView(R.layout.dialog_select_share_layout);
        window.getDecorView().setPadding(0, 0, 0, 0);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);
        shareParam = (ShareParamModel) getIntent().getSerializableExtra("shareParam");
        init();
    }

    private void init() {

        dialogSelectShareTxtWeixin = (TextView) findViewById(R.id.dialog_select_share_txt_weixin);
        dialogSelectShareTxtMoment = (TextView) findViewById(R.id.dialog_select_share_txt_moment);
        dialogSelectShareTxtCancel = (TextView) findViewById(R.id.dialog_select_share_txt_cancel);
        dialogSelectShareTxtSina = (TextView) findViewById(R.id.dialog_select_share_txt_sina);
        dialogSelectShareTxtSina.setVisibility(View.VISIBLE);

        dialogSelectShareTxtCancel.setOnClickListener(this);
        dialogSelectShareTxtMoment.setOnClickListener(this);
        dialogSelectShareTxtWeixin.setOnClickListener(this);
        dialogSelectShareTxtSina.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.dialog_select_share_txt_weixin) {  //分享给微信好友
            requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(ShareH5PostDialogActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                @Override
                public void requestSuccess() {
                    ShareSDK.initSDK(ShareH5PostDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                    HashMap<String, Object> hasMap1 = new HashMap<String, Object>();
                    hasMap1.put("Id", "4");
                    hasMap1.put("SortId", "4");
                    hasMap1.put("AppId", TongleAppInstance.getInstance().getWECHAT_APPID());
                    hasMap1.put("AppSecret", TongleAppInstance.getInstance().getWECHAT_SECRET());
                    hasMap1.put("BypassApproval", "false");
                    hasMap1.put("Enable", "true");
                    ShareSDK.setPlatformDevInfo(Wechat.NAME, hasMap1);

                    Platform platform_weinxin = ShareSDK.getPlatform(Wechat.NAME);

                    //设置分享的参数：
                    Wechat.ShareParams sp = new Wechat.ShareParams();
                    sp.setShareType(Platform.SHARE_WEBPAGE);
                    sp.setTitle(shareParam.title);
                    sp.setText(shareParam.desc);
                    sp.setUrl(shareParam.link);
                    sp.setImageUrl(shareParam.imgUrl);
                    // 分享
                    platform_weinxin.share(sp);
                    setPlatformActionListener(platform_weinxin);
                }

                @Override
                public void requestFail() {
                    SelectDialog dialog = new SelectDialog(ShareH5PostDialogActivity.this, getString(R.string.please_open_the_storage_permission));
                    dialog.show();
                }
            });
            requestPermissionUtils.checkPermissions(ShareH5PostDialogActivity.this);

        } else if (v.getId() == R.id.dialog_select_share_txt_moment) {  //分享到朋友圈

            requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(ShareH5PostDialogActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                @Override
                public void requestSuccess() {
                    ShareSDK.initSDK(ShareH5PostDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                    HashMap<String, Object> hasMap2 = new HashMap<String, Object>();
                    hasMap2.put("Id", "5");
                    hasMap2.put("SortId", "5");
                    hasMap2.put("AppId", TongleAppInstance.getInstance().getWECHAT_APPID());
                    hasMap2.put("AppSecret", TongleAppInstance.getInstance().getWECHAT_SECRET());
                    hasMap2.put("BypassApproval", "false");
                    hasMap2.put("Enable", "true");
                    ShareSDK.setPlatformDevInfo(WechatMoments.NAME, hasMap2);
                    Platform platformMoments = ShareSDK.getPlatform(WechatMoments.NAME);
                    //设置分享的参数：
                    Wechat.ShareParams shareParams = new Wechat.ShareParams();
                    shareParams.setTitle(shareParam.title);
                    shareParams.setText(shareParam.desc);
                    shareParams.setUrl(shareParam.link);
                    shareParams.setImageUrl(shareParam.imgUrl);
                    shareParams.setShareType(Platform.SHARE_WEBPAGE);
                    // 分享
                    platformMoments.share(shareParams);
                    setPlatformActionListener(platformMoments);
                }

                @Override
                public void requestFail() {
                    SelectDialog dialog = new SelectDialog(ShareH5PostDialogActivity.this, getString(R.string.please_open_the_storage_permission));
                    dialog.show();
                }
            });
            requestPermissionUtils.checkPermissions(ShareH5PostDialogActivity.this);

        } else if (v.getId() == R.id.dialog_select_share_txt_sina) {  //分享到微博

            requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(ShareH5PostDialogActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                @Override
                public void requestSuccess() {
                    ShareSDK.initSDK(ShareH5PostDialogActivity.this, TongleAppInstance.getInstance().getSHARE_SDK_ID());
                    HashMap<String, Object> hasMap3 = new HashMap<String, Object>();
                    hasMap3.put("Id", "1");
                    hasMap3.put("SortId", "1");
                    hasMap3.put("AppKey", TongleAppInstance.getInstance().getSINA_APPKEY());
                    hasMap3.put("AppSecret", TongleAppInstance.getInstance().getSINA_APPSECRET());
                    hasMap3.put("RedirectUrl", TongleAppInstance.getInstance().getSINA_REDIRECT());
                    hasMap3.put("ShareByAppClient", "true");
                    hasMap3.put("Enable", "true");
                    hasMap3.put("isNewApi", "true");
                    ShareSDK.setPlatformDevInfo(SinaWeibo.NAME, hasMap3);
                    Platform platformSina = ShareSDK.getPlatform(SinaWeibo.NAME);
                    //设置分享的参数：
                    SinaWeibo.ShareParams shareParamsSina = new SinaWeibo.ShareParams();
                    shareParamsSina.setText(shareParam.desc);
                    shareParamsSina.setImageUrl(shareParam.imgUrl);
                    setPlatformActionListener(platformSina);
                    // 分享
                    platformSina.share(shareParamsSina);
                }

                @Override
                public void requestFail() {
                    SelectDialog dialog = new SelectDialog(ShareH5PostDialogActivity.this, getString(R.string.please_open_the_storage_permission));
                    dialog.show();
                }
            });
            requestPermissionUtils.checkPermissions(ShareH5PostDialogActivity.this);
        } else if (v.getId() == R.id.dialog_select_share_txt_cancel) {
            this.finish();
        }
    }

    /**
     * 分享回调监听
     *
     * @param platform
     */
    private void setPlatformActionListener(Platform platform) {
        platform.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                handler.sendEmptyMessage(shareSuccess);
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                Message msg = new Message();
                msg.what = shareFailure;
                msg.obj = throwable.getLocalizedMessage();
                handler.sendMessage(msg);
            }

            @Override
            public void onCancel(Platform platform, int i) {
                handler.sendEmptyMessage(shareCancel);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
