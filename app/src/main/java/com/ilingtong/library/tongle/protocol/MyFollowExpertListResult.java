package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/5/18.
 * mail: wuqian@ilingtong.com
 * Description:1012接口 取得我的关注人列表 的返回json
 */
public class MyFollowExpertListResult implements Serializable {
    public BaseInfo head;
    public MyFollowExpertList body;
}
