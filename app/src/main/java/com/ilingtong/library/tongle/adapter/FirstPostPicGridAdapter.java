package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.FirstPostThumbnailPicUrl;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/6/15
 * Time: 14:23
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class FirstPostPicGridAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<FirstPostThumbnailPicUrl> list = new ArrayList<>();
    private Context mContext;


    public FirstPostPicGridAdapter(Context context, ArrayList<FirstPostThumbnailPicUrl> list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        //过滤url为空的图片
        for (int i = 0; i < list.size() ; i++) {
            if (!TextUtils.isEmpty(list.get(i).pic_url)){
               this.list.add(list.get(i));
            }
        }
    }

    @Override
    public int getCount() {
        if (list.size() > 9) {
            return 9;
        } else {
            return list.size();
        }

    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.grid_item_post_pic, null);
            holder = new ViewHolder();
            holder.itemImageView = (ImageView) view.findViewById(R.id.item_image);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        ImageLoader.getInstance().displayImage(list.get(position).pic_url, holder.itemImageView, ImageOptionsUtils.getOptions());
        return view;
    }

    static class ViewHolder {
        ImageView itemImageView;
    }
}
