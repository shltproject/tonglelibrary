package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/24
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc: 订单详细信息
 * use by 1029
 *
 */
public class OrderreturnInfo implements Serializable {
    public String order_no;
    public String order_status;

    @Override
    public String toString() {
        return "order_no:" + order_no + "\r\n" +
                "order_status:" + order_status;
    }
}
