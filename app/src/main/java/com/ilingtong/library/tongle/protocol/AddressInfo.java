package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 22:05
 * Email: jqleng@isoftstone.com
 * Desc: 地址信息类，该类包含通过网络返回的地址列表数据
 */
public class AddressInfo implements Serializable {
    private ArrayList<AddressListItem> my_address_list;

    public ArrayList getMy_address_list() {
        return my_address_list;
    }

    @Override
    public String toString() {
        return "my_address_list:" + my_address_list;
    }
}
