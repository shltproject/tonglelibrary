package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Package:com.ilingtong.library.tongle.protocol
 * author:liuting
 * Date:2017/2/14
 * Desc:店铺分类商品/关键字检索商品信息类
 * used by 6021、6022
 */

public class SortProdItemInfo implements Serializable{
    private String prod_id;//商品ID
    private String prod_name;//商品名称
    private String prod_thumbnail_pic_url;//商品缩略图URL
    private Double prod_show_price;//原售价
    private Double prod_price;//现售价
    private int prod_point;//积分
    private String prod_points_rule;//返利规则说明
    private String post_id;//关联帖子ID
    private String mstore_id;//魔店ID
    private String prod_favorited_by_me;//是否被当前用户收藏
    private String trade_prod_favorited_by_me;//是否为当前用户收藏为宝贝
    private String relation_id;//二维码关联ID
    private String conpon_flag;//团购券标识
    private String sort_key;//SortKey，翻页用主键

    public String getProd_id() {
        return prod_id;
    }

    public void setProd_id(String prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getProd_thumbnail_pic_url() {
        return prod_thumbnail_pic_url;
    }

    public void setProd_thumbnail_pic_url(String prod_thumbnail_pic_url) {
        this.prod_thumbnail_pic_url = prod_thumbnail_pic_url;
    }

    public Double getProd_show_price() {
        return prod_show_price;
    }

    public void setProd_show_price(Double prod_show_price) {
        this.prod_show_price = prod_show_price;
    }

    public Double getProd_price() {
        return prod_price;
    }

    public void setProd_price(Double prod_price) {
        this.prod_price = prod_price;
    }

    public int getProd_point() {
        return prod_point;
    }

    public void setProd_point(int prod_point) {
        this.prod_point = prod_point;
    }

    public String getProd_points_rule() {
        return prod_points_rule;
    }

    public void setProd_points_rule(String prod_points_rule) {
        this.prod_points_rule = prod_points_rule;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getMstore_id() {
        return mstore_id;
    }

    public void setMstore_id(String mstore_id) {
        this.mstore_id = mstore_id;
    }

    public String getProd_favorited_by_me() {
        return prod_favorited_by_me;
    }

    public void setProd_favorited_by_me(String prod_favorited_by_me) {
        this.prod_favorited_by_me = prod_favorited_by_me;
    }

    public String getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(String relation_id) {
        this.relation_id = relation_id;
    }

    public String getConpon_flag() {
        return conpon_flag;
    }

    public void setConpon_flag(String conpon_flag) {
        this.conpon_flag = conpon_flag;
    }

    public String getTrade_prod_favorited_by_me() {
        return trade_prod_favorited_by_me;
    }

    public void setTrade_prod_favorited_by_me(String trade_prod_favorited_by_me) {
        this.trade_prod_favorited_by_me = trade_prod_favorited_by_me;
    }

    public String getSort_key() {
        return sort_key;
    }

    public void setSort_key(String sort_key) {
        this.sort_key = sort_key;
    }
}
