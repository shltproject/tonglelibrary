package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: syc
 * Date: 2015/6/10
 * Time: 17:01
 * Email: ycshi@isoftstone.com
 * Dest:  belong to [1040]   order form data    prod_detail
 */
public class ProdDetailListItem implements Serializable {
    public String order_detail_no;
    public String prod_pic_url;
    public String prod_id;
    public String prod_name;
    public String price;
    public String quantity;
    public ArrayList<ProdSpecListItem> prod_spec_list;
    public String country_pic_url;
    public String import_info_desc;
    public String transfer_fee_desc ;
    public String tariff_desc;
    public String import_goods_flag;
    public String customs_flag;
}
