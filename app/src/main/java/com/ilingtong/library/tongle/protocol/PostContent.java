package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/20
 * Time: 14:18
 * Email: jqleng@isoftstone.com
 * Desc: 帖子内容类
 */
public class PostContent implements Serializable {
    public String pic_url;
    public String pic_memo;
    public String related_type;
    public String object_id;
    public String relation_id;
    public String coupon_flag;    //团购商品标识   add at 2016/04/11
}
