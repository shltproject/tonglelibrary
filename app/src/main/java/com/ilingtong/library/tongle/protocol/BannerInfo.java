package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/2
 * Time: 17:54
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class BannerInfo implements Serializable {
    // 首页广告位列表数据
    private ArrayList<PromotionListItem> top_promotion_list;

    public ArrayList getPromotionList() {
        return top_promotion_list;
    }

    @Override
    public String toString() {
        return "top_promotion_list:" + top_promotion_list;
    }
}
