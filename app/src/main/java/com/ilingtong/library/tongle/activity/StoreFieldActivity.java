package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.StoreFieldListAdapter;
import com.ilingtong.library.tongle.external.MyListView;
import com.ilingtong.library.tongle.protocol.StoreFieldItemInfo;
import com.ilingtong.library.tongle.protocol.StoreFieldResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.activity
 * author:liuting
 * Date:2017/2/14
 * Desc:店铺分类
 */

public class StoreFieldActivity extends Activity implements View.OnClickListener {
    private LinearLayout llyBack;//返回
    private LinearLayout llyAll;//全部宝贝
    private MyListView lvField;//分类列表
    private StoreFieldListAdapter storeFieldListAdapter;//店铺分类列表Adapter
    private Dialog dialog;//提示dialog
    private String mStoreId;//魔店ID
    private List<StoreFieldItemInfo> listField;//分类列表

    /**
     * 页面跳转
     *
     * @param activity
     * @param store_id 魔店ID
     */
    public static void launcher(Activity activity, String store_id) {
        Intent intent = new Intent(activity, StoreFieldActivity.class);
        intent.putExtra("store_id", store_id);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_field);
        initView();
        doRequest();
    }

    /**
     * 初始化
     */
    private void initView() {
        mStoreId = getIntent().getExtras().getString("store_id");
        listField = new ArrayList<>();
        llyBack = (LinearLayout) findViewById(R.id.store_field_lly_back);
        llyAll = (LinearLayout) findViewById(R.id.store_field_lly_all);
        lvField = (MyListView) findViewById(R.id.store_field_lv_field);

        dialog = DialogUtils.createLoadingDialog(StoreFieldActivity.this);
        dialog.setCancelable(true);

        llyBack.setOnClickListener(this);
        llyAll.setOnClickListener(this);
        storeFieldListAdapter = new StoreFieldListAdapter(StoreFieldActivity.this, listField, new StoreFieldListAdapter.IOnClickListener() {
            @Override
            public void onFieldClickListener(View view, String field_id) {
                SearchResultsActivity.launchActivityByField(StoreFieldActivity.this,mStoreId,TongleAppConst.SEARCH_CLASS,field_id);
            }

            @Override
            public void onSubFieldClickListener(View view, String field_id) {
                SearchResultsActivity.launchActivityByField(StoreFieldActivity.this,mStoreId,TongleAppConst.SEARCH_CLASS,field_id);
            }
        });
        lvField.setAdapter(storeFieldListAdapter);
    }

    /**
     * 请求数据
     */
    public void doRequest() {
        showDialog();
        ServiceManager.getStoreFieldList(mStoreId, new Response.Listener<StoreFieldResult>() {
            @Override
            public void onResponse(StoreFieldResult storeFieldResult) {
                dismissDialog();
                if (TongleAppConst.SUCCESS.equals(storeFieldResult.getHead().getReturn_flag())) {
                    listField.addAll(storeFieldResult.getBody().getField_list());
                    storeFieldListAdapter.notifyDataSetChanged();
                } else {
                    ToastUtils.toastLong(getResources().getString(R.string.para_exception) + storeFieldResult.getHead().getReturn_message());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dismissDialog();
                ToastUtils.toastLong(getResources().getString(R.string.sys_exception));
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.store_field_lly_back) {//返回
            finish();
        } else if (v.getId() == R.id.store_field_lly_all) {//所有宝贝
            SearchResultsActivity.launchActivityByField(StoreFieldActivity.this,mStoreId,TongleAppConst.SEARCH_CLASS,"");
        }
    }

    /**
     * dismiss dialog
     */
    public void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    /**
     * 显示Dialog
     */
    public void showDialog() {
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissDialog();
    }
}
