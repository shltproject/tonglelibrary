package com.ilingtong.library.tongle.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.ProStoreListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.CouponStoreInfo;
import com.ilingtong.library.tongle.protocol.GroupStoreListResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;
import com.ilingtong.library.tongle.widget.SelectDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/14
 * Time: 13:08
 * Email: liuting@ilingtong.com
 * Desc:商户列表
 */
public class StoreInfoActivity extends BaseActivity implements View.OnClickListener, XListView.IXListViewListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标
    private XListView mLvStore;//商户列表
    private RelativeLayout mRlyReplace;//无相关信息

    private GroupStoreListResult mGroupStoreListResult;//商户信息
    private ProStoreListAdapter mProStoreListAdapter;//商户列表Adapter
    private List<CouponStoreInfo> mStoreList;//商户列表

    private Dialog mDialog;//加载对话框
    private String mProductId;//商品ID

    public static final int REFRESH_LIST = 0;  //刷新列表
    public static final int NO_DATA = 1; //隐藏listview，显示rl_replace
    private boolean loadMoreFlag = true; //是否能加载更多
    private boolean clearFlag=true;//清空标志,true为清空
    private String[] needPermissions={Manifest.permission.CALL_PHONE};//拨打电话权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_LIST:
                    mLvStore.setRefreshTime();
                    mProStoreListAdapter.notifyDataSetChanged();
                    break;
                case NO_DATA:
                    mLvStore.setVisibility(View.GONE);
                    mRlyReplace.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xlistview_common_layout);
        initView();
        getData("");
    }

    public void initView() {
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle.setText(getResources().getString(R.string.store_info_top_name));
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);

        mLvStore = (XListView) findViewById(R.id.xlistview_common_lv_list);
        mRlyReplace = (RelativeLayout) findViewById(R.id.xlistview_common_rly_replace);
        mRlyReplace.setVisibility(View.GONE);

        //初始加载对话框
        mDialog = DialogUtils.createLoadingDialog(StoreInfoActivity.this);
        mDialog.setCancelable(false);

        mStoreList = new ArrayList<>();
        mStoreList.clear();
        mProStoreListAdapter = new ProStoreListAdapter(StoreInfoActivity.this, mStoreList, new ProStoreListAdapter.ICallPhoneListener() {
            @Override
            public void callPhone(View view, final int position) {
                requestPermissionUtils= RequestPermissionUtils.getRequestPermissionUtils(StoreInfoActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                    @Override
                    public void requestSuccess() {
                        //拨打电话
                        Utils.callPhone(StoreInfoActivity.this,mStoreList.get(position).getPhone());
                    }
                    @Override
                    public void requestFail() {
                        SelectDialog dialog=new SelectDialog(StoreInfoActivity.this,getString(R.string.please_open_call_phone_permission));
                        dialog.show();
                    }
                });
                requestPermissionUtils.checkPermissions(StoreInfoActivity.this);
            }
        });
        mLvStore.setAdapter(mProStoreListAdapter);
        mLvStore.setPullLoadEnable(false);
        mLvStore.setPullRefreshEnable(true);
        mLvStore.setXListViewListener(this, 0);

        mProductId = getIntent().getExtras().getString("product_id");
        mDialog.show();
    }

    public void getData(String store_id) {

        mGroupStoreListResult = new GroupStoreListResult();
//        mGroupStoreListResult = (GroupStoreListResult) TestInterface.parseJson(GroupStoreListResult.class, "6008.txt");
        ServiceManager.doProdStoreListRequest(store_id, mProductId, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, successListener(), errorListener());
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<GroupStoreListResult>() {
            @Override
            public void onResponse(GroupStoreListResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    //刷新获取数据后清空数据
                    if(mStoreList!=null&&clearFlag){
                        mStoreList.clear();
                    }
                    mGroupStoreListResult = response;
                    mStoreList.addAll(mGroupStoreListResult.getBody().getCoupon_store_info().getStore_list());
                    if (mGroupStoreListResult.getBody().getCoupon_store_info().getStore_total_count() < 1) {
                        mHandler.sendEmptyMessage(NO_DATA);
                    } else {
                        if (mGroupStoreListResult.getBody().getCoupon_store_info().getStore_total_count() > mStoreList.size()) {
                            loadMoreFlag = true;
                        } else {
                            loadMoreFlag = false;
                        }
                        mLvStore.setPullLoadEnable(loadMoreFlag);
                        mHandler.sendEmptyMessage(REFRESH_LIST);
                    }
                } else {
                    ToastUtils.toastLong(response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
                mDialog.dismiss();
            }
        };
    }

    /**
     * @param context
     * @param product_id 商品ID
     */
    public static void launcher(Activity context, String product_id) {
        Intent intent = new Intent(context, StoreInfoActivity.class);
        intent.putExtra("product_id", (Serializable) product_id);
        context.startActivity(intent);
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.left_arrow_btn:
//                finish();
//                break;
//            default:
//                break;
//        }

        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        }
    }

    @Override
    public void onRefresh(int id) {
//        mStoreList.clear();
        clearFlag=true;
        getData("");
    }

    @Override
    public void onLoadMore(int id) {
        if (loadMoreFlag) {
            clearFlag=false;
            getData(mStoreList.get(mStoreList.size() - 1).getCode());
        } else {
            ToastUtils.toastShort(getString(R.string.fragment_order_list_no_data));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode,permissions,paramArrayOfInt);
    }

}
