package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:6012接口的body
 * use by 6012
 */
public class GroupOrderListInfo implements Serializable{
    private int data_total_count;   //订单件数
    private List<CouponOrderBaseInfo> order_list;  //订单列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<CouponOrderBaseInfo> getOrder_list() {
        return order_list;
    }

    public void setOrder_list(List<CouponOrderBaseInfo> order_list) {
        this.order_list = order_list;
    }
}
