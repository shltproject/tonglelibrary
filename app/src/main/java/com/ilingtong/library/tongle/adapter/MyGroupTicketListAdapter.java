package com.ilingtong.library.tongle.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.external.MyListView;
import com.ilingtong.library.tongle.protocol.CouponCode;
import com.ilingtong.library.tongle.protocol.CouponOrderInfo;
import com.ilingtong.library.tongle.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/18
 * Time: 14:51
 * Email: liuting@ilingtong.com
 * Desc:商品团购券列表Adapter
 */
public class MyGroupTicketListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<CouponOrderInfo> mLvTicket;//团购券列表
   // private CouponOrderInfo mTicketItem = new CouponOrderInfo();
    private Context mContext;
    //    private TicketCodeListAdapter mTicketCodeListAdapter;//券号列表Adapter
    private AlertDialog mAlert;//对话框

    public MyGroupTicketListAdapter(Context context, List<CouponOrderInfo> ticketList) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvTicket = ticketList;
    }

    @Override
    public int getCount() {
        return mLvTicket.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvTicket.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        final List<CouponCode> mAllCode;//所有券号列表
        final boolean[] mHasOpen = new boolean[1];//是否展开
        mAllCode = new ArrayList<>();
        mAllCode.clear();

        if (view == null) {
            view = mInflater.inflate(R.layout.my_ticket_layout, null);
            holder = new ViewHolder();
            holder.myTicketTvProduct = (TextView) view.findViewById(R.id.my_ticket_tv_product);
            holder.myTicketTvTime = (TextView) view.findViewById(R.id.my_ticket_tv_time);
            holder.myTicketLvList = (MyListView) view.findViewById(R.id.my_ticket_lv_list);
            holder.myTicketImgQr = (ImageView) view.findViewById(R.id.my_ticket_list_item_img_qr);
            holder.myTicketRlyLine = (RelativeLayout) view.findViewById(R.id.my_ticket_list_item_rly_line);
            holder.myTicketRlyTop = (RelativeLayout) view.findViewById(R.id.my_ticket_rly_top);
            holder.mLlyFooter = (LinearLayout) view.findViewById(R.id.my_ticket_lv_footer);
            holder.mRlyLine = (RelativeLayout) view.findViewById(R.id.my_ticket_rly_line);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final CouponOrderInfo mTicketItem = (CouponOrderInfo) getItem(position);
        final String out_of_date = mTicketItem.getOut_of_date();
        final int count = mTicketItem.getCoupon_code_list().size();

        holder.myTicketTvProduct.setText(mTicketItem.getMstore_name() + "：" + mTicketItem.getProd_name());
        holder.myTicketTvTime.setText(mTicketItem.getOverdue_date_from() + "-" + mTicketItem.getOverdue_date_to());

        if (out_of_date.equals(TongleAppConst.GROUP_TICKET_NOT_USED)) {//未消费
            holder.myTicketRlyTop.setBackgroundResource(R.drawable.tuangou_coupon_top);
            holder.myTicketImgQr.setVisibility(View.VISIBLE);
            holder.myTicketRlyLine.setVisibility(View.VISIBLE);
            holder.myTicketImgQr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAlert = DialogUtils.showQrDialog(mContext, mTicketItem.getCoupon_codeurl_info().getCoupon_1d_qrcode_url(), mTicketItem.getCoupon_codeurl_info().getCoupon_2d_qrcode_url());//显示一维码和二维码对话框
                }
            });
        } else {//过期
            holder.myTicketRlyTop.setBackgroundResource(R.drawable.used_coupon_top);
            holder.myTicketImgQr.setVisibility(View.GONE);
            holder.myTicketRlyLine.setVisibility(View.GONE);
        }

        //最多显示两张团购券
        if (count > 0 && count <= 2) {
            mAllCode.clear();
            mAllCode.addAll(mLvTicket.get(position).getCoupon_code_list());
            TicketCodeListAdapter ticketCodeListAdapter = new TicketCodeListAdapter(mContext, mAllCode, mTicketItem.getOrder_no(), mTicketItem.getOut_of_date());
            holder.myTicketLvList.setAdapter(ticketCodeListAdapter);

            holder.mLlyFooter.setVisibility(View.GONE);
            holder.mRlyLine.setVisibility(View.GONE);
            DialogUtils.setLineHeight(holder.myTicketRlyLine, TongleAppConst.IMG_LINE_HEIGHT * count);
        } else if (count > 2) {//如果团购券大于两张
            mAllCode.clear();
            mHasOpen[0] = false;
            for (int i = 0; i < 2; i++) {
                mAllCode.add(mLvTicket.get(position).getCoupon_code_list().get(i));
            }
            TicketCodeListAdapter ticketCodeListAdapter = new TicketCodeListAdapter(mContext, mAllCode, mTicketItem.getOrder_no(), mTicketItem.getOut_of_date());
            holder.myTicketLvList.setAdapter(ticketCodeListAdapter);

            RelativeLayout rlyFoot = (RelativeLayout) holder.mLlyFooter.findViewById(R.id.my_ticket_rly_foot);
            final TextView tvMore = (TextView) holder.mLlyFooter.findViewById(R.id.my_ticket_tv_more);
            final ImageView imgMore = (ImageView) holder.mLlyFooter.findViewById(R.id.my_ticket_img_more);
            imgMore.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_more_coupon));
            tvMore.setText(String.format(mContext.getResources().getString(R.string.my_ticket_list_item_more_txt), (count - 2)));

            DialogUtils.setLineHeight(holder.myTicketRlyLine, TongleAppConst.IMG_LINE_HEIGHT * 3);
            holder.mLlyFooter.setVisibility(View.VISIBLE);
            holder.mRlyLine.setVisibility(View.VISIBLE);

            rlyFoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mHasOpen[0]) {//展开显示所有团购券
                        mAllCode.clear();
                        mAllCode.addAll(mLvTicket.get(position).getCoupon_code_list());
                        TicketCodeListAdapter ticketCodeListAdapter = new TicketCodeListAdapter(mContext, mAllCode, mTicketItem.getOrder_no(), mTicketItem.getOut_of_date());
                        tvMore.setText(mContext.getString(R.string.my_ticket_list_item_close_txt));
                        imgMore.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_shouqi));
                        mHasOpen[0] = true;
                        holder.myTicketLvList.setAdapter(ticketCodeListAdapter);

                        DialogUtils.setLineHeight(holder.myTicketRlyLine, (TongleAppConst.IMG_LINE_HEIGHT * (count + 1)));
                    } else {//收起
                        mAllCode.clear();
                        for (int i = 0; i < 2; i++) {
                            mAllCode.add(mLvTicket.get(position).getCoupon_code_list().get(i));
                        }
                        TicketCodeListAdapter ticketCodeListAdapter = new TicketCodeListAdapter(mContext, mAllCode, mTicketItem.getOrder_no(), mTicketItem.getOut_of_date());
                        holder.myTicketLvList.setAdapter(ticketCodeListAdapter);
                        imgMore.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_more_coupon));
                        tvMore.setText(String.format(mContext.getResources().getString(R.string.my_ticket_list_item_more_txt), (count - 2)));
                        mHasOpen[0] = false;

                        DialogUtils.setLineHeight(holder.myTicketRlyLine, TongleAppConst.IMG_LINE_HEIGHT * 3);
                    }
                }
            });
        }
        return view;
    }

    static class ViewHolder {
        TextView myTicketTvProduct;//团购券名称
        TextView myTicketTvTime;//团购券时间
        ListView myTicketLvList;//团购券券号列表
        ImageView myTicketImgQr;//团购券二维码
        RelativeLayout myTicketRlyLine;//垂直虚线
        RelativeLayout myTicketRlyTop;//头部
        LinearLayout mLlyFooter;//底部一栏
        RelativeLayout mRlyLine;//底部分隔线
    }
}

