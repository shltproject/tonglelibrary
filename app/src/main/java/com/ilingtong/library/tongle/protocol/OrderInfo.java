package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/4/7.
 * mail: wuqian@ilingtong.com
 * Description:订单info类
 * use by 1029
 */
public class OrderInfo implements Serializable {
    public OrderreturnInfo order_info;
}
