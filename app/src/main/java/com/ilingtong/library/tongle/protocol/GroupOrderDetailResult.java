package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:6013接口（团购订单详情）返回json对应的entity
 */
public class GroupOrderDetailResult extends BaseResult implements Serializable{

    private GroupOrderDetailInfo body;

    public GroupOrderDetailInfo getBody() {
        return body;
    }

    public void setBody(GroupOrderDetailInfo body) {
        this.body = body;
    }
}
