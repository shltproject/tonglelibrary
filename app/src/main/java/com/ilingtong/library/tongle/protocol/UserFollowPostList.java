package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/13
 * Time: 14:52
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class UserFollowPostList implements Serializable{
    public String user_id;
    public String user_nick_name;
    public String user_signature;
    public String user_head_photo_url;
    public String user_favorited_by_me;
    public String post_id;
    public String post_title;
    public String post_comment;
    public String post_time;
    public ArrayList<PostThumbnailPicUrl> post_thumbnail_pic_url;
    public String post_favorited_by_me;
    public String first_user_id;
    public String first_user_nick_name;
    public String first_user_signature;
    public String first_user_head_photo_url;
    public String first_post_id;
    public String first_post_title;
    public ArrayList<FirstPostThumbnailPicUrl> first_post_thumbnail_pic_url;
    public String first_post_time;
    public String rownum;     //行号  分页用  add on 2017/10/19

    @Override
    public String toString() {
        return "UserFollowPostList{" +
                "user_id='" + user_id + '\'' +
                ", user_nick_name='" + user_nick_name + '\'' +
                ", user_signature='" + user_signature + '\'' +
                ", user_head_photo_url='" + user_head_photo_url + '\'' +
                ", user_favorited_by_me='" + user_favorited_by_me + '\'' +
                ", post_id='" + post_id + '\'' +
                ", post_title='" + post_title + '\'' +
                ", post_comment='" + post_comment + '\'' +
                ", post_time='" + post_time + '\'' +
                ", post_thumbnail_pic_url=" + post_thumbnail_pic_url +
                ", post_favorited_by_me='" + post_favorited_by_me + '\'' +
                ", first_user_id='" + first_user_id + '\'' +
                ", first_user_nick_name='" + first_user_nick_name + '\'' +
                ", first_user_signature='" + first_user_signature + '\'' +
                ", first_user_head_photo_url='" + first_user_head_photo_url + '\'' +
                ", first_post_id='" + first_post_id + '\'' +
                ", first_post_title='" + first_post_title + '\'' +
                ", first_post_thumbnail_pic_url=" + first_post_thumbnail_pic_url +
                ", first_post_time='" + first_post_time + '\'' +
                '}';
    }
}
