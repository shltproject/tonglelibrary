package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/20
 * Time: 14:15
 * Email: jqleng@isoftstone.com
 * Desc: 用户的帖子详情信息类
 */
public class UserPostInfo implements Serializable {
    public String user_id;
    public String user_nick_name;
    public String user_signature;
    public String user_favorite_by_me;
    public String post_comment;
    public String user_head_photo_url;
    public String post_id;
    public String post_title;
    public String post_time;
    public String post_favorited_by_me;
    public String cover_picture_url;
    public ArrayList<PostContent> post_content;
    public ArrayList<PostPicUrlInfo> post_thumbnail_pic_url;
    public ArrayList<PostPicUrlInfo> first_post_thumbnail_pic_url;
    public String first_user_id;
    public String first_user_nick_name;
    public String first_user_signature;
    public String first_user_head_photo_url;
    public String first_post_id;
    public String first_post_title;
    public String first_post_time;
}
