package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.CouponPurchaseContentInfo;
import com.ilingtong.library.tongle.utils.FontUtils;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/17
 * Time: 9:09
 * Email: liuting@ilingtong.com
 * Desc:商品团购信息明细列表Adapter
 */
public class ProInfoListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<CouponPurchaseContentInfo> mLvInfo;
//    private CouponPurchaseContentInfo mInfoItem = new CouponPurchaseContentInfo();
    private Context mContext;

    public ProInfoListAdapter(Context context, List<CouponPurchaseContentInfo> infoList) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvInfo = infoList;
    }

    @Override
    public int getCount() {
        return mLvInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.ticket_info_list_item, null);
            holder = new ViewHolder();
            holder.infoName = (TextView) view.findViewById(R.id.ticket_info_lv_name);
            holder.infoNum = (TextView) view.findViewById(R.id.ticket_info_lv_num);
            holder.infoPrice = (TextView) view.findViewById(R.id.ticket_info_lv_price);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        CouponPurchaseContentInfo mInfoItem = (CouponPurchaseContentInfo) getItem(position);
        holder.infoName.setText(mInfoItem.getItem());
        holder.infoNum.setText(mInfoItem.getQty());
        holder.infoPrice.setText(String.format(mContext.getResources().getString(R.string.ticket_detail_total_txt), FontUtils.setTwoDecimal(mInfoItem.getPrice())));

        return view;
    }

    static class ViewHolder {
        TextView infoName;//团购信息名称
        TextView infoNum;//团购信息数量
        TextView infoPrice;//团购信息价格
    }
}

