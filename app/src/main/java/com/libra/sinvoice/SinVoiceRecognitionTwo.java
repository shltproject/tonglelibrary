package com.libra.sinvoice;

import java.io.UnsupportedEncodingException;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class SinVoiceRecognitionTwo implements SinVoiceRecognition.Listener {
    private final static String TAG = "SinVoiceRecognitionTwo";

    private final static int MSG_SET_RECG_TEXT = 1;
    private final static int MSG_RECG_START = 2;
    private final static int MSG_RECG_END = 3;
    private final static int MSG_PLAY_TEXT = 4;

    private final static int RECOGNITION_TYPE_1 = 1;
    private final static int RECOGNITION_TYPE_2 = 2;

    private Handler mHanlder;
    private char mRecgs[] = new char[100];
    private int mRecgCount;
    private char mRecgs_2[] = new char[100];
    private int mRecgCount_2;

    private SinVoiceRecognition mRecognition1;
    private SinVoiceRecognition mRecognition2;
    private RecordAsync mRecordAsync;
    private Listener mListener;

    public static interface Listener {
        void onSinVoiceRecognitionTwoStart();

        void onSinVoiceRecognitionTwoText(String text,int type);

        void onSinVoiceRecognitionTwoStop();
    }

    public SinVoiceRecognitionTwo(Context context,Listener listener) {
        mListener = listener;

        int bufferSize = AudioRecord.getMinBufferSize(
                Common.DEFAULT_SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);

        mRecordAsync = RecordAsync.getThis(Common.DEFAULT_SAMPLE_RATE,
                bufferSize);

        mRecognition1 = new SinVoiceRecognition();
        mRecognition1.init(context,VoiceDecoder.TYPE_1);
        mRecognition1.setListener(this);

        mRecognition2 = new SinVoiceRecognition();
        mRecognition2.init(context,VoiceDecoder.TYPE_2);
        mRecognition2.setListener(this);

        mHanlder = new RegHandler(this);
    }

    private static class RegHandler extends Handler {
        private StringBuilder mTextBuilder = new StringBuilder();
        private SinVoiceRecognitionTwo mAct;

        public RegHandler(SinVoiceRecognitionTwo act) {
            mAct = act;
        }

        @Override
        public void handleMessage(Message msg) {
            int current_type = msg.arg2;
            switch (msg.what) {
                case MSG_SET_RECG_TEXT:
                    char ch = (char) msg.arg1;
                    // mTextBuilder.append(ch);
                    if(current_type == RECOGNITION_TYPE_1){
                        mAct.mRecgs[mAct.mRecgCount++] = ch;
                    }else{
                        mAct.mRecgs_2[mAct.mRecgCount_2++] = ch;
                    }
//                mAct.mRecgs[mAct.mRecgCount++] = ch;
                    break;

                case MSG_RECG_START:
                    // mTextBuilder.delete(0, mTextBuilder.length());
                    mAct.mRecgCount = 0;
                    mAct.mRecgCount_2 = 0;
                    break;

                case MSG_RECG_END:
                    LogHelper.d(TAG, "recognition end gIsError:" + msg.arg1);
                    if(current_type == RECOGNITION_TYPE_1){
                        if (mAct.mRecgCount > 0) {
                            byte[] strs = new byte[mAct.mRecgCount];
                            for (int i = 0; i < mAct.mRecgCount; ++i) {
                                strs[i] = (byte) mAct.mRecgs[i];
                            }
                            try {
                                String strReg = new String(strs, "UTF8");
                                if (msg.arg1 >= 0) {
                                    Log.d(TAG, "reg ok!!!!!!!!!!!!");
                                    if (null != mAct.mListener) {

                                        mAct.mListener
                                                .onSinVoiceRecognitionTwoText(strReg,current_type);
                                    }
                                } else {
                                    Log.d(TAG, "reg error!!!!!!!!!!!!!");
                                    if (null != mAct.mListener) {
                                        mAct.mListener
                                                .onSinVoiceRecognitionTwoText(strReg,current_type);
                                    }
                                }
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    }else{
                        if (mAct.mRecgCount_2 > 0) {
                            byte[] strs = new byte[mAct.mRecgCount_2];
                            for (int i = 0; i < mAct.mRecgCount_2; ++i) {
                                strs[i] = (byte) mAct.mRecgs_2[i];
                            }
                            try {
                                String strReg = new String(strs, "UTF8");
                                if (msg.arg1 >= 0) {
                                    Log.d(TAG, "reg ok!!!!!!!!!!!!");
                                    if (null != mAct.mListener) {
                                        mAct.mListener
                                                .onSinVoiceRecognitionTwoText(strReg,current_type);
                                    }
                                } else {
                                    Log.d(TAG, "reg error!!!!!!!!!!!!!");
                                    if (null != mAct.mListener) {
                                        mAct.mListener
                                                .onSinVoiceRecognitionTwoText(strReg,current_type);
                                    }
                                }
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    }


                case MSG_PLAY_TEXT:
                    // mAct.mPlayTextView.setText(mAct.mPlayText);
                    break;
            }
            super.handleMessage(msg);
        }
    }

    public void start(final int tokenCount, final boolean isReadFromFile) {
        mRecognition1.start(tokenCount, isReadFromFile);
        mRecognition2.start(tokenCount, isReadFromFile);

        mRecordAsync.start(isReadFromFile);
    }

    public void stop() {
        mRecordAsync.stop();

        mRecognition1.stop();
        mRecognition2.stop();
    }

    public void uninit() {
        mRecognition1.uninit();
        mRecognition2.uninit();
    }

    @Override
    public void onSinVoiceRecognitionStart(SinVoiceRecognition recog) {
//        int current_type = 0;
        if (mRecognition1 == recog) {
//            current_type = RECOGNITION_TYPE_1;
            Log.d(TAG, "mRecognition1 start");
        } else {
//            current_type = RECOGNITION_TYPE_2;
            Log.d(TAG, "mRecognition2 start");
        }
        mHanlder.sendEmptyMessage(MSG_RECG_START);
    }

    @Override
    public void onSinVoiceRecognition(SinVoiceRecognition recog, char ch) {
        int current_type = 0;
        if (mRecognition1 == recog) {
            current_type = RECOGNITION_TYPE_1;
            Log.d(TAG, "mRecognition1 char:" + ch);
        } else {
            current_type = RECOGNITION_TYPE_2;
            Log.d(TAG, "mRecognition2 char:" + ch);
        }
        mHanlder.sendMessage(mHanlder.obtainMessage(MSG_SET_RECG_TEXT, ch, current_type));
    }

    @Override
    public void onSinVoiceRecognitionEnd(SinVoiceRecognition recog, int result) {
        int current_type = 0;
        if (mRecognition1 == recog) {
            current_type = RECOGNITION_TYPE_1;
            Log.d(TAG, "mRecognition1 end result:" + result);
        } else {
            current_type = RECOGNITION_TYPE_2;
            Log.d(TAG, "mRecognition2 end result:" + result);
        }
        mHanlder.sendMessage(mHanlder.obtainMessage(MSG_RECG_END, result, current_type));
    }
}
